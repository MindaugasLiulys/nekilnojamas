function show_hide(div_content) {
    $('#' + div_content).slideToggle("slow");
}

function add_to_wishlist(space_id) {
    $.ajax({
        type: "POST",
        url: "/wishlist/add",
        data: 'space_id=' + space_id,
        dataType: "json",
        success: function (data) {
            if (data.error != 1) {
                $(data.wishlist_btn).insertBefore('.add-to-wishlist');
                $('.add-to-wishlist').remove();
            }
            $('#pop_up_content').html(data.response);
            $('#for-messages').foundation('reveal', 'open');
        }
    });
}

$(document).ready(function () {

    $('.icheck-activate').iCheck({
        checkboxClass: 'icheckbox_minimal-orange',
        radioClass: 'iradio_minimal',
        increaseArea: '20%' // optional
    });

    $(".trigger").click(function () {
        $(".nav ul").toggleClass("close");
        $(".trigger").toggleClass("close");
    });

    $('.intro-left').animate({left: 0}, 300);
    $('.intro-right').animate({right: 0}, 300);
    $('.header-after').animate({top: 95}, 420);

    $(".trigger-lang").click(function () {
        $(".languages-mobile").toggleClass("close");
        $(".trigger-lang").toggleClass("close");
    });

    $(document).on('click', 'ul.pagination li a', function(){
        $('body,html').animate({
            scrollTop: 0
        }, 600);
    });

    $('a.roomspace').on('click touch', function(e) {
        var link = $(this).attr('href');
        window.location = link;
    });

});