function remove_list_item(item_id, row) {

	$.ajax({
		url: '/admin/general/remove_list_item',
		type: 'post',
		data: 'item_id=' + item_id,
		success: function(data) {
			$('.'+row).remove();
        }
	});
}