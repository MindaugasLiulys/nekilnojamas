<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('logged_in'))
{
	function logged_in() {
		$CI = &get_instance();

		return (bool) $CI->session->userdata('identity');
	}
}

if ( ! function_exists('get_avatar'))
{
	function get_avatar($file_name = "", $show_myself = 0) {

		if($show_myself == 1)
		{
			$CI = &get_instance();
			$name = $CI->session->userdata('avatar');
			if($name == "" && empty($name)) {
				$name = 'default.jpg'.'?'.date('ymdhis');
			}
		} else
		{
			if($file_name == NULL || $file_name == "")
			{
				$name = 'default.jpg'.'?'.date('ymdhis');
			} else {
				$name = $file_name;
			}
		}
		$file_name = 'images/avatars/'.$name.'?'.date('ymdhis');

		return $file_name;
	}
}

if ( ! function_exists('get_user_avatar'))
{
	function get_user_avatar($file_name = "", $show_myself = 0, $name) {

		if($show_myself == 1)
		{
			if($name == "" && empty($name)) {
				$name = 'default.jpg'.'?'.date('ymdhis');
			}
		} else
		{
			if($file_name == NULL || $file_name == "")
			{
				$name = 'default.jpg'.'?'.date('ymdhis');
			} else {
				$name = $file_name;
			}
		}
		$file_name = 'images/avatars/'.$name.'?'.date('ymdhis');

		return $file_name;
	}
}

if ( ! function_exists('get_image'))
{
	function get_image($file, $folder = '', $size = 0) {

		if($folder != "" && $file != "empty.jpg")
		{
			$temp      = pathinfo($file);
			$file_name = 'images/'.$folder.'/'.$temp['filename'].'_'.$size.'.'.$temp['extension'].'?'.date('ymdhis');
		} else 
		{
			$file_name = 'images/'.$file.'?'.date('ymdhis');;
		}
		return $file_name;
	}
}

if ( ! function_exists('room_type_helper'))
{
    function room_type_helper($room_type_id) {

        switch ($room_type_id) {
            case 1:
                $name = lang('apartment');
                break;
            case 2:
                $name = lang('house');
                break;
            case 3:
                $name = lang('land');
                break;
            default:
                $name = lang('commercial');
                break;
        }

        return $name;
    }
}

if ( ! function_exists('room_plural'))
{
    function room_plural($room_number) {

        if($room_number < 2){
            $room = lang('room_sin');
        }else{
            $room = lang('room_plur');
        }

        return $room;
    }
}

if ( ! function_exists('number_plur'))
{
    function number_plur($number)
    {
        $ending = 'ai';

        if ($number <= 0) {
            $ending = 'ų';
        }elseif ($number > 0 && $number < 10) {
            $ending = 'ai';
        }elseif ($number > 9 && $number < 20) {
            $ending = 'ų';
        }elseif($number % 10 == 0){
            $ending = 'ų';
        }

        return $ending;
    }
}

if ( ! function_exists('get_facilities'))
{
	function get_facilities($facilities_str)
    {
        $CI =& get_instance();
        $CI->load->database();

        $array = explode(";", $facilities_str);

		$result_line = "";
		if(!empty($array))
		{
			foreach ($array as $k => $id) {
                if($id != 0){
                    $sql ="SELECT * FROM tr56_facilities WHERE facility_id=$id";

                    $query = $CI->db->query($sql);
                    $name = $query->row('name');
                    $fac_id = $query->row('facility_id');

                    if($fac_id == $id){
                        $result_line .= $name;
                    }
                }
				$result_line .= ", ";
			}
			$result_line = substr($result_line, 0, -2);
		}

		return $result_line;
	}
}

if ( ! function_exists('real_price'))
{
	function real_price($base_price) {

		$result = $base_price * (100 + OVERCHARGE) / 100;

		return number_format($result, 2, '.', '');
	}
}

if ( ! function_exists('format'))
{
	function format($number) {

        $number = number_format((float)$number, 0, '.', '');
		$CI = &get_instance();
		$number .= ' ' . $CI->session->userdata('currency');

		return $number;
	}
}

if ( ! function_exists('category'))
{
	function category($number) {

		switch($number){
            case 0: $category = lang('category-sell');
                break;
            case 1: $category = lang('category-rent');
        }
		return $category;
	}
}

if ( ! function_exists('place_size'))
{
	function place_size($number) {

		$number .= 'm';

		return $number;
	}
}

if ( ! function_exists('slugify'))
{
    function slugify($text) {

        $result = strtolower(trim(str_replace('&', '', $text)));

        if (empty($result)) {
            return 'n-a';
        }
        return $result;
    }
}

if ( ! function_exists('get_address')) {
	function get_address($latitude,$longitude){
		if(!empty($latitude) && !empty($longitude)){
			//Send request and receive json data by address
			$geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false');
			$output = json_decode($geocodeFromLatLong);
			$status = $output->status;
			//Get address from json data
			$address = ($status=="OK")?$output->results[1]->formatted_address:'';
			//Return address of the given latitude and longitude
			if(!empty($address)){
				return $address;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}