<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Facebook App details
| -------------------------------------------------------------------
|
| To get an facebook app details you have to be a registered developer
| at http://developer.facebook.com and create an app for your project.
|
|  facebook_app_id               string  Your facebook app ID.
|  facebook_app_secret           string  Your facebook app secret.
|  facebook_login_type           string  Set login type. (web, js, canvas)
|  facebook_login_redirect_url   string  URL tor redirect back to after login. Do not include domain.
|  facebook_logout_redirect_url  string  URL tor redirect back to after login. Do not include domain.
|  facebook_permissions          array   The permissions you need.
*/

$config['facebook_app_id']   = '438565016515653';
$config['appId']   = '883406275119985';
$config['facebook_app_secret']  = '59043823ac1281df3f5f465c3cbf1926';
$config['secret']  = 'b4236a2e070d78d9d1c64ecd693b84fc';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'auth/authenticate_user';
$config['facebook_logout_redirect_url'] = 'auth/logout';
$config['facebook_permissions']         = array('email');
$config['facebook_graph_version']       = 'v2.9';
$config['facebook_auth_on_load']        = TRUE;