<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('SLIDER_PRICE_FROM', 1);
define('SLIDER_PRICE_FROM', 1);
define('SLIDER_PRICE_TO', 2000);
define('SLIDER_SIZE_FROM', 1);
define('SLIDER_SIZE_TO', 300);

define('DEFAULT_LAT', 54.958694);
define('DEFAULT_LNG', 24.873047);

define('GOOGLE_CLIENT_ID', '909184290802-464aa3m8tdica4fpjsfpaj5igq9344t8.apps.googleusercontent.com');
//define('GOOGLE_CLIENT_ID', '799962206869-qqsee60c9aj9b3m8b08gsuti1i38l799.apps.googleusercontent.com');
//define('GOOGLE_CLIENT_ID', '883547263276-f0q9pm8boqf55gdht1ldqto0qruvrr5q.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET', 'VLM-yZBIzPDHdx06Xs-pkmsW');
//define('GOOGLE_CLIENT_SECRET', 'v3dezZ0OPOfTEvtjP45O1BSf');
//define('GOOGLE_CLIENT_SECRET', 'werpXC458ui-RPEqOG27jo7H');
define('GOOGLE_API_KEY', 'AIzaSyDt-htKS0iLbP8mf3Aklf_BPF8YpDdemPU');
//define('GOOGLE_API_KEY', 'AIzaSyCEt-BkxAzzL26EC1jXmPl66fjqGLPUvMg');

define('FOLDER_SALT', 'C0iRl~3eKywgMfhQ69RtbOcIvcDQ7PAHc+OJfz2C_EFgkcoQvT5t2=clU4HY');
define('AVATAR_SALT', '=5Dk063n^DHScQDMwp1cj~EsMjE0~6zYy+BEEEEb6Oa=lUfP2Ca+goO-35vR');
define('ORDER_SALT', 'TSx8yyj7O^Kwier1vg_3izq0WOTO8CCyIA+KhCR0TKzdGWAzwhgz2v2Lqhq0');
define('LOGIN_SALT', '53fba23d0^4a484d4_fec6874de92~53fba23d014a4=4d49fec6874de927');

define('MAIN_IMG_THUMB', '101x101');
define('MAIN_IMG_MEDIUM', '491x376');
define('IMG_GALLERY', '580x326');

define('OVERCHARGE', 25);
define('EPAY_MERCHANT_NUMBER', 8024249);
define('EPAY_MD5_SECRET_KEY', '61b224c9eaedfcd9de3ab0a1ce8fd8d6');

define('MAMBORA_MERCHANT_ID', '300210966');
define('MAMBORA_API_PASSCODE', '8161ef5D829246ca9068Eb0fAE2dDf62');

define('DATE_FORMAT', 'd-m-Y');
define('DATEPICKER_FORMAT', 'dd-mm-yyyy');

define('COMPANY_NAME', 'Nekilnojamas');