<?php
class Loader
{
    function initialize() {
        $ci =& get_instance();
//        $lang = $ci->session->userdata('lang');
        $lang = 'lithuanian';
//        $currency = $ci->session->userdata('currency');
        $currency = 'eur';
        if ($lang) {
            $ci->lang->load('main', $lang);
            $ci->lang->load('terms', $lang);
            $ci->lang->load('information', $lang);
            $ci->lang->load('about_us', $lang);
             $ci->session->set_userdata(array('lang' => $lang, 'currency' => strtoupper($currency)));
//            $ci->session->set_userdata(array('lang' => $lang, 'currency' => 'EUR'));
        } else {
            if($_SERVER['REMOTE_ADDR'] == '84.240.26.57')
            {
                $ci->lang->load('main','lithuanian');
                $ci->lang->load('terms','lithuanian');
                $ci->session->set_userdata(array('lang' => 'lithuanian', 'currency' => 'EUR'));
            } else {
                $ci->lang->load('main','danish');
                $ci->lang->load('terms','danish');
                $ci->session->set_userdata(array('lang' => 'danish', 'currency' => 'DKK'));
            }
        }
    }
}