<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wishlist_model extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function save_space($space_id)
    {
        $clean = array(
            'space_id' => $space_id,
            'user_id'  => $this->session->userdata('user_id')
        );

        $this->db->insert('wishlists', $clean);
    }

    public function space_saved_in_wishlist($space_id)
    {
        $this->db->where('space_id', $space_id);
        $this->db->where('user_id', $this->session->userdata('user_id'));

        if($this->db->count_all_results('wishlists') == 1)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public function get_user_list($params = array())
    {
        $this->load->helper('text');

        $this->db->select('u.avatar, u.id as user_id, s.*');
        $this->db->where('wishlists.user_id', $this->session->userdata('user_id'));
        $this->db->join('spaces as s', 's.space_id = wishlists.space_id', 'inner');
        $this->db->join('users as u', 'u.id = s.user_id', 'inner');
        
        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
        }
     
        $query = $this->db->get('wishlists');

        $this->load->model('spacereview_model');

        $results = array();
        foreach($query->result_array() AS $k => $row)
        {
            
            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image']       = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            $row['user_avatar'] = get_avatar($row['avatar']);
            $row['space_type'] = $row['room_type'] == 1 ? 'Buto plotas': $row['room_type'] == 2 ? 'Namo plotas' : 'Komercinių patalpų plotas';
            $results[] = $row;
        }

        return $results;
    }

    public function remove($space_id)
    {
        $this->db->where('space_id', $space_id);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->delete('wishlists');        
    }
}