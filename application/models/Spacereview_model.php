<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Spacereview_model extends CI_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function insert_space_review($data)
    {
        $clean = array(
            'user_id' => $this->session->userdata('user_id'),
            'space_id' => $data['space_id'],
            'rate' => $data['rate'],
            'comment' => $data['comment'],
            'date_added' => date('Y-m-d h:i:s')
        );
        //check if review exists
        $this->db->where('space_id', $data['space_id']);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $query = $this->db->get('space_rate', 1);
        
        if ($query->num_rows() > 0) {
            $this->db->where('space_id', $data['space_id']);
            $this->db->where('user_id', $this->session->userdata('user_id'));
            $this->db->update('space_rate', $clean);
            return lang('success_review_updated');
        } else {
            $this->db->insert('space_rate', $clean);
            return lang('success_review_saved');
        }

    }

    public function get_ratings($space_id)
    {
        $this->db->where('space_id', $space_id);
        $query = $this->db->get('space_rate');
        $data['total_reviews'] = $query->num_rows();

        $this->db->where('space_id', $space_id);
        $this->db->select_sum('rate');
        $sum = $this->db->get('space_rate')->row_array();
        $data['reviews_rate'] =
            is_nan($sum['rate'] / $data['total_reviews'])?(int)0:$sum['rate'] / $data['total_reviews'];

        return $data;
    }
    
    public function get_reviews($space_id)
    {
        $this->db->where('space_id', $space_id);
        $query = $this->db->get('space_rate');
        
        $this->load->model('users_model');
        
        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $row['user_data'] = $this->users_model->get_username($row['user_id']);
            $results[] = $row;
        }
        return $results;
    }

    public function get_user_review($space_id)
    {
        $this->db->where('space_id', $space_id);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $query = $this->db->get('space_rate', 1);        

        if ($query->num_rows() > 0) {
            $data['left_review'] = true;
            foreach ($query->result_array() AS $k => $row) {
                $data['comment'] = $row['comment'];
                $data['rate'] = $row['rate'];
            }

        } else {
            $data['left_review'] = false;
        }
        return $data;
    }

}