<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_model extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

//    public function get_future_transactions()
//    {
//        $user_to = $this->session->userdata('user_id');
//
//        $this->db->where('user_to', $user_to);
//        $this->db->order_by('date_added', 'desc');
//        $query = $this->db->get('transactions');
//
//        $results = array();
//        foreach($query->result_array() AS $k => $row)
//        {
//            $row['amount'] = $this->currency->convert($this->price_without_commisions($row['amount']));
//            $results[] = $row;
//        }
//
//        return $results;
//
//    }

//    public function price_without_commisions($price)
//    {
//        $price = ($price * 100) / (100 + OVERCHARGE);
//        return $price;
//    }
//
//    public function add($order_info, $transaction)
//    {
//    	$currency = 'USD';
//
//    	$timestamp = $transaction['date'] . ' ' . $transaction['time'];
//    	$cls_date = new DateTime($timestamp);
//		$timestamp = $cls_date->format('Y-m-d H:i:s');
//
//    	$array = array(
//			'conversation_id' => $order_info['conversation_id'],
//			'amount'          => $transaction['amount'],
//			'currency'        => $currency,
//			'user_from'       => $order_info['from'],
//			'user_to'         => $order_info['to'],
//			'card_number'     => '',
//			'txnid'			  => '',
//			'date_added'      => $timestamp
//    	);
//
//    	$this->db->insert('transactions', $array);
//    }

    public function has_user_rented($space_id)
    {
        $user_id = $this->session->userdata('user_id');

        $this->db->select('*');
        $this->db->from('conversations as cons');
        $this->db->where('from', $user_id);
        $this->db->where('space_id', $space_id);
        $this->db->join('transactions as trans', 'cons.conversation_id = trans.conversation_id');
        $this->db->limit(1);
        $query = $this->db->get();

        $this->load->model('spacereview_model');

        if (!empty($query->result_array())) {
            $data = $this->spacereview_model->get_user_review($space_id);
            $data['rented'] = true;
        }else {
            $data['rented'] = false;
        }

        return $data;
    }
}