<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('emails', 'lithuanian');
    }

    public function send_owner_contacts($order)
    {
        $this->load->model('users_model');
        $tenant = $this->users_model->get_user($order['from']);

        $owner = $this->users_model->get_user($order['to']);
        $owner['conversation_id'] = $order['conversation_id'];
        $message = $this->load->view('email/owner_contacts', compact('owner'), true);

        $html = $this->load->view('email/layout', [
            'html' => $message,
        ], true);

        $this->db->insert('messages', [
            'from'            => (int)$order['from'],
            'to'              => (int)$order['to'],
            'conversation_id' => $order['conversation_id'],
            'message'         => $message,
            'date_added'      => date('Y-m-d H:i:s')
        ]);

        $title = lang('emails_contact_info');
        $this->send($html, $title, $tenant['email']);
    }

    public function send_status_declined($info)
    {
        $data['html'] = $this->load->view('email/status_declined', $info, true);
        $html = $this->load->view('email/layout', $data, true);

        $title = lang('emails_status_declined');
        $this->send($html, $title, $info['tenant_email']);
    }

    public function send_status_approved($info, $hash, $login_hash)
    {
        $info['safe_link'] = site_url('order/payment/'.$info['conversation_id'].'/'.$hash.'/'.$login_hash);
        $data['html'] = $this->load->view('email/status_approved', $info, true);
        $html = $this->load->view('email/layout', $data, true);

        $title = lang('emails_status_approved');
        $this->send($html, $title, $info['tenant_email']);
    }

    public function send_for_owner_about_rent($space_info, $start_date, $end_date, $comments, $name, $surname, $email, $phone)
    {
    	$this->load->model('users_model');
    	$this->load->model('messages_model');


    	$owner_info = $this->users_model->get_user($space_info['user_id']);
        $tmp['conversation_id'] = $this->messages_model->add($this->session->userdata('user_id'), $owner_info['id'], $space_info['space_id'], $comments, $start_date, $end_date, $name, $surname, $email, $phone);

    	$tmp['info'] = $space_info;
    	$tmp['renter_start_date'] = $start_date;
    	$tmp['renter_end_date'] = $end_date;
    	$tmp['comments'] = $comments;
    	$tmp['name'] = $name;
    	$tmp['surname'] = $surname;
    	$tmp['phone'] = $phone;
    	$tmp['email'] = $email;

    	$tmp['thumb'] = $space_info['image'];

    	$data['html'] = $this->load->view('email/send_owner', $tmp, true);
    	$html = $this->load->view('email/layout', $data, true);

    	$title = lang('emails_waiting_confirmation');
//        $email = 'liulysmindaugas@gmail.com';
//    	$this->send($html, $title, $owner_info['email']);

    	$this->send($html, $title, $email);
    }

    public function send_for_owner_about_review($space_id)
    {
        $this->load->model('space_model');
        $this->load->model('users_model');

        $space = $this->space_model->get_space_information($space_id);
        $user = $this->users_model->get_user($space['user_id']);

        $message = $this->load->view('email/new_review_owner', $space_id, true);
        $html = $this->load->view('email/layout', [
            'html' => $message,
        ], true);

        $title = lang('emails_subject_new_review_owner');
        $this->send($html, $title, $user['email']);
    }

    public function send_payment_made($order)
    {
        $this->send_owner_contacts($order);
        $this->send_payment_made_for_tenant($order);
        $this->send_payment_made_for_owner($order);
        $this->send_payment_made_for_admin($order);
    }

    public function send_payment_made_for_tenant($order)
    {
        $this->load->model('users_model');
        $tenant = $this->users_model->get_user($order['from']);
        $conversation_id = $order['conversation_id'];
        $message = $this->load->view('email/payment_made_tenant', $conversation_id, true);
        $html = $this->load->view('email/layout', [
            'html' => $message,
        ], true);

        $this->db->insert('messages', [
            'from'            => (int)$order['from'],
            'to'              => (int)$order['to'],
            'conversation_id' => $conversation_id,
            'message'         => $message,
            'date_added'      => date('Y-m-d H:i:s')
        ]);

        $title = lang('emails_subject_payment_made_tenant');
        $this->send($html, $title, $tenant['email']);
    }

    public function send_payment_made_for_owner($order)
    {
        $this->load->model('users_model');
        $owner = $this->users_model->get_user($order['to']);
        $conversation_id = $order['conversation_id'];

        $message = $this->load->view('email/payment_made_owner', $conversation_id, true);
        $html = $this->load->view('email/layout', [
            'html' => $message,
        ], true);

        $this->db->insert('messages', [
            'from'            => (int)$order['to'],
            'to'              => (int)$order['from'],
            'conversation_id' => $conversation_id,
            'message'         => $message,
            'date_added'      => date('Y-m-d H:i:s')
        ]);

        $title = lang('emails_subject_payment_made_owner');
        $this->send($html, $title, $owner['email']);
    }

    public function send_payment_made_for_admin($order)
    {
        $this->load->model('users_model');
        $conversation_id = $order['conversation_id'];

        $message = $this->load->view('email/payment_made_admin', $conversation_id, true);
        $html = $this->load->view('email/layout', [
            'html' => $message,
        ], true);

        $title = lang('emails_subject_payment_made_admin');
        $this->send($html, $title);
    }

    public function send_message_received($conversation, $receiver_id)
    {
        $this->load->model('users_model');
        $receiver = $this->users_model->get_user($receiver_id);

        $message = $this->load->view('email/message_received', null, true);
        $html = $this->load->view('email/layout', [
            'html' => $message,
        ], true);

        $title = lang('emails_subject_message_received');
        $this->send($html, $title, $receiver['email']);
    }

    private function send($message, $title, $email = 'no-reply@procreo.eu')
    {
        $config['driver'] = 'smtp';
        $config['mailtype'] = 'text';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes

        $this->load->library('email');
        $this->mail->SMTPDebug = 4;
        $result = $this->email
            ->from('no-reply@procreo.eu', 'Administratorius')
		    ->to($email)
            ->subject($title)
		    ->message($message)
            ->send();

        var_dump($result);
        echo '<br />';
        echo $this->email->print_debugger();
    }





}