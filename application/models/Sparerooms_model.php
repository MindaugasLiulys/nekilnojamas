<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sparerooms_model extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_spareroom($space_id)
    {
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('space_id', $space_id);
        $query = $this->db->get('spaces');

        $result = $query->row_array();

        if($result['category'] != 0){
            $result['real_price'] = real_price($result['price']);
        }else{
            $result['real_price'] = $result['price'];
        }

        $result['facilities'] = explode(";", $result['facilities']);
        $result['image']       = get_image($result['image'], $result['folder_name'], IMG_GALLERY);

        $result['images'] = array();
        if(!empty($result['folder_name']))
        {
            $directory = "images/".$result['folder_name'].'/gallery/';
            $images = glob($directory . "*thumb.*");
            $result['images'] = $images;
        }

        return $result;
    }

    public function get_space_plain_info($space_id)
    {
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('space_id', $space_id);
        $query = $this->db->get('spaces');

        return $query->row_array();
    }

    public function update_space($data)
    {
        $facilities = "";
        if(isset($data['facilities']) && is_array($data['facilities']))
        {
            $facilities = implode(';', $data['facilities']);
        }

        $location = str_replace(', ', '-', $data['location-slug']);
        $location = str_replace(' ', '-', $location);

        $roomSlug = room_plural($data['room_number']) . '-' . room_type_helper($data['room_type']);

        if($data['room_number'] == 0){
            $slug = room_type_helper($data['room_type']) . '-' . $location;
        }else{
            $slug = $data['room_number'] . '-' . $roomSlug . '-' . $location;
        }

        $final_slug = slugify($slug);

        $clean = array(
            'price'        => $data['price'],
            'size'         => $data['size'],
            'room_number'  => $data['room_number'],
            'category'     => $data['category'],
            'slug'         => $final_slug,
            'description'  => $data['description'],
            'start_date'   => $data['start_date'],
            'available'    => isset($data['available']) ? 1 : 0,
            'facilities'   => $facilities,
            'room_type'    => $data['room_type'],
        );

        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('space_id', $data['space_id']);
        $this->db->update('spaces', $clean);
    }

    public function get_my_list()
    {
    	$this->load->helper('text');

        $this->db->where('deleted', 0);
    	$this->db->where('user_id', $this->session->userdata('user_id'));
        $query = $this->db->get('spaces');

        $results = array();
        foreach($query->result_array() AS $k => $row)
        {
			$row['description'] = character_limiter($row['description'], 50, '...');
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            $results[] = $row;
        }

        return $results;
    }

    public function delete($space_id)
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('space_id', $space_id);
        $this->db->update('spaces', array('deleted' => 1));

        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('space_id', $space_id);
        $this->db->delete('wishlists');
    }

    public function activate_space($space_id)
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('space_id', $space_id);
        $this->db->update('spaces', array('active' => 1));
    }

    public function deactivate_space($space_id)
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('space_id', $space_id);
        $this->db->update('spaces', array('active' => 0));
    }

    public function exist_not_active_space($space_id)
    {
        $this->db->where('active', 0);
        $this->db->where('deleted', 0);
        $this->db->where('space_id', $space_id);
        // $query = $this->db->get('spaces');

        if($this->db->count_all_results('spaces') == 1)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public function exist_active_space($space_id)
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->where('space_id', $space_id);
        // $query = $this->db->get('spaces');

        if($this->db->count_all_results('spaces') == 1)
        {
            return true;
        } else
        {
            return false;
        }
    }
}