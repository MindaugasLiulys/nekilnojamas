<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function download_profile_image($user_id, $photo_url)
	{
		$image = file_get_contents($photo_url);
		$file_info = pathinfo($photo_url);

		if($image !== FALSE && !empty($file_info))
		{
			if(!isset($file_info['extension']) && empty($file_info['extension'])) { $file_info['extension'] = 'jpg'; }

			$avatar_name = SHA1(AVATAR_SALT.$user_id).'.'.$file_info['extension'];
			
			$file_destination = FCPATH . 'images/avatars/'.$avatar_name;
			$return_value = file_put_contents($file_destination, $image);

			if($return_value !== FALSE)
			{
				$this->db->where('id', $user_id);
				$this->db->update('users', array('avatar' => $avatar_name));
			}
		}
	}

	public function process_uploaded_images($space_id)
	{
		$user_id = $this->session->userdata('user_id');

		if(file_exists(realpath(FCPATH . 'images/'.$user_id)))
		{
			$main_file_name = $this->session->userdata('main_file_name');
			$temp_info      = pathinfo($main_file_name);
			
			$file_name      = $temp_info['filename'];
			$file_extension = $temp_info['extension'];

			$folder_name = SHA1(FOLDER_SALT.date('U').$user_id);
			rename(FCPATH . 'images/'.$user_id, FCPATH . 'images/'.$folder_name);
		
			$image_to_db = 'empty.jpg';

			if(file_exists(realpath(FCPATH . 'images/'.$folder_name.'/'.$main_file_name))) 
			{
				$image_to_db = $main_file_name;
			}

			$this->db->where('space_id', $space_id);
			$this->db->update('spaces', array('folder_name' => $folder_name, 'image' => $image_to_db));
		}
	}
}