<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function update_fields($array)
    {
        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('users', $array);
    }

    public function get_notification_settings()
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $query = $this->db->get('users_notification_settings', 1);

        $result = $query->row_array();
        if (!empty($result)) {
            return $result;
        } else {
            $this->db->insert('users_notification_settings', array('user_id' => $this->session->userdata('user_id')));
            $this->db->where('user_id', $this->session->userdata('user_id'));
            $query = $this->db->get('users_notification_settings', 1);

            return $query->row_array();
        }
    }

    public function update_notification_settings($info)
    {
        $clean = array(
            'msg_rezervation_status' => isset($info['msg_rezervation_status']) ? 1 : 0,
            'msg_rezervation_request' => isset($info['msg_rezervation_request']) ? 1 : 0,
            'msg_account_changes' => isset($info['msg_account_changes']) ? 1 : 0,
            'msg_promotions' => isset($info['msg_promotions']) ? 1 : 0,
            'msg_review_reminder' => isset($info['msg_review_reminder']) ? 1 : 0
        );

        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->update('users_notification_settings', $clean);
    }

    public function get_user($user_id = 0)
    {
        if ($user_id == 0) {
            $user_id = $this->session->userdata('user_id');
        }

        $this->db->where('id', $user_id);
        $query = $this->db->get('users', 1);

        $result = $query->row_array();

        $result['year'] = substr($result['birthdate'], 0, 4);
        $result['month'] = substr($result['birthdate'], 5, 2);
        $result['day'] = substr($result['birthdate'], 8, 2);

        unset($result['password']);

        return $result;
    }

    public function save_user($info)
    {
        $login_type = $this->session->userdata('login_type');

        $clean = array(
            'first_name' => $info['first_name'],
            'last_name' => $info['last_name'],
            'birthdate' => ($info['year'] . '-' . $info['month'] . '-' . $info['day']),
            'phone' => $info['phone'],
            'location' => $info['location'],
            'description' => $info['description']
        );

        if ($login_type == 0) {
            $clean['email'] = $info['email'];
        }

        $array = array(
            'username' => $info['first_name'],
            'phone' => $info['phone']
        );

        $this->session->set_userdata($array);

        if (!empty($clean['first_name']) && !empty($clean['last_name']) && !empty($clean['birthdate']) && !empty($clean['phone']) && !empty($clean['location']) && (($login_type == 0 && !empty($clean['email'])) || ($login_type != 0 && empty($clean['email'])))) {
            $clean['allow_list_space'] = 1;
            $this->session->set_userdata(array('allow_list_space' => 1));
        }

        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('users', $clean);;
    }

    public function get_username($user_id)
    {
        $this->db->select('first_name, avatar');
        $this->db->where('id', $user_id);
        $query = $this->db->get('users');
        return $query->row();
    }
}