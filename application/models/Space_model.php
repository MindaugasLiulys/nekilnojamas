<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Space_model extends CI_Model
{

    public $array_check_wishlist = array();

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->model('spacereview_model');
    }

    public function get_final_price($start_date, $end_date, $price_per_month)
    {
        $day_length = 60 * 60 * 24;
        $days = (strtotime($end_date) - strtotime($start_date)) / $day_length;

        $result = ($days * $price_per_month) / 30;

        return number_format($result, 0, '.', '');
    }

    public function exist_renting($space_info, $start_date, $end_date, $final_price)
    {
        $final_price = $this->currency->convert($final_price, $this->session->userdata('currency'), 'DKK');

        $this->db->where('start_date', $start_date);
        $this->db->where('end_date', $end_date);
        $this->db->where('space_id', $space_info['space_id']);
        $this->db->where('from', $this->session->userdata('user_id'));
        $this->db->where('to', $space_info['user_id']);
        $this->db->where('price', $final_price);
        $this->db->where('paid', 0);

        if ($this->db->count_all_results('conversations') == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function validate_date($date)
    {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_space_information($space_id)
    {
        $this->db->select('u.first_name, u.created_on, u.avatar, u.id as user_id, u.description user_description, spaces.*, w.space_id as in_wishlist');
        $this->db->where('spaces.active', 1);
        $this->db->where('spaces.deleted', 0);
        $this->db->where('spaces.space_id', $space_id);
        $this->db->join('users as u', 'u.id = spaces.user_id', 'inner');
        $this->db->join('wishlists as w', 'w.space_id = spaces.space_id AND w.user_id = spaces.user_id', 'left');
        $query = $this->db->get('spaces');

        $result = $query->row_array();
        $result['image'] = get_image($result['image'], $result['folder_name'], IMG_GALLERY);

        if($result['category'] != 0){
            $result['price'] = (real_price($result['price']));
        }

        $result['user_avatar'] = get_avatar($result['avatar']);

        return $result;
    }

    public function split_price_range($price_range)
    {
        $tmp['from'] = SLIDER_PRICE_FROM;
        $tmp['to'] = SLIDER_PRICE_TO;

        if (!empty($price_range)) {
            $splitted = explode('-', $price_range);

            if (is_numeric($splitted[0]) && is_numeric($splitted[1])) {
                $tmp['from'] = $splitted[0];
                $tmp['to'] = $splitted[1];
            }
        }

        return $tmp;
    }

    public function insert_space($data)
    {
        $facilities = "";
        if (isset($data['facilities']) && is_array($data['facilities'])) {
            $facilities = implode(';', $data['facilities']);
        }

        $unform_location = get_address($data['lat'], $data['lng']);
        $location = str_replace(', ', '-', $unform_location);
        $location = str_replace(' ', '-', $location);

        $roomSlug = room_plural($data['room_number']) . '-' . room_type_helper($data['room_type']);

        if($data['room_number'] == 0){
            $slug = room_type_helper($data['room_type']) . '-' . $location;
        }else{
            $slug = $data['room_number'] . '-' . $roomSlug . '-' . $location;
        }

        $final_slug = slugify($slug);

        $clean = array(
            'user_id' => $this->session->userdata('user_id'),
            'price' => $data['price'],
            'size' => $data['size'],
            'room_number' => $data['room_number'],
            'category' => $data['category'],
            'slug' => $final_slug,
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'street_address' => $data['formatted_address'],
            'description' => $data['description'],
            'start_date' => $data['start_date'],
            'facilities' => $facilities,
            'room_type' => $data['room_type'],
            'date_added' => date('Y-m-d h:i:s'),
            'active' => isset($data['active']) ? 1 : 0
        );

        $this->db->insert('spaces', $clean);
        return $this->db->insert_id();
    }

    public function get_search_results($data, $params = array())
    {
        $this->load->helper('text');
        $latitude = $data['lat'];
        $longtitude = $data['lng'];
        $correct_dates = FALSE;

        $price_from = $data['price_from'];
        $price_to = $data['price_to'];

        $factor = (100 + OVERCHARGE) / 100;

        list($sql, $correct_dates) = $this->get_sql_for_spaces_search($data, $latitude, $longtitude, $factor, $price_from, $price_to);

        $sqlSearch = $sql . " HAVING distance <= 20";
        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $sqlSearch .= " LIMIT " . (int)$params['limit'] . " OFFSET " . (int)$params['start'];
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $sqlSearch .= " LIMIT " . (int)$params['limit'];
        }

        $query = $this->db->query($sqlSearch);

        $distance = 40;
        while (($query->num_rows() == 0)) {
            $sqlSearch = $sql . " HAVING distance <= " . $distance;
            if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $sqlSearch .= " LIMIT " . (int)$params['limit'] . " OFFSET " . (int)$params['start'];
            } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $sqlSearch .= " LIMIT " . (int)$params['limit'];
            }
            $query = $this->db->query($sqlSearch);
            $distance += 20;
            if ($distance > 210) {
                break;
            }
        }

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $this->array_check_wishlist[] = $row['space_id'];

            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);

            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }

            $row['space_type'] = $row['room_type'] == 1 ? 'Buto plotas': $row['room_type'] == 2 ? 'Namo plotas' : 'Komercinių patalpų plotas';
            if ($correct_dates) {
                $row['link'] = site_url('perziureti/' . $row['slug'] . '/' . $row['space_id']) . '?sd=' . date(DATE_FORMAT, strtotime($data['start_date'])) . '&ed=' . date(DATE_FORMAT, strtotime($data['end_date']));
            } else {
                $row['link'] = site_url('perziureti/' . $row['slug'] . '/' . $row['space_id']);
            }

            $row['user_avatar'] = get_avatar($row['avatar']);

            $row['in_wishlist'] = 0;
            if (isset($row['wish']) && !empty($row['wish'])) {
                $row['in_wishlist'] = 1;
            }

            $results[] = $row;
        }

        return $results;
    }

    public function get_all_results($data, $params = array())
    {
        $this->load->helper('text');
        $latitude = $data['lat'];
        $longtitude = $data['lng'];
        $correct_dates = FALSE;

        $price_from = $data['price_from'];
        $price_to = $data['price_to'];

        $factor = (100 + OVERCHARGE) / 100;
        list($sql, $correct_dates) = $this->get_sql_for_spaces_search($data, $latitude, $longtitude, $factor, $price_from, $price_to);

        $sql .= " HAVING distance <= 210";

        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $sql .= " LIMIT " . $params['limit'] . " OFFSET " . $params['start'];
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $sql .= " LIMIT " . $params['limit'];
        }

        $query = $this->db->query($sql);

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $this->array_check_wishlist[] = $row['space_id'];

            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);

            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }

            $row['space_type'] = $row['room_type'] == 1 ? 'Buto plotas': $row['room_type'] == 2 ? 'Namo plotas' : 'Komercinių patalpų plotas';
            $row['link'] = site_url('space/view/' . $row['space_id']);

            $row['user_avatar'] = get_avatar($row['avatar']);

            $row['in_wishlist'] = 0;
            if (isset($row['wish']) && !empty($row['wish'])) {
                $row['in_wishlist'] = 1;
            }

            $results[] = $row;
        }

        return $results;
    }

    public function get_all_results_sql($data)
    {
        $this->load->helper('text');
        $latitude = $data['lat'];
        $longtitude = $data['lng'];
        $correct_dates = FALSE;

        $price_from = $data['price_from'];
        $price_to = $data['price_to'];

        $factor = (100 + OVERCHARGE) / 100;
        list($sql, $correct_dates) = $this->get_sql_for_spaces_search($data, $latitude, $longtitude, $factor, $price_from, $price_to);

        $sql .= " HAVING distance <= 300";

        $query = $this->db->query($sql);
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    public function get_spaces_in_wishlist()
    {
        $result = array();
        if (!empty($this->array_check_wishlist)) {
            $this->db->where('user_id', $this->session->userdata('user_id'));
            $this->db->where_in('space_id', $this->array_check_wishlist);
            $query = $this->db->get('wishlists');

            foreach ($query->result_array() AS $k => $row) {
                $result[] = $row['space_id'];
            }
        }

        return $result;
    }

    public function results_for_map($results, $space_id = 0)
    {
        $rows = array();
        $number = 1;

        foreach ($results as $k => $row) {
            $price = format($row['price']) . (($row['category'] != 0) ? '/' . (lang('month')):'');
            $tmp = array();
            $tmp[] = $row['lat'];
            $tmp[] = $row['lng'];
            $tmp[] = $number;
            $tmp[] = '
                <div class="small-5 columns" style="padding: 0"><a target="_blank" href="' . site_url('perziureti/' . $row['slug'] . '/'  . $row['space_id']) . '">
                    <img class="map-thumb" src="' . site_url($row['image']) . '">
                </a></div>
                <div class="small-7 columns" style="padding-right: 0">
               
                <p class="map-price">' . $price . '</p>
                <p class="place-address">
                    <img src="' . base_url() . 'assets/img/icon-search.png">
                       ' . $row['street_address'] . '</p>
                <a target="_blank" href="' . site_url('perziureti/' . $row['slug'] . '/'  . $row['space_id']) . '">Skaityti daugiau</a></div>';
            $tmp[] = $row["space_id"] == $space_id ? true : false;            
            if($row["space_id"] == $space_id) {
                $tmp[] = site_url("images") . '/gmap-main.png';
            } elseif($row['room_type'] == 1) {
                $tmp[] = $row["category"] != 0 ? site_url("assets") . '/img/index/shopping_rent.png': site_url("assets") . '/img/index/shopping.png' ;
            } elseif($row['room_type'] == 2) {
                $tmp[] = $row["category"] != 0 ? site_url("assets") . '/img/index/home_rent.png' : site_url("assets") . '/img/index/home.png' ;
            }elseif ($row['room_type'] == 3) {
                $tmp[] = $row["category"] != 0 ? site_url("assets") . '/img/index/landscape_rent.png' : site_url("assets") . '/img/index/landscape.png';
            }else {
                $tmp[] = $row["category"] != 0 ? site_url("assets") . '/img/index/supermarket_rent.png' : site_url("assets") . '/img/index/supermarket.png';
            }

            $rows[] = $tmp;
            $number++;
        }

        return json_encode($rows);
    }

    public function get_featured_spaces()
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->order_by('space_id', 'desc');
        $query = $this->db->get('spaces', 8);

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }
            $row['space_type'] = $row['room_type'] == 1 ? 'Buto plotas': $row['room_type'] == 2 ? 'Namo plotas' : 'Komercinių patalpų plotas';
            $results[] = $row;
        }

        return $results;
    }

    public function get_featured_spaces_sell()
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->where('category', 0);
        $this->db->order_by('space_id', 'desc');
        $query = $this->db->get('spaces', 8);

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }
            $results[] = $row;
        }

        return $results;
    }

    public function get_featured_spaces_rent()
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->where('category', 1);
        $this->db->order_by('space_id', 'desc');
        $query = $this->db->get('spaces', 8);

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }
            $results[] = $row;
        }

        return $results;
    }

    public function get_all_featured_spaces_sql()
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->order_by('space_id', 'desc');
        return $this->db->get_compiled_select('spaces');
    }

    public function get_search_results_sql($data)
    {
        $this->load->helper('text');
        $latitude = $data['lat'];
        $longtitude = $data['lng'];
        $correct_dates = FALSE;

        $price_from = $data['price_from'];
        $price_to = $data['price_to'];

        $factor = (100 + OVERCHARGE) / 100;

        list($sql, $correct_dates) = $this->get_sql_for_spaces_search($data, $latitude, $longtitude, $factor, $price_from, $price_to);

        $sqlSearch = $sql . " HAVING distance <= 20";

        $query = $this->db->query($sqlSearch);

        $distance = 40;
        while (($query->num_rows() == 0)) {
            $sqlSearch = $sql . " HAVING distance <= " . $distance;
            $query = $this->db->query($sqlSearch);
            $distance += 20;
            if ($distance > 210) {
                break;
            }
        }

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    public function get_available_now_spaces()
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->order_by('space_id', 'RANDOM');
        $query = $this->db->get('spaces', 6);

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }
            $row['space_type'] = $row['room_type'] == 1 ? 'Buto plotas': $row['room_type'] == 2 ? 'Namo plotas' : 'Komercinių patalpų plotas';
            $results[] = $row;
        }

        return $results;
    }

    public function get_available_now_spaces_sell()
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->where('category', 0);
        $this->db->order_by('space_id', 'RANDOM');
        $query = $this->db->get('spaces', 6);

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }
            $results[] = $row;
        }

        return $results;
    }

    public function get_available_now_spaces_rent()
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->where('category', 1);
        $this->db->order_by('space_id', 'RANDOM');
        $query = $this->db->get('spaces', 6);

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }
            $results[] = $row;
        }

        return $results;
    }

    public function get_related_spaces($exclude_space, $data, $params = array())
    {
        $this->load->helper('text');
        $latitude = $data['lat'];
        $longtitude = $data['lng'];
        $correct_dates = FALSE;

        $price_from = $data['price_from'];
        $price_to = $data['price_to'];

        $factor = (100 + OVERCHARGE) / 100;

        list($sql, $correct_dates) = $this->get_sql_for_spaces_search($data, $latitude, $longtitude, $factor, $price_from, $price_to);

        $exclude_space ? $sql .= " AND s.`space_id` != " . (int)$data['space_id'] . "":'';

        $sqlSearch = $sql . " HAVING distance <= 30";

        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $sqlSearch .= " LIMIT " . (int)$params['limit'] . " OFFSET " . (int)$params['start'];
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $sqlSearch .= " LIMIT " . (int)$params['limit'];
        }

        $query = $this->db->query($sqlSearch);

        $distance = 40;
        while (($query->num_rows() == 0)) {
            $sqlSearch = $sql . " HAVING distance <= " . $distance;
            if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $sqlSearch .= " LIMIT " . (int)$params['limit'] . " OFFSET " . (int)$params['start'];
            } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $sqlSearch .= " LIMIT " . (int)$params['limit'];
            }
            $query = $this->db->query($sqlSearch);
            $distance += 20;
            if ($distance > 210) {
                break;
            }
        }

        $results = array();
        foreach ($query->result_array() AS $k => $row) {
            $this->array_check_wishlist[] = $row['space_id'];

            $row['rating'] = $this->spacereview_model->get_ratings($row['space_id']);
            $row['image'] = get_image($row['image'], $row['folder_name'], MAIN_IMG_MEDIUM);
            if($row['category'] != 0){
                $row['price'] = real_price($row['price']);
            }
            $row['space_type'] = $row['room_type'] == 1 ? 'Buto plotas': $row['room_type'] == 2 ? 'Namo plotas' : 'Komercinių patalpų plotas';
            if ($correct_dates) {
                $row['link'] = site_url('perziureti/' . $row['slug'] . '/' . $row['space_id']) . '?sd=' . date(DATE_FORMAT, strtotime($data['start_date'])) . '&ed=' . date(DATE_FORMAT, strtotime($data['end_date']));
            } else {
                $row['link'] = site_url('perziureti/' . $row['slug'] . '/' . $row['space_id']);
            }

            $row['user_avatar'] = get_avatar($row['avatar']);

            $row['in_wishlist'] = 0;
            if (isset($row['wish']) && !empty($row['wish'])) {
                $row['in_wishlist'] = 1;
            }

            $results[] = $row;
        }

        return $results;
    }

    public function is_space_exist($space_id)
    {
        $this->db->where('active', 1);
        $this->db->where('deleted', 0);
        $this->db->where('space_id', $space_id);

        if ($this->db->count_all_results('spaces') == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function date_check($str)
    {
        if ($str != "") {
            $str = date("Y-m-d", strtotime($str));
        }

        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $str)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function increase_views_count($space_id)
    {
        $this->db->where('space_id', $space_id);
        $this->db->set('views', 'views + 1', FALSE);
        $this->db->update('spaces');
    }

    /**
     * @param $data
     * @param $latitude
     * @param $longtitude
     * @param $factor
     * @param $price_from
     * @param $price_to
     * @return array
     */
    protected function get_sql_for_spaces_search($data, $latitude, $longtitude, $factor, $price_from, $price_to)
    {
        $sql = "
            SELECT u.avatar, u.id AS user_id, s.*,
                ROUND(
                    3963.0 * ACOS(  
                        SIN( $latitude*PI()/180 ) * SIN( s.lat*PI()/180 )
                        + COS( $latitude*PI()/180 ) * COS( s.lat*PI()/180 )  *  COS( (s.lng*PI()/180) - ($longtitude*PI()/180) )   ) 
                , 1) AS distance
            FROM 
                `tr56_spaces` s
            INNER JOIN `tr56_users` u ON (u.id = s.user_id)
            WHERE
                s.`active` = 1 AND s.`deleted` = 0
                AND (s.`price` * " . $factor . ") >= " . $price_from . "
                AND (s.`price` * " . $factor . ") <= " . $price_to;

        if ($this->date_check($data['start_date']) && $this->date_check($data['end_date'])) {
            $correct_dates = TRUE;
            $sql .= " AND s.`start_date` <= '" . date('Y-m-d', strtotime($data['start_date'])) . "'
                AND s.`end_date` >= '" . date('Y-m-d', strtotime($data['end_date'])) . "'";
        } else {
            // $sql .= " AND s.`start_date` >= '".date('Y-m-d')."'";
        }

        $sql .= " AND s.`size` >= " . $data['size_from'] . "
                AND s.`size` <= " . $data['size_to'] . "
        ";


        if (!empty($data['category'])) {
            $sql .= " AND s.`category` IN (" . implode(',', $data['category']) . ")";
        }

        if (!empty($data['room_type'])) {
            $sql .= " AND s.`room_type` IN (" . implode(',', $data['room_type']) . ")";
        }

        if (!empty($data['facilities'])) {
            $sql .= " AND (";

            foreach ($data['facilities'] as $k => $facility_id) {
                $sql .= "s.`facilities` LIKE '%" . $facility_id . "%' AND ";
            }

            $sql = substr($sql, 0, -5);

            $sql .= ")";
        }

        return array($sql, $correct_dates);
    }

    public function get_min_space_price()
    {
        $this->db->select_min('price');
        $result = $this->db->get('spaces')->row();
        return $result->price;
    }

    public function get_max_space_price()
    {
        $this->db->select_max('price');
        $result = $this->db->get('spaces')->row();
        return $this->ceiling($result->price, 1000);
    }

    public function get_min_space_size()
    {
        $this->db->select_min('size');
        $result = $this->db->get('spaces')->row();
        return $result->size;
    }

    public function get_max_space_size()
    {
        $this->db->select_max('size');
        $result = $this->db->get('spaces')->row();
        return $this->ceiling($result->size, 100);
    }

    function ceiling($number, $significance = 1)
    {
        return ( is_numeric($number) && is_numeric($significance) ) ? (ceil($number/$significance)*$significance) : false;
    }
}