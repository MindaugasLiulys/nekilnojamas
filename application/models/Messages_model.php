<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages_model extends CI_Model {

    var $db_table = 'conversations';

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function update_fields($id, $fields, $table = '') {

        $db_table = ($table == '') ? $this->db_table : $table;

        $this->db->where('conversation_id', $id);
        $this->db->update($db_table, $fields);
    }

    public function make_hash_for_rezervation($info)
    {
        $hash = SHA1(ORDER_SALT.($info['conversation_id']+154));
        $this->update_fields($info['conversation_id'], array('hash' => $hash, 'status' => 2));

        return $hash;
    }

    public function generate_login_hash($info)
    {
        $hash = SHA1(LOGIN_SALT.($info['conversation_id']+298).$info['to']);
        $this->update_fields($info['conversation_id'], array('login_hash' => $hash));

        return $hash;
    }

    public function check_if_exist_conversation($conversation_id, $hash, $login_hash)
    {
        $this->db->select('conversations.*');
        $this->db->where('conversation_id', $conversation_id);
        $this->db->where('paid', 0);
        $this->db->where('hash', $hash);
        $this->db->where('login_hash', $login_hash);

        $query = $this->db->get('conversations', 1);

        if($query->num_rows() == 0)
        {
            return array();
        } else {
            return $query->row_array();
        }
    }

    public function get_messages($conversation_id)
    {
        $this->db->where('conversation_id', $conversation_id);
        $this->db->order_by('date_added', 'desc');
        $query = $this->db->get('messages');

        $results = array();
        foreach($query->result_array() AS $k => $row)
        {
            $results[] = $row;
        }

        return $results;
    }

    public function get_conversation_row($conversation_id)
    {
        $user_id = $this->session->userdata('user_id');

        $this->db->select('conversations.*, s.description, s.price as price_month');
        $this->db->join('spaces as s', 's.space_id = conversations.space_id', 'inner');
        $this->db->where('conversation_id', $conversation_id);

        $where = "(to = ".$user_id." OR from = ".$user_id.")";
        $this->db->where($where);

        $query = $this->db->get('conversations', 1);

        return $query->row_array();        
    }

    public function get_plain_conversation($conversation_id)
    {
        $this->db->where('conversation_id', $conversation_id);
        $query = $this->db->get('conversations');
        $result = $query->row_array();

        return $result;
    }

//    public function get_messages($conversation_id)
//    {
//        $this->db->where('conversation_id', $conversation_id);
//        $query = $this->db->get('spaces');
//
//        $result = $query->row_array();
//
//        return $result;
//    }

    public function get_conversation($conversation_id, $where_to = 0)
    {
        $this->load->helper('text');
        $user_id = $this->session->userdata('user_id');

        $this->db->select('conversations.*, s.description, s.price as price_month, IF(u.avatar = "", NULL, u.avatar) as from_avatar, IF(u2.avatar = "", NULL, u2.avatar) as to_avatar, u.email as tenant_email, CONCAT(u.first_name, " ", u.last_name) as full_name', FALSE);
        $this->db->join('users as u', 'u.id = conversations.from', 'inner');
        $this->db->join('users as u2', 'u2.id = conversations.to', 'inner');
        $this->db->join('spaces as s', 's.space_id = conversations.space_id', 'inner');
        $this->db->where('conversation_id', $conversation_id);

        if($where_to == 0)
        {
            $where = "(to = ".$user_id." OR from = ".$user_id.")";
            $this->db->where($where);
        } else 
        {
            $this->db->where('to', $user_id);
        }

        $query = $this->db->get('conversations');
        $result = $query->row_array();

        // echo $this->db->last_query();

        // var_dump($result);die();

        //  grab the right one avatar
        if($result['to'] == $this->session->userdata('user_id'))
        {
            $result['avatar'] = get_avatar($result['from_avatar']);
        } else {
            $result['avatar'] = get_avatar($result['to_avatar']);
        }

        $result['real_price'] = $this->currency->convert(real_price($result['price_month']));
        $result['descript'] = character_limiter($result['description'], 50, '...');
        $result['price'] = $this->currency->convert($result['price']);

        if(mb_strlen($result['descript']) > 51)
        {
            $result['descript'] = substr($result['descript'], 0, 47) . '...';
        }
        
        return $result;
    }

    public function add_message($conversation, $message)
    {
        $sender = $this->session->userdata('user_id');

        if($conversation['from'] == $sender)
        {
            $receiver = (int)$conversation['to'];
            $this->update_fields($conversation['conversation_id'], array('readed_to' => 0));
        } elseif($conversation['to'] == $sender)
        {
            $receiver = (int)$conversation['from'];
            $this->update_fields($conversation['conversation_id'], array('readed_from' => 0));
        } else {
            $receiver = 0;
        }

        $cleaned = array(
            'from'            => $sender,
            'to'              => $receiver,
            'conversation_id' => $conversation['conversation_id'],
            'message'         => $this->db->escape_str($message),
            'name'            => $conversation['name'],
            'phone'           => $conversation['phone'],
            'surname'         => $conversation['surname'],
            'email'           => $conversation['email'],
            'date_added'      => date('Y-m-d H:i:s')
        );

        $this->db->insert('messages', $cleaned);

        $this->load->model('emails_model');
        $this->emails_model->send_message_received($conversation, $receiver);

        return $cleaned;       
    }

    public function add($from, $to, $space_id, $message, $start_date, $end_date, $name, $surname, $email, $phone, $status = 0)
    {
    	$clean = array(
            'from'       => (int)$from,
//            'from'       => 22,
            'to'         => (int)$to,
            'space_id'   => (int)$space_id,
            'date_added' => date('Y-m-d H:i:s'),
            'start_date' => $start_date,
            'end_date'   => $end_date,
            'price'      => 0,
            'status'     => (int)$status
    	);

    	$this->db->insert('conversations', $clean);
    	$conversation_id = $this->db->insert_id();

    	if($message != "" && !empty($message))
    	{
	    	$cleaned_arr = array(
				'from'            => (int)$from,
//				'from'            => 22,
				'to'              => (int)$to,
				'conversation_id' => $conversation_id,
                'date_added'      => date('Y-m-d H:i:s'),
                'message'         => $this->db->escape_str($message),
				'name'            => $name,
                'surname'         => $surname,
                'email'           => $email,
                'phone'           => $phone,
	    	);

	    	$this->db->insert('messages', $cleaned_arr);
    	}

        return  $conversation_id;
    }

    public function get_conversations($status_filter = 'all')
    {
        $user_id = $this->session->userdata('user_id');
        $this->load->helper('text');

        $where_condition = '';
        if($status_filter == 'waiting')
        {
            $where_condition = '(c.status = 0) AND ';
        } elseif($status_filter == 'unread')
        {
            $where_condition = '((CASE WHEN c.to = '.$user_id.' THEN c.readed_to ELSE c.readed_from end) = 0) AND ';
        }

        // $sql = "SELECT
        //     c.*, m.message, m.date_added as msg_date,
        //     IF(u.avatar = '', NULL, u.avatar) from_avatar, IF(u2.avatar = '', NULL, u2.avatar) as to_avatar, u.first_name from_name, u2.first_name to_name,
        //     IF(c.to = ".$user_id.", c.readed_to, c.readed_from) as readed
        //     FROM
        //         `tr56_conversations` c
        // INNER JOIN `tr56_users` as `u` ON (`u`.`id` = c.`from`)
        // INNER JOIN `tr56_users` as `u2` ON (`u2`.`id` = c.`to`)
        // LEFT JOIN `tr56_messages` as `m` ON (`m`.`conversation_id` = c.`conversation_id`)
        //     WHERE ".$where_condition."
        //         m.message_id IN (SELECT MAX(message_id) FROM `tr56_messages` WHERE `from` = ". $user_id ." OR `to` = ". $user_id ." GROUP BY conversation_id)
        //         OR (c.`to` = ". $user_id ." OR c.`from` = ". $user_id .")
        //         GROUP BY conversation_id ORDER BY readed ASC, c.date_added DESC";

        $sql = "SELECT
            c.*, m.message, m.date_added as msg_date, 
            IF(u.avatar = '', NULL, u.avatar) from_avatar, IF(u2.avatar = '', NULL, u2.avatar) as to_avatar, u.first_name from_name, u2.first_name to_name,
            IF(c.to = ".$user_id.", c.readed_to, c.readed_from) as readed,
            IF(c.to = ".$user_id.", c.deleted_to, c.deleted_from) as deleted
            FROM 
                `tr56_conversations` c
        INNER JOIN `tr56_users` as `u` ON (`u`.`id` = c.`from`)
        INNER JOIN `tr56_users` as `u2` ON (`u2`.`id` = c.`to`)
        LEFT join
(
  SELECT MAX(message_id) max_id, conversation_id, message, date_added
  FROM `tr56_messages`
  group by conversation_id
) p2 ON p2.conversation_id = c.conversation_id
        LEFT JOIN `tr56_messages` m ON (p2.max_id = m.message_id)
            WHERE ".$where_condition."
                 (c.`to` = ". $user_id ." OR c.`from` = ". $user_id .")
                ORDER BY readed ASC, c.date_added DESC";

        $query = $this->db->query($sql);

        $results = array();
        foreach($query->result_array() AS $k => $row)
        {
            if($row['deleted'] == 0)
            {
                // var_dump($row);
                if($row['to'] == $user_id)
                {
                    $row['avatar'] = get_avatar($row['from_avatar']);
                    $row['first_name'] = $row['from_name'];
                } else {
                    $row['avatar'] = get_avatar($row['to_avatar']);
                    $row['first_name'] = $row['to_name'];
                }
                $row['message'] = character_limiter($row['message'], 50, '...');
                $row['price'] = $this->currency->convert($row['price']);

                $results[] = $row;
            }
        }

        return $results;
    }
}