<?php if (!empty($results)): ?>
    <?php foreach ($results as $k => $row): ?>
        <a class="wish-review roomspace" href="<?php echo base_url('perziureti/' . $row['space_id']) ?>">
            <div class="small-12 medium-6 large-4 columns wish-item">
                <div class="wish-item-block rev">
                    <div class="img-holder">
                        <div class="overlay"></div><span class="wish-review"></span>
                        <img src="<?php echo site_url($row['image']); ?>">
                    </div>
                    <div class="wish-item-block-bottom <?= $row['category'] != 0 ? 'rent' : '' ?>">
                        <div class="small-12 columns wish-item-price nopad">
                            <?php echo format($row['price']); ?><?php if($row['category'] != 0){echo('/'); echo(lang('month'));}?>
                        </div>
                    </div>
                    <div class="small-12 columns wishlist-description">
                        <p>
                            <img src="<?php echo base_url(); ?>assets/img/size.png">
                            <?php if($row['room_type'] == 3): ?>
                                <?php echo ' ' . lang('plot') . ' : ' . $row['size'] . ' ' . lang('acre') . number_plur($row['size']); ?>
                            <?php else: ?>
                                <?php echo $row['space_type'] . ' ' . $row['size']; ?> m<sup>2</sup>
                            <?php endif; ?>
                        </p>
                        <p>
                            <img src="<?php echo base_url(); ?>assets/img/slider-date.png">
                            <?php if($row['category'] != 0): ?>
                                <?php    echo(lang('free_from')); ?>
                            <?php else: ?>
                                <?php    echo(lang('sell_from')); ?>
                            <?php endif; ?>
                            : <?php echo date('d/m/Y', strtotime($row['start_date'])); ?>
                        </p>
                        <p class="place-address"><img src="<?php echo base_url(); ?>assets/img/icon-search.png">
                            <?php echo get_address($row['lat'], $row['lng']) ?></p>
                        <p class="place-rating">
                            <img src="<?php echo base_url(); ?>assets/img/eye.png"> <?php echo number_format($row['views'], 0, ",", " ") ?>
                        </p>
                    </div>
                </div>
            </div>
        </a>
    <?php endforeach ?>
<?php else: ?>
    <?= lang('no_results') ?>
<?php endif ?>