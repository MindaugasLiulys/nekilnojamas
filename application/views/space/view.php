<div class="row content">
    <div class="small-12 columns spare-room-view">
        <div class="small-12 columns nopad">
            <div class="small-12 medium-6 large-6 columns spare-room-images nopadleft">
                <div class="small-12 columns spare-room-slider nopad">
                    <ul class="clearing-thumbs clearing-feature" data-clearing>
                        <?php if (!empty($info['image'])): ?>
                            <li class="clearing-featured-img">
                                <a href="<?php echo site_url($info['image']); ?>">
                                    <img data-caption="<?=lang('space_image')?> 1 <?=lang('of')?> <?php echo count($images) + 1 ?>"
                                         src="<?php echo site_url($info['image']); ?>">
                                </a>
                            </li>
                        <?php endif ?>
                        <?php if (!empty($images)): ?>
                            <?php foreach ($images as $key => $image): ?>
                                <li>
                                    <a href="<?php echo site_url($image); ?>">
                                        <img
                                            data-caption="<?=lang('space_image')?> <?php echo $key + 2 ?> <?=lang('of')?> <?php echo count($images) + 1 ?>"
                                            src="<?php echo site_url($image); ?>">
                                    </a>
                                </li>
                            <?php endforeach ?>
                        <?php endif ?>
                    </ul>
                    <div class="view-space-photos">
                        <a href="#" class="button btn-white" id="view-space-photos" data-thumb-index="0">
                            <?=lang('view_photos')?>
                        </a>
                    </div>
                    <?php if (logged_in()): ?>
                        <?php if ($is_in_wishlist): ?>
                            <div class="wish-item-fav active">
                                <a title="<?= lang('remove_from_wishlist') ?>" data-id="<?= $info['space_id'] ?>"
                                   href="#">
                                </a>
                            </div>
                        <?php else: ?>
                            <div class="add-to-wishlist">
                                <a onclick="add_to_wishlist(<?php echo $info['space_id'] ?>);return false;"
                                   title="<?= lang('add_to_wishlist') ?>" class="btn-orange"
                                   href="#"><?= lang('add_to_wishlist') ?> <i class="fa fa-heart-o" aria-hidden="true"></i></a>
                            </div>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>
            <div class="small-12 medium-6 large-6 columns spare-room-about nopad">
                <div class="half-block-title">
                    <?= lang('additional_information') ?>
                </div>
                <div class="half-block-text columns">
                    <div class="small-12 columns about-room-attributes">
                       <h2><?php
                           if($info['room_number'] == 0){
                               $slug = '';
                           }else{
                               $slug = $info['room_number'].' ' . room_plural($info['room_number']) . ' ';
                           }
                                $viewTitle = $slug . room_type_helper($info['room_type']) . ', ' . get_address($info['lat'], $info['lng']);
                                echo $viewTitle; ?>
                       </h2>
                        <div class="small-12 columns nopad">
                            <div class="small-4 columns attribute-title text-right nopad">
                                <?= lang('category') ?>:
                            </div>
                            <div class="small-8 columns attribute-text nopadright">
                                <?php echo category($info['category']) ?>
                            </div>
                        </div>
                        <div class="small-12 columns nopad">
                            <div class="small-4 columns attribute-title text-right nopad">
                                <?= lang('location') ?>:
                            </div>
                            <div class="small-8 columns attribute-text nopadright">
                                <?= get_address($info['lat'], $info['lng']) ?>
                            </div>
                        </div>
                        <div class="small-12 columns nopad">
                            <div class="small-4 columns attribute-title text-right nopad">
                                <?= lang('room_type') ?>:
                            </div>
                            <div class="small-8 columns attribute-text nopadright">
                                <?php echo room_type_helper($info['room_type']); ?>
                            </div>
                        </div>
                        <div class="small-12 columns nopad">
                            <div class="small-4 columns attribute-title text-right nopad">
                                <?= lang('price') ?>:
                            </div>
                            <div class="small-8 columns attribute-text nopadright">
                                <?php echo format($info['price']) ?><?php if($info['category'] != 0){echo('/'); echo(lang('month'));}?>
                            </div>
                        </div>
                        <?php if($info['room_number'] != 0){?>
                        <div class="small-12 columns nopad">
                            <div class="small-4 columns attribute-title text-right nopad">
                                <?= lang('number_of_rooms') ?>:
                            </div>
                            <div class="small-8 columns attribute-text nopadright">
                                <?php echo $info['room_number'] ?>
                            </div>
                        </div>
                        <?php }?>
                        <div class="small-12 columns nopad">
                            <div class="small-4 columns attribute-title text-right nopad">
                                <?= lang('size') ?>:
                            </div>
                            <div class="small-8 columns attribute-text nopadright">
                                <?php echo $info['size'] ?>
                                <?php if($info['room_type'] == 3){
                                    echo lang('acre');
                                    echo(number_plur($info['size']));
                                   }else{?>
                                m<sup>2</sup>
                                <?php }?>
                            </div>
                        </div>
                        <?php if($info['room_type'] != 3 && $info['room_type'] != 4){?>
                            <div class="small-12 columns nopad">
                                <div class="small-4 columns attribute-title text-right nopad">
                                    <?= lang('available_from') ?>:
                                </div>
                                <div class="small-8 columns attribute-text nopadright">
                                    <?php echo date(DATE_FORMAT, strtotime($info['start_date'])); ?>
                                </div>
                            </div>
                        <?php }?>
                        <?php if (!empty(get_facilities($info['facilities']))): ?>
                            <div class="small-12 columns nopad">
                                <div class="small-4 columns attribute-title text-right nopad">
                                    <?= lang('facilities') ?>:
                                </div>
                                <div class="small-8 columns attribute-text nopadright">
                                    <?= get_facilities($info['facilities']) ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="small-12 columns nopad">
                            <div class="small-4 columns attribute-title text-right nopad">
                                <?= lang('views') ?>:
                            </div>
                            <div class="small-8 place-rating columns attribute-text nopadright">
                                <img src="<?php echo base_url(); ?>assets/img/eye.png"> <?php echo number_format($info['views'], 0, ",", " ") ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="small-12 columns spare-room-options nopad">
                <div class="half-block-title"><?=lang('rent_this_space')?></div>
                <div class="half-block-text columns">
                    <div class="small-12 medium-6 large-6 columns spare-room-comment form-element">
                        <input id="name" type="text" name="name" placeholder="Jūsų vardas">
                    </div>
                    <div class="small-12 medium-6 large-6 columns spare-room-comment form-element">
                        <input id="surname" type="text" name="surname" placeholder="Jūsų pavardė">
                    </div>
                    <div class="small-12 medium-6 large-6 columns spare-room-comment form-element">
                        <input id="phone" type="text" name="phone" placeholder="Jūsų telefonas">
                    </div>
                    <div class="small-12 medium-6 large-6 columns spare-room-comment form-element">
                        <input id="email" type="text" name="email" placeholder="Jūsų el-paštas">
                    </div>
                    <div class="small-12 medium-9 large-9 columns spare-room-comment form-element">
                        <textarea placeholder="<?= lang('ask_host_question') ?>" name="comments"
                                  id="comments"><?php echo $comment ?></textarea>
                    </div>
                    <div class="small-12 medium-3 large-3 columns spare-room-btns nopad">
                        <div class="small-12 columns nopad text-right small-only-text-center">
                            <a class="btn-green button expand" href="#" id="rent-btn"><?= lang('rent') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="small-12 columns nopad">
            <div class="small-12
            <?php echo (logged_in() && $user_rating['rented']) ? 'medium-4 large-4' : 'medium-6 large-6'; ?>
                columns spare-room-about-owner nopadleft">
                <div class="half-block-title">
                    <?= lang('about_the_owner') ?>
                </div>
                <div class="half-block-text">
                    <div class="small-4 medium-2 columns nopad">
                        <a class="user-img" href="
                        <?php echo site_url('vartotojas/perziureti/' . $info['user_id']); ?>"><img
                                src="
                        <?php echo site_url($info['user_avatar']); ?>">
                        </a>
                    </div>
                    <div class="small-12">
                        <?php
                        if($info['user_description'] !== ''){

                            $string = utf8_decode($info['user_description']);
                            echo '<div class="comment more">'.$string.' </div>';

                        }else{
                            echo(lang('hey_i_am').' '.ucfirst(htmlentities($info['first_name'])).'! <br>');
                            echo(lang('member_since').' '.date('F Y', $info['created_on']));
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="small-12
            <?php echo (logged_in() && $user_rating['rented']) ? 'medium-4 large-4' : ' medium-6 large-6 '; ?>
                columns spare-room-additional-info nopadleft nopadright">
                <div class="half-block-title">
                    <?= lang('about_spare_room') ?>
                    <?php if (isset($row['rating'])): ?>
                        <div class="wish-item-stars text-right nopad">
                            <span class="green">0/10</span>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="half-block-text">
                    <?php echo '<div class="comment more">'.nl2br($info['description']).' </div>';?>
                </div>
            </div>
        </div>

        <div class="small-12 columns nopad">
            <div class="small-12 columns gmap spare-room-map nopad">
                <div id="map"></div>
            </div>
            <?php if (!empty($results)): ?>
                <div class="small-12 columns spare-room-similar nopad">
                    <div class="small-12 columns spare-room-similar-block nopad">
                        <div class="similar-list-title">
                            <?= lang('similar_sparerooms') ?>
                        </div>
                        <div class="small-12 columns wishlist nopad text-center">
                            <div class="row" id="similar-spaces">
                                <?php foreach ($results as $k => $row): ?>
                                <a class="roomspace" href="<?php echo base_url('perziureti/' . $row['slug'] . '/'  . $row['space_id']) ?>">
                                    <div class="small-12 medium-6 large-3 columns wish-item">
                                        <div class="wish-item-block rev">
                                            <div class="img-holder">
                                                    <div class="overlay"></div><span class="wish-review"></span>
                                                    <img src="<?php echo site_url($row['image']); ?>">
                                            </div>
                                            <div class="wish-item-block-bottom <?= $row['category'] != 0 ? 'rent' : '' ?>">
                                                <div class="small-12 columns wish-item-price nopad">
                                                    <?php echo format($row['price']); ?><?php if($row['category'] != 0){echo('/'); echo(lang('month'));}?>
                                                </div>
                                            </div>
                                            <div class="small-12 columns wishlist-description">
                                                <p>
                                                    <img src="<?php echo base_url(); ?>assets/img/size.png">
                                                    <?php if($row['room_type'] == 3): ?>
                                                        <?php echo ' ' . lang('plot') . ' : ' . $row['size'] . ' ' . lang('acre') . number_plur($row['size']); ?>
                                                    <?php else: ?>
                                                        <?php echo $row['space_type'] . ' ' . $row['size']; ?> m<sup>2</sup>
                                                    <?php endif; ?>
                                                </p>
                                                <p>
                                                    <img src="<?php echo base_url(); ?>assets/img/slider-date.png">
                                                    <?php if($row['category'] != 0): ?>
                                                        <?php    echo(lang('free_from')); ?>
                                                    <?php else: ?>
                                                        <?php    echo(lang('sell_from')); ?>
                                                    <?php endif; ?>
                                                    : <?php echo date('d/m/Y', strtotime($row['start_date'])); ?>
                                                </p>
                                                <p class="place-address"><img
                                                        src="<?php echo base_url(); ?>assets/img/icon-search.png">
                                                    <?php echo $row['street_address'] ?></p>
                                                <p class="place-rating">
                                                    <img src="<?php echo base_url(); ?>assets/img/eye.png"> <?php echo number_format($row['views'], 0, ",", " ") ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <?php endforeach ?>
                            </div>
                            <?php if(count($total_spaces) > 4){?>
                            <a class="show_more_main btn-orange" id="show_more_main">
                                <i class="fa fa-plus-square" aria-hidden="true"></i>
                                <span id="<?php echo count($results); ?>" class="show_more"
                                    title="<?= lang('show_more_spoaces')?>"><?= lang('show_more_spaces')?></span>
                            </a>
                            <?php }?>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>

    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery.nouislider.all.min.js"></script>

<script type="text/javascript">

    var space_id = <?php echo $info['space_id']; ?>;

    var googleMapApp = {
        markers: [],
        bounds: "",
        map: "",
        setAllMap: function (map) {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(map);
            }
        },

        clearMarkers: function () {
            this.setAllMap(null);
        },

        centerMap: function (map, latitude, longtitude) {
            var center = new google.maps.LatLng(latitude, longtitude);
            map.setCenter(center);
        },

        setMarkers: function (map, locations, center_lat, center_lng) {
            var that = this;

            var bounds = "";
            var bounds = new google.maps.LatLngBounds();
            var coordinates_hash = new Array();

            for (var i = 0; i < locations.length; i++) {

                var image = {
                    url: locations[i][5],
                    size: new google.maps.Size(35, 35),// 20 pixels wide by 32 pixels tall.
                    origin: new google.maps.Point(0, 0),// The origin for this image is 0,0.
                    anchor: new google.maps.Point(0, 35)// The anchor -  base of the flagpole at 0,32.
                };

                var coordinates_str, actual_lat, actual_lon, adjusted_lat, adjusted_lon;

                actual_lat = adjusted_lat = locations[i][0];
                actual_lon = adjusted_lon = locations[i][1];
                coordinates_str = actual_lat + actual_lon;

                while (coordinates_hash[coordinates_str] != null) {
                    // adjust coord by 50m or so
                    adjusted_lat = parseFloat(actual_lat) + (Math.random() - 0.5) / 750;
                    adjusted_lon = parseFloat(actual_lon) + (Math.random() - 0.5) / 750;
                    coordinates_str = String(adjusted_lat) + String(adjusted_lon);
                }

                coordinates_hash[coordinates_str] = 1;

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(adjusted_lat, adjusted_lon),
                    map: map,
                    icon: image
                });

                //extend the bounds to include each marker's position
                bounds.extend(marker.position);

                var infowindow = new google.maps.InfoWindow({
                    content: '',
                    maxWidth: 300
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][3]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                if (locations[i][4]) {
                    infowindow.setContent(locations[i][3]);
                    infowindow.open(map, marker);
                }

                that.markers.push(marker);
            }

            this.centerMap(map, center_lat, center_lng);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
            map.setZoom(map.getZoom() - 1);
            map.setOptions({
                maxZoom: 17
            });

            if (map.getZoom() > 15) {
                map.setZoom(15);
            }
        },

        invoke: function () {
            var that = this;

            this.map = null;
            var mapDefaults = {
                scrollwheel: false,
                zoom: 15,
                center: {lat:<?= $info['lat'] ?>, lng: <?= $info['lng'] ?>},
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            $(function () {
                that.map = new google.maps.Map(document.getElementById('map'), mapDefaults);
                var points = <?php echo $json_prods ?>;
                if (points.length > 0) {
                    that.setMarkers(that.map, points, <?php echo $info['lat'] ?>, <?php echo $info['lng'] ?>);
                }
            });
        }
    };

    $(function () {

        googleMapApp.invoke();

        $("#loadMore").on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('show-reviews');
            if ($(this).hasClass('show-reviews')) {
                $("#show-all-reviews div.row:hidden").slice(0, <?php echo count($reviews)?>).slideDown();
                if ($("#show-all-reviews div.row:hidden").length == 0) {
                    $("#load").fadeOut('slow');
                }
                $(this).text('<?php echo lang('hide_all_reviews') ?>');
            } else {
                $("#show-all-reviews div.row").slideUp();
                $(this).text('<?php echo lang('show_all_reviews') ?>');
            }

        });

        $(document).on('click', '.show_more_main', function () {
            var id = parseInt($('.show_more').attr('id'));
            var to = id + 4;
            var total = <?php echo $json_prods ?>;
            $('#loader').show();
            $.ajax({
                type: 'POST',
                url: "/space/ajax_load_more_spaces",
                data: 'page=' + id + '&space_id=' + space_id,
                success: function (data) {
                    if (to >= (total.length - 1)) {
                        $('#show_more_main').remove();
                    }else {
                        $('.show_more').attr('id', to);
                    }
                    $('#similar-spaces').append(data);
                    $('#loader').hide();
                }
            });
        });

        $('#view-space-photos').on('click', function (e) {
            e.preventDefault();
            $('[data-clearing] li img').eq($(this).data('thumb-index')).trigger('click');
        });

        function update_price() {

            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var info = "&start_date=" + start_date + "&end_date=" + end_date + "&space_id=" + space_id;
            $.ajax({
                type: "POST",
                url: "/space/ajax_final_price/",
                data: info,

                dataType: "json",
                success: function (data) {
                    if (data.error != 1) {
                        $('#final_price').html(data.response);
                    } else {
                        $('#pop_up_content').html(data.response);
                        $('#for-messages').foundation('reveal', 'open');
                    }
                }
            });
        }

        $(function () {
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            var checkin = $('#start_date').fdatepicker({
                format: '<?= DATEPICKER_FORMAT ?>',
                onRender: function (date) {
                    return date.valueOf() < now.valueOf() ? 'disabled' : '';
                }
            }).on('changeDate', function (ev) {
                if (ev.date.valueOf() > checkout.date.valueOf()) {
                    var newDate = new Date(ev.date)
                    newDate.setDate(newDate.getDate() + 1);
                    checkout.update(newDate);
                }
                checkin.hide();
                $('#end_date')[0].focus();
            }).data('datepicker');
            var checkout = $('#end_date').fdatepicker({
                format: '<?= DATEPICKER_FORMAT ?>',
                onRender: function (date) {
                    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
                }
            }).on('changeDate', function (ev) {
                checkout.hide();
                update_price();
            }).data('datepicker');

            $('#rent-btn').click(function (e) {
                e.preventDefault();

                var comments = $('#comments').val();
                var name = $('#name').val();
                var phone = $('#phone').val();
                var surname = $('#surname').val();
                var email = $('#email').val();
                var info = "&space_id=" + space_id + "&comments=" + comments + "&name=" + name + "&phone=" + phone + "&surname=" + surname + "&email=" + email;

                $.ajax({
                    type: "POST",
                    url: "/space/rent/",
                    data: info,
                    dataType: "json",
                    success: function (data) {
                        $('#pop_up_content').html(data.response);
                        $('#for-messages').foundation('reveal', 'open');
                    }
                });
            });

            $("div.sp-div").text(function(index, currentText) {
                var count = currentText.substr(0, num);
                return count;
            });

            $(".more").on('click', function(){
                num = 1000;
                alert(currentText);
                count = currentText.substr(0, num);
                return count;
            });

            $(document).on('click', '.wish-item-fav a', function() {
                var info = "&id=" + $(this).data('id');
                $.ajax({
                    type: "POST",
                    url: '/wishlist/ajax_remove_wishlist/',
                    data: info,
                    dataType: "json",
                    success: function (data) {
                        $('.wish-item-fav').after(data.wishlist_btn);
                        $('.wish-item-fav').remove();

                        $('#pop_up_content').html(data.response);
                        $('#for-messages').foundation('reveal', 'open');
                    }
                });
            })
        });

        var showChar = 190;
        var ellipsestext = "...";
        var moretext = "Skaityti daugiau...";
        var lesstext = "mažiau";
        $('.more').each(function() {
            var content = $(this).html();

            if(content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar-1, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }
        });

        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });

        $('.pricettl').on('click', function(){
            $('input#start_date').trigger('input').focus();
        })

    });
</script>