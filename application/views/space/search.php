<div class="row content padding-top20">
    <div class="small-12 large-8 columns list-edit-your-space search-page nopad">
        <div style="display:<?= (isset($error) && !empty($error)) ? 'block' : 'none' ?>"
             id="errors"><?= (isset($error) && !empty($error)) ? $error : '' ?></div>
        <div class="small-12 columns search-filter">
            <form action="#" method="post" id="search">
                <input type="hidden" name="lat" value="<?php echo $lat ?>">
                <input type="hidden" name="lng" value="<?php echo $lng ?>">
                <input type="hidden" name="formatted_address" value="">
                <div class="small-12 medium-12 large-8 main-filter columns nopad s-place">
                    <input class="place-search" type="text" name="" id="place"
                           placeholder="<?= lang('place_example') ?>">
                </div>
                <div class="small-5 medium-6 large-2 main-filter columns nopad text-right small-only-text-center">
                    <button class="btn-slider-search btn-orange" type="submit">
                        <?= lang('search') ?>
                    </button>
                </div>
                <div class="small-5 medium-6 large-2 main-filter columns submit small-only-text-center nopad">
                    <button id="apply_filters" class="btn-orange-inverse left"
                            type="button"><?= lang('more_filters') ?></button>
                </div>
                <div id="more-filters-content">
                    <div class="small-12 large-6 columns nopad price-filter">
                        <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                            <?= lang('price_range') ?>
                        </div>
                        <div class="small-12 medium-9 large-9 columns rs form-element">
                            <div id="slider"></div>
                            <div class="small-6 columns nopad">
                                <i class="fa fa-eur" aria-hidden="true"></i> <span id="value-span1"></span>
                            </div>
                            <div class="small-6 columns text-right nopad">
                                <i class="fa fa-eur" aria-hidden="true"></i> <span id="value-span2"></span>
                            </div>
                            <input id="price_from" type="hidden" value="<?php echo $price_from ?>" name="price_from" />
                            <input id="price_to" type="hidden" value="<?php echo $price_to ?>" name="price_to" />
                        </div>
                    </div>
                    <div class="small-12 large-6 columns nopad size-fitler">
                        <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                            <?= lang('size_range') ?>
                        </div>
                        <div class="small-12 medium-9 large-9 columns rs form-element">
                            <div id="slider2"></div>
                            <div class="small-6 columns nopad">
                                <span id="value-span3"></span>
                            </div>
                            <div class="small-6 columns text-right nopad">
                                <span id="value-span4"></span>
                            </div>
                            <input id="size_from" type="hidden" value="<?php echo $size_from ?>" name="size_from"/>
                            <input id="size_to" type="hidden" value="<?php echo $size_to ?>" name="size_to"/>
                        </div>
                    </div>
                    <div class="small-12 columns nopad">
                        <div class="small-12 columns text-left form-label small-only-text-left nopad">
                            <?= lang('category') ?>
                        </div>
                        <div class="small-12 columns form-element nopad">
                            <label class="small-6">
                                <input id="rent" class="icheck-activate" type="checkbox" name="category[]" value="1"><?= lang('rent-btn') ?>
                            </label>
                            <label class="small-6">
                                <input id="sell" class="icheck-activate" type="checkbox" name="category[]" value="0"><?= lang('sell-btn') ?>
                            </label>
                        </div>
                    </div>
                    <div class="small-12 columns nopad">
                        <div class="small-12 columns text-left form-label small-only-text-left nopad">
                            <?= lang('room_type') ?>
                        </div>
                        <div class="small-12 columns form-element nopad">
                            <label class="small-6">
                                <input class="icheck-activate" type="checkbox" name="room_type[]" value="1"><?= lang('apartment') ?>
                            </label>
                            <label class="small-6">
                                <input class="icheck-activate" type="checkbox" name="room_type[]" value="2"><?= lang('house') ?>
                            </label>
                            <label class="small-6">
                                <input class="icheck-activate" type="checkbox" name="room_type[]" value="3"><?= lang('land') ?>
                            </label>
                            <label class="small-6">
                                <input class="icheck-activate" type="checkbox" name="room_type[]" value="4"><?= lang('commercial') ?>
                            </label>
                        </div>
                    </div>
                    <div class="small-12 columns pad20">
                        <div class="small-12 columns text-left form-label small-only-text-left nopad">
                            <?= lang('facilities') ?>
                        </div>
                        <div class="small-12 columns form-element nopad">
                            <?php
                            foreach($facilities as $k => $v){ ?>
                                <label class="small-6">
                                    <input class="icheck-activate" type="checkbox" name="facilities[]" value="<?=$v['facility_id'] ?>"><?= $v['name'] ?>
                                </label>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="small-12 columns wishlist nopad">
            <div class="row" id="search_results">
                <?php if (!empty($results)): ?>
                    <?php foreach ($results as $k => $row): ?>
                      <a class="roomspace" href="<?php echo base_url('perziureti/' . $row['slug'] . '/' . $row['space_id']) ?>">
                        <div class="small-12 medium-4 large-4 columns wish-item">
                            <div class="wish-item-block rev">
                                <div class="img-holder">
                                        <div class="overlay"></div><span class="wish-review"></span>
                                        <img src="<?php echo site_url($row['image']); ?>">
                                </div>
                                <div class="wish-item-block-bottom <?= $row['category'] != 0 ? 'rent' : '' ?>">
                                    <div class="small-12 columns wish-item-price nopad">
                                        <?php echo format($row['price']); ?><?php if($row['category'] != 0){echo('/'); echo(lang('month'));}?>
                                    </div>
                                </div>
                                <div class="small-12 columns wishlist-description">
                                    <p>
                                        <img src="<?php echo base_url(); ?>assets/img/size.png">
                                        <?php if($row['room_type'] == 3): ?>
                                            <?php echo ' ' . lang('plot') . ' : ' . $row['size'] . ' ' . lang('acre') . number_plur($row['size']); ?>
                                        <?php else: ?>
                                            <?php echo $row['space_type'] . ' ' . $row['size']; ?> m<sup>2</sup>
                                        <?php endif; ?>
                                    </p>
                                    <p>
                                        <img src="<?php echo base_url(); ?>assets/img/slider-date.png">
                                        <?php if($row['category'] != 0): ?>
                                            <?php    echo(lang('free_from')); ?>
                                        <?php else: ?>
                                            <?php    echo(lang('sell_from')); ?>
                                        <?php endif; ?>
                                        : <?php echo date('d/m/Y', strtotime($row['start_date'])); ?>
                                    </p>
                                    <p class="place-address"><img src="<?php echo base_url(); ?>assets/img/icon-search.png">
                                        <?php echo $row['street_address'] ?></p>
                                    <p class="place-rating">
                                        <img src="<?php echo base_url(); ?>assets/img/eye.png"> <?php echo number_format($row['views'], 0, ",", " ") ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                      </a>
                    <?php endforeach ?>
                <?php else: ?>
                    <?= lang('no_results') ?>
                <?php endif ?>
                <?php echo $this->ajax_pagination->create_links(); ?>
            </div>
        </div>
    </div>
    <div class="small-12 large-4 gmap columns">
        <div id="map"></div>
    </div>
</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.nouislider.all.min.js"></script>
<?php if($checkGet == 1){ ?>
    <script>
        var checkbox = '<?=$checkbox ?>';
        $('#' + checkbox).attr('checked', true);

        var search_info = $("#search, #more-filters-content :input:not(:hidden)").serialize();

        $.ajax({
            type: "POST",
            url: "/space/ajax_search",
            data: search_info,
            dataType: "json",
            beforeSend: function () {
                $('#loader').show();
            },
            success: function (data) {
                if (data.error == 1) {
                    $('#errors').hide();
                    $('#errors').html(data.response);
                    $('#errors').show();
                    $('html, body').animate({
                        scrollTop: $("#errors").offset().top
                    }, 1000);
                } else {
                    if (data.error == 2) {
                        $('#errors').html(data.products);
                        $('#errors').show();
                        $('html, body').animate({
                            scrollTop: $("#errors").offset().top
                        }, 1000);
                    } else {
                        $('#errors').hide();
                        $('#search_results').html(data.products);

                        googleMapApp.reload_map(data.json_prods, data.lat, data.lng);
                    }
                }
                $('#loader').hide();
            }
        });
    </script>
<?php } ?>

<script type="text/javascript">

    var googleMapApp = {
        markers: [],
        bounds: "",
        map: "",
        setAllMap: function (map) {
            for (var i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(map);
            }
        },
        clearMarkers: function () {
            this.setAllMap(null);
        },
        centerMap: function (map, latitude, longtitude) {

            var center = new google.maps.LatLng(latitude, longtitude);
            map.setCenter(center);
        },
        setMarkers: function (map, locations, center_lat, center_lng) {
            var that = this;

            var bounds = "";
            var bounds = new google.maps.LatLngBounds();
            var coordinates_hash = new Array();

            for (var i = 0; i < locations.length; i++) {
                var coordinates_str, actual_lat, actual_lon, adjusted_lat, adjusted_lon;

                var image = {
                    url: locations[i][5],
                    size: new google.maps.Size(24, 24),// 20 pixels wide by 32 pixels tall.
                    origin: new google.maps.Point(0, 0),// The origin for this image is 0,0.
                    anchor: new google.maps.Point(0, 24)// The anchor -  base of the flagpole at 0,32.
                };

                actual_lat = adjusted_lat = locations[i][0];
                actual_lon = adjusted_lon = locations[i][1];
                coordinates_str = actual_lat + actual_lon;
                while (coordinates_hash[coordinates_str] != null) {
                    // adjust coord by 50m or so
                    adjusted_lat = parseFloat(actual_lat) + (Math.random() - 0.5) / 750;
                    adjusted_lon = parseFloat(actual_lon) + (Math.random() - 0.5) / 750;
                    coordinates_str = String(adjusted_lat) + String(adjusted_lon);
                }
                coordinates_hash[coordinates_str] = 1;

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(adjusted_lat, adjusted_lon),
                    map: map,
                    icon: image
                });

                //extend the bounds to include each marker's position
                bounds.extend(marker.position);

                var infowindow = new google.maps.InfoWindow({
                    content: '',
                    maxWidth: 350,
                    minWidth: 300
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][3]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));

                that.markers.push(marker);
            }

            this.centerMap(map, center_lat, center_lng);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
            map.setZoom(map.getZoom() - 1);

            if (map.getZoom() > 15) {
                map.setZoom(15);
            }
        },
        reload_map: function (points, lat, lng) {
            var that = this;

            that.clearMarkers();
            points = JSON.parse(points);

            if (points.length > 0) {
                that.setMarkers(that.map, points, lat, lng);
            } else {
                that.centerMap(that.map, lat, lng);
            }
        },
        invoke: function () {
            var that = this;

            this.map = null;
            var mapDefaults = {
                zoom: 15,
                scrollwheel: false,
                center: new google.maps.LatLng(<?php echo $lat ?>, <?php echo $lng ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            $(function () {
                that.map = new google.maps.Map(document.getElementById("map"), mapDefaults);

                var points = <?php echo $json_prods ?>;

                if (points.length > 0) {
                    that.setMarkers(that.map, points, <?php echo $lat ?>, <?php echo $lng ?>);
                }
            });
        }
    };

    $(function () {
        googleMapApp.invoke();

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var checkin = $('#start_date').fdatepicker({
            format: '<?= DATEPICKER_FORMAT ?>',
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date);
                newDate.setDate(newDate.getDate() + 1);
                checkout.update(newDate);
            }
            checkin.hide();
            $('#end_date')[0].focus();
        }).data('datepicker');
        var checkout = $('#end_date').fdatepicker({
            format: '<?= DATEPICKER_FORMAT ?>',
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');

        $("#slider").noUiSlider({
            start: [<?php echo $price_from ?>, <?php echo $price_to ?>],
            connect: true,
            range: {
                'min': <?php echo $price_from ?>,
                'max': <?php echo $price_to ?>
            },
            format: wNumb({
                decimals: 0,
                thousand: '',
                postfix: ''
            })
        });
        $('#slider').Link('lower').to($('#value-span1'), 'html');
        $('#slider').Link('lower').to($('#price_from'));
        $('#slider').Link('upper').to($('#value-span2'), 'html');
        $('#slider').Link('upper').to($('#price_to'));

        $("#slider2").noUiSlider({
            start: [<?php echo $size_from ?>, <?php echo $size_to ?>],
            connect: true,
            range: {
                'min': <?php echo $size_from ?>,
                'max': <?php echo $size_to ?>
            },
            format: wNumb({
                decimals: 0,
                thousand: '',
                postfix: ''
            })
        });

        $('#slider2').Link('lower').to($('#value-span3'), 'html');
        $('#slider2').Link('lower').to($('#size_from'));
        $('#slider2').Link('upper').to($('#value-span4'), 'html');
        $('#slider2').Link('upper').to($('#size_to'));

        $("#search").submit(function (event) {
            event.preventDefault();

            var search_info = $("#search, #more-filters-content :input:not(:hidden)").serialize();

            $.ajax({
                type: "POST",
                url: "/space/ajax_search",
                data: search_info,
                dataType: "json",
                beforeSend: function () {
                    $('#loader').show();
                },
                success: function (data) {
                    console.log(search_info);
                    if (data.error == 1) {
                        $('#errors').hide();
                        $('#errors').html(data.response);
                        $('#errors').show();
                        $('html, body').animate({
                            scrollTop: $("#errors").offset().top
                        }, 1000);
                    } else {
                        if (data.error == 2) {
                            $('#errors').html(data.products);
                            $('#errors').show();
                            $('html, body').animate({
                                scrollTop: $("#errors").offset().top
                            }, 1000);
                        } else {
                            $('#errors').hide();
                            $('#search_results').html(data.products);

                            googleMapApp.reload_map(data.json_prods, data.lat, data.lng);
                        }
                    }
                    $('#loader').hide();
                }
            });
        });

        $("#apply_filters").click(function (e) {
            e.preventDefault();

            if ($("#more-filters-content").is(":hidden")) {
                $("#more-filters-content").slideDown("slow");
                $(this).text('<?= lang('less_filters') ?>');
            } else {
                $("#more-filters-content").hide();
                $(this).text('<?= lang('more_filters') ?>');
            }
        });
    });
</script>