<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo COMPANY_NAME; ?></title>
<style type="text/css">
	table { border-collapse: collapse; }
	table, tr, td, th { border:1px solid #eee;  }
	td, th { padding: 5px;  }
</style>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #000000;">
	<div style="width: 800px;">
		<a href="<?php echo base_url(); ?>" title="">
			<img src="<?php echo base_url(); ?>assets/img/logo.png?2016" alt="<?php echo COMPANY_NAME; ?>" style="margin-bottom: 20px; border: none;" />
		</a>
		<?php echo $html; ?>
		<br/><br/><br/>
		------<br/>
		<?= lang('emails_best_regards') ?><br/>
		<?= lang('emails_team') ?>
	</div>
</body>
</html>