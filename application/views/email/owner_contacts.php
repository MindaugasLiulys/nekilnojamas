<p style="font-size:13px"><?= lang('emails_contacts') ?></p>

<p style="font-size:13px"><b><?= $owner['first_name'] .' '.$owner['last_name'] ?></b></p>	
<p style="font-size:13px"><b><?= $owner['email'] ?></b></p>	
<p style="font-size:13px"><b><?= $owner['phone'] ?></b></p>
<p style="font-size:13px"><b><?= $owner['location'] ?></b></p>

<a href="<?php echo base_url(); ?>inbox/conversation/<?php echo $owner['conversation_id']?>"><?= lang('read_message') ?></a>