<table>
	<tr>
		<th><?= lang('emails_image') ?></th>
		<th><?= lang('emails_comment_from_renter') ?></th>
		<th>Vardas</th>
		<th>Pavardė</th>
		<th>Telefonas</th>
		<th>El-paštas</th>
	</tr>
	<tr>
		<td><a href="<?php echo base_url(); ?>perziureti/<?php echo $info['slug'] . '/'  .$info['space_id']?>"><img width="100" src="<?php echo base_url() . $thumb; ?>" alt="" /></a></td>
		<td><?php echo $comments; ?></td>
		<td><?php echo $name; ?></td>
		<td><?php echo $surname; ?></td>
		<td><?php echo $phone; ?></td>
		<td><?php echo $email; ?></td>
	</tr>
</table><br/>
<p style="font-size:13px"><?= lang('emails_log_in_for_more_info') ?></p>
<a href="<?php echo base_url(); ?>perziureti/<?php echo $info['slug'] . '/'  .$info['space_id']?>"><?= lang('read_message') ?></a>