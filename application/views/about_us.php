<div class="row content">

    <div class="small-12 columns page-title text-center">

        <?= lang('about_us_title') ?>

    </div>

    <div class="small-12 columns entry textpad">

        <?= lang('about_us_text') ?>

    </div>

</div>