<div class="row content">
    
    <div class="small-12 columns page-title text-center">

        <?= lang('information_title') ?>

    </div>

    <div class="small-12 columns entry textpad">
        <div class="helpText">
            <?= lang('information_text') ?>
        </div>
    </div>

    <div class="small-12 columns entry textpad">
        <form action="#" method="post">
            <div class="small-6 columns padding-right-30">
                <div class="text-center">
                    <span style="font-size: 24px">Susisiekime</span>
                </div>
                <div class="form-group">
                    <label for="name">Vardas</label>
                    <input type="text" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="email">El.paštas</label>
                    <input type="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="phone">Tel.numeris</label>
                    <input type="text" class="form-control" id="phone">
                </div>
                <div class="form-group">
                    <label for="message">Žinutė</label>
                    <textarea class="form-control" id="message" rows="5"></textarea>
                </div>
                <div class="form-group">
                   <input type="submit" class="btn btn-green-small" style="padding: 0 10px" value="Siųsti">
                </div>
            </div>
        </form>
        <div class="small-6 columns entry padding-left-30">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7757.352719194672!2d25.275821080359904!3d54.687192399566115!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd9411b3c1cb6b%3A0x7ee6ccf0836bd456!2sLabdari%C5%B3+g.+-+Vilniaus+g.+-+Gedimino+pr.+blokas!5e0!3m2!1slt!2slt!4v1506507052492" width="100%" height="470" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>

</div>