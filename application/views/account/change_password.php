<div class="row content dash">
    <div class="small-12 medium-4 large-3 columns dashboard nopad">
       <?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
    <div class="small-12 medium-8 large-9 columns dashboard-content edit-profile">
        <div class="dashboard-content-title text-center">
            <?= lang('change_password') ?>
        </div>
        <form method="post" action="<?php echo ''; ?>">
            <?php if (isset($validation_errors) && !empty($validation_errors)): ?>
                <div id="errors"><?php echo $validation_errors; ?></div>
            <?php endif ?>
            <?php if (isset($success_message)): ?>
                <div id="success"><?php echo $success_message; ?></div>
            <?php endif ?>
            <div class="small-12 medium-4 large-4 columns text-right form-label small-only-text-left nopad">
                <?= lang('old_password') ?> <span>*</span>
            </div>
            <div class="small-12 medium-8 large-8 columns form-element">
                <input type="password" name="oldpw" placeholder="<?= lang('old_password') ?>">
            </div>
            <div class="small-12 medium-4 large-4 columns text-right form-label small-only-text-left nopad">
                <?= lang('new_password') ?> <span>*</span>
            </div>
            <div class="small-12 medium-8 large-8 columns form-element">
                <input type="password" name="newpw" placeholder="<?= lang('new_password') ?>">
            </div>
            <div class="small-12 medium-4 large-4 columns text-right form-label small-only-text-left nopad">
                <?= lang('confirm_new_pass') ?> <span>*</span>
            </div>
            <div class="small-12 medium-8 large-8 columns form-element">
                <input type="password" name="repeatpw" placeholder="<?= lang('confirm_new_pass') ?>">
            </div>
            <div class="small-12 medium-4 large-4 columns text-right form-label small-only-text-left nopad">
            </div>
            <div class="small-12 medium-8 large-8 columns submit form-element small-only-text-center">
                <span>*</span> <?= lang('required_fields') ?>
                <button class="btn-green" type="submit" name="submit"><?= lang('save') ?></button>
            </div>
        </form>
    </div>
</div>