<div class="row content dash">
    <div class="small-12 medium-4 large-3 columns dashboard nopad">
	   <?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
    <div class="small-12 medium-8 large-9 columns dashboard-content edit-profile">
        <div class="dashboard-content-title text-center">
          	<?= lang('my_profile') ?>
        </div>
        <form method="post" action="<?php echo site_url('users/save') ?>">
            <div id="upload-errors" style="display:none"></div>
        	<?php if (isset($validation_errors) && !empty($validation_errors)): ?>
        		<div id="errors"><?php echo $validation_errors; ?></div>
        	<?php endif ?>
        	<?php if (isset($success_message)): ?>
        		<div id="success"><?php echo $success_message; ?></div>
        	<?php endif ?>
            <div class="row margin-bottom-15 profile">
              <div class="small-12 medium-8 large-12 columns form-element profile-comm">
                  <span id="profile-img-wrap"><img src="<?php echo site_url(get_avatar("", 1)) ?>" / alt="<?php echo htmlentities($info['first_name']) ?>"></span>
              </div>
			  <div class="small-12 medium-8 large-12 columns form-element profile-comm">
                  <span class="btn btn-orange btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span><?= lang('upload_new_photo') ?></span>
                    <input id="profile_photo" type="file" name="profile_photo">
                  </span>
              </div>
            </div>
			<div class="row profile-info">
				<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
					<?= lang('first_name') ?> <span>*</span>
				</div>
				<div class="small-12 medium-8 large-9 columns form-element">
					<input value="<?php echo set_value('first_name', $info['first_name']); ?>" type="text" name="first_name" placeholder="<?= lang('first_name') ?>">
				</div>
				<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
					<?= lang('last_name') ?> <span>*</span>
				</div>
				<div class="small-12 medium-8 large-9 columns form-element">
					<input value="<?php echo set_value('last_name', $info['last_name']); ?>" type="text" name="last_name" placeholder="<?= lang('last_name') ?>">
				</div>
				<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
					<?= lang('i_am') ?> <span>*</span>
				</div>
				<div class="small-12 medium-8 large-9 columns form-element">
					<select name="gender">
						<option value="1" <?= set_select('gender', 1, ($info['gender'] == 1)) ?>><?= lang('male') ?></option>
						<option value="2" <?= set_select('gender', 2, ($info['gender'] == 2)) ?>><?= lang('female') ?></option>
					</select>
				</div>
				<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
					<?= lang('birth_date') ?> <span>*</span>
				</div>
				<div class="small-12 medium-8 large-9 columns form-element">
					<select class="one-third" name="year">
						<option value="" style="display:none;">Year</option>
						<?php
							for ($y=1997; $y > 1910; $y--) {
								echo '<option '.set_select('year', $y, ($info['year'] == $y)).' value="'.$y.'">'.$y.'</option>';
							}
						?>
					</select>
					<select class="one-third" name="month">
						<option value="" selected style="display:none;">Month</option>
						<?php
							for ($m=1; $m <= 12; $m++) {
								echo '<option '.set_select('month', $m, ($info['month']) == $m).' value="'.$m.'">'.$m.'</option>';
							}
						?>
					</select>
					<select class="one-third" name="day">
						<option value="" selected style="display:none;">Day</option>
						<?php
							for ($d=1; $d <= 31; $d++) {
								echo '<option '.set_select('day', $d, ($info['day']) == $d).' value="'.$d.'">'.$d.'</option>';
							}
						?>
					</select>
				</div>
				<?php if($login_type == 0): ?>
					<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
						<?= lang('email') ?> <span>*</span>
					</div>
					<div class="small-12 medium-8 large-9 columns form-element">
						<input value="<?php echo set_value('email', $info['email']); ?>" type="text" name="email" placeholder="<?= lang('email') ?>">
					</div>
				<?php endif ?>
				<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
					<?= lang('phone_number') ?> <span>*</span>
				</div>
				<div class="small-12 medium-8 large-9 columns form-element">
					<input value="<?php echo set_value('phone', $info['phone']); ?>" type="text" name="phone" placeholder="<?= lang('phone_number') ?>">
				</div>
				<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
				<?= lang('location') ?> <span>*</span>
				</div>
				<div class="small-12 medium-8 large-9 columns form-element">
					<input value="<?php echo set_value('location', $info['location']); ?>" type="text" name="location" placeholder="<?= lang('where_you_live') ?>">
				</div>
				<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
					<?= lang('describe_yourself') ?>
				</div>
				<div class="small-12 medium-8 large-9 columns form-element">
					<textarea name="description"><?php echo $info['description'] ?></textarea>
				</div>
				<div class="small-12 medium-4 large-3 columns text-right form-label small-only-text-left nopad">
				</div>
				<div class="small-12 medium-9 large-9 columns form-element submit small-only-text-center">
					<span>*</span> <?= lang('required_fields') ?>
					<button class="btn-green" type="submit" name="submit"><?= lang('save') ?></button>
				</div>
			</div>
        </form>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo base_url(); ?>assets/js/uploader/jquery.iframe-transport.js"></script>
<script src="<?php echo base_url(); ?>assets/js/uploader/jquery.fileupload.js"></script>
<script type="text/javascript">
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url  = '<?php echo site_url("images/upload_profile_img") ?>';

        $('#profile_photo').fileupload({
            url: url,
            dataType: 'json',
            maxNumberOfFiles: 1,
            done: function (e, data) {
                if(data.result.error != 0)
                {
                    $('#upload-errors').html(data.result.error);
                    $('#upload-errors').show();
                } else
                {
                    $.each(data.result.files, function (index, file) {
                        $('#profile-img-wrap').html("<img src='"+file.url+"' />");
                        $('.user img').attr("src", file.url);
                    });
                }
                $('#loader').hide();
            },
            progressall: function (e, data) {
              $('#loader').show();
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
</script>