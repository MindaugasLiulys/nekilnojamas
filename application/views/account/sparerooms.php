<div class="row content dash">
    <div class="small-12 medium-4 large-3 columns dashboard nopad">
        <?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
    <div class="small-12 medium-8 large-9 columns dashboard-content">
        <div class="dashboard-content-title text-center">
            <?= lang('your_sparerooms') ?>
        </div>
        <div class="small-12 columns wishlist nopad">
            <div class="row">
                <?php if (!empty($results)): ?>
                    <?php foreach ($results as $k => $row): ?>
                        <a class="wish-review" target="_blank"
                           href="<?php echo base_url('perziureti/' . $row['slug'] . '/'  . $row['space_id']) ?>">
                            <div class="overlay"></div><span class="wish-review"></span>
                            <div class="small-12 large-6 columns wish-item">
                                <div class="wish-item-block">
                                    <div class="img-holder">
                                        <a class="wish-remove"
                                           href="<?php echo base_url('sparerooms/delete/' . $row['space_id']) ?>"></a>
                                        <img src="<?php echo site_url($row['image']); ?>">
                                    </div>
                                    <div class="wish-item-block-bottom <?= $row['category'] != 0 ? 'rent' : '' ?>">
                                        <div class="columns wish-item-price nopad">
                                            <?php echo format($row['price']); ?><?php if($row['category'] != 0){echo('/'); echo(lang('month'));}?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wish-item-desc">
                                    <span class=""><?php echo utf8_decode($row['description']); ?></span>
                                </div>
                                <div class="smaller-buttons">
                                    <?php if ($row['active'] == 0): ?>
                                        <a class="btn-green"
                                           href="<?php echo site_url('sparerooms/complete/' . $row['space_id']) ?>"><?= lang('list_space') ?></a>
                                    <?php else: ?>
                                        <a class="btn-green"
                                           href="<?php echo site_url('sparerooms/unlist_space/' . $row['space_id']) ?>"><?= lang('unlist_space') ?></a>
                                    <?php endif ?>
                                    <a class="btn-orange"
                                       href="<?php echo site_url('sparerooms/edit/' . $row['space_id']) ?>"><?= lang('edit_space') ?></a>
                                </div>
                            </div>
                        </a>
                    <?php endforeach ?>
                <?php else: ?>
                    <?= lang('no_sparerooms') ?>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>