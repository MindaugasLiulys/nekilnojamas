<div class="row content dash">
    <div class="small-12 medium-4 large-3 columns dashboard nopad">
        <?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
    <div class="small-12 medium-8 large-9 columns dashboard-content">
        <div class="dashboard-content-title text-center">
            <?= lang('my_wishlist') ?> (<?=($this->ajax_wishlist_pagination->create_links() !== '')? $this->ajax_wishlist_pagination->create_links():lang('showing').': 0';?>)
        </div>
        <div class="small-12 columns wishlist nopad">
            <div class="row" id="wishlist">
                <?php if (!empty($results)): ?>
                    <?php foreach ($results as $k => $row): ?>
                        <div class="small-12 medium-6 large-4 columns wish-item item-<?= $row['space_id'] ?>">
                            <div class="wish-item-block">
                                <div class="wish-item-user">
                                    <a href="<?php echo site_url('vartotojas/perziureti/' . $row['user_id']); ?>"><img
                                            src="<?php echo site_url($row['user_avatar']); ?>"></a>
                                </div>
                                <div class="wish-item-fav active">
                                    <a title="<?= lang('remove_from_wishlist') ?>" data-id="<?= $row['space_id'] ?>"
                                       href="#">
                                    </a>
                                </div>
                                <a target="_blank" href="<?php echo base_url('perziureti/' . $row['slug'] . '/' . $row['space_id']) ?>">
                                    <div class="img-holder">
                                        <div class="overlay"></div><span class="wish-review wish-review-wishlist"></span>
                                        <img src="<?php echo site_url($row['image']); ?>">
                                    </div>
                                </a>
                            </div>
                            <a target="_blank" href="<?php echo base_url('perziureti/' . $row['slug'] . '/' . $row['space_id']) ?>">
                                <div class="wish-item-block-bottom <?= $row['category'] != 0 ? 'rent' : '' ?>">
                                    <div class="small-12 columns wish-item-price nopad">
                                        <?php echo format($row['price']); ?><?php if($row['category'] != 0){echo('/'); echo(lang('month'));}?>
                                    </div>
                                </div>
                                <div class="small-12 columns wishlist-description">
                                    <p>
                                        <img src="<?php echo base_url(); ?>assets/img/size.png">
                                        <?php if($row['room_type'] == 3): ?>
                                            <?php echo ' ' . lang('plot') . ' : ' . $row['size'] . ' ' . lang('acre') . number_plur($row['size']); ?>
                                        <?php else: ?>
                                            <?php echo $row['space_type'] . ' ' . $row['size']; ?> m<sup>2</sup>
                                        <?php endif; ?>
                                    </p>
                                    <p>
                                        <img src="<?php echo base_url(); ?>assets/img/slider-date.png">
                                        <?php if($row['category'] != 0): ?>
                                            <?php    echo(lang('free_from')); ?>
                                        <?php else: ?>
                                            <?php    echo(lang('sell_from')); ?>
                                        <?php endif; ?>
                                        : <?php echo date('d/m/Y', strtotime($row['start_date'])); ?>
                                    </p>
                                    <p class="place-address"><img src="<?php echo base_url(); ?>assets/img/icon-search.png">
                                        <?php echo $row['street_address'] ?></p>
                                    <p class="place-rating">
                                        <img src="<?php echo base_url(); ?>assets/img/eye.png"> <?php echo $row['views']; ?>
                                    </p>
                                </div>
                            </a>
                        </div>
                    <?php endforeach ?>
                <?php else: ?>
                    <?= lang('empty_wishlist') ?>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).on('click', '.wish-item-fav a', function() {
        var info = "&id=" + $(this).data('id');
        var itemId = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '/wishlist/ajax_remove_wishlist/',
            data: info,
            dataType: "json",
            success: function (data) {
                $('#pop_up_content').html(data.response);
                $('#for-messages').foundation('reveal', 'open');

                $('.item-'+itemId).remove();
            }
        });
    })

</script>