<div class="row content dash">
    <div class="small-12 medium-4 large-3 columns dashboard nopad">
	   <?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
    <div class="small-12 medium-8 large-9 columns dashboard-content">
    	<form class="with-border" method="post" action="<?php echo site_url('users/update_notifications') ?>" accept-charset="UTF-8">
	        <div class="dashboard-menu-title">
	          	<?= lang('email_notifications') ?>
	        </div>
	        <div class="dashboard-panel">
	        	<div class="row">
			        <?php if (isset($validation_errors) && !empty($validation_errors)): ?>
		        		<div id="errors"><?php echo $validation_errors; ?></div>
		        	<?php endif ?>
		        	<?php if (isset($success_message)): ?>
		        		<div id="success"><?php echo $success_message; ?></div>
		        	<?php endif ?>
		        	<div class="small-12 medium-4 large-4 columns nopad">
		        		<h5><?= lang('notify_me_when') ?>:</h5>
		        		<p><?= lang('disable_any_time') ?></p>
		        	</div>
		        	<div class="small-12 medium-8 large-8 columns form-element">
		        		<?php if(1 == 0): ?>
		        		<div class="small-12 columns pad10">
			        		<div class="small-1 medium-1 large-1 columns text-element">
			        			<input type="checkbox" name="msg_rezervation_status" value="1" 
			        			<?php echo ($settings['msg_rezervation_status'] == 1) ? 'checked="checked"' : '' ?>>
			        		</div>
			        		<div class="small-11 medium-11 large-11 columns text-element">
			        			<label><?= lang('notify_reservation_accepted') ?></label>
			        		</div>
	                	</div>
	                	<?php endif ?>
		        		<div class="small-12 columns pad10">
			        		<div class="small-1 medium-1 large-1 columns text-element">
			        			<input type="checkbox" name="msg_rezervation_request" value="1" 
			        			<?php echo ($settings['msg_rezervation_request'] == 1) ? 'checked="checked"' : '' ?>>
			        		</div>
			        		<div class="small-11 medium-11 large-11 columns text-element">
			        			<label><?= lang('notify_receive_request') ?></label>
			        		</div>
	                	</div>
		        		<div class="small-12 columns pad10">
			        		<div class="small-1 medium-1 large-1 columns text-element">
			        			<input type="checkbox" name="msg_account_changes" value="1" 
			        			<?php echo ($settings['msg_account_changes'] == 1) ? 'checked="checked"' : '' ?>>
			        		</div>
			        		<div class="small-11 medium-11 large-11 columns text-element">
			        			<label><?= lang('notify_changes_my_account') ?></label>
			        		</div>
	                	</div>
		        		<div class="small-12 columns pad10">
			        		<div class="small-1 medium-1 large-1 columns text-element">
			        			<input type="checkbox" name="msg_promotions" value="1" 
			        			<?php echo ($settings['msg_promotions'] == 1) ? 'checked="checked"' : '' ?>>
			        		</div>
			        		<div class="small-11 medium-11 large-11 columns text-element">
			        			<label><?= lang('notify_news_about') ?></label>
			        		</div>
	                	</div>
		        		<div class="small-12 columns pad10">
			        		<div class="small-1 medium-1 large-1 columns text-element">
			        			<input type="checkbox" name="msg_review_reminder" value="1" 
			        			<?php echo ($settings['msg_review_reminder'] == 1) ? 'checked="checked"' : '' ?>>
			        		</div>
			        		<div class="small-11 medium-11 large-11 columns text-element">
			        			<label><?= lang('notify_reminders') ?></label>
			        		</div>
	                	</div>
              		</div>
	        	</div>    	
	        </div>
	        <div class="dashboard-panel-footer">
	        	<button class="btn-green" type="submit" name="submit"><?= lang('save') ?></button>
	        </div>
        </form>
    </div>
</div>