<div class="row content dash">
	<div class="small-12 medium-4 large-4 columns dashboard">
	   	<img class="avatar-img" src="<?php echo site_url(get_avatar($info['avatar'])); ?>" alt=""/>

	   	<div class="dashboard-menu-title">
  			<?= lang('verifications') ?>
		</div>
		<ul id="verification-list">
    		<li><i class="fa fa-check-circle fa-2x"></i><span><?= lang('email_address') ?></span></li>
    		<li><i class="fa fa-check-circle fa-2x <?php echo ($info['phone_verif'] == 1) ? '' : 'red' ?>"></i><span><?= lang('phone_number') ?></span></li>
    	</ul>
    </div>
    <div class="small-12 medium-8 large-8 columns profile-view">
    	<h1><?= lang('hey_i_am') ?> <?php echo ucfirst(htmlentities($info['first_name'])) ?>!</h1>
    	<h2><?= lang('member_since') ?> <?php echo date('F Y', $info['created_on']); ?></h2>
    	<h3><?php echo $info['location'] ?></h3>

    	<h2><?= lang('reviews') ?></h2>
    	<p><?= lang('no_review') ?></p>
    </div>
</div>