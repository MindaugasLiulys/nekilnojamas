<?php if (!empty($results)): ?>
    <?php foreach ($results as $k => $row): ?>
        <div class="small-12 medium-6 large-4 columns wish-item">
            <div class="wish-item-block">
                <div class="wish-item-user">
                    <a href="<?php echo site_url('vartotojas/perziureti/' . $row['user_id']); ?>"><img
                            src="<?php echo site_url($row['user_avatar']); ?>"></a>
                </div>
                <div class="wish-item-fav active">
                    <a title="<?= lang('remove_from_wishlist') ?>"
                       href="<?php echo base_url('wishlist/remove/' . $row['space_id']) ?>"></a>
                </div>
                <a target="_blank" href="<?php echo base_url('perziureti/' . $row['slug'] . '/' . $row['space_id']) ?>">
                    <div class="img-holder">
                        <div class="overlay"></div><span class="wish-review"></span>
                        <img src="<?php echo site_url($row['image']); ?>">
                    </div>
                </a>
            </div>
            <div class="wish-item-block-bottom <?= $row['category'] != 0 ? 'rent' : '' ?>">
                <div class="small-12 columns wish-item-price nopad">
                    <?php echo format($row['price']); ?><?php if($row['category'] != 0){echo('/'); echo(lang('month'));}?>
                </div>
            </div>
            <div class="small-12 columns wishlist-description">
                <p>
                    <img src="<?php echo base_url(); ?>assets/img/size.png">
                    <?php if($row['room_type'] == 3): ?>
                        <?php echo ' ' . lang('plot') . ' : ' . $row['size'] . ' ' . lang('acre') . number_plur($row['size']); ?>
                    <?php else: ?>
                        <?php echo $row['space_type'] . ' ' . $row['size']; ?> m<sup>2</sup>
                    <?php endif; ?>
                </p>
                <p>
                    <img src="<?php echo base_url(); ?>assets/img/slider-date.png">
                    <?php if($row['category'] != 0): ?>
                        <?php    echo(lang('free_from')); ?>
                    <?php else: ?>
                        <?php    echo(lang('sell_from')); ?>
                    <?php endif; ?>
                    : <?php echo date('d/m/Y', strtotime($row['start_date'])); ?>
                </p>
                <p class="place-address"><img src="<?php echo base_url(); ?>assets/img/icon-search.png">
                    <?php echo $row['street_address'] ?></p>
                <p class="place-rating">
                    <img src="<?php echo base_url(); ?>assets/img/eye.png"> <?php echo $row['views']; ?>
<!--                    <span class="rating-stars">-->
<!--                                        --><?php //for ($i = 0; $i < floor($row['rating']['reviews_rate']); $i++): ?>
<!--                                            <i class="icon-star"></i>-->
<!--                                        --><?php //endfor; ?>
<!--                        --><?php //if ($row['rating']['reviews_rate'] != 5): ?>
<!--                            --><?php //echo (is_float($row['rating']['reviews_rate']) && $row['rating']['reviews_rate'] != 0) ?
//                                '<i class="icon-star-half-empty"></i>' : '<i class="icon-star-empty"></i>'; ?>
<!--                            --><?php //for ($i = 0; $i < 4 - floor($row['rating']['reviews_rate']); $i++): ?>
<!--                                <i class="icon-star-empty"></i>-->
<!--                            --><?php //endfor; ?>
<!--                        --><?php //endif ?><!--&nbsp-->
<!--                                        <span-->
<!--                                            class="review-rate">--><?//= sprintf("%.1f", $row['rating']['reviews_rate']) ?><!--</span>-->
<!--                                            </span>-->
                </p>
            </div>
        </div>
    <?php endforeach ?>
<?php else: ?>
    <?= lang('empty_wishlist') ?>
<?php endif ?>
<?php echo $this->ajax_wishlist_pagination->create_links(); ?>
