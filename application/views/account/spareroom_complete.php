<div class="row content dash">
    <div class="small-12 medium-4 large-3 columns dashboard nopad">
	   <?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
    <div class="small-12 medium-8 large-9 columns dashboard-content">
    	<div class="with-border small-text-center">
    		<div class="dashboard-menu-title">
		        <?= lang('complete_listing') ?>
		    </div>
		    <div class="dashboard-panel">
		    	<div class="row">
		    		<img src="<?php echo base_url(); ?>assets/img/logo_small.png" alt="shareboxit"><br/>
		    		<h3><?= lang('ready_to_list') ?></h3>
		    		<p><?= lang('listing_info') ?></p>
		    		<a class="btn-green" href="<?php echo site_url('sparerooms/list_space/'.$space_id) ?>"><?= lang('list_space') ?></a>
		    	</div>
		    </div>
	    </div>
    </div>
</div>