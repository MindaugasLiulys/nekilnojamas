<div class="row content">
  <div class="small-12 columns page-title text-center">
    <?= lang('edit_space') ?>
  </div>
  <div class="small-12 columns list-edit-your-space nopad">
    <?php if (isset($validation_errors) && !empty($validation_errors)): ?>
        <div id="errors"><?php echo $validation_errors; ?></div>
    <?php endif ?>
    <div class="small-12 medium-push-6 medium-6 large-6 list-edit-img-upload columns">
      <div id="img_upload_main" class="small-12 columns img-wrap img-upload-main nopad delete-bg">
          <img src="<?php echo base_url($info['image']); ?>">
          <a class="img-button" href="#" title="<?= lang('delete') ?>" onclick="delete_img('main');return false" style="display: block;"><i class="fa fa-trash-o fa-lg"></i></a>
      </div>
      <div class="small-12 columns img-upload-thumbs">
        <ul id="img_upload_thumbs">
            <?php if(!empty($info['images'])): ?>
                <?php foreach ($info['images'] as $k => $image): ?>
                    <li id="img_<?= $k ?>" class="img-wrap">
                        <img  style="width:90px;height:90px" src="<?php echo base_url($image); ?>">
                        <a style="display:block" class='img-button' href='#' title='<?= lang('delete') ?>' onclick="delete_img('<?= $k ?>');return false"><i class='fa fa-trash-o fa-lg'></i></a>
                    </li>
                <?php endforeach ?>
            <?php else: ?>
                <li class="empty_thumb"><img src="<?php echo base_url(); ?>assets/img/img-place-thumb.png"></li>
                <li class="empty_thumb"><img src="<?php echo base_url(); ?>assets/img/img-place-thumb.png"></li>
                <li class="empty_thumb"><img src="<?php echo base_url(); ?>assets/img/img-place-thumb.png"></li>
                <li class="empty_thumb"><img src="<?php echo base_url(); ?>assets/img/img-place-thumb.png"></li>
            <?php endif ?>
        </ul>
      </div>
      <div class="small-12 columns nopad">
        <span class="btn btn-orange btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span><?= lang('upload_main_file') ?></span>
            <input id="main_photo" type="file" name="main_image">
        </span>
        <span class="btn btn-orange btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span><?= lang('upload_files') ?></span>
            <input id="files" type="file" name="files[]" multiple>
        </span>
      </div>

    </div>
    <div class="small-12 medium-pull-6 medium-6 large-6 columns">
        <form action="#" method="post" id="save_space">
            <input type="hidden" name="space_id" value="<?= $info['space_id'] ?>" />
            <input type="hidden" name="location-slug" value="<?=get_address($info['lat'], $info['lng'])?>">
            <div style="display:none" id="errors"></div><div style="display:none" id="success"></div>
            <div class="small-12 columns nopad">
              <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                <?= lang('description') ?> <span>*</span>
              </div>
              <div class="small-12 medium-9 large-9 columns form-element">
                <textarea name="description"><?= htmlentities($info['description']) ?></textarea>
              </div>
            </div>
            <div class="small-12 columns nopad">
                <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                    <?= lang('price') ?> (<?= $this->session->userdata('currency'); ?>/<?= lang('month') ?>) <span>*</span>
                </div>
                <div class="small-12 medium-9 large-9 columns form-element">
                    <div class="small-12 medium-4 large-4 columns form-element padding-left-none">
                        <input id="price" type="text" value="<?= $info['price'] ?>" name="price" placeholder="e.g. 200">
                    </div>
                    <div class="small-12 medium-8 large-8 columns form-element">
                        <div class="small-12 medium-12 large-12 columns form-label small-only-text-left nopad">
                            <?= lang('price_with_comm') ?>: <span id="price-with-comm"><?= format($info['real_price']) ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="small-12 columns nopad">
                <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                    <?= lang('category') ?>
                </div>
                <div class="small-12 medium-9 large-9 columns form-element">
                    <select class="" name="category">
                        <option <?= ($info['category'] == 0) ? 'selected="selected"' : '' ?> value="0"><?= lang('category-sell') ?></option>
                        <option <?= ($info['category'] == 1) ? 'selected="selected"' : '' ?> value="1"><?= lang('category-rent') ?></option>
                    </select>
                </div>
            </div>
            <div class="small-12 columns nopad">
              <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                <?= lang('available') ?> <span>*</span>
              </div>
              <div class="small-12 medium-9 large-9 columns form-element">
                <input class="half" type="text" data-date-format="<?= DATEPICKER_FORMAT ?>" name="start_date" placeholder="<?= lang('start_date') ?>" id="start_date" value="<?= date(DATE_FORMAT, strtotime($info['start_date'])) ?>">
              </div>
            </div>
            <div class="small-12 columns nopad">
              <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                <?= lang('size') ?> (<span id="size">m<sup>2</sup></span>) <span>*</span>
              </div>
              <div class="small-12 medium-9 large-9 columns form-element">
                <input class="half" type="text" name="size" value="<?= $info['size'] ?>" placeholder="e.g. 50">
              </div>
            </div>
            <div class="small-12 columns nopad">
              <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                <?= lang('room_type') ?>
              </div>
              <div class="small-12 medium-9 large-9 columns form-element">
                <select id="type" class="" name="room_type">
                  <option <?= ($info['room_type'] == 1) ? 'selected="selected"' : '' ?> value="1"><?= lang('apartment') ?></option>
                  <option <?= ($info['room_type'] == 2) ? 'selected="selected"' : '' ?> value="2"><?= lang('house') ?></option>
                  <option <?= ($info['room_type'] == 3) ? 'selected="selected"' : '' ?> value="3"><?= lang('land') ?></option>
                  <option <?= ($info['room_type'] == 4) ? 'selected="selected"' : '' ?> value="4"><?= lang('commercial') ?></option>
                </select> 
              </div>
            </div>
            <div id="room-number" class="small-12 columns nopad">
                <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                    <?= lang('number_of_rooms') ?>
                </div>
                <div class="small-12 medium-9 large-9 columns form-element">
                    <input type="text" name="room_number" value="<?=$info['room_number']?>">
                </div>
            </div>
            <div class="small-12 columns nopad">
              <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                <?= lang('facilities') ?>
              </div>
              <div class="small-12 medium-9 large-9 columns form-element">
                  <?php
                  foreach($facilities as $k => $v){ ?>
                      <label>
                          <input <?= (in_array($v['facility_id'], $info['facilities'])) ? 'checked="checked"' : '' ?> type="checkbox" name="facilities[]" value="<?=$v['facility_id'] ?>"><?= $v['name'] ?>
                      </label>
                  <?php }?>
              </div>
            </div>
            <div class="small-12 columns nopad">
              <div class="small-12 medium-3 large-3 columns text-right form-label small-only-text-left nopad">
                  <span>*</span> <?= lang('required_fields') ?>
              </div>
              <div class="small-12 medium-9 large-9 columns form-element submit small-only-text-center">
                  <button class="btn-green" type="submit" name="submit"><?= lang('save_your_space') ?></button>
              </div>
            </div>
        </form>
    </div>
  </div>
</div>
<div id="loader"><img src="<?php echo base_url(); ?>assets/img/spinner.gif" alt="loader"/></div>
<script src="<?php echo base_url(); ?>assets/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo base_url(); ?>assets/js/uploader/jquery.iframe-transport.js"></script>
<script src="<?php echo base_url(); ?>assets/js/uploader/jquery.fileupload.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&amp;libraries=places"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.geocomplete.min.js"></script>
<script type="text/javascript">

function delete_img(img) {
    $.ajax({
        type: "POST",
        url: "/images/delete/<?= $info['space_id'] ?>?",
        data: 'image=' + img,
        dataType: "json",
        success: function(data) {
            if(data.error != 1)
            {
                if(img == "main")
                {
                    $('#img_upload_main').html("<img src='"+data.response+"' />");
                } else {
                    $('#img_'+img).remove();
                }
            }

            // $('#pop_up_content').html(data.response);
            // $('#for-messages').foundation('reveal', 'open');
        }
    });
}

if($('#type option:selected').val()){
    $('#room-number').hide();
    $('#size').html('arais')
}

$(function () {
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#start_date').fdatepicker({
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            checkout.update(newDate);
        }
        checkin.hide();
        $('#end_date')[0].focus();
    }).data('datepicker');
    var checkout = $('#end_date').fdatepicker({
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');

    $("#place").geocomplete({ details: "form" }); 

    $( "#save_space" ).submit(function( event ) {
        event.preventDefault();

        var space_info = $("#save_space").serialize();

        $.ajax({
            type: "POST",
            url: "/sparerooms/save",
            data: space_info,
            dataType: "json",
            success: function(data) {
                if(data.error == 1)
                {
                    $('#success').hide();
                    $('#errors').html(data.response);
                    $('#errors').show();
                    $('html, body').animate({
                        scrollTop: $("#errors").offset().top
                    }, 1000);
                } else {
                    $('#errors').hide();
                    $('#success').html(data.response);
                    $('#success').show();
                    $('html, body').animate({
                        scrollTop: $("#success").offset().top
                    }, 1000);
                }
            }
        });
    });


    'use strict';
    // Change this to the location of your server-side upload handler:
    var main_url  = '<?php echo site_url("images/upload_main_img/".$info['space_id']) ?>';
    var other_url = '<?php echo site_url("images/upload_images/".$info['space_id']) ?>';

    $('#main_photo').fileupload({
        url: main_url,
        dataType: 'json',
        maxNumberOfFiles: 1,
        done: function (e, data) {
            if(data.result.error != 0)
            {
                $('#errors').html(data.result.error);
                $('#errors').show();
                $('html, body').animate({
                    scrollTop: $("#errors").offset().top
                }, 1000);
            } else
            {
                $.each(data.result.files, function (index, file) {
                    $('#img_upload_main').html("<img src='"+file.url+"' /><a style='display:block' class='img-button' href='#' title='Delete' onclick=\"delete_img('main');return false\"><i class='fa fa-trash-o fa-lg'></i></a>");
                });
            }
            $('#loader').hide();
        },
        progressall: function (e, data) {
            $('#loader').show();
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('#files').fileupload({
        url: other_url,
        dataType: 'json',
        maxNumberOfFiles: 1,
        done: function (e, data) {
            if(data.result.error != 0)
            {
                $('#errors').html(data.result.error);
                $('#errors').show();
                $('html, body').animate({
                    scrollTop: $("#errors").offset().top
                }, 1000);
            } else
            {
                $( ".empty_thumb" ).remove();
                $.each(data.result.files, function (index, file) {
                    $('<li id="img_'+file.ident+'" class="img-wrap"/>').html("<img style='width:90px;height:90px' src='"+file.url+"' /><a style='display:block' class='img-button' href='#' title='<?= lang('delete') ?>' onclick=\"delete_img('"+file.ident+"');return false\"><i class='fa fa-trash-o fa-lg'></i></a>").appendTo('#img_upload_thumbs');
                });
            }
            $('#loader').hide();
        },
        progressall: function (e, data) {
            $('#loader').show();
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $( "#price" ).keyup(function( event ) {

        $.ajax({
            type: "POST",
            url: "/space/get_commission_price",
            data: "&price=" + $('#price').val(),
            dataType: "json",
            success: function(data) {
                if(data.error = '0')
                {
                    $('#price-with-comm').html(data.response);
                }
            }
        });
    });

});  
</script>