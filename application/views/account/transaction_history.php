<div class="row content dash">
    <div class="small-12 medium-3 large-3 columns dashboard transaction nopad">
       <?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
      <div class="small-12 medium-9 large-9 columns dashboard-content pl45">
        
        <div class="transactions-tabs">
          <ul class="tabs" data-tab>
            <li class="tab-title active"><a href="#panela"><?= lang('completed_transactions') ?></a></li>
            <li class="tab-title"><a href="#panelb"><?= lang('future_transactions') ?></a></li>
          </ul>
          <div class="tabs-content">
            <div class="content active" id="panela">
                <p><?= lang('no_transactions') ?></p>
            </div>
            <div class="content" id="panelb">
                <?php if (!empty($future_transactions)): ?>
                    <table style="width:100%">
                        <tr>
                            <th><?= lang('dates') ?>
                            <th>Amount</th>
                        </tr>
                        <?php foreach ($future_transactions as $k => $row): ?>
                            <tr>
                                <td><?= $row['date_added'] ?></td>
                                <td><?= format($row['amount']) ?></td>
                            </tr>                              
                        <?php endforeach ?>
                    </table> 
                <?php else: ?>
                    <p><?= lang('no_transactions') ?></p>
                <?php endif ?>
            </div>
          </div>
        </div>

        <ul class="accordion transactions-accordion" data-accordion>
          <li class="accordion-navigation">
            <a href="#panel1a"><?= lang('completed_transactions') ?></a>
            <div id="panel1a" class="content active">
                <p><?= lang('no_transactions') ?></p>
            </div>
          </li>
          <li class="accordion-navigation">
            <a href="#panel2a"><?= lang('future_transactions') ?></a>
            <div id="panel2a" class="content">
                <?php if (!empty($future_transactions)): ?>
                    <?php foreach ($future_transactions as $k => $row): ?>
                        <div class="single-transaction">
                            <ul>
                                <li>
                                    <div class="single-transaction-label"><?= lang('dates') ?></div>
                                    <span><?= $row['date_added'] ?></span>
                                </li>
                                <li>
                                    <div class="single-transaction-label">Amount</div>
                                    <span><?= format($row['amount']) ?></span>
                                </li>
                            </ul>
                        </div>                         
                    <?php endforeach ?>
                <?php else: ?>
                    <p><?= lang('no_transactions') ?></p>
                <?php endif ?>
            </div>
          </li>
        </ul>
      </div>
    </div>
</div>