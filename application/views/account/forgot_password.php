<div class="row content dash">
    <div class="small-12 medium-12 large-12 columns dashboard-content edit-profile">
        <div class="dashboard-content-title text-center">
            <?= lang('change_password') ?>
        </div>
        <form method="post" action="<?php echo site_url('auth/reset_password/'.$code); ?>">
            <?php if (isset($message) && !empty($message)): ?>
                <div id="errors"><?php echo $message; ?></div>
            <?php endif ?>
            <?php if (isset($success_message)): ?>
                <div id="success"><?php echo $success_message; ?></div>
            <?php endif ?>
            <div class="small-12 medium-4 large-4 columns text-right form-label small-only-text-left nopad">
                <?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?> <span>*</span>
            </div>
            <div class="small-12 medium-8 large-8 columns form-element">
                <input type="password" name="new" placeholder="<?= lang('new_password') ?>">
            </div>
            <div class="small-12 medium-4 large-4 columns text-right form-label small-only-text-left nopad">
                <?= lang('confirm_new_pass') ?> <span>*</span>
            </div>
            <div class="small-12 medium-8 large-8 columns form-element">
                <input type="password" name="new_confirm" placeholder="<?= lang('confirm_new_pass') ?>">
            </div>
            <div class="small-12 medium-4 large-4 columns text-right form-label small-only-text-left nopad">
            </div>
            <div class="small-12 medium-8 large-8 columns submit form-element small-only-text-center">
                <span>*</span> <?= lang('required_fields') ?>
                <button class="btn-green" type="submit" name="submit"><?= lang('save') ?></button>
            </div>
            <?php echo form_input($user_id);?>
            <?php echo form_hidden($csrf); ?>
        </form>
    </div>
</div>