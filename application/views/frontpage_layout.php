<div class="small-12 columns front-slider nopad">
    <div class="text-center">
        <p class="slider-text-main"><?= lang('sharing_is_caring') ?></p>
        <p class="slider-title"><?= sprintf(lang('slide_title'), '<span class="orange">', '</span>') ?></p>
    </div>
    <?php if ($this->session->flashdata('success')): ?>
        <div id="front-success" class="front_page_errors success">
            <?= $this->session->flashdata('success') ?>
            <a class="close_btn" onclick="$('#front-success').hide();"><i class="fa fa-times"></i></a>
        </div>
    <?php endif ?>
    <?php if ($this->session->flashdata('message')): ?>
        <div id="front-error" class="front_page_errors">
            <?= $this->session->flashdata('message') ?>
            <a class="close_btn" onclick="$('#front-error').hide();"><i class="fa fa-times"></i></a>
        </div>
    <?php endif ?>
    <div class="front-search row">
        <div class="small-12 columns front-search-block text-center">
            <form class="slider-form" action="<?php echo base_url('skelbimai') ?>" method="get">
                <input type="hidden" name="lat" value="">
                <input type="hidden" name="lng" value="">
                <input type="hidden" name="formatted_address" value="">
                <div class="small-12 medium-10 large-8 columns nopad s-place">
                    <input class="place-search" type="text" name="" id="place"
                           placeholder="<?= lang('place_example') ?>">
                </div>
                <div class="small-12 medium-7 large-2 columns nopad s-price form-element">
                    <span id="slider-price-tag"><img
                            src="<?php echo base_url(); ?>assets/img/slider-price-tag.svg"></span>
                    <select name="price_range" class="price-tag">
                        <option selected><?= lang('price_range') ?></option>
                        <option value="1-999999"><?= lang('all') ?></option>
                        <option value="1-100">1-100</option>
                        <option value="101-200">101-200</option>
                        <option value="201-400">201-400</option>
                        <option value="401-600">401-600</option>
                        <option value="601-1000">601-1000</option>
                        <option value="1001-2000">1001-2000</option>
                    </select>
                </div>
                <div class="small-12 medium-5 large-2 columns nopad text-right small-only-text-center">
                    <button class="btn-slider-search s-submit" type="submit" name=""><?= lang('search') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="clear"></div>

<div class="row shareboxit-information">
    <div class="small-12 columns text-center" id="shareboxit-information">
        <div class="small-12 columns hide-for-small-only">
            <img class="path-image" src="<?php echo base_url(); ?>assets/img/path.png">
        </div>
        <div class="small-12 medium-4 columns">
            <img src="<?php echo base_url(); ?>assets/img/index/credit-card.svg">
            <p class="information-title"><?= lang('transactions') ?></p>
            <p><?= lang('trust_and_secure') ?></p>
        </div>
        <div class="small-12 medium-4 columns">
            <img src="<?php echo base_url(); ?>assets/img/index/piggy-bank.svg">
            <p class="information-title"><?= lang('save_money') ?></p>
            <p><?= lang('save_money_text') ?></p>
        </div>
        <div class="small-12 medium-4 columns">
            <img style="width: 150px" src="<?php echo base_url(); ?>assets/img/1-Variety-of-unit-sizes@2x.png">
            <p class="information-title"><?= lang('variety_unit_sizes') ?></p>
            <p><?= lang('variety_unit_sizes_small_text') ?></p>
        </div>
    </div>
</div>
<div class="row new-places-for-rent">
    <div class="small-12 columns page-title text-center">
        <?= lang('latest_sparerooms') ?>
    </div>
    <div class="small-12 columns wishlist nopad">
        <?php foreach ($featured as $key => $line): ?>
            <div class="row">
                <?php foreach ($line as $k => $row): ?>
                <a class="roomspace" href="<?php echo base_url('perziureti/' . $row['slug'] . '/' . $row['space_id']) ?>">
                    <div class="small-12 medium-6 large-3 columns wish-item">
                        <div class="wish-item-block rev">
                            <div class="img-holder">
                                    <div class="overlay"></div><span class="wish-review"></span>
                                    <img src="<?php echo site_url($row['image']); ?>">
                            </div>
                            <div class="wish-item-block-bottom <?= $row['category'] != 0 ? 'rent' : '' ?>">
                                <div class="small-12 columns wish-item-price nopad">
                                    <?php echo format($row['price']); ?><?php if($row['category'] != 0){echo('/'); echo(lang('month'));}?>
                                </div>
                            </div>
                            <div class="small-12 columns wishlist-description">
                                <p>
                                    <img src="<?php echo base_url(); ?>assets/img/size.png">
                                    <?php if($row['room_type'] == 3): ?>
                                        <?php echo ' ' . lang('plot') . ' : ' . $row['size'] . ' ' . lang('acre') . number_plur($row['size']); ?>
                                    <?php else: ?>
                                        <?php echo $row['space_type'] . ' ' . $row['size']; ?> m<sup>2</sup>
                                    <?php endif; ?>
                                </p>
                                <p>
                                    <img src="<?php echo base_url(); ?>assets/img/slider-date.png">
                                    <?php if($row['category'] != 0): ?>
                                    <?php    echo(lang('free_from')); ?>
                                    <?php else: ?>
                                    <?php    echo(lang('sell_from')); ?>
                                    <?php endif; ?>
                                    : <?php echo date('d/m/Y', strtotime($row['start_date'])); ?>
                                </p>
                                <p class="place-address"><img src="<?php echo base_url(); ?>assets/img/icon-search.png">
                                    <?php echo $row['street_address'] ?></p>
                                <p class="place-rating">
                                    <img src="<?php echo base_url(); ?>assets/img/eye.png"> <?php echo number_format($row['views'], 0, ",", " "); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
                <?php endforeach ?>
            </div>
        <?php endforeach ?>
    </div>
    <div class="row save-money-information">
        <div class="small-12 columns page-title text-center">
            <?= lang('save_money_share_storage') ?>
        </div>
        <div class="small-12 columns text-left">
            <div class="small-12 medium-4 columns save-money-information-img">
                <div class="small-3 medium-4 columns nopad "><img
                        src="<?php echo base_url(); ?>assets/img/index/buy.svg"></div>
                <div class="small-9 medium-8 columns nopadright save-money-information-text"><p><?= lang('earn_money') ?>
                        <br><?= lang('share_your_space') ?></p></div>
            </div>
            <div class="small-12 medium-4 columns save-money-information-img">
                <div class="small-3 medium-4 columns nopad"><img
                        src="<?php echo base_url(); ?>assets/img/index/invoice.svg"></div>
                <div class="small-9 medium-8 columns nopadright save-money-information-text"><p><?= lang('get_instant_price_calculation') ?></p>
                </div>
            </div>
            <div class="small-12 medium-4 columns save-money-information-img">
                <div class="small-3 medium-4 columns nopad"><img
                        src="<?php echo base_url(); ?>assets/img/index/shield.svg"></div>
                <div class="small-9 medium-8 columns nopadright save-money-information-text"><p><?= lang('confidence_and_secure_handling') ?></p></div>
            </div>
        </div>
    </div>
</div>
<div class="small-12 view-all-storages columns text-center">
    <a class="btn-orange" href="<?php echo base_url('informacija') ?>"><?= lang('read_more') ?></a>
</div>
<div class="small-12 columns earn-money-hero">
    <div class="row">
        <div class="small-12 medium-8 large-6">
            <p class="earn-money"><?= lang('earn_money_by_sharing') ?></p>
            <?php if (logged_in()): ?>
                <a class="btn-orange"
                   href="<?php echo base_url('naujas-skelbimas') ?>"><?= lang('list_your_space') ?></a>
            <?php else: ?>
                <a class="btn-orange" href="<?php echo base_url('ajax/get/sign_up') ?>"
                   data-reveal-id="sign-up"
                   data-reveal-ajax="true"><?= lang('list_your_space') ?></a>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="row available-now-places">
    <div class="small-12 columns page-title text-center">
        <?= lang('available_now') ?>
    </div>
    <div class="small-12 columns wishlist nopad">
        <div class="row">
            <?php foreach ($available as $key => $line): ?>
                <?php foreach ($line as $k => $row): ?>
                 <a class="" href="<?php echo base_url('perziureti/' . $row['slug'] . '/'  . $row['space_id']) ?>">
                    <div class="small-12 medium-6 large-4 columns wish-item">
                        <div class="wish-item-block rev">
                            <div class="img-holder">
                                    <div class="overlay"></div><span class="wish-review"></span>
                                    <img src="<?php echo site_url($row['image']); ?>">
                            </div>
                            <div class="wish-item-block-bottom <?= $row['category'] != 0 ? 'rent' : '' ?>">
                                <div class="small-12 columns wish-item-price nopad">
                                    <?php echo format($row['price']); ?><?php if($row['category'] != 0){echo('/'); echo(lang('month'));}?>
                                </div>
                            </div>
                            <div class="small-12 columns available-now-description">
                                <p>
                                    <img src="<?php echo base_url(); ?>assets/img/size.png">
                                    <?php if($row['room_type'] == 3): ?>
                                        <?php echo ' ' . lang('plot') . ' : ' . $row['size'] . ' ' . lang('acre') . number_plur($row['size']); ?>
                                    <?php else: ?>
                                        <?php echo $row['space_type'] . ' ' . $row['size']; ?> m<sup>2</sup>
                                    <?php endif; ?>
                                </p>
                                <p class="place-address">
                                    <img src="<?php echo base_url(); ?>assets/img/icon-search.png"> <?php echo $row['street_address'] ?>
                                </p>
                                <p class="place-rating">
                                    <img src="<?php echo base_url(); ?>assets/img/eye.png"> <?php echo number_format($row['views'], 0, ",", " ") ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                 </a>
                <?php endforeach ?>
            <?php endforeach ?>
        </div>
    </div>
    <div class="small-12 columns view-all-available-paces text-center">
        <a class="btn-orange" href="<?php echo base_url('skelbimai') ?>"><?= lang('view_all') ?></a>
    </div>
</div>
<div class="small-12 about-shareboxit">
    <div class="row">
        <div class="small-12 large-5 columns">
            <p class="about-shareboxit-title"><?= lang('about_shareboxit') ?></p>
            <p class="about-shareboxit-text"><?= lang('about_shareboxit_text') ?></p>
            <p class="more-about-us"><a href="<?php echo base_url('apie-mus') ?>"><?= lang('more_about_us') ?></a> </p>
        </div>
        <div class="small-12 large-7 columns">
            <img src="<?php echo base_url(); ?>assets/img/boxes.png"/>
        </div>
    </div>
</div>
<?php if (isset($_SESSION['first_time'])): ?>
    <div id="first_time_instruction">
        <h2><?= lang('welcome') ?></h2><br/>
        <div class="small-12 medium-8 large-4 columns">
            <i class="fa fa-search-plus fa-5x"></i><br/>
            <?= lang('find_a_place') ?>
        </div>
        <div class="small-12 medium-8 large-4 columns">
            <i class="fa fa-book fa-5x"></i><br/>
            <?= lang('book_a_spareroom') ?>
        </div>
        <div class="small-12 medium-8 large-4 columns">
            <i class="fa fa-thumbs-o-up fa-5x"></i><br/>
            <?= lang('keep_safe') ?>
        </div>
        <br/><br/>
        <p style="margin-top:80px;clear:both">Prašome <a target="_blank" style="color:#000;font-weight:bold"
                                                        href="<?php echo base_url('informacija') ?>"><?= lang('click_here') ?></a> <?= lang('how_use') ?>
        </p>
        <a class="close_btn" onclick="$('#first_time_instruction').hide();"><i class="fa fa-times"></i></a>
    </div>
    <?php unset($_SESSION['first_time']); ?>
<?php endif ?>
<script type="text/javascript">
    $(function () {
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        var checkin = $('#start_date').fdatepicker({
            format: '<?= DATEPICKER_FORMAT ?>',
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.update(newDate);
            }
            checkin.hide();
            $('#end_date')[0].focus();
        }).data('datepicker');
        var checkout = $('#end_date').fdatepicker({
            format: '<?= DATEPICKER_FORMAT ?>',
            onRender: function (date) {
                return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            checkout.hide();
        }).data('datepicker');
    });
</script>
<?php if (isset($_SESSION['show_login'])): ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#h_login_btn").trigger("click");
        });
    </script>
    <?php unset($_SESSION['show_login']); ?>
<?php endif ?>