<div class="row content">
    <div class="small-12 columns page-title text-center">
        Sådan bruger du Shareboxit
    </div>
    <div class="small-12 columns entry textpad">
        <div class="help-item">
            <a href="#" onclick="show_hide('reservation-price');return false;">
                <img class="helpIcons" src="<?php echo base_url(); ?>assets/img/coins.svg">
                Hvordan fastsættes prisen for min reservation?
            </a>
        </div>
        <div class="display-none" id="reservation-price">
            Den samlede pris for en reservation på shareboxit er baseret på et par faktorer. Bemærk venligst, at hele beløbet opkræves, når en vært accepterer din reservationsanmodning (eller hvis du bruger Øjeblikkelig booking).<br/>
            Priser som værten bestemmer:<br/>

            Pris pr. dag/uge/måned: Prisen per nat bestemmes af udlejeren.<br/>

            Rengøringsgebyr: Engangsgebyr som nogle værter opkræver for at dække udgifter til rengøring af deres bolig.<br/>


            Priser som Shareboxit bestemmer:<br/>

            Shareboxit´s servicegebyr: Servicegebyr der opkræves for alle reservationer for at hjælpe Shareboxit med at køre problemfrit samt at tilbyde kundesupport.
        </div>

        <div class="help-item">
            <a href="#" onclick="show_hide('payment-process');return false;">
                <img class="helpIcons" src="<?php echo base_url(); ?>assets/img/credit-card.svg">
                Hvordan fungerer Shareboxit´s betalingssystem?
            </a>
        </div>
        <div class="display-none" id="payment-process">
            Shareboxit´s betalingssystem er designet til at gøre overførsel af penge mellem gæster og værter så simpelt og sikkert som muligt. Det virker på denne måde:<br/>
            Gæster betaler Shareboxit, når de booker et opbevaringsrum.<br/>
            Shareboxit overfører pengene til værten, 24 timer efter lejeren er tjekket ind.<br/>
            Det er påkrævet at bruge Shareboxit´s betalingssystem, når man laver en reservation og det sikrer, at begge parter er beskyttede under vores servicevilkår, aflysningspolitikker, gæsterefusionspolitik og andre sikkerhedsforanstaltninger.<br/>
            Shareboxit inkluderer automatisk et servicegebyr i hver transaktion, og regeringsskatter, hvor det er påkrævet.
        </div>

        <div class="help-item">
            <a href="#" onclick="show_hide('when-pay');return false;">
                <img class="helpIcons" src="<?php echo base_url(); ?>assets/img/hotel.svg">
                Hvornår betaler jeg for en reservation?
            </a>
        </div>
        <div class="display-none" id="when-pay">
            Dine betalingsoplysninger indhentes, når du indsender en reservationsanmodning. Når din udlejer accepterer din anmodning, eller hvis du booker en reservation med Øjeblikkelig booking, vil din betalingsmetode blive opkrævet det samlede beløb på det tidspunkt.<br/>

            Uanset om reservationen begynder om to dage eller to måneder, tilbageholder vi betalingen indtil 24 timer efter indtjekning, før vi frigiver den til værten. Dette giver begge parter tid til at sikre, at alt er, som det skal være.
        </div>

        <div class="help-item">
            <a href="#" onclick="show_hide('identification');return false;">
                <img class="helpIcons" src="<?php echo base_url(); ?>assets/img/key.svg">
                Hvad er Verificeret identifikation?
            </a>
        </div>
        <div class="display-none" id="identification">
            Vi har oprettet Verificeret identifikation for at bygge tillid i vores netværk og for at hjælpe med at give dig flere oplysninger, når du beslutter, hvem du vil være vært for, eller hvem du vil bo hos.<br/>

            Sådan fungerer Verificeret identifikation<br/>

            Verificeret identifikation forbinder din Shareboxit-profil med andre oplysninger om dig. Som en del af Verificeret kan du blive bedt om at:<br/>

            Inden reservering af opbevaringsrum bliver du bedt om en sms verificering.
            Forbind en anden online profil til din Shareboxit-konto, f.eks. en Facebook og Google+
            Uploade et Shareboxit profilbillede og oplyse et telefonnummer og en e-mailadresse.<br/>

            Hvornår skal du gennemføre Verificeret identifikation<br/>

            Under visse omstændigheder kan du blive bedt om at gennemføre Verificeret identifikation. For eksempel kan værter kræve, at gæster gennemfører Verificeret identifikation, inden de booker deres bolig.<br/>

            Hvis din vært kræver, at du gennemfører Verificeret identifikation, sendes du videre for at gennemføre processen lige efter, du har sendt din reservationsanmodning, og du har 12 timer til at gennemføre den.<br/>

            Privatliv og kryptering:<br/>

            Vi tager privatliv alvorligt. De oplysninger, du giver under denne proces er krypteret og underlagt vores Fortrolighedspolitik.<br/>

            Vi deler ikke den officielle id eller de personlige oplysninger, du giver os under Verificeret identifikation, med din vært eller gæst—de ved kun, at du har gennemført processen. Og vi slår aldrig noget op på din Facebook, Google, eller andre tilknyttede konti uden din tilladelse.
        </div>

        <div class="help-item">
            <a href="#" onclick="show_hide('payment-methods');return false;">
                <img class="helpIcons" src="<?php echo base_url(); ?>assets/img/pay-per-click.svg">
                Hvilke betalingsmetoder accepterer Shareboxit?
            </a>
        </div>
        <div class="display-none" id="payment-methods">
            Vi understøtter flere udbetalingsmetoder, som afhænger af, i hvilket land din udbetalingskonto ligger.<br/>

            Du kan se, hvilke udbetalingsmetoder du kan benytte på bestillingssiden, inden du indsender en reservationsanmodning. Når du har valgt dit land, kan du se alle dine betalingsoplysninger.<br/>

            Offline betalinger eller betalinger i kontanter er en overtrædelse af vores Servicevilkår og kan føre til fjernelse fra Shareboxit. Vi tillader ikke betalinger uden for hjemmesiden, da det gør det sværere for at at beskytte dine oplysninger og øger risikoen for, at du udsættes for svindel og andre sikkerhedsproblemer.<br/>

            Betalingsmulighederne kan omfatte:<br/>

            Større kreditkort og forudbetalte kreditkort (Visa, MasterCard, Amex, Discover, JCB)<br/>
            Mange debetkort, der kan behandles som kredit<br/>
            PayPal for udvalgte lande<br/>
            Alipay udelukkende for Kina<br/>
            Postepay udelukkende for Italien<br/>
            Sofort Überweisung udelukkende for Tyskland<br/>
            iDEAL udelukkende for Holland<br/>
            Boleto Bancário, Hipercard, Elo og Aura udelukkende for Brasilien<br/>
            Google Wallet udelukkende for Android-appen i USA<br/>
            Apple Pay udelukkende for iOS-appen
        </div>
    </div>
</div>