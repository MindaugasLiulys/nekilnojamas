<div class="row content dash">
    <div class="small-12 medium-4 large-3 columns dashboard nopad">
        <?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
    <div class="small-12 medium-8 large-9 columns dashboard-content">
        <div id="new-msg-place">
            <?php if (!empty($message)): ?>
                <?php foreach ($message as $k => $row): ?>
                    <div class="row clearfix chat">
                            <div class="small-2 medium-2 large-2 columns">
                                <img src="<?php echo site_url('/images/avatars/default.jpg?170925095128?170925095128')?>" alt="" />
                            </div>
                            <div class="small-10 medium-10 large-10 columns sms">
                                <?php echo $row->name . ' ' . $row->surname?><br>
                                Telefonas: <?=$row->phone ?><br>
                                El-paštas: <?=$row->email ?><br>
                                Žinutė: <?=$row->message ?><br>
                                Vieta: <a href="<?php echo base_url('perziureti/' . $space_slug . '/' . $spaceId) ?>" target="_blank">Peržiūrėti vietą</a>
                                <span class="date-add"><?php echo $row->date_added ?></span>
                            </div>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</div>