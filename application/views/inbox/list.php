<div class="row content dash">
    <div class="small-12 medium-4 large-3 columns dashboard nopad">
       	<?php echo $this->load->view('includes/dashboard_menu', "", true); ?>
    </div>
    <div class="small-12 medium-8 large-9 columns dashboard-content pl45">
        <select name="" style="width:50%" onchange="javascript:location.href = this.value;">
        	<option <?php echo ($active_filter == 'all') ? 'selected="selected"' : '' ?> value="<?php echo site_url('/inbox?filter=all') ?>"><?= lang('all_messages') ?></option>
        	<option <?php echo ($active_filter == 'unread') ? 'selected="selected"' : '' ?> value="<?php echo site_url('/inbox?filter=unread') ?>"><?= lang('unread') ?></option>
            <option <?php echo ($active_filter == 'waiting') ? 'selected="selected"' : '' ?> value="<?php echo site_url('/inbox?filter=waiting') ?>"><?= lang('waiting_confirmation') ?></option>
        	<option <?php echo ($active_filter == 'reservations') ? 'selected="selected"' : '' ?> value="<?php echo site_url('/inbox?filter=reservations') ?>"><?= lang('reservations') ?></option>
        </select>

        <?php $success = $this->session->flashdata('success'); ?>
        <?php if (isset($success)): ?>
            <div id="success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php endif ?>

        <div class="content active">
        	<?php if(!empty($messages)): ?>
				<table id="inbox">
        			<?php foreach ($messages as $k => $row): ?>
        				<tr>
        					<td style="min-width: 80px">
        						<img src="<?php echo site_url('/images/avatars/default.jpg?170925095128?170925095128')?>" alt="" />
        					</td>
        					<td>
        						<a href="<?php echo site_url('inbox/conversation/'.$row->conversation_id) ?>">
									<?=$row->name?> <?=$row->surname?>
									<br/><?php echo $row->date_added ?></a>
        					</td>
        					<td>
        						<a href="<?php echo site_url('inbox/conversation/'.$row->conversation_id) ?>">
                                    <?php echo $row->message; ?>
								</a>
        					</td>
							<td>
        						<a href="<?php echo site_url('inbox/conversation/'.$row->conversation_id) ?>">
                                    El-paštas<br>
									<?php echo $row->email; ?>
								</a>
        					</td>
                            <td>
                                <a class="green" href="<?php echo site_url('inbox/conversation/'.$row->conversation_id) ?>"><?= lang('read') ?></a><br/>
                                <a class="red" href="<?php echo site_url('inbox/delete/'.$row->conversation_id) ?>" data-confirm><?= lang('delete') ?></a>
                            </td>
        				</tr>
        			<?php endforeach ?>
        		</table>
            <?php else: ?>
                <?= lang('no_messages') ?>
        	<?php endif ?>
        </div>
    </div>
</div>

<script type="text/javascript">
        $(function () {
            $(document).confirmWithReveal({
                'modal_class': 'white-bg',
            });
        });
</script>