<div id="loader"><img src="<?php echo base_url(); ?>assets/img/spinner.gif" alt="loader"/></div>
<div class="footer">
    <div class="small-12 columns footer-widgets nopad">
        <div class="row">
            <div class="small-12 columns text-center footer-main">
                <div class="small-12 large-7 large-offset-5 columns text-center footer-contacts">
                    <div class="small-12 medium-6 large-5 columns nopad">
                        <img src="<?php echo base_url(); ?>assets/img/phone-white.svg"> +45 29 22 91 56
                    </div>
                    <div class="small-12 medium-6 large-7 columns nopad">
                        <img src="<?php echo base_url(); ?>assets/img/mail.svg">
                        <a class="cont-mail" href="mailto:mickeysoussah@hotmail.com">
                            mickeysoussah@hotmail.com
                        </a>
                    </div>
                </div>
            </div>
            <div class="small-12 columns footer-links footer-social">
                <div class="small-12 medium-6 columns text-center nopad">
                    <div class="small-12 large-2 columns nopad"><p class="follow-us"><?= lang('follow_us') ?>:</p></div>
                    <div class="small-12 large-10 columns nopad">
                        <ul class="social-links">
                            <li><a href="#"><img src="<?php echo base_url(); ?>assets/img/socials/facebook.svg"></a>
                            </li>
                            <li><a href="#"><img src="<?php echo base_url(); ?>assets/img/socials/twitter.svg"></a></li>
                            <li><a href="#"><img src="<?php echo base_url(); ?>assets/img/socials/google-plus.svg"></a>
                            </li>
                            <li><a href="#"><img src="<?php echo base_url(); ?>assets/img/socials/youtube.svg"></a></li>
                            <li><a href="#"><img src="<?php echo base_url(); ?>assets/img/socials/pinterest.svg"></a>
                            </li>
                            <li><a href="#"><img src="<?php echo base_url(); ?>assets/img/socials/instagram.svg"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="small-12 medium-6 columns footer-widget-title nopad">
                    <ul class="page-links">
                        <li><a href="<?php echo base_url('apie-mus') ?>"><?= lang('about_us') ?></a></li>
                        <li><a href="<?php echo base_url('informacija') ?>"><?= lang('help') ?></a></li>
                        <li>
                            <?php if (logged_in()): ?>
                                <a href="<?php echo base_url('naujas-skelbimas') ?>"><?= lang('list_your_space') ?></a>
                            <?php endif ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="small-12 columns footer-bottom">
        <div class="row">
            <div class="small-12 medium-6 columns copyrights small-only-text-center nopad">
                <?= lang('rights_reserved') ?> © <?= date('Y') ?>
                    <a style="color: #ffffff" href="<?php echo base_url('ajax/get/login') ?>" data-reveal-id="sign-up" data-reveal-ajax="true"><i class="fa fa-lock" aria-hidden="true"></i></a>
            </div>
            <div class="small-12 medium-6 columns solution text-right small-only-text-center nopad">
                <?= lang('solution') ?> - <a href="http://cimbersoftware.com/" target="_blank">Procreo</a>
            </div>
        </div>
    </div>
</div>
<div class="reveal-modal tiny" id="sign-up" data-reveal aria-hidden="true" role="dialog"></div>
<div class="reveal-modal tiny" id="login" data-reveal aria-hidden="true" role="dialog"></div>
<div class="reveal-modal tiny" id="sign-up-email" data-reveal aria-hidden="true" role="dialog"></div>
<div class="reveal-modal tiny" id="reset-pass" data-reveal aria-hidden="true" role="dialog"></div>
<div class="reveal-modal tiny" id="for-messages" data-reveal aria-hidden="true" role="dialog">
    <div class="pop-up-box" id="pop_up_content">
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('.meniu-link').on('click', function () {
            $('div.header').toggleClass('fixed');
        })
        $(".place-search").geocomplete({details: "form"});
    })
</script>
<script>
    $(document).foundation({
        <?php if (isset($frontpage)): ?>
        "magellan-expedition": {
            active_class: 'active', // specify the class used for active sections
            threshold: 650, // how many pixels until the magellan bar sticks, 0 = auto
            destination_threshold: 20, // pixels from the top of destination for it to be considered active
            throttle_delay: 50, // calculation throttling to increase framerate
            fixed_top: 0, // top distance in pixels assigend to the fixed element on scroll
            offset_by_height: true // whether to offset the destination by the expedition height. Usually you want this to be true, unless your expedition is on the side.
        }
        <?php endif ?>
    });
</script>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '438565016515653',
            xfbml: true,
            version: 'v2.9'
        });
        FB.AppEvents.logPageView();
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>