<div class="dashboard-menu-title">
  	<?= lang('dashboard') ?>
</div>
<ul>
    <li <?php echo (($active == "messages") ? 'class="current"' : "") ?>>
        <a href="<?php echo base_url('inbox') ?>">
          <?php if($this->session->userdata('created_spaces') == 0): ?>
            <?= lang('your_reservations') ?>
          <?php else: ?>
            <?= lang('your_requests') ?>
          <?php endif ?>
        </a>
    </li>
  	<li <?php echo (($active == "account") ? 'class="current"' : "") ?>>
        <a href="<?php echo base_url('users/edit') ?>"><?= lang('edit_account') ?></a>
    </li>
    <li <?php echo (($active == "notifications") ? 'class="current"' : "") ?>>
        <a href="<?php echo base_url('users/notifications') ?>"><?= lang('notifications') ?></a>
    </li>
  	<li <?php echo (($active == "sparerooms") ? 'class="current"' : "") ?>>
        <a href="<?php echo base_url('sparerooms') ?>"><?= lang('your_sparerooms') ?></a>
    </li>
    <?php if($this->session->userdata('created_spaces') == 0 || 1 == 1): ?>
    	<li <?php echo (($active == "wishlist") ? 'class="current"' : "") ?>>
          <a href="<?php echo base_url('wishlist/my') ?>"><?= lang('wish_lists') ?></a>
      </li>
    <?php endif ?>
    <?php if($this->session->userdata('allow_list_space') && $this->session->userdata('login_type') == 0): ?>
        <li <?php echo (($active == "password") ? 'class="current"' : "") ?>>
            <a href="<?php echo base_url('auth/change_password') ?>"><?= lang('change_password') ?></a>
        </li>
    <?php endif ?>
</ul>