<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php
        if($_SERVER['REQUEST_URI'] == '/'){
            echo('Nekilnojamas.dev');
        }elseif(strpos($_SERVER['REQUEST_URI'],'nuoma') || strpos($_SERVER['REQUEST_URI'],'pardavimas')){
            echo $info['room_number'] . ' ' . room_plural($info['room_number']) . ' ' . room_type_helper($info['room_type']) . ', ' . get_address($info['lat'], $info['lng']) . ' | Nekilnojamas.dev' ;
        }else{
            echo('Nekilnojamas.dev');
        }

        ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico?2016"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/foundation.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.nouislider.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css?v=1.000000028"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/orange.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500,300,100,900&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>

    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/starrr.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/foundation.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/foundation/foundation.reveal.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/foundation/foundation.datepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/icheck.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;libraries=places&key=AIzaSyDt-htKS0iLbP8mf3Aklf_BPF8YpDdemPU"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.geocomplete.min.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url(); ?>assets/js/foundation/confirm_with_reveal.min.js"></script>
</head>
<body class="<?= isset($searchpage) ? 'searchpage' : ''; ?>">
<?php if (isset($frontpage)): ?>
<div class="small-12 columns header" data-magellan-expedition="fixed">
    <div class="row">
        <div class="small-10 medium-3 large-2 columns logo nopad">
            <a class="logo-desktop" href="<?php echo base_url() ?>"><img
                    src="<?php echo base_url(); ?>assets/img/logo.png?201604"></a>
            <a class="logo-mobile" href="<?php echo base_url() ?>"><img
                    src="<?php echo base_url(); ?>assets/img/logo-mobile.png?201604"></a>
        </div>
        <div class="show-for-large-up header-search frontapage large-3 columns">
            <form class="header-search-form" action="<?php echo base_url('skelbimai') ?>" method="get">
                <input type="hidden" name="lat" value="">
                <input type="hidden" name="lng" value="">
                <input type="hidden" name="formatted_address" value="">
                <input class="place-search" type="text" name="" placeholder="<?= lang('place_example') ?>">
                <button class="place-search-btn"><img src="<?php echo base_url(); ?>assets/img/icon-search-dark.svg">
                </button>
            </form>
        </div>
        <div class="small-2 medium-9 large-7 columns nav text-right nopad right">
            <?php else: ?>
            <div class="small-12 columns header" data-magellan-expedition="fixed"">
                <div class="row">
                    <div class="small-10 medium-3 large-2 columns logo nopad">
                        <a class="logo-desktop" href="<?php echo base_url() ?>"><img
                                src="<?php echo base_url(); ?>assets/img/logo.png?201604"></a>
                        <a class="logo-mobile" href="<?php echo base_url() ?>"><img
                                src="<?php echo base_url(); ?>assets/img/logo-mobile.png?201604"></a>
                    </div>
                    <div class="show-for-large-up header-search large-3 columns">
                        <?php if (!isset($searchpage)): ?>
                            <form class="header-search-form" action="<?php echo base_url('skelbimai') ?>" method="get">
                                <input type="hidden" name="lat" value="">
                                <input type="hidden" name="lng" value="">
                                <input type="hidden" name="formatted_address" value="">
                                <input class="place-search" type="text" name=""
                                       placeholder="<?= lang('place_example') ?>">
                                <button class="place-search-btn"><img
                                        src="<?php echo base_url(); ?>assets/img/icon-search-dark.svg"></button>
                            </form>
                        <?php endif; ?>
                    </div>
                    <div class="small-2 medium-9 large-7 columns nav text-right nopad right">
                        <?php endif; ?>
                        <div class="trigger show-for-small-only meniu-link">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </div>
                        <ul
                            <?php if (!logged_in()): ?>
                                style="height: auto; line-height: 80px;" id="no-user"
                            <?php endif; ?>>
                            <?php if (logged_in()): ?>
                                <li>
                                    <a class="user" href="#">
                                        <img src="<?php echo site_url(get_avatar("", 1)); ?>">
                                        <?php echo $this->session->userdata('username'); ?> <span></span>
                                    </a>
                                    <ul class="users-links">
                                        <li class="show-for-small-only">
                                            <a href="<?php echo base_url('skelbimai') ?>">
                                                <span class="orange"><?= lang('search') ?></span>
                                                <img src="<?php echo base_url(); ?>assets/img/icon-search-dark.svg">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('admin') ?>"><?= lang('admin_panel') ?></a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('sparerooms') ?>"><?= lang('your_sparerooms') ?></a>
                                        </li>
                                        <li><a href="<?php echo base_url('inbox') ?>">
                                                <?php if ($this->session->userdata('created_spaces') == 0): ?>
                                                    <?= lang('your_reservations') ?><?php else: ?>
                                                    <?= lang('your_requests') ?>
                                                <?php endif; ?>
                                            </a></li>
                                        <?php if ($this->session->userdata('created_spaces') == 0 || 1 == 1): ?>
                                            <li>
                                                <a href="<?php echo base_url('wishlist/my') ?>"><?= lang('wish_lists') ?>
                                                    <span><?= lang('new') ?></span></a></li>
                                        <?php endif ?>
                                        <li>
                                            <a href="<?php echo base_url('users/edit') ?>"><?= lang('edit_profile') ?></a>
                                        </li>
                                        <li><a href="<?php echo base_url('auth/logout') ?>"><?= lang('log_out') ?></a>
                                        </li>
                                    </ul>
                                </li>
                            <?php else: ?>
                                <li class="show-for-small-only">
                                    <a href="<?php echo base_url('skelbimai') ?>">
                                        <span class="orange"><?= lang('search') ?></span>
                                        <img src="<?php echo base_url(); ?>assets/img/icon-search-dark.svg">
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li><a href="<?php echo base_url('informacija') ?>"><?= lang('help') ?></a></li>

                            <?php if (logged_in()): ?>
                                <li>
                                    <a class="btn-green"
                                       href="<?php echo base_url('naujas-skelbimas') ?>"><?= lang('list_your_space') ?></a>
                                </li>
                            <?php else: ?>
                                <li>
                                    <a class="btn-orange" href="<?php echo base_url(); ?>skelbimai?paieska=pardavimas"><?= lang('sell-btn') ?></a>
                                </li>
                                <li>
                                    <a class="btn-green" href="<?php echo base_url(); ?>skelbimai?paieska=nuoma"><?= lang('rent-btn') ?></a>
                                </li>
                            <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>