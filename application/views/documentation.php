<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Documentation</title>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Slab:400,700|Inconsolata:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/temps.css" type="text/css" />
</head>
<body>
<div class="wy-nav-content rst-content">
<div class="section" id="installation-instructions">
<h1>Installation Instructions<a class="headerlink" href="#installation-instructions" title="Permalink to this headline">¶</a></h1>
<p>CodeIgniter is installed in four steps:</p>
<ol class="arabic simple">
<li>Unzip the package.</li>
<li>Upload the CodeIgniter folders and files to your server. Normally the
<em>index.php</em> file will be at your root.</li>
<li>Open the <em>application/config/config.php</em> file with a text editor and
set your base URL. If you intend to use encryption or sessions, set
your encryption key.</li>
<li>If you intend to use a database, open the
<em>application/config/database.php</em> file with a text editor and set your
database settings.</li>
</ol>
<p>If you wish to increase security by hiding the location of your
CodeIgniter files you can rename the system and application folders to
something more private. If you do rename them, you must open your main
<em>index.php</em> file and set the <tt class="docutils literal"><span class="pre">$system_path</span></tt> and <tt class="docutils literal"><span class="pre">$application_folder</span></tt>
variables at the top of the file with the new name you’ve chosen.</p>
<p>For the best security, both the system and any application folders
should be placed above web root so that they are not directly accessible
via a browser. By default, <em>.htaccess</em> files are included in each folder
to help prevent direct access, but it is best to remove them from public
access entirely in case the web server configuration changes or doesn’t
abide by the <em>.htaccess</em>.</p>
<p>If you would like to keep your views public it is also possible to move
the views folder out of your application folder.</p>
<p>After moving them, open your main index.php file and set the
<tt class="docutils literal"><span class="pre">$system_path</span></tt>, <tt class="docutils literal"><span class="pre">$application_folder</span></tt> and <tt class="docutils literal"><span class="pre">$view_folder</span></tt> variables,
preferably with a full path, e.g. ‘<em>/www/MyUser/system</em>‘.</p>
<p>One additional measure to take in production environments is to disable
PHP error reporting and any other development-only functionality. In
CodeIgniter, this can be done by setting the <tt class="docutils literal"><span class="pre">ENVIRONMENT</span></tt> constant, which
is more fully described on the <a class="reference internal" href="../user_guide/general/security.html"><em>security
page</em></a>.</p>
<p>That’s it!</p>
<p>If you’re new to CodeIgniter, please read the <a class="reference internal" href="../user_guide/overview/getting_started.html"><em>Getting
Started</em></a> section of the User Guide
to begin learning how to build dynamic PHP applications. Enjoy!</p>
<div class="toctree-wrapper compound">
</div>
</div>
<div class="section" id="codeigniter-features">
<h1>CodeIgniter Features<a class="headerlink" href="#codeigniter-features" title="Permalink to this headline">¶</a></h1>
<p>Features in and of themselves are a very poor way to judge an
application since they tell you nothing about the user experience, or
how intuitively or intelligently it is designed. Features don’t reveal
anything about the quality of the code, or the performance, or the
attention to detail, or security practices. The only way to really judge
an app is to try it and get to know the code.
<a class="reference internal" href="../user_guide/installation/index.html"><em>Installing</em></a> CodeIgniter is child’s play so
we encourage you to do just that. In the mean time here’s a list of
CodeIgniter’s main features.</p>
<ul class="simple">
<li>Model-View-Controller Based System</li>
<li>Extremely Light Weight</li>
<li>Full Featured database classes with support for several platforms.</li>
<li>Query Builder Database Support</li>
<li>Form and Data Validation</li>
<li>Security and XSS Filtering</li>
<li>Session Management</li>
<li>Email Sending Class. Supports Attachments, HTML/Text email, multiple
protocols (sendmail, SMTP, and Mail) and more.</li>
<li>Image Manipulation Library (cropping, resizing, rotating, etc.).
Supports GD, ImageMagick, and NetPBM</li>
<li>File Uploading Class</li>
<li>FTP Class</li>
<li>Localization</li>
<li>Pagination</li>
<li>Data Encryption</li>
<li>Benchmarking</li>
<li>Full Page Caching</li>
<li>Error Logging</li>
<li>Application Profiling</li>
<li>Calendaring Class</li>
<li>User Agent Class</li>
<li>Zip Encoding Class</li>
<li>Template Engine Class</li>
<li>Trackback Class</li>
<li>XML-RPC Library</li>
<li>Unit Testing Class</li>
<li>Search-engine Friendly URLs</li>
<li>Flexible URI Routing</li>
<li>Support for Hooks and Class Extensions</li>
<li>Large library of “helper” functions</li>
</ul>
</div>
<div class="section" id="application-flow-chart">
<h1>Application Flow Chart<a class="headerlink" href="#application-flow-chart" title="Permalink to this headline">¶</a></h1>
<p>The following graphic illustrates how data flows throughout the system:</p>
<p><img alt="CodeIgniter application flow" src="http://www.codeigniter.com/user_guide/_images/appflowchart.gif"></p>
<ol class="arabic simple">
<li>The index.php serves as the front controller, initializing the base
resources needed to run CodeIgniter.</li>
<li>The Router examines the HTTP request to determine what should be done
with it.</li>
<li>If a cache file exists, it is sent directly to the browser, bypassing
the normal system execution.</li>
<li>Security. Before the application controller is loaded, the HTTP
request and any user submitted data is filtered for security.</li>
<li>The Controller loads the model, core libraries, helpers, and any
other resources needed to process the specific request.</li>
<li>The finalized View is rendered then sent to the web browser to be
seen. If caching is enabled, the view is cached first so that on
subsequent requests it can be served.</li>
</ol>
</div>
<div class="section" id="model-view-controller">
<h1>Model-View-Controller<a class="headerlink" href="#model-view-controller" title="Permalink to this headline">¶</a></h1>
<p>CodeIgniter is based on the Model-View-Controller development pattern.
MVC is a software approach that separates application logic from
presentation. In practice, it permits your web pages to contain minimal
scripting since the presentation is separate from the PHP scripting.</p>
<ul class="simple">
<li>The <strong>Model</strong> represents your data structures. Typically your model
classes will contain functions that help you retrieve, insert, and
update information in your database.</li>
<li>The <strong>View</strong> is the information that is being presented to a user. A
View will normally be a web page, but in CodeIgniter, a view can also
be a page fragment like a header or footer. It can also be an RSS
page, or any other type of “page”.</li>
<li>The <strong>Controller</strong> serves as an <em>intermediary</em> between the Model, the
View, and any other resources needed to process the HTTP request and
generate a web page.</li>
</ul>
<p>CodeIgniter has a fairly loose approach to MVC since Models are not
required. If you don’t need the added separation, or find that
maintaining models requires more complexity than you want, you can
ignore them and build your application minimally using Controllers and
Views. CodeIgniter also enables you to incorporate your own existing
scripts, or even develop core libraries for the system, enabling you to
work in a way that makes the most sense to you.</p>
</div>
<div class="section" id="codeigniter-urls">
<h1>CodeIgniter URLs<a class="headerlink" href="#codeigniter-urls" title="Permalink to this headline">¶</a></h1>
<p>By default, URLs in CodeIgniter are designed to be search-engine and
human friendly. Rather than using the standard “query string” approach
to URLs that is synonymous with dynamic systems, CodeIgniter uses a
<strong>segment-based</strong> approach:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">news</span><span class="o">/</span><span class="nx">article</span><span class="o">/</span><span class="nx">my_article</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Query string URLs can be optionally enabled, as described
below.</p>
</div>
<div class="section" id="uri-segments">
<h2>URI Segments<a class="headerlink" href="#uri-segments" title="Permalink to this headline">¶</a></h2>
<p>The segments in the URL, in following with the Model-View-Controller
approach, usually represent:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">class</span><span class="o">/</span><span class="nx">function</span><span class="o">/</span><span class="nx">ID</span>
</pre></div>
</div>
<ol class="arabic simple">
<li>The first segment represents the controller <strong>class</strong> that should be
invoked.</li>
<li>The second segment represents the class <strong>function</strong>, or method, that
should be called.</li>
<li>The third, and any additional segments, represent the ID and any
variables that will be passed to the controller.</li>
</ol>
<p>The <a class="reference internal" href="../user_guide/libraries/uri.html"><em>URI Library</em></a> and the <a class="reference internal" href="../user_guide/helpers/url_helper.html"><em>URL Helper</em></a> contain functions that make it easy to work
with your URI data. In addition, your URLs can be remapped using the
<a class="reference internal" href="routing.html"><em>URI Routing</em></a> feature for more flexibility.</p>
</div>
<div class="section" id="removing-the-index-php-file">
<h2>Removing the index.php file<a class="headerlink" href="#removing-the-index-php-file" title="Permalink to this headline">¶</a></h2>
<p>By default, the <strong>index.php</strong> file will be included in your URLs:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">news</span><span class="o">/</span><span class="nx">article</span><span class="o">/</span><span class="nx">my_article</span>
</pre></div>
</div>
<p>If your Apache server has <em>mod_rewrite</em> enabled, you can easily remove this
file by using a .htaccess file with some simple rules. Here is an example
of such a file, using the “negative” method in which everything is redirected
except the specified items:</p>
<div class="highlight-ci"><div class="highlight"><pre>RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php/$1 [L]
</pre></div>
</div>
<p>In the above example, any HTTP request other than those for existing
directories and existing files is treated as a request for your index.php file.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">These specific rules might not work for all server configurations.</p>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Make sure to also exclude from the above rule any assets that you
might need to be accessible from the outside world.</p>
</div>
</div>
<div class="section" id="adding-a-url-suffix">
<h2>Adding a URL Suffix<a class="headerlink" href="#adding-a-url-suffix" title="Permalink to this headline">¶</a></h2>
<p>In your <strong>config/config.php</strong> file you can specify a suffix that will be
added to all URLs generated by CodeIgniter. For example, if a URL is
this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">products</span><span class="o">/</span><span class="nx">view</span><span class="o">/</span><span class="nx">shoes</span>
</pre></div>
</div>
<p>You can optionally add a suffix, like <strong>.html,</strong> making the page appear to
be of a certain type:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">products</span><span class="o">/</span><span class="nx">view</span><span class="o">/</span><span class="nx">shoes</span><span class="o">.</span><span class="nx">html</span>
</pre></div>
</div>
</div>
<div class="section" id="enabling-query-strings">
<h2>Enabling Query Strings<a class="headerlink" href="#enabling-query-strings" title="Permalink to this headline">¶</a></h2>
<p>In some cases you might prefer to use query strings URLs:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">?</span><span class="nx">c</span><span class="o">=</span><span class="nx">products</span><span class="o">&amp;</span><span class="nx">m</span><span class="o">=</span><span class="nx">view</span><span class="o">&amp;</span><span class="nx">id</span><span class="o">=</span><span class="mi">345</span>
</pre></div>
</div>
<p>CodeIgniter optionally supports this capability, which can be enabled in
your <strong>application/config.php</strong> file. If you open your config file you’ll
see these items:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$config</span><span class="p">[</span><span class="s1">'enable_query_strings'</span><span class="p">]</span> <span class="o">=</span> <span class="k">FALSE</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'controller_trigger'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'c'</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'function_trigger'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'m'</span><span class="p">;</span>
</pre></div>
</div>
<p>If you change “enable_query_strings” to TRUE this feature will become
active. Your controllers and functions will then be accessible using the
“trigger” words you’ve set to invoke your controllers and methods:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">?</span><span class="nx">c</span><span class="o">=</span><span class="nx">controller</span><span class="o">&amp;</span><span class="nx">m</span><span class="o">=</span><span class="nx">method</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">If you are using query strings you will have to build your own
URLs, rather than utilizing the URL helpers (and other helpers
that generate URLs, like some of the form helpers) as these are
designed to work with segment based URLs.</p>
</div>
</div>
<div class="section" id="controllers">
<h1><a class="toc-backref" href="#id1">Controllers</a><a class="headerlink" href="#controllers" title="Permalink to this headline">¶</a></h1>
<p>Controllers are the heart of your application, as they determine how
HTTP requests should be handled.</p>
<div class="contents topic" id="page-contents">
<p class="topic-title first">Page Contents</p>
<ul class="simple">
<li><a class="reference internal" href="#controllers" id="id1">Controllers</a><ul>
<li><a class="reference internal" href="#what-is-a-controller" id="id2">What is a Controller?</a></li>
<li><a class="reference internal" href="#let-s-try-it-hello-world" id="id3">Let’s try it: Hello World!</a></li>
<li><a class="reference internal" href="#methods" id="id4">Methods</a></li>
<li><a class="reference internal" href="#passing-uri-segments-to-your-methods" id="id5">Passing URI Segments to your methods</a></li>
<li><a class="reference internal" href="#defining-a-default-controller" id="id6">Defining a Default Controller</a></li>
<li><a class="reference internal" href="#remapping-method-calls" id="id7">Remapping Method Calls</a></li>
<li><a class="reference internal" href="#processing-output" id="id8">Processing Output</a></li>
<li><a class="reference internal" href="#private-methods" id="id9">Private methods</a></li>
<li><a class="reference internal" href="#organizing-your-controllers-into-sub-directories" id="id10">Organizing Your Controllers into Sub-directories</a></li>
<li><a class="reference internal" href="#class-constructors" id="id11">Class Constructors</a></li>
<li><a class="reference internal" href="#reserved-method-names" id="id12">Reserved method names</a></li>
<li><a class="reference internal" href="#that-s-it" id="id13">That’s it!</a></li>
</ul>
</li>
</ul>
</div>
<div class="section" id="what-is-a-controller">
<h2><a class="toc-backref" href="#id2">What is a Controller?</a><a class="headerlink" href="#what-is-a-controller" title="Permalink to this headline">¶</a></h2>
<p><strong>A Controller is simply a class file that is named in a way that can be
associated with a URI.</strong></p>
<p>Consider this URI:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">blog</span><span class="o">/</span>
</pre></div>
</div>
<p>In the above example, CodeIgniter would attempt to find a controller
named Blog.php and load it.</p>
<p><strong>When a controller’s name matches the first segment of a URI, it will
be loaded.</strong></p>
</div>
<div class="section" id="let-s-try-it-hello-world">
<h2><a class="toc-backref" href="#id3">Let’s try it: Hello World!</a><a class="headerlink" href="#let-s-try-it-hello-world" title="Permalink to this headline">¶</a></h2>
<p>Let’s create a simple controller so you can see it in action. Using your
text editor, create a file called Blog.php, and put the following code
in it:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">Blog</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">index</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">echo</span> <span class="s1">'Hello World!'</span><span class="p">;</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Then save the file to your <em>application/controllers/</em> directory.</p>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">The file must be called ‘Blog.php’, with a capital ‘B’.</p>
</div>
<p>Now visit the your site using a URL similar to this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">blog</span><span class="o">/</span>
</pre></div>
</div>
<p>If you did it right, you should see:</p>
<blockquote>
<div>Hello World!</div></blockquote>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">Class names must start with an uppercase letter.</p>
</div>
<p>This is valid:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">Blog</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

<span class="p">}</span>
</pre></div>
</div>
<p>This is <strong>not</strong> valid:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">blog</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

<span class="p">}</span>
</pre></div>
</div>
<p>Also, always make sure your controller extends the parent controller
class so that it can inherit all its methods.</p>
</div>
<div class="section" id="methods">
<h2><a class="toc-backref" href="#id4">Methods</a><a class="headerlink" href="#methods" title="Permalink to this headline">¶</a></h2>
<p>In the above example the method name is <tt class="docutils literal"><span class="pre">index()</span></tt>. The “index” method
is always loaded by default if the <strong>second segment</strong> of the URI is
empty. Another way to show your “Hello World” message would be this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">blog</span><span class="o">/</span><span class="nx">index</span><span class="o">/</span>
</pre></div>
</div>
<p><strong>The second segment of the URI determines which method in the
controller gets called.</strong></p>
<p>Let’s try it. Add a new method to your controller:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">Blog</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">index</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">echo</span> <span class="s1">'Hello World!'</span><span class="p">;</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">comments</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">echo</span> <span class="s1">'Look at this!'</span><span class="p">;</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Now load the following URL to see the comment method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">blog</span><span class="o">/</span><span class="nx">comments</span><span class="o">/</span>
</pre></div>
</div>
<p>You should see your new message.</p>
</div>
<div class="section" id="passing-uri-segments-to-your-methods">
<h2><a class="toc-backref" href="#id5">Passing URI Segments to your methods</a><a class="headerlink" href="#passing-uri-segments-to-your-methods" title="Permalink to this headline">¶</a></h2>
<p>If your URI contains more than two segments they will be passed to your
method as parameters.</p>
<p>For example, let’s say you have a URI like this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">products</span><span class="o">/</span><span class="nx">shoes</span><span class="o">/</span><span class="nx">sandals</span><span class="o">/</span><span class="mi">123</span>
</pre></div>
</div>
<p>Your method will be passed URI segments 3 and 4 (“sandals” and “123”):</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">Products</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">shoes</span><span class="p">(</span><span class="nv">$sandals</span><span class="p">,</span> <span class="nv">$id</span><span class="p">)</span>
        <span class="p">{</span>
                <span class="k">echo</span> <span class="nv">$sandals</span><span class="p">;</span>
                <span class="k">echo</span> <span class="nv">$id</span><span class="p">;</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">If you are using the <a class="reference internal" href="routing.html"><em>URI Routing</em></a>
feature, the segments passed to your method will be the re-routed
ones.</p>
</div>
</div>
<div class="section" id="defining-a-default-controller">
<h2><a class="toc-backref" href="#id6">Defining a Default Controller</a><a class="headerlink" href="#defining-a-default-controller" title="Permalink to this headline">¶</a></h2>
<p>CodeIgniter can be told to load a default controller when a URI is not
present, as will be the case when only your site root URL is requested.
To specify a default controller, open your <strong>application/config/routes.php</strong>
file and set this variable:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'default_controller'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'blog'</span><span class="p">;</span>
</pre></div>
</div>
<p>Where ‘blog’ is the name of the controller class you want used. If you now
load your main index.php file without specifying any URI segments you’ll
see your “Hello World” message by default.</p>
<p>For more information, please refer to the “Reserved Routes” section of the
<a class="reference internal" href="routing.html"><em>URI Routing</em></a> documentation.</p>
</div>
<div class="section" id="remapping-method-calls">
<h2><a class="toc-backref" href="#id7">Remapping Method Calls</a><a class="headerlink" href="#remapping-method-calls" title="Permalink to this headline">¶</a></h2>
<p>As noted above, the second segment of the URI typically determines which
method in the controller gets called. CodeIgniter permits you to override
this behavior through the use of the <tt class="docutils literal"><span class="pre">_remap()</span></tt> method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">public</span> <span class="k">function</span> <span class="nf">_remap</span><span class="p">()</span>
<span class="p">{</span>
        <span class="c1">// Some code here...</span>
<span class="p">}</span>
</pre></div>
</div>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">If your controller contains a method named _remap(),
it will <strong>always</strong> get called regardless of what your URI contains. It
overrides the normal behavior in which the URI determines which method
is called, allowing you to define your own method routing rules.</p>
</div>
<p>The overridden method call (typically the second segment of the URI) will
be passed as a parameter to the <tt class="docutils literal"><span class="pre">_remap()</span></tt> method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">public</span> <span class="k">function</span> <span class="nf">_remap</span><span class="p">(</span><span class="nv">$method</span><span class="p">)</span>
<span class="p">{</span>
        <span class="k">if</span> <span class="p">(</span><span class="nv">$method</span> <span class="o">===</span> <span class="s1">'some_method'</span><span class="p">)</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="nv">$method</span><span class="p">();</span>
        <span class="p">}</span>
        <span class="k">else</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">default_method</span><span class="p">();</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Any extra segments after the method name are passed into <tt class="docutils literal"><span class="pre">_remap()</span></tt> as an
optional second parameter. This array can be used in combination with
PHP’s <a class="reference external" href="http://php.net/call_user_func_array">call_user_func_array()</a>
to emulate CodeIgniter’s default behavior.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">public</span> <span class="k">function</span> <span class="nf">_remap</span><span class="p">(</span><span class="nv">$method</span><span class="p">,</span> <span class="nv">$params</span> <span class="o">=</span> <span class="k">array</span><span class="p">())</span>
<span class="p">{</span>
        <span class="nv">$method</span> <span class="o">=</span> <span class="s1">'process_'</span><span class="o">.</span><span class="nv">$method</span><span class="p">;</span>
        <span class="k">if</span> <span class="p">(</span><span class="nb">method_exists</span><span class="p">(</span><span class="nv">$this</span><span class="p">,</span> <span class="nv">$method</span><span class="p">))</span>
        <span class="p">{</span>
                <span class="k">return</span> <span class="nb">call_user_func_array</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="nv">$this</span><span class="p">,</span> <span class="nv">$method</span><span class="p">),</span> <span class="nv">$params</span><span class="p">);</span>
        <span class="p">}</span>
        <span class="nx">show_404</span><span class="p">();</span>
<span class="p">}</span>
</pre></div>
</div>
</div>
<div class="section" id="processing-output">
<h2><a class="toc-backref" href="#id8">Processing Output</a><a class="headerlink" href="#processing-output" title="Permalink to this headline">¶</a></h2>
<p>CodeIgniter has an output class that takes care of sending your final
rendered data to the web browser automatically. More information on this
can be found in the <a class="reference internal" href="views.html"><em>Views</em></a> and <a class="reference internal" href="../user_guide/libraries/output.html"><em>Output Class</em></a> pages. In some cases, however, you might want to
post-process the finalized data in some way and send it to the browser
yourself. CodeIgniter permits you to add a method named <tt class="docutils literal"><span class="pre">_output()</span></tt>
to your controller that will receive the finalized output data.</p>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">If your controller contains a method named <tt class="docutils literal"><span class="pre">_output()</span></tt>,
it will <strong>always</strong> be called by the output class instead of
echoing the finalized data directly. The first parameter of the
method will contain the finalized output.</p>
</div>
<p>Here is an example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">public</span> <span class="k">function</span> <span class="nf">_output</span><span class="p">(</span><span class="nv">$output</span><span class="p">)</span>
<span class="p">{</span>
        <span class="k">echo</span> <span class="nv">$output</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p>Please note that your <tt class="docutils literal"><span class="pre">_output()</span></tt> method will receive the
data in its finalized state. Benchmark and memory usage data
will be rendered, cache files written (if you have caching
enabled), and headers will be sent (if you use that
<a class="reference internal" href="../user_guide/libraries/output.html"><em>feature</em></a>) before it is handed off
to the <tt class="docutils literal"><span class="pre">_output()</span></tt> method.
To have your controller’s output cached properly, its
<tt class="docutils literal"><span class="pre">_output()</span></tt> method can use:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">output</span><span class="o">-&gt;</span><span class="na">cache_expiration</span> <span class="o">&gt;</span> <span class="mi">0</span><span class="p">)</span>
<span class="p">{</span>
        <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">output</span><span class="o">-&gt;</span><span class="na">_write_cache</span><span class="p">(</span><span class="nv">$output</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>
</div>
<p class="last">If you are using this feature the page execution timer and
memory usage stats might not be perfectly accurate since they
will not take into account any further processing you do.
For an alternate way to control output <em>before</em> any of the
final processing is done, please see the available methods
in the <a class="reference internal" href="../user_guide/libraries/output.html"><em>Output Library</em></a>.</p>
</div>
</div>
<div class="section" id="private-methods">
<h2><a class="toc-backref" href="#id9">Private methods</a><a class="headerlink" href="#private-methods" title="Permalink to this headline">¶</a></h2>
<p>In some cases you may want certain methods hidden from public access.
In order to achieve this, simply declare the method as being private
or protected and it will not be served via a URL request. For example,
if you were to have a method like this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">private</span> <span class="k">function</span> <span class="nf">_utility</span><span class="p">()</span>
<span class="p">{</span>
        <span class="c1">// some code</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Trying to access it via the URL, like this, will not work:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">blog</span><span class="o">/</span><span class="nx">_utility</span><span class="o">/</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Prefixing method names with an underscore will also prevent
them from being called. This is a legacy feature that is left
for backwards-compatibility.</p>
</div>
</div>
<div class="section" id="organizing-your-controllers-into-sub-directories">
<h2><a class="toc-backref" href="#id10">Organizing Your Controllers into Sub-directories</a><a class="headerlink" href="#organizing-your-controllers-into-sub-directories" title="Permalink to this headline">¶</a></h2>
<p>If you are building a large application you might want to hierarchically
organize or structure your controllers into sub-directories. CodeIgniter
permits you to do this.</p>
<p>Simply create sub-directories under the main <em>application/controllers/</em>
one and place your controller classes within them.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p>When using this feature the first segment of your URI must
specify the folder. For example, let’s say you have a controller located
here:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">application</span><span class="o">/</span><span class="nx">controllers</span><span class="o">/</span><span class="nx">products</span><span class="o">/</span><span class="nx">Shoes</span><span class="o">.</span><span class="nx">php</span>
</pre></div>
</div>
<p>To call the above controller your URI will look something like this:</p>
<div class="last highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">products</span><span class="o">/</span><span class="nx">shoes</span><span class="o">/</span><span class="nx">show</span><span class="o">/</span><span class="mi">123</span>
</pre></div>
</div>
</div>
<p>Each of your sub-directories may contain a default controller which will be
called if the URL contains <em>only</em> the sub-directory. Simply put a controller
in there that matches the name of your ‘default_controller’ as specified in
your <em>application/config/routes.php</em> file.</p>
<p>CodeIgniter also permits you to remap your URIs using its <a class="reference internal" href="routing.html"><em>URI
Routing</em></a> feature.</p>
</div>
<div class="section" id="class-constructors">
<h2><a class="toc-backref" href="#id11">Class Constructors</a><a class="headerlink" href="#class-constructors" title="Permalink to this headline">¶</a></h2>
<p>If you intend to use a constructor in any of your Controllers, you
<strong>MUST</strong> place the following line of code in it:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">parent</span><span class="o">::</span><span class="na">__construct</span><span class="p">();</span>
</pre></div>
</div>
<p>The reason this line is necessary is because your local constructor will
be overriding the one in the parent controller class so we need to
manually call it.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">Blog</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">parent</span><span class="o">::</span><span class="na">__construct</span><span class="p">();</span>
                <span class="c1">// Your own constructor code</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Constructors are useful if you need to set some default values, or run a
default process when your class is instantiated. Constructors can’t
return a value, but they can do some default work.</p>
</div>
<div class="section" id="reserved-method-names">
<h2><a class="toc-backref" href="#id12">Reserved method names</a><a class="headerlink" href="#reserved-method-names" title="Permalink to this headline">¶</a></h2>
<p>Since your controller classes will extend the main application
controller you must be careful not to name your methods identically to
the ones used by that class, otherwise your local functions will
override them. See <a class="reference internal" href="reserved_names.html"><em>Reserved Names</em></a> for a full
list.</p>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">You should also never have a method named identically
to its class name. If you do, and there is no <tt class="docutils literal"><span class="pre">__construct()</span></tt>
method in the same class, then your e.g. <tt class="docutils literal"><span class="pre">Index::index()</span></tt>
method will be executed as a class constructor! This is a PHP4
backwards-compatibility feature.</p>
</div>
</div>
<div class="section" id="that-s-it">
<h2><a class="toc-backref" href="#id13">That’s it!</a><a class="headerlink" href="#that-s-it" title="Permalink to this headline">¶</a></h2>
<p>That, in a nutshell, is all there is to know about controllers.</p>
</div>
</div>
<div class="section" id="reserved-names">
<h1>Reserved Names<a class="headerlink" href="#reserved-names" title="Permalink to this headline">¶</a></h1>
<p>In order to help out, CodeIgniter uses a series of function, method,
class and variable names in its operation. Because of this, some names
cannot be used by a developer. Following is a list of reserved names
that cannot be used.</p>
<div class="section" id="controller-names">
<h2>Controller names<a class="headerlink" href="#controller-names" title="Permalink to this headline">¶</a></h2>
<p>Since your controller classes will extend the main application
controller you must be careful not to name your methods identically to
the ones used by that class, otherwise your local methods will
override them. The following is a list of reserved names. Do not name
your controller any of these:</p>
<ul class="simple">
<li>CI_Controller</li>
<li>Default</li>
<li>index</li>
</ul>
</div>
<div class="section" id="functions">
<h2>Functions<a class="headerlink" href="#functions" title="Permalink to this headline">¶</a></h2>
<ul class="simple">
<li><a class="reference internal" href="common_functions.html#is_php" title="is_php"><tt class="xref php php-func docutils literal"><span class="pre">is_php()</span></tt></a></li>
<li><a class="reference internal" href="common_functions.html#is_really_writable" title="is_really_writable"><tt class="xref php php-func docutils literal"><span class="pre">is_really_writable()</span></tt></a></li>
<li><tt class="docutils literal"><span class="pre">load_class()</span></tt></li>
<li><tt class="docutils literal"><span class="pre">is_loaded()</span></tt></li>
<li><tt class="docutils literal"><span class="pre">get_config()</span></tt></li>
<li><a class="reference internal" href="common_functions.html#config_item" title="config_item"><tt class="xref php php-func docutils literal"><span class="pre">config_item()</span></tt></a></li>
<li><a class="reference internal" href="errors.html#show_error" title="show_error"><tt class="xref php php-func docutils literal"><span class="pre">show_error()</span></tt></a></li>
<li><a class="reference internal" href="errors.html#show_404" title="show_404"><tt class="xref php php-func docutils literal"><span class="pre">show_404()</span></tt></a></li>
<li><a class="reference internal" href="errors.html#log_message" title="log_message"><tt class="xref php php-func docutils literal"><span class="pre">log_message()</span></tt></a></li>
<li><a class="reference internal" href="common_functions.html#set_status_header" title="set_status_header"><tt class="xref php php-func docutils literal"><span class="pre">set_status_header()</span></tt></a></li>
<li><a class="reference internal" href="common_functions.html#get_mimes" title="get_mimes"><tt class="xref php php-func docutils literal"><span class="pre">get_mimes()</span></tt></a></li>
<li><a class="reference internal" href="common_functions.html#html_escape" title="html_escape"><tt class="xref php php-func docutils literal"><span class="pre">html_escape()</span></tt></a></li>
<li><a class="reference internal" href="common_functions.html#remove_invisible_characters" title="remove_invisible_characters"><tt class="xref php php-func docutils literal"><span class="pre">remove_invisible_characters()</span></tt></a></li>
<li><a class="reference internal" href="common_functions.html#is_https" title="is_https"><tt class="xref php php-func docutils literal"><span class="pre">is_https()</span></tt></a></li>
<li><a class="reference internal" href="common_functions.html#function_usable" title="function_usable"><tt class="xref php php-func docutils literal"><span class="pre">function_usable()</span></tt></a></li>
<li><a class="reference internal" href="ancillary_classes.html#get_instance" title="get_instance"><tt class="xref php php-func docutils literal"><span class="pre">get_instance()</span></tt></a></li>
<li><tt class="docutils literal"><span class="pre">_error_handler()</span></tt></li>
<li><tt class="docutils literal"><span class="pre">_exception_handler()</span></tt></li>
<li><tt class="docutils literal"><span class="pre">_stringify_attributes()</span></tt></li>
</ul>
</div>
<div class="section" id="variables">
<h2>Variables<a class="headerlink" href="#variables" title="Permalink to this headline">¶</a></h2>
<ul class="simple">
<li><tt class="docutils literal"><span class="pre">$config</span></tt></li>
<li><tt class="docutils literal"><span class="pre">$db</span></tt></li>
<li><tt class="docutils literal"><span class="pre">$lang</span></tt></li>
</ul>
</div>
<div class="section" id="constants">
<h2>Constants<a class="headerlink" href="#constants" title="Permalink to this headline">¶</a></h2>
<ul class="simple">
<li>ENVIRONMENT</li>
<li>FCPATH</li>
<li>SELF</li>
<li>BASEPATH</li>
<li>APPPATH</li>
<li>VIEWPATH</li>
<li>CI_VERSION</li>
<li>MB_ENABLED</li>
<li>ICONV_ENABLED</li>
<li>UTF8_ENABLED</li>
<li>FILE_READ_MODE</li>
<li>FILE_WRITE_MODE</li>
<li>DIR_READ_MODE</li>
<li>DIR_WRITE_MODE</li>
<li>FOPEN_READ</li>
<li>FOPEN_READ_WRITE</li>
<li>FOPEN_WRITE_CREATE_DESTRUCTIVE</li>
<li>FOPEN_READ_WRITE_CREATE_DESTRUCTIVE</li>
<li>FOPEN_WRITE_CREATE</li>
<li>FOPEN_READ_WRITE_CREATE</li>
<li>FOPEN_WRITE_CREATE_STRICT</li>
<li>FOPEN_READ_WRITE_CREATE_STRICT</li>
<li>SHOW_DEBUG_BACKTRACE</li>
<li>EXIT_SUCCESS</li>
<li>EXIT_ERROR</li>
<li>EXIT_CONFIG</li>
<li>EXIT_UNKNOWN_FILE</li>
<li>EXIT_UNKNOWN_CLASS</li>
<li>EXIT_UNKNOWN_METHOD</li>
<li>EXIT_USER_INPUT</li>
<li>EXIT_DATABASE</li>
<li>EXIT__AUTO_MIN</li>
<li>EXIT__AUTO_MAX</li>
</ul>
</div>
</div>
<div class="section" id="views">
<h1>Views<a class="headerlink" href="#views" title="Permalink to this headline">¶</a></h1>
<p>A view is simply a web page, or a page fragment, like a header, footer,
sidebar, etc. In fact, views can flexibly be embedded within other views
(within other views, etc., etc.) if you need this type of hierarchy.</p>
<p>Views are never called directly, they must be loaded by a
<a class="reference internal" href="controllers.html"><em>controller</em></a>. Remember that in an MVC framework, the
Controller acts as the traffic cop, so it is responsible for fetching a
particular view. If you have not read the
<a class="reference internal" href="controllers.html"><em>Controllers</em></a> page you should do so before
continuing.</p>
<p>Using the example controller you created in the
<a class="reference internal" href="controllers.html"><em>controller</em></a> page, let’s add a view to it.</p>
<div class="section" id="creating-a-view">
<h2>Creating a View<a class="headerlink" href="#creating-a-view" title="Permalink to this headline">¶</a></h2>
<p>Using your text editor, create a file called blogview.php, and put this
in it:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;</span><span class="nx">html</span><span class="o">&gt;</span>
<span class="o">&lt;</span><span class="nx">head</span><span class="o">&gt;</span>
        <span class="o">&lt;</span><span class="nx">title</span><span class="o">&gt;</span><span class="nx">My</span> <span class="nx">Blog</span><span class="o">&lt;/</span><span class="nx">title</span><span class="o">&gt;</span>
<span class="o">&lt;/</span><span class="nx">head</span><span class="o">&gt;</span>
<span class="o">&lt;</span><span class="nx">body</span><span class="o">&gt;</span>
        <span class="o">&lt;</span><span class="nx">h1</span><span class="o">&gt;</span><span class="nx">Welcome</span> <span class="nx">to</span> <span class="nx">my</span> <span class="nx">Blog</span><span class="o">!&lt;/</span><span class="nx">h1</span><span class="o">&gt;</span>
<span class="o">&lt;/</span><span class="nx">body</span><span class="o">&gt;</span>
<span class="o">&lt;/</span><span class="nx">html</span><span class="o">&gt;</span>
</pre></div>
</div>
<p>Then save the file in your <em>application/views/</em> directory.</p>
</div>
<div class="section" id="loading-a-view">
<h2>Loading a View<a class="headerlink" href="#loading-a-view" title="Permalink to this headline">¶</a></h2>
<p>To load a particular view file you will use the following method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'name'</span><span class="p">);</span>
</pre></div>
</div>
<p>Where name is the name of your view file.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">The .php file extension does not need to be specified
unless you use something other than .php.</p>
</div>
<p>Now, open the controller file you made earlier called Blog.php, and
replace the echo statement with the view loading method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">Blog</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">index</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'blogview'</span><span class="p">);</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>If you visit your site using the URL you did earlier you should see your
new view. The URL was similar to this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">index</span><span class="o">.</span><span class="nx">php</span><span class="o">/</span><span class="nx">blog</span><span class="o">/</span>
</pre></div>
</div>
</div>
<div class="section" id="loading-multiple-views">
<h2>Loading multiple views<a class="headerlink" href="#loading-multiple-views" title="Permalink to this headline">¶</a></h2>
<p>CodeIgniter will intelligently handle multiple calls to
<tt class="docutils literal"><span class="pre">$this-&gt;load-&gt;view()</span></tt> from within a controller. If more than one call
happens they will be appended together. For example, you may wish to
have a header view, a menu view, a content view, and a footer view. That
might look something like this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>

<span class="k">class</span> <span class="nc">Page</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">index</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$data</span><span class="p">[</span><span class="s1">'page_title'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'Your title'</span><span class="p">;</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'header'</span><span class="p">);</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'menu'</span><span class="p">);</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'content'</span><span class="p">,</span> <span class="nv">$data</span><span class="p">);</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'footer'</span><span class="p">);</span>
        <span class="p">}</span>

<span class="p">}</span>
</pre></div>
</div>
<p>In the example above, we are using “dynamically added data”, which you
will see below.</p>
</div>
<div class="section" id="storing-views-within-sub-directories">
<h2>Storing Views within Sub-directories<a class="headerlink" href="#storing-views-within-sub-directories" title="Permalink to this headline">¶</a></h2>
<p>Your view files can also be stored within sub-directories if you prefer
that type of organization. When doing so you will need to include the
directory name loading the view. Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'directory_name/file_name'</span><span class="p">);</span>
</pre></div>
</div>
</div>
<div class="section" id="adding-dynamic-data-to-the-view">
<h2>Adding Dynamic Data to the View<a class="headerlink" href="#adding-dynamic-data-to-the-view" title="Permalink to this headline">¶</a></h2>
<p>Data is passed from the controller to the view by way of an <strong>array</strong> or
an <strong>object</strong> in the second parameter of the view loading method. Here
is an example using an array:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$data</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span>
        <span class="s1">'title'</span> <span class="o">=&gt;</span> <span class="s1">'My Title'</span><span class="p">,</span>
        <span class="s1">'heading'</span> <span class="o">=&gt;</span> <span class="s1">'My Heading'</span><span class="p">,</span>
        <span class="s1">'message'</span> <span class="o">=&gt;</span> <span class="s1">'My Message'</span>
<span class="p">);</span>

<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'blogview'</span><span class="p">,</span> <span class="nv">$data</span><span class="p">);</span>
</pre></div>
</div>
<p>And here’s an example using an object:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$data</span> <span class="o">=</span> <span class="k">new</span> <span class="nx">Someclass</span><span class="p">();</span>
<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'blogview'</span><span class="p">,</span> <span class="nv">$data</span><span class="p">);</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">If you use an object, the class variables will be turned
into array elements.</p>
</div>
<p>Let’s try it with your controller file. Open it add this code:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">Blog</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">index</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$data</span><span class="p">[</span><span class="s1">'title'</span><span class="p">]</span> <span class="o">=</span> <span class="s2">"My Real Title"</span><span class="p">;</span>
                <span class="nv">$data</span><span class="p">[</span><span class="s1">'heading'</span><span class="p">]</span> <span class="o">=</span> <span class="s2">"My Real Heading"</span><span class="p">;</span>

                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'blogview'</span><span class="p">,</span> <span class="nv">$data</span><span class="p">);</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Now open your view file and change the text to variables that correspond
to the array keys in your data:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;</span><span class="nx">html</span><span class="o">&gt;</span>
<span class="o">&lt;</span><span class="nx">head</span><span class="o">&gt;</span>
        <span class="o">&lt;</span><span class="nx">title</span><span class="o">&gt;&lt;?</span><span class="nx">php</span> <span class="k">echo</span> <span class="nv">$title</span><span class="p">;</span><span class="cp">?&gt;</span><span class="nt">&lt;/title&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
        <span class="nt">&lt;h1&gt;</span><span class="cp">&lt;?php</span> <span class="k">echo</span> <span class="nv">$heading</span><span class="p">;</span><span class="cp">?&gt;</span><span class="nt">&lt;/h1&gt;</span>
<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>
</div>
<p>Then load the page at the URL you’ve been using and you should see the
variables replaced.</p>
</div>
<div class="section" id="creating-loops">
<h2>Creating Loops<a class="headerlink" href="#creating-loops" title="Permalink to this headline">¶</a></h2>
<p>The data array you pass to your view files is not limited to simple
variables. You can pass multi dimensional arrays, which can be looped to
generate multiple rows. For example, if you pull data from your database
it will typically be in the form of a multi-dimensional array.</p>
<p>Here’s a simple example. Add this to your controller:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="k">class</span> <span class="nc">Blog</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">index</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$data</span><span class="p">[</span><span class="s1">'todo_list'</span><span class="p">]</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span><span class="s1">'Clean House'</span><span class="p">,</span> <span class="s1">'Call Mom'</span><span class="p">,</span> <span class="s1">'Run Errands'</span><span class="p">);</span>

                <span class="nv">$data</span><span class="p">[</span><span class="s1">'title'</span><span class="p">]</span> <span class="o">=</span> <span class="s2">"My Real Title"</span><span class="p">;</span>
                <span class="nv">$data</span><span class="p">[</span><span class="s1">'heading'</span><span class="p">]</span> <span class="o">=</span> <span class="s2">"My Real Heading"</span><span class="p">;</span>

                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'blogview'</span><span class="p">,</span> <span class="nv">$data</span><span class="p">);</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Now open your view file and create a loop:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;</span><span class="nx">html</span><span class="o">&gt;</span>
<span class="o">&lt;</span><span class="nx">head</span><span class="o">&gt;</span>
        <span class="o">&lt;</span><span class="nx">title</span><span class="o">&gt;&lt;?</span><span class="nx">php</span> <span class="k">echo</span> <span class="nv">$title</span><span class="p">;</span><span class="cp">?&gt;</span><span class="nt">&lt;/title&gt;</span>
<span class="nt">&lt;/head&gt;</span>
<span class="nt">&lt;body&gt;</span>
        <span class="nt">&lt;h1&gt;</span><span class="cp">&lt;?php</span> <span class="k">echo</span> <span class="nv">$heading</span><span class="p">;</span><span class="cp">?&gt;</span><span class="nt">&lt;/h1&gt;</span>

        <span class="nt">&lt;h3&gt;</span>My Todo List<span class="nt">&lt;/h3&gt;</span>

        <span class="nt">&lt;ul&gt;</span>
        <span class="cp">&lt;?php</span> <span class="k">foreach</span> <span class="p">(</span><span class="nv">$todo_list</span> <span class="k">as</span> <span class="nv">$item</span><span class="p">)</span><span class="o">:?&gt;</span>

                <span class="o">&lt;</span><span class="nx">li</span><span class="o">&gt;&lt;?</span><span class="nx">php</span> <span class="k">echo</span> <span class="nv">$item</span><span class="p">;</span><span class="cp">?&gt;</span><span class="nt">&lt;/li&gt;</span>

        <span class="cp">&lt;?php</span> <span class="k">endforeach</span><span class="p">;</span><span class="cp">?&gt;</span>
        <span class="nt">&lt;/ul&gt;</span>

<span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">You’ll notice that in the example above we are using PHP’s
alternative syntax. If you are not familiar with it you can read about
it <a class="reference internal" href="alternative_php.html"><em>here</em></a>.</p>
</div>
</div>
<div class="section" id="returning-views-as-data">
<h2>Returning views as data<a class="headerlink" href="#returning-views-as-data" title="Permalink to this headline">¶</a></h2>
<p>There is a third <strong>optional</strong> parameter lets you change the behavior of
the method so that it returns data as a string rather than sending it
to your browser. This can be useful if you want to process the data in
some way. If you set the parameter to TRUE (boolean) it will return
data. The default behavior is false, which sends it to your browser.
Remember to assign it to a variable if you want the data returned:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$string</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'myfile'</span><span class="p">,</span> <span class="s1">''</span><span class="p">,</span> <span class="k">TRUE</span><span class="p">);</span>
</pre></div>
</div>
</div>
</div>
<div class="section" id="models">
<h1><a class="toc-backref" href="#id1">Models</a><a class="headerlink" href="#models" title="Permalink to this headline">¶</a></h1>
<p>Models are <strong>optionally</strong> available for those who want to use a more
traditional MVC approach.</p>
<div class="contents topic" id="page-contents">
<p class="topic-title first">Page Contents</p>
<ul class="simple">
<li><a class="reference internal" href="#models" id="id1">Models</a><ul>
<li><a class="reference internal" href="#what-is-a-model" id="id2">What is a Model?</a></li>
<li><a class="reference internal" href="#anatomy-of-a-model" id="id3">Anatomy of a Model</a></li>
<li><a class="reference internal" href="#loading-a-model" id="id4">Loading a Model</a></li>
<li><a class="reference internal" href="#auto-loading-models" id="id5">Auto-loading Models</a></li>
<li><a class="reference internal" href="#connecting-to-your-database" id="id6">Connecting to your Database</a></li>
</ul>
</li>
</ul>
</div>
<div class="section" id="what-is-a-model">
<h2><a class="toc-backref" href="#id2">What is a Model?</a><a class="headerlink" href="#what-is-a-model" title="Permalink to this headline">¶</a></h2>
<p>Models are PHP classes that are designed to work with information in
your database. For example, let’s say you use CodeIgniter to manage a
blog. You might have a model class that contains functions to insert,
update, and retrieve your blog data. Here is an example of what such a
model class might look like:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">Blog_model</span> <span class="k">extends</span> <span class="nx">CI_Model</span> <span class="p">{</span>

        <span class="k">public</span> <span class="nv">$title</span><span class="p">;</span>
        <span class="k">public</span> <span class="nv">$content</span><span class="p">;</span>
        <span class="k">public</span> <span class="nv">$date</span><span class="p">;</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="c1">// Call the CI_Model constructor</span>
                <span class="k">parent</span><span class="o">::</span><span class="na">__construct</span><span class="p">();</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">get_last_ten_entries</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$query</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">db</span><span class="o">-&gt;</span><span class="na">get</span><span class="p">(</span><span class="s1">'entries'</span><span class="p">,</span> <span class="mi">10</span><span class="p">);</span>
                <span class="k">return</span> <span class="nv">$query</span><span class="o">-&gt;</span><span class="na">result</span><span class="p">();</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">insert_entry</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">title</span>    <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">'title'</span><span class="p">];</span> <span class="c1">// please read the below note</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">content</span>  <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">'content'</span><span class="p">];</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">date</span>     <span class="o">=</span> <span class="nb">time</span><span class="p">();</span>

                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">db</span><span class="o">-&gt;</span><span class="na">insert</span><span class="p">(</span><span class="s1">'entries'</span><span class="p">,</span> <span class="nv">$this</span><span class="p">);</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">update_entry</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">title</span>    <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">'title'</span><span class="p">];</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">content</span>  <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">'content'</span><span class="p">];</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">date</span>     <span class="o">=</span> <span class="nb">time</span><span class="p">();</span>

                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">db</span><span class="o">-&gt;</span><span class="na">update</span><span class="p">(</span><span class="s1">'entries'</span><span class="p">,</span> <span class="nv">$this</span><span class="p">,</span> <span class="k">array</span><span class="p">(</span><span class="s1">'id'</span> <span class="o">=&gt;</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">'id'</span><span class="p">]));</span>
        <span class="p">}</span>

<span class="p">}</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">The methods in the above example use the <a class="reference internal" href="../user_guide/database/query_builder.html"><em>Query Builder</em></a> database methods.</p>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">For the sake of simplicity in this example we’re using <tt class="docutils literal"><span class="pre">$_POST</span></tt>
directly. This is generally bad practice, and a more common approach
would be to use the <a class="reference internal" href="../user_guide/libraries/input.html"><em>Input Library</em></a>
<tt class="docutils literal"><span class="pre">$this-&gt;input-&gt;post('title')</span></tt>.</p>
</div>
</div>
<div class="section" id="anatomy-of-a-model">
<h2><a class="toc-backref" href="#id3">Anatomy of a Model</a><a class="headerlink" href="#anatomy-of-a-model" title="Permalink to this headline">¶</a></h2>
<p>Model classes are stored in your <strong>application/models/</strong> directory.
They can be nested within sub-directories if you want this type of
organization.</p>
<p>The basic prototype for a model class is this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">Model_name</span> <span class="k">extends</span> <span class="nx">CI_Model</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">parent</span><span class="o">::</span><span class="na">__construct</span><span class="p">();</span>
        <span class="p">}</span>

<span class="p">}</span>
</pre></div>
</div>
<p>Where <strong>Model_name</strong> is the name of your class. Class names <strong>must</strong> have
the first letter capitalized with the rest of the name lowercase. Make
sure your class extends the base Model class.</p>
<p>The file name must match the class name. For example, if this is your class:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">User_model</span> <span class="k">extends</span> <span class="nx">CI_Model</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">parent</span><span class="o">::</span><span class="na">__construct</span><span class="p">();</span>
        <span class="p">}</span>

<span class="p">}</span>
</pre></div>
</div>
<p>Your file will be this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">application</span><span class="o">/</span><span class="nx">models</span><span class="o">/</span><span class="nx">User_model</span><span class="o">.</span><span class="nx">php</span>
</pre></div>
</div>
</div>
<div class="section" id="loading-a-model">
<h2><a class="toc-backref" href="#id4">Loading a Model</a><a class="headerlink" href="#loading-a-model" title="Permalink to this headline">¶</a></h2>
<p>Your models will typically be loaded and called from within your
<a class="reference internal" href="controllers.html"><em>controller</em></a> methods. To load a model you will use
the following method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">model</span><span class="p">(</span><span class="s1">'model_name'</span><span class="p">);</span>
</pre></div>
</div>
<p>If your model is located in a sub-directory, include the relative path
from your models directory. For example, if you have a model located at
<em>application/models/blog/Queries.php</em> you’ll load it using:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">model</span><span class="p">(</span><span class="s1">'blog/queries'</span><span class="p">);</span>
</pre></div>
</div>
<p>Once loaded, you will access your model methods using an object with the
same name as your class:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">model</span><span class="p">(</span><span class="s1">'model_name'</span><span class="p">);</span>

<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">model_name</span><span class="o">-&gt;</span><span class="na">method</span><span class="p">();</span>
</pre></div>
</div>
<p>If you would like your model assigned to a different object name you can
specify it via the second parameter of the loading method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">model</span><span class="p">(</span><span class="s1">'model_name'</span><span class="p">,</span> <span class="s1">'foobar'</span><span class="p">);</span>

<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">foobar</span><span class="o">-&gt;</span><span class="na">method</span><span class="p">();</span>
</pre></div>
</div>
<p>Here is an example of a controller, that loads a model, then serves a
view:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">Blog_controller</span> <span class="k">extends</span> <span class="nx">CI_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">blog</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">model</span><span class="p">(</span><span class="s1">'blog'</span><span class="p">);</span>

                <span class="nv">$data</span><span class="p">[</span><span class="s1">'query'</span><span class="p">]</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">blog</span><span class="o">-&gt;</span><span class="na">get_last_ten_entries</span><span class="p">();</span>

                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'blog'</span><span class="p">,</span> <span class="nv">$data</span><span class="p">);</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
</div>
<div class="section" id="auto-loading-models">
<h2><a class="toc-backref" href="#id5">Auto-loading Models</a><a class="headerlink" href="#auto-loading-models" title="Permalink to this headline">¶</a></h2>
<p>If you find that you need a particular model globally throughout your
application, you can tell CodeIgniter to auto-load it during system
initialization. This is done by opening the
<strong>application/config/autoload.php</strong> file and adding the model to the
autoload array.</p>
</div>
<div class="section" id="connecting-to-your-database">
<h2><a class="toc-backref" href="#id6">Connecting to your Database</a><a class="headerlink" href="#connecting-to-your-database" title="Permalink to this headline">¶</a></h2>
<p>When a model is loaded it does <strong>NOT</strong> connect automatically to your
database. The following options for connecting are available to you:</p>
<ul>
<li><p class="first">You can connect using the standard database methods <a class="reference internal" href="../user_guide/database/connecting.html"><em>described
here</em></a>, either from within your
Controller class or your Model class.</p>
</li>
<li><p class="first">You can tell the model loading method to auto-connect by passing
TRUE (boolean) via the third parameter, and connectivity settings,
as defined in your database config file will be used:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">model</span><span class="p">(</span><span class="s1">'model_name'</span><span class="p">,</span> <span class="s1">''</span><span class="p">,</span> <span class="k">TRUE</span><span class="p">);</span>
</pre></div>
</div>
</li>
<li><p class="first">You can manually pass database connectivity settings via the third
parameter:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$config</span><span class="p">[</span><span class="s1">'hostname'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'localhost'</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'username'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'myusername'</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'password'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'mypassword'</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'database'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'mydatabase'</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'dbdriver'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'mysqli'</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'dbprefix'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">''</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'pconnect'</span><span class="p">]</span> <span class="o">=</span> <span class="k">FALSE</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'db_debug'</span><span class="p">]</span> <span class="o">=</span> <span class="k">TRUE</span><span class="p">;</span>

<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">model</span><span class="p">(</span><span class="s1">'model_name'</span><span class="p">,</span> <span class="s1">''</span><span class="p">,</span> <span class="nv">$config</span><span class="p">);</span>
</pre></div>
</div>
</li>
</ul>
</div>
</div>
<div class="section" id="helper-functions">
<h1>Helper Functions<a class="headerlink" href="#helper-functions" title="Permalink to this headline">¶</a></h1>
<p>Helpers, as the name suggests, help you with tasks. Each helper file is
simply a collection of functions in a particular category. There are <strong>URL
Helpers</strong>, that assist in creating links, there are Form Helpers that help
you create form elements, <strong>Text Helpers</strong> perform various text formatting
routines, <strong>Cookie Helpers</strong> set and read cookies, File Helpers help you
deal with files, etc.</p>
<p>Unlike most other systems in CodeIgniter, Helpers are not written in an
Object Oriented format. They are simple, procedural functions. Each
helper function performs one specific task, with no dependence on other
functions.</p>
<p>CodeIgniter does not load Helper Files by default, so the first step in
using a Helper is to load it. Once loaded, it becomes globally available
in your <a class="reference internal" href="controllers.html"><em>controller</em></a> and
<a class="reference internal" href="views.html"><em>views</em></a>.</p>
<p>Helpers are typically stored in your <strong>system/helpers</strong>, or
<strong>application/helpers directory</strong>. CodeIgniter will look first in your
<strong>application/helpers directory</strong>. If the directory does not exist or the
specified helper is not located there CI will instead look in your
global <em>system/helpers/</em> directory.</p>
<div class="section" id="loading-a-helper">
<h2>Loading a Helper<a class="headerlink" href="#loading-a-helper" title="Permalink to this headline">¶</a></h2>
<p>Loading a helper file is quite simple using the following method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span><span class="s1">'name'</span><span class="p">);</span>
</pre></div>
</div>
<p>Where <strong>name</strong> is the file name of the helper, without the .php file
extension or the “helper” part.</p>
<p>For example, to load the <strong>URL Helper</strong> file, which is named
<strong>url_helper.php</strong>, you would do this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span><span class="s1">'url'</span><span class="p">);</span>
</pre></div>
</div>
<p>A helper can be loaded anywhere within your controller methods (or
even within your View files, although that’s not a good practice), as
long as you load it before you use it. You can load your helpers in your
controller constructor so that they become available automatically in
any function, or you can load a helper in a specific function that needs
it.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">The Helper loading method above does not return a value, so
don’t try to assign it to a variable. Just use it as shown.</p>
</div>
</div>
<div class="section" id="loading-multiple-helpers">
<h2>Loading Multiple Helpers<a class="headerlink" href="#loading-multiple-helpers" title="Permalink to this headline">¶</a></h2>
<p>If you need to load more than one helper you can specify them in an
array, like this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span>
        <span class="k">array</span><span class="p">(</span><span class="s1">'helper1'</span><span class="p">,</span> <span class="s1">'helper2'</span><span class="p">,</span> <span class="s1">'helper3'</span><span class="p">)</span>
<span class="p">);</span>
</pre></div>
</div>
</div>
<div class="section" id="auto-loading-helpers">
<h2>Auto-loading Helpers<a class="headerlink" href="#auto-loading-helpers" title="Permalink to this headline">¶</a></h2>
<p>If you find that you need a particular helper globally throughout your
application, you can tell CodeIgniter to auto-load it during system
initialization. This is done by opening the <strong>application/config/autoload.php</strong>
file and adding the helper to the autoload array.</p>
</div>
<div class="section" id="using-a-helper">
<h2>Using a Helper<a class="headerlink" href="#using-a-helper" title="Permalink to this headline">¶</a></h2>
<p>Once you’ve loaded the Helper File containing the function you intend to
use, you’ll call it the way you would a standard PHP function.</p>
<p>For example, to create a link using the <tt class="docutils literal"><span class="pre">anchor()</span></tt> function in one of
your view files you would do this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span> <span class="k">echo</span> <span class="nx">anchor</span><span class="p">(</span><span class="s1">'blog/comments'</span><span class="p">,</span> <span class="s1">'Click Here'</span><span class="p">);</span><span class="cp">?&gt;</span>
</pre></div>
</div>
<p>Where “Click Here” is the name of the link, and “blog/comments” is the
URI to the controller/method you wish to link to.</p>
</div>
<div class="section" id="extending-helpers">
<h2>“Extending” Helpers<a class="headerlink" href="#extending-helpers" title="Permalink to this headline">¶</a></h2>
<p>To “extend” Helpers, create a file in your <strong>application/helpers/</strong> folder
with an identical name to the existing Helper, but prefixed with <strong>MY_</strong>
(this item is configurable. See below.).</p>
<p>If all you need to do is add some functionality to an existing helper -
perhaps add a function or two, or change how a particular helper
function operates - then it’s overkill to replace the entire helper with
your version. In this case it’s better to simply “extend” the Helper.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">The term “extend” is used loosely since Helper functions are
procedural and discrete and cannot be extended in the traditional
programmatic sense. Under the hood, this gives you the ability to
add to or or to replace the functions a Helper provides.</p>
</div>
<p>For example, to extend the native <strong>Array Helper</strong> you’ll create a file
named <strong>application/helpers/MY_array_helper.php</strong>, and add or override
functions:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="c1">// any_in_array() is not in the Array Helper, so it defines a new function</span>
<span class="k">function</span> <span class="nf">any_in_array</span><span class="p">(</span><span class="nv">$needle</span><span class="p">,</span> <span class="nv">$haystack</span><span class="p">)</span>
<span class="p">{</span>
        <span class="nv">$needle</span> <span class="o">=</span> <span class="nb">is_array</span><span class="p">(</span><span class="nv">$needle</span><span class="p">)</span> <span class="o">?</span> <span class="nv">$needle</span> <span class="o">:</span> <span class="k">array</span><span class="p">(</span><span class="nv">$needle</span><span class="p">);</span>

        <span class="k">foreach</span> <span class="p">(</span><span class="nv">$needle</span> <span class="k">as</span> <span class="nv">$item</span><span class="p">)</span>
        <span class="p">{</span>
                <span class="k">if</span> <span class="p">(</span><span class="nb">in_array</span><span class="p">(</span><span class="nv">$item</span><span class="p">,</span> <span class="nv">$haystack</span><span class="p">))</span>
                <span class="p">{</span>
                        <span class="k">return</span> <span class="k">TRUE</span><span class="p">;</span>
                <span class="p">}</span>
        <span class="p">}</span>

        <span class="k">return</span> <span class="k">FALSE</span><span class="p">;</span>
<span class="p">}</span>

<span class="c1">// random_element() is included in Array Helper, so it overrides the native function</span>
<span class="k">function</span> <span class="nf">random_element</span><span class="p">(</span><span class="nv">$array</span><span class="p">)</span>
<span class="p">{</span>
        <span class="nb">shuffle</span><span class="p">(</span><span class="nv">$array</span><span class="p">);</span>
        <span class="k">return</span> <span class="nb">array_pop</span><span class="p">(</span><span class="nv">$array</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>
</div>
<div class="section" id="setting-your-own-prefix">
<h3>Setting Your Own Prefix<a class="headerlink" href="#setting-your-own-prefix" title="Permalink to this headline">¶</a></h3>
<p>The filename prefix for “extending” Helpers is the same used to extend
libraries and core classes. To set your own prefix, open your
<strong>application/config/config.php</strong> file and look for this item:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$config</span><span class="p">[</span><span class="s1">'subclass_prefix'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'MY_'</span><span class="p">;</span>
</pre></div>
</div>
<p>Please note that all native CodeIgniter libraries are prefixed with <strong>CI_</strong>
so DO NOT use that as your prefix.</p>
</div>
</div>
<div class="section" id="now-what">
<h2>Now What?<a class="headerlink" href="#now-what" title="Permalink to this headline">¶</a></h2>
<p>In the Table of Contents you’ll find a list of all the available Helper
Files. Browse each one to see what they do.</p>
</div>
</div>
<div class="section" id="using-codeigniter-libraries">
<h1>Using CodeIgniter Libraries<a class="headerlink" href="#using-codeigniter-libraries" title="Permalink to this headline">¶</a></h1>
<p>All of the available libraries are located in your <em>system/libraries/</em>
directory. In most cases, to use one of these classes involves initializing
it within a <a class="reference internal" href="controllers.html"><em>controller</em></a> using the following
initialization method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'class_name'</span><span class="p">);</span>
</pre></div>
</div>
<p>Where ‘class_name’ is the name of the class you want to invoke. For
example, to load the <a class="reference internal" href="../user_guide/libraries/form_validation.html"><em>Form Validation Library</em></a> you would do this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'form_validation'</span><span class="p">);</span>
</pre></div>
</div>
<p>Once initialized you can use it as indicated in the user guide page
corresponding to that class.</p>
<p>Additionally, multiple libraries can be loaded at the same time by
passing an array of libraries to the load method.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="k">array</span><span class="p">(</span><span class="s1">'email'</span><span class="p">,</span> <span class="s1">'table'</span><span class="p">));</span>
</pre></div>
</div>
<div class="section" id="creating-your-own-libraries">
<h2>Creating Your Own Libraries<a class="headerlink" href="#creating-your-own-libraries" title="Permalink to this headline">¶</a></h2>
<p>Please read the section of the user guide that discusses how to
<a class="reference internal" href="creating_libraries.html"><em>create your own libraries</em></a>.</p>
</div>
</div>
<div class="section" id="creating-libraries">
<h1>Creating Libraries<a class="headerlink" href="#creating-libraries" title="Permalink to this headline">¶</a></h1>
<p>When we use the term “Libraries” we are normally referring to the
classes that are located in the libraries directory and described in the
Class Reference of this user guide. In this case, however, we will
instead describe how you can create your own libraries within your
application/libraries directory in order to maintain separation between
your local resources and the global framework resources.</p>
<p>As an added bonus, CodeIgniter permits your libraries to extend native
classes if you simply need to add some functionality to an existing
library. Or you can even replace native libraries just by placing
identically named versions in your <em>application/libraries</em> directory.</p>
<p>In summary:</p>
<ul class="simple">
<li>You can create entirely new libraries.</li>
<li>You can extend native libraries.</li>
<li>You can replace native libraries.</li>
</ul>
<p>The page below explains these three concepts in detail.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">The Database classes can not be extended or replaced with your
own classes. All other classes are able to be replaced/extended.</p>
</div>
<div class="section" id="storage">
<h2>Storage<a class="headerlink" href="#storage" title="Permalink to this headline">¶</a></h2>
<p>Your library classes should be placed within your <em>application/libraries</em>
directory, as this is where CodeIgniter will look for them when they are
initialized.</p>
</div>
<div class="section" id="naming-conventions">
<h2>Naming Conventions<a class="headerlink" href="#naming-conventions" title="Permalink to this headline">¶</a></h2>
<ul class="simple">
<li>File names must be capitalized. For example: Myclass.php</li>
<li>Class declarations must be capitalized. For example: class Myclass</li>
<li>Class names and file names must match.</li>
</ul>
</div>
<div class="section" id="the-class-file">
<h2>The Class File<a class="headerlink" href="#the-class-file" title="Permalink to this headline">¶</a></h2>
<p>Classes should have this basic prototype:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span>
<span class="nb">defined</span><span class="p">(</span><span class="s1">'BASEPATH'</span><span class="p">)</span> <span class="k">OR</span> <span class="k">exit</span><span class="p">(</span><span class="s1">'No direct script access allowed'</span><span class="p">);</span>

<span class="k">class</span> <span class="nc">Someclass</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">some_method</span><span class="p">()</span>
        <span class="p">{</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">We are using the name Someclass purely as an example.</p>
</div>
</div>
<div class="section" id="using-your-class">
<h2>Using Your Class<a class="headerlink" href="#using-your-class" title="Permalink to this headline">¶</a></h2>
<p>From within any of your <a class="reference internal" href="controllers.html"><em>Controller</em></a> methods you
can initialize your class using the standard:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'someclass'</span><span class="p">);</span>
</pre></div>
</div>
<p>Where <em>someclass</em> is the file name, without the ”.php” file extension.
You can submit the file name capitalized or lower case. CodeIgniter
doesn’t care.</p>
<p>Once loaded you can access your class using the lower case version:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">someclass</span><span class="o">-&gt;</span><span class="na">some_method</span><span class="p">();</span><span class="nx">&nbsp;</span> <span class="c1">// Object instances will always be lower case</span>
</pre></div>
</div>
</div>
<div class="section" id="passing-parameters-when-initializing-your-class">
<h2>Passing Parameters When Initializing Your Class<a class="headerlink" href="#passing-parameters-when-initializing-your-class" title="Permalink to this headline">¶</a></h2>
<p>In the library loading method you can dynamically pass data as an
array via the second parameter and it will be passed to your class
constructor:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$params</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span><span class="s1">'type'</span> <span class="o">=&gt;</span> <span class="s1">'large'</span><span class="p">,</span> <span class="s1">'color'</span> <span class="o">=&gt;</span> <span class="s1">'red'</span><span class="p">);</span>

<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'someclass'</span><span class="p">,</span> <span class="nv">$params</span><span class="p">);</span>
</pre></div>
</div>
<p>If you use this feature you must set up your class constructor to expect
data:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span> <span class="nb">defined</span><span class="p">(</span><span class="s1">'BASEPATH'</span><span class="p">)</span> <span class="k">OR</span> <span class="k">exit</span><span class="p">(</span><span class="s1">'No direct script access allowed'</span><span class="p">);</span>

<span class="k">class</span> <span class="nc">Someclass</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">(</span><span class="nv">$params</span><span class="p">)</span>
        <span class="p">{</span>
                <span class="c1">// Do something with $params</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>You can also pass parameters stored in a config file. Simply create a
config file named identically to the class file name and store it in
your <em>application/config/</em> directory. Note that if you dynamically pass
parameters as described above, the config file option will not be
available.</p>
</div>
<div class="section" id="utilizing-codeigniter-resources-within-your-library">
<h2>Utilizing CodeIgniter Resources within Your Library<a class="headerlink" href="#utilizing-codeigniter-resources-within-your-library" title="Permalink to this headline">¶</a></h2>
<p>To access CodeIgniter’s native resources within your library use the
<tt class="docutils literal"><span class="pre">get_instance()</span></tt> method. This method returns the CodeIgniter super
object.</p>
<p>Normally from within your controller methods you will call any of the
available CodeIgniter methods using the <tt class="docutils literal"><span class="pre">$this</span></tt> construct:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span><span class="s1">'url'</span><span class="p">);</span>
<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'session'</span><span class="p">);</span>
<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">config</span><span class="o">-&gt;</span><span class="na">item</span><span class="p">(</span><span class="s1">'base_url'</span><span class="p">);</span>
<span class="c1">// etc.</span>
</pre></div>
</div>
<p><tt class="docutils literal"><span class="pre">$this</span></tt>, however, only works directly within your controllers, your
models, or your views. If you would like to use CodeIgniter’s classes
from within your own custom classes you can do so as follows:</p>
<p>First, assign the CodeIgniter object to a variable:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$CI</span> <span class="o">=&amp;</span> <span class="nx">get_instance</span><span class="p">();</span>
</pre></div>
</div>
<p>Once you’ve assigned the object to a variable, you’ll use that variable
<em>instead</em> of <tt class="docutils literal"><span class="pre">$this</span></tt>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$CI</span> <span class="o">=&amp;</span> <span class="nx">get_instance</span><span class="p">();</span>

<span class="nv">$CI</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span><span class="s1">'url'</span><span class="p">);</span>
<span class="nv">$CI</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'session'</span><span class="p">);</span>
<span class="nv">$CI</span><span class="o">-&gt;</span><span class="na">config</span><span class="o">-&gt;</span><span class="na">item</span><span class="p">(</span><span class="s1">'base_url'</span><span class="p">);</span>
<span class="c1">// etc.</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p>You’ll notice that the above <tt class="docutils literal"><span class="pre">get_instance()</span></tt> function is being
passed by reference:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$CI</span> <span class="o">=&amp;</span> <span class="nx">get_instance</span><span class="p">();</span>
</pre></div>
</div>
<p class="last">This is very important. Assigning by reference allows you to use the
original CodeIgniter object rather than creating a copy of it.</p>
</div>
<p>However, since a library is a class, it would be better if you
take full advantage of the OOP principles. So, in order to
be able to use the CodeIgniter super-object in all of the class
methods, you’re encouraged to assign it to a property instead:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">Example_library</span> <span class="p">{</span>

        <span class="k">protected</span> <span class="nv">$CI</span><span class="p">;</span>

        <span class="c1">// We'll use a constructor, as you can't directly call a function</span>
        <span class="c1">// from a property definition.</span>
        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="c1">// Assign the CodeIgniter super-object</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">CI</span> <span class="o">=&amp;</span> <span class="nx">get_instance</span><span class="p">();</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">foo</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">CI</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span><span class="s1">'url'</span><span class="p">);</span>
                <span class="nx">redirect</span><span class="p">();</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">bar</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">echo</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">CI</span><span class="o">-&gt;</span><span class="na">config</span><span class="o">-&gt;</span><span class="na">item</span><span class="p">(</span><span class="s1">'base_url'</span><span class="p">);</span>
        <span class="p">}</span>

<span class="p">}</span>
</pre></div>
</div>
</div>
<div class="section" id="replacing-native-libraries-with-your-versions">
<h2>Replacing Native Libraries with Your Versions<a class="headerlink" href="#replacing-native-libraries-with-your-versions" title="Permalink to this headline">¶</a></h2>
<p>Simply by naming your class files identically to a native library will
cause CodeIgniter to use it instead of the native one. To use this
feature you must name the file and the class declaration exactly the
same as the native library. For example, to replace the native Email
library you’ll create a file named <em>application/libraries/Email.php</em>,
and declare your class with:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">CI_Email</span> <span class="p">{</span>

<span class="p">}</span>
</pre></div>
</div>
<p>Note that most native classes are prefixed with CI_.</p>
<p>To load your library you’ll see the standard loading method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'email'</span><span class="p">);</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">At this time the Database classes can not be replaced with
your own versions.</p>
</div>
</div>
<div class="section" id="extending-native-libraries">
<h2>Extending Native Libraries<a class="headerlink" href="#extending-native-libraries" title="Permalink to this headline">¶</a></h2>
<p>If all you need to do is add some functionality to an existing library -
perhaps add a method or two - then it’s overkill to replace the entire
library with your version. In this case it’s better to simply extend the
class. Extending a class is nearly identical to replacing a class with a
couple exceptions:</p>
<ul class="simple">
<li>The class declaration must extend the parent class.</li>
<li>Your new class name and filename must be prefixed with MY_ (this
item is configurable. See below.).</li>
</ul>
<p>For example, to extend the native Email class you’ll create a file named
<em>application/libraries/MY_Email.php</em>, and declare your class with:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">MY_Email</span> <span class="k">extends</span> <span class="nx">CI_Email</span> <span class="p">{</span>

<span class="p">}</span>
</pre></div>
</div>
<p>If you need to use a constructor in your class make sure you
extend the parent constructor:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">MY_Email</span> <span class="k">extends</span> <span class="nx">CI_Email</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">(</span><span class="nv">$config</span> <span class="o">=</span> <span class="k">array</span><span class="p">())</span>
        <span class="p">{</span>
                <span class="k">parent</span><span class="o">::</span><span class="na">__construct</span><span class="p">(</span><span class="nv">$config</span><span class="p">);</span>
        <span class="p">}</span>

<span class="p">}</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Not all of the libraries have the same (or any) parameters
in their constructor. Take a look at the library that you’re
extending first to see how it should be implemented.</p>
</div>
<div class="section" id="loading-your-sub-class">
<h3>Loading Your Sub-class<a class="headerlink" href="#loading-your-sub-class" title="Permalink to this headline">¶</a></h3>
<p>To load your sub-class you’ll use the standard syntax normally used. DO
NOT include your prefix. For example, to load the example above, which
extends the Email class, you will use:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'email'</span><span class="p">);</span>
</pre></div>
</div>
<p>Once loaded you will use the class variable as you normally would for
the class you are extending. In the case of the email class all calls
will use:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">email</span><span class="o">-&gt;</span><span class="na">some_method</span><span class="p">();</span>
</pre></div>
</div>
</div>
<div class="section" id="setting-your-own-prefix">
<h3>Setting Your Own Prefix<a class="headerlink" href="#setting-your-own-prefix" title="Permalink to this headline">¶</a></h3>
<p>To set your own sub-class prefix, open your
<em>application/config/config.php</em> file and look for this item:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$config</span><span class="p">[</span><span class="s1">'subclass_prefix'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'MY_'</span><span class="p">;</span>
</pre></div>
</div>
<p>Please note that all native CodeIgniter libraries are prefixed with CI_
so DO NOT use that as your prefix.</p>
</div>
</div>
</div>
<div class="section" id="using-codeigniter-drivers">
<h1>Using CodeIgniter Drivers<a class="headerlink" href="#using-codeigniter-drivers" title="Permalink to this headline">¶</a></h1>
<p>Drivers are a special type of Library that has a parent class and any
number of potential child classes. Child classes have access to the
parent class, but not their siblings. Drivers provide an elegant syntax
in your <a class="reference internal" href="controllers.html"><em>controllers</em></a> for libraries that benefit
from or require being broken down into discrete classes.</p>
<p>Drivers are found in the <em>system/libraries/</em> directory, in their own
sub-directory which is identically named to the parent library class.
Also inside that directory is a subdirectory named drivers, which
contains all of the possible child class files.</p>
<p>To use a driver you will initialize it within a controller using the
following initialization method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">driver</span><span class="p">(</span><span class="s1">'class_name'</span><span class="p">);</span>
</pre></div>
</div>
<p>Where class name is the name of the driver class you want to invoke. For
example, to load a driver named “Some_parent” you would do this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">driver</span><span class="p">(</span><span class="s1">'some_parent'</span><span class="p">);</span>
</pre></div>
</div>
<p>Methods of that class can then be invoked with:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">some_parent</span><span class="o">-&gt;</span><span class="na">some_method</span><span class="p">();</span>
</pre></div>
</div>
<p>The child classes, the drivers themselves, can then be called directly
through the parent class, without initializing them:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">some_parent</span><span class="o">-&gt;</span><span class="na">child_one</span><span class="o">-&gt;</span><span class="na">some_method</span><span class="p">();</span>
<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">some_parent</span><span class="o">-&gt;</span><span class="na">child_two</span><span class="o">-&gt;</span><span class="na">another_method</span><span class="p">();</span>
</pre></div>
</div>
<div class="section" id="creating-your-own-drivers">
<h2>Creating Your Own Drivers<a class="headerlink" href="#creating-your-own-drivers" title="Permalink to this headline">¶</a></h2>
<p>Please read the section of the user guide that discusses how to <a class="reference internal" href="creating_drivers.html"><em>create
your own drivers</em></a>.</p>
</div>
</div>
<div class="section" id="creating-core-system-classes">
<h1>Creating Core System Classes<a class="headerlink" href="#creating-core-system-classes" title="Permalink to this headline">¶</a></h1>
<p>Every time CodeIgniter runs there are several base classes that are
initialized automatically as part of the core framework. It is possible,
however, to swap any of the core system classes with your own versions
or even extend the core versions.</p>
<p><strong>Most users will never have any need to do this, but the option to
replace or extend them does exist for those who would like to
significantly alter the CodeIgniter core.</strong></p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Messing with a core system class has a lot of implications, so
make sure you know what you are doing before attempting it.</p>
</div>
<div class="section" id="system-class-list">
<h2>System Class List<a class="headerlink" href="#system-class-list" title="Permalink to this headline">¶</a></h2>
<p>The following is a list of the core system files that are invoked every
time CodeIgniter runs:</p>
<ul class="simple">
<li>Benchmark</li>
<li>Config</li>
<li>Controller</li>
<li>Exceptions</li>
<li>Hooks</li>
<li>Input</li>
<li>Language</li>
<li>Loader</li>
<li>Log</li>
<li>Output</li>
<li>Router</li>
<li>Security</li>
<li>URI</li>
<li>Utf8</li>
</ul>
</div>
<div class="section" id="replacing-core-classes">
<h2>Replacing Core Classes<a class="headerlink" href="#replacing-core-classes" title="Permalink to this headline">¶</a></h2>
<p>To use one of your own system classes instead of a default one simply
place your version inside your local <em>application/core/</em> directory:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">application</span><span class="o">/</span><span class="nx">core</span><span class="o">/</span><span class="nx">some_class</span><span class="o">.</span><span class="nx">php</span>
</pre></div>
</div>
<p>If this directory does not exist you can create it.</p>
<p>Any file named identically to one from the list above will be used
instead of the one normally used.</p>
<p>Please note that your class must use CI as a prefix. For example, if
your file is named Input.php the class will be named:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">CI_Input</span> <span class="p">{</span>

<span class="p">}</span>
</pre></div>
</div>
</div>
<div class="section" id="extending-core-class">
<h2>Extending Core Class<a class="headerlink" href="#extending-core-class" title="Permalink to this headline">¶</a></h2>
<p>If all you need to do is add some functionality to an existing library -
perhaps add a method or two - then it’s overkill to replace the entire
library with your version. In this case it’s better to simply extend the
class. Extending a class is nearly identical to replacing a class with a
couple exceptions:</p>
<ul class="simple">
<li>The class declaration must extend the parent class.</li>
<li>Your new class name and filename must be prefixed with MY_ (this
item is configurable. See below.).</li>
</ul>
<p>For example, to extend the native Input class you’ll create a file named
application/core/MY_Input.php, and declare your class with:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">MY_Input</span> <span class="k">extends</span> <span class="nx">CI_Input</span> <span class="p">{</span>

<span class="p">}</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p>If you need to use a constructor in your class make sure you
extend the parent constructor:</p>
<div class="last highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">MY_Input</span> <span class="k">extends</span> <span class="nx">CI_Input</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">parent</span><span class="o">::</span><span class="na">__construct</span><span class="p">();</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
</div>
<p><strong>Tip:</strong> Any functions in your class that are named identically to the
methods in the parent class will be used instead of the native ones
(this is known as “method overriding”). This allows you to substantially
alter the CodeIgniter core.</p>
<p>If you are extending the Controller core class, then be sure to extend
your new class in your application controller’s constructors.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">Welcome</span> <span class="k">extends</span> <span class="nx">MY_Controller</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="k">parent</span><span class="o">::</span><span class="na">__construct</span><span class="p">();</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">index</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">view</span><span class="p">(</span><span class="s1">'welcome_message'</span><span class="p">);</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<div class="section" id="setting-your-own-prefix">
<h3>Setting Your Own Prefix<a class="headerlink" href="#setting-your-own-prefix" title="Permalink to this headline">¶</a></h3>
<p>To set your own sub-class prefix, open your
<em>application/config/config.php</em> file and look for this item:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$config</span><span class="p">[</span><span class="s1">'subclass_prefix'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'MY_'</span><span class="p">;</span>
</pre></div>
</div>
<p>Please note that all native CodeIgniter libraries are prefixed
with CI_ so DO NOT use that as your prefix.</p>
</div>
</div>
</div>
<div class="section" id="creating-ancillary-classes">
<h1>Creating Ancillary Classes<a class="headerlink" href="#creating-ancillary-classes" title="Permalink to this headline">¶</a></h1>
<p>In some cases you may want to develop classes that exist apart from your
controllers but have the ability to utilize all of CodeIgniter’s
resources. This is easily possible as you’ll see.</p>
<div class="section" id="get-instance">
<h2>get_instance()<a class="headerlink" href="#get-instance" title="Permalink to this headline">¶</a></h2>
<dl class="function">
<dt id="get_instance">
<tt class="descname">get_instance</tt><big>(</big><big>)</big><a class="headerlink" href="#get_instance" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Returns:</th><td class="field-body">Reference to your controller’s instance</td>
</tr>
<tr class="field-even field"><th class="field-name">Return type:</th><td class="field-body">CI_Controller</td>
</tr>
</tbody>
</table>
</dd></dl>

<p><strong>Any class that you instantiate within your controller methods can
access CodeIgniter’s native resources</strong> simply by using the
<tt class="docutils literal"><span class="pre">get_instance()</span></tt> function. This function returns the main
CodeIgniter object.</p>
<p>Normally, to call any of the available CodeIgniter methods requires
you to use the <tt class="docutils literal"><span class="pre">$this</span></tt> construct:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span><span class="s1">'url'</span><span class="p">);</span>
<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'session'</span><span class="p">);</span>
<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">config</span><span class="o">-&gt;</span><span class="na">item</span><span class="p">(</span><span class="s1">'base_url'</span><span class="p">);</span>
<span class="c1">// etc.</span>
</pre></div>
</div>
<p><tt class="docutils literal"><span class="pre">$this</span></tt>, however, only works within your controllers, your models,
or your views. If you would like to use CodeIgniter’s classes from
within your own custom classes you can do so as follows:</p>
<p>First, assign the CodeIgniter object to a variable:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$CI</span> <span class="o">=&amp;</span> <span class="nx">get_instance</span><span class="p">();</span>
</pre></div>
</div>
<p>Once you’ve assigned the object to a variable, you’ll use that variable
<em>instead</em> of <tt class="docutils literal"><span class="pre">$this</span></tt>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$CI</span> <span class="o">=&amp;</span> <span class="nx">get_instance</span><span class="p">();</span>

<span class="nv">$CI</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span><span class="s1">'url'</span><span class="p">);</span>
<span class="nv">$CI</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">library</span><span class="p">(</span><span class="s1">'session'</span><span class="p">);</span>
<span class="nv">$CI</span><span class="o">-&gt;</span><span class="na">config</span><span class="o">-&gt;</span><span class="na">item</span><span class="p">(</span><span class="s1">'base_url'</span><span class="p">);</span>
<span class="c1">// etc.</span>
</pre></div>
</div>
<p>If you’ll be using <tt class="docutils literal"><span class="pre">get_instance()</span></tt> inside another class, then it would
be better if you assign it to a property. This way, you won’t need to call
<tt class="docutils literal"><span class="pre">get_instance()</span></tt> in every single method.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">Example</span> <span class="p">{</span>

        <span class="k">protected</span> <span class="nv">$CI</span><span class="p">;</span>

        <span class="c1">// We'll use a constructor, as you can't directly call a function</span>
        <span class="c1">// from a property definition.</span>
        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="c1">// Assign the CodeIgniter super-object</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">CI</span> <span class="o">=&amp;</span> <span class="nx">get_instance</span><span class="p">();</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">foo</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">CI</span><span class="o">-&gt;</span><span class="na">load</span><span class="o">-&gt;</span><span class="na">helper</span><span class="p">(</span><span class="s1">'url'</span><span class="p">);</span>
                <span class="nx">redirect</span><span class="p">();</span>
        <span class="p">}</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">bar</span><span class="p">()</span>
        <span class="p">{</span>
                <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">CI</span><span class="o">-&gt;</span><span class="na">config</span><span class="o">-&gt;</span><span class="na">item</span><span class="p">(</span><span class="s1">'base_url'</span><span class="p">);</span>
        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>In the above example, both methods <tt class="docutils literal"><span class="pre">foo()</span></tt> and <tt class="docutils literal"><span class="pre">bar()</span></tt> will work
after you instantiate the Example class, without the need to call
<tt class="docutils literal"><span class="pre">get_instance()</span></tt> in each of them.</p>
</div>
</div>
<div class="section" id="hooks-extending-the-framework-core">
<h1>Hooks - Extending the Framework Core<a class="headerlink" href="#hooks-extending-the-framework-core" title="Permalink to this headline">¶</a></h1>
<p>CodeIgniter’s Hooks feature provides a means to tap into and modify the
inner workings of the framework without hacking the core files. When
CodeIgniter runs it follows a specific execution process, diagramed in
the <a class="reference internal" href="../user_guide/overview/appflow.html"><em>Application Flow</em></a> page. There may be
instances, however, where you’d like to cause some action to take place
at a particular stage in the execution process. For example, you might
want to run a script right before your controllers get loaded, or right
after, or you might want to trigger one of your own scripts in some
other location.</p>
<div class="section" id="enabling-hooks">
<h2>Enabling Hooks<a class="headerlink" href="#enabling-hooks" title="Permalink to this headline">¶</a></h2>
<p>The hooks feature can be globally enabled/disabled by setting the
following item in the <strong>application/config/config.php</strong> file:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$config</span><span class="p">[</span><span class="s1">'enable_hooks'</span><span class="p">]</span> <span class="o">=</span> <span class="k">TRUE</span><span class="p">;</span>
</pre></div>
</div>
</div>
<div class="section" id="defining-a-hook">
<h2>Defining a Hook<a class="headerlink" href="#defining-a-hook" title="Permalink to this headline">¶</a></h2>
<p>Hooks are defined in the <strong>application/config/hooks.php</strong> file.
Each hook is specified as an array with this prototype:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$hook</span><span class="p">[</span><span class="s1">'pre_controller'</span><span class="p">]</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span>
        <span class="s1">'class'</span>    <span class="o">=&gt;</span> <span class="s1">'MyClass'</span><span class="p">,</span>
        <span class="s1">'function'</span> <span class="o">=&gt;</span> <span class="s1">'Myfunction'</span><span class="p">,</span>
        <span class="s1">'filename'</span> <span class="o">=&gt;</span> <span class="s1">'Myclass.php'</span><span class="p">,</span>
        <span class="s1">'filepath'</span> <span class="o">=&gt;</span> <span class="s1">'hooks'</span><span class="p">,</span>
        <span class="s1">'params'</span>   <span class="o">=&gt;</span> <span class="k">array</span><span class="p">(</span><span class="s1">'beer'</span><span class="p">,</span> <span class="s1">'wine'</span><span class="p">,</span> <span class="s1">'snacks'</span><span class="p">)</span>
<span class="p">);</span>
</pre></div>
</div>
<p><strong>Notes:</strong></p>
<p>The array index correlates to the name of the particular hook point you
want to use. In the above example the hook point is pre_controller. A
list of hook points is found below. The following items should be
defined in your associative hook array:</p>
<ul class="simple">
<li><strong>class</strong> The name of the class you wish to invoke. If you prefer to
use a procedural function instead of a class, leave this item blank.</li>
<li><strong>function</strong> The function (or method) name you wish to call.</li>
<li><strong>filename</strong> The file name containing your class/function.</li>
<li><strong>filepath</strong> The name of the directory containing your script.
Note:
Your script must be located in a directory INSIDE your <em>application/</em>
directory, so the file path is relative to that directory. For example,
if your script is located in <em>application/hooks/</em>, you will simply use
‘hooks’ as your filepath. If your script is located in
<em>application/hooks/utilities/</em> you will use ‘hooks/utilities’ as your
filepath. No trailing slash.</li>
<li><strong>params</strong> Any parameters you wish to pass to your script. This item
is optional.</li>
</ul>
<p>If you’re running PHP 5.3+, you can also use lambda/anoymous functions
(or closures) as hooks, with a simpler syntax:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$hook</span><span class="p">[</span><span class="s1">'post_controller'</span><span class="p">]</span> <span class="o">=</span> <span class="k">function</span><span class="p">()</span>
<span class="p">{</span>
        <span class="cm">/* do something here */</span>
<span class="p">};</span>
</pre></div>
</div>
</div>
<div class="section" id="multiple-calls-to-the-same-hook">
<h2>Multiple Calls to the Same Hook<a class="headerlink" href="#multiple-calls-to-the-same-hook" title="Permalink to this headline">¶</a></h2>
<p>If want to use the same hook point with more than one script, simply
make your array declaration multi-dimensional, like this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$hook</span><span class="p">[</span><span class="s1">'pre_controller'</span><span class="p">][]</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span>
        <span class="s1">'class'</span>    <span class="o">=&gt;</span> <span class="s1">'MyClass'</span><span class="p">,</span>
        <span class="s1">'function'</span> <span class="o">=&gt;</span> <span class="s1">'MyMethod'</span><span class="p">,</span>
        <span class="s1">'filename'</span> <span class="o">=&gt;</span> <span class="s1">'Myclass.php'</span><span class="p">,</span>
        <span class="s1">'filepath'</span> <span class="o">=&gt;</span> <span class="s1">'hooks'</span><span class="p">,</span>
        <span class="s1">'params'</span>   <span class="o">=&gt;</span> <span class="k">array</span><span class="p">(</span><span class="s1">'beer'</span><span class="p">,</span> <span class="s1">'wine'</span><span class="p">,</span> <span class="s1">'snacks'</span><span class="p">)</span>
<span class="p">);</span>

<span class="nv">$hook</span><span class="p">[</span><span class="s1">'pre_controller'</span><span class="p">][]</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span>
        <span class="s1">'class'</span>    <span class="o">=&gt;</span> <span class="s1">'MyOtherClass'</span><span class="p">,</span>
        <span class="s1">'function'</span> <span class="o">=&gt;</span> <span class="s1">'MyOtherMethod'</span><span class="p">,</span>
        <span class="s1">'filename'</span> <span class="o">=&gt;</span> <span class="s1">'Myotherclass.php'</span><span class="p">,</span>
        <span class="s1">'filepath'</span> <span class="o">=&gt;</span> <span class="s1">'hooks'</span><span class="p">,</span>
        <span class="s1">'params'</span>   <span class="o">=&gt;</span> <span class="k">array</span><span class="p">(</span><span class="s1">'red'</span><span class="p">,</span> <span class="s1">'yellow'</span><span class="p">,</span> <span class="s1">'blue'</span><span class="p">)</span>
<span class="p">);</span>
</pre></div>
</div>
<p>Notice the brackets after each array index:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$hook</span><span class="p">[</span><span class="s1">'pre_controller'</span><span class="p">][]</span>
</pre></div>
</div>
<p>This permits you to have the same hook point with multiple scripts. The
order you define your array will be the execution order.</p>
</div>
<div class="section" id="hook-points">
<h2>Hook Points<a class="headerlink" href="#hook-points" title="Permalink to this headline">¶</a></h2>
<p>The following is a list of available hook points.</p>
<ul class="simple">
<li><strong>pre_system</strong>
Called very early during system execution. Only the benchmark and
hooks class have been loaded at this point. No routing or other
processes have happened.</li>
<li><strong>pre_controller</strong>
Called immediately prior to any of your controllers being called.
All base classes, routing, and security checks have been done.</li>
<li><strong>post_controller_constructor</strong>
Called immediately after your controller is instantiated, but prior
to any method calls happening.</li>
<li><strong>post_controller</strong>
Called immediately after your controller is fully executed.</li>
<li><strong>display_override</strong>
Overrides the <tt class="docutils literal"><span class="pre">_display()</span></tt> method, used to send the finalized page
to the web browser at the end of system execution. This permits you
to use your own display methodology. Note that you will need to
reference the CI superobject with <tt class="docutils literal"><span class="pre">$this-&gt;CI</span> <span class="pre">=&amp;</span> <span class="pre">get_instance()</span></tt> and
then the finalized data will be available by calling
<tt class="docutils literal"><span class="pre">$this-&gt;CI-&gt;output-&gt;get_output()</span></tt>.</li>
<li><strong>cache_override</strong>
Enables you to call your own method instead of the <tt class="docutils literal"><span class="pre">_display_cache()</span></tt>
method in the <a class="reference internal" href="../user_guide/libraries/output.html"><em>Output Library</em></a>. This permits
you to use your own cache display mechanism.</li>
<li><strong>post_system</strong>
Called after the final rendered page is sent to the browser, at the
end of system execution after the finalized data is sent to the
browser.</li>
</ul>
</div>
</div>
<div class="section" id="auto-loading-resources">
<h1>Auto-loading Resources<a class="headerlink" href="#auto-loading-resources" title="Permalink to this headline">¶</a></h1>
<p>CodeIgniter comes with an “Auto-load” feature that permits libraries,
helpers, and models to be initialized automatically every time the
system runs. If you need certain resources globally throughout your
application you should consider auto-loading them for convenience.</p>
<p>The following items can be loaded automatically:</p>
<ul class="simple">
<li>Classes found in the <em>libraries/</em> directory</li>
<li>Helper files found in the <em>helpers/</em> directory</li>
<li>Custom config files found in the <em>config/</em> directory</li>
<li>Language files found in the <em>system/language/</em> directory</li>
<li>Models found in the <em>models/</em> folder</li>
</ul>
<p>To autoload resources, open the <strong>application/config/autoload.php</strong>
file and add the item you want loaded to the autoload array. You’ll
find instructions in that file corresponding to each type of item.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Do not include the file extension (.php) when adding items to
the autoload array.</p>
</div>
<p>Additionally, if you want CodeIgniter to use a <a class="reference external" href="https://getcomposer.org/">Composer</a>
auto-loader, just set <tt class="docutils literal"><span class="pre">$config['composer_autoload']</span></tt> to <tt class="docutils literal"><span class="pre">TRUE</span></tt> or
a custom path in <strong>application/config/config.php</strong>.</p>
</div>
<div class="section" id="common-functions">
<h1>Common Functions<a class="headerlink" href="#common-functions" title="Permalink to this headline">¶</a></h1>
<p>CodeIgniter uses a few functions for its operation that are globally
defined, and are available to you at any point. These do not require
loading any libraries or helpers.</p>
<div class="custom-index container"></div><dl class="function">
<dt id="is_php">
<tt class="descname">is_php</tt><big>(</big><em>$version</em><big>)</big><a class="headerlink" href="#is_php" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$version</strong> (<em>string</em>) – Version number</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">TRUE if the running PHP version is at least the one specified or FALSE if not</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">bool</p>
</td>
</tr>
</tbody>
</table>
<p>Determines if the PHP version being used is greater than the
supplied version number.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nx">is_php</span><span class="p">(</span><span class="s1">'5.3'</span><span class="p">))</span>
<span class="p">{</span>
        <span class="nv">$str</span> <span class="o">=</span> <span class="nb">quoted_printable_encode</span><span class="p">(</span><span class="nv">$str</span><span class="p">);</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Returns boolean TRUE if the installed version of PHP is equal to or
greater than the supplied version number. Returns FALSE if the installed
version of PHP is lower than the supplied version number.</p>
</dd></dl>

<dl class="function">
<dt id="is_really_writable">
<tt class="descname">is_really_writable</tt><big>(</big><em>$file</em><big>)</big><a class="headerlink" href="#is_really_writable" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$file</strong> (<em>string</em>) – File path</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">TRUE if the path is writable, FALSE if not</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">bool</p>
</td>
</tr>
</tbody>
</table>
<p><tt class="docutils literal"><span class="pre">is_writable()</span></tt> returns TRUE on Windows servers when you really can’t
write to the file as the OS reports to PHP as FALSE only if the
read-only attribute is marked.</p>
<p>This function determines if a file is actually writable by attempting
to write to it first. Generally only recommended on platforms where
this information may be unreliable.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nx">is_really_writable</span><span class="p">(</span><span class="s1">'file.txt'</span><span class="p">))</span>
<span class="p">{</span>
        <span class="k">echo</span> <span class="s2">"I could write to this if I wanted to"</span><span class="p">;</span>
<span class="p">}</span>
<span class="k">else</span>
<span class="p">{</span>
        <span class="k">echo</span> <span class="s2">"File is not writable"</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">See also <a class="reference external" href="https://bugs.php.net/bug.php?id=54709">PHP bug #54709</a> for more info.</p>
</div>
</dd></dl>

<dl class="function">
<dt id="config_item">
<tt class="descname">config_item</tt><big>(</big><em>$key</em><big>)</big><a class="headerlink" href="#config_item" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$key</strong> (<em>string</em>) – Config item key</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Configuration key value or NULL if not found</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">mixed</p>
</td>
</tr>
</tbody>
</table>
<p>The <a class="reference internal" href="../user_guide/libraries/config.html"><em>Config Library</em></a> is the preferred way of
accessing configuration information, however <tt class="docutils literal"><span class="pre">config_item()</span></tt> can be used
to retrieve single keys. See <a class="reference internal" href="../user_guide/libraries/config.html"><em>Config Library</em></a>
documentation for more information.</p>
</dd></dl>

<dl class="function">
<dt id="set_status_header">
<tt class="descname">set_status_header</tt><big>(</big><em>$code</em><span class="optional">[</span>, <em>$text = ''</em><span class="optional">]</span><big>)</big><a class="headerlink" href="#set_status_header" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$code</strong> (<em>int</em>) – HTTP Reponse status code</li>
<li><strong>$text</strong> (<em>string</em>) – A custom message to set with the status code</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">void</p>
</td>
</tr>
</tbody>
</table>
<p>Permits you to manually set a server status header. Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">set_status_header</span><span class="p">(</span><span class="mi">401</span><span class="p">);</span>
<span class="c1">// Sets the header as:  Unauthorized</span>
</pre></div>
</div>
<p><a class="reference external" href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html">See here</a> for
a full list of headers.</p>
</dd></dl>

<dl class="function">
<dt id="remove_invisible_characters">
<tt class="descname">remove_invisible_characters</tt><big>(</big><em>$str</em><span class="optional">[</span>, <em>$url_encoded = TRUE</em><span class="optional">]</span><big>)</big><a class="headerlink" href="#remove_invisible_characters" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$str</strong> (<em>string</em>) – Input string</li>
<li><strong>$url_encoded</strong> (<em>bool</em>) – Whether to remove URL-encoded characters as well</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Sanitized string</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">string</p>
</td>
</tr>
</tbody>
</table>
<p>This function prevents inserting NULL characters between ASCII
characters, like Java\0script.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">remove_invisible_characters</span><span class="p">(</span><span class="s1">'Java\\0script'</span><span class="p">);</span>
<span class="c1">// Returns: 'Javascript'</span>
</pre></div>
</div>
</dd></dl>

<dl class="function">
<dt id="html_escape">
<tt class="descname">html_escape</tt><big>(</big><em>$var</em><big>)</big><a class="headerlink" href="#html_escape" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$var</strong> (<em>mixed</em>) – Variable to escape (string or array)</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">HTML escaped string(s)</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">mixed</p>
</td>
</tr>
</tbody>
</table>
<p>This function acts as an alias for PHP’s native <tt class="docutils literal"><span class="pre">htmlspecialchars()</span></tt>
function, with the advantage of being able to accept an array of strings.</p>
<p>It is useful in preventing Cross Site Scripting (XSS).</p>
</dd></dl>

<dl class="function">
<dt id="get_mimes">
<tt class="descname">get_mimes</tt><big>(</big><big>)</big><a class="headerlink" href="#get_mimes" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Returns:</th><td class="field-body">An associative array of file types</td>
</tr>
<tr class="field-even field"><th class="field-name">Return type:</th><td class="field-body">array</td>
</tr>
</tbody>
</table>
<p>This function returns a <em>reference</em> to the MIMEs array from
<em>application/config/mimes.php</em>.</p>
</dd></dl>

<dl class="function">
<dt id="is_https">
<tt class="descname">is_https</tt><big>(</big><big>)</big><a class="headerlink" href="#is_https" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Returns:</th><td class="field-body">TRUE if currently using HTTP-over-SSL, FALSE if not</td>
</tr>
<tr class="field-even field"><th class="field-name">Return type:</th><td class="field-body">bool</td>
</tr>
</tbody>
</table>
<p>Returns TRUE if a secure (HTTPS) connection is used and FALSE
in any other case (including non-HTTP requests).</p>
</dd></dl>

<dl class="function">
<dt id="is_cli">
<tt class="descname">is_cli</tt><big>(</big><big>)</big><a class="headerlink" href="#is_cli" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Returns:</th><td class="field-body">TRUE if currently running under CLI, FALSE otherwise</td>
</tr>
<tr class="field-even field"><th class="field-name">Return type:</th><td class="field-body">bool</td>
</tr>
</tbody>
</table>
<p>Returns TRUE if the application is run through the command line
and FALSE if not.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">This function checks both if the <tt class="docutils literal"><span class="pre">PHP_SAPI</span></tt> value is ‘cli’
or if the <tt class="docutils literal"><span class="pre">STDIN</span></tt> constant is defined.</p>
</div>
</dd></dl>

<dl class="function">
<dt id="function_usable">
<tt class="descname">function_usable</tt><big>(</big><em>$function_name</em><big>)</big><a class="headerlink" href="#function_usable" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$function_name</strong> (<em>string</em>) – Function name</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">TRUE if the function can be used, FALSE if not</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">bool</p>
</td>
</tr>
</tbody>
</table>
<p>Returns TRUE if a function exists and is usable, FALSE otherwise.</p>
<p>This function runs a <tt class="docutils literal"><span class="pre">function_exists()</span></tt> check and if the
<cite>Suhosin extension &lt;http://www.hardened-php.net/suhosin/&gt;</cite> is loaded,
checks if it doesn’t disable the function being checked.</p>
<p>It is useful if you want to check for the availability of functions
such as <tt class="docutils literal"><span class="pre">eval()</span></tt> and <tt class="docutils literal"><span class="pre">exec()</span></tt>, which are dangerous and might be
disabled on servers with highly restrictive security policies.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">This function was introduced because Suhosin terminated
script execution, but this turned out to be a bug. A fix
has been available for some time (version 0.9.34), but is
unfortunately not released yet.</p>
</div>
</dd></dl>

</div>
<div class="section" id="compatibility-functions">
<h1>Compatibility Functions<a class="headerlink" href="#compatibility-functions" title="Permalink to this headline">¶</a></h1>
<p>CodeIgniter provides a set of compatibility functions that enable
you to use functions what are otherwise natively available in PHP,
but only in higher versions or depending on a certain extension.</p>
<p>Being custom implementations, these functions will also have some
set of dependencies on their own, but are still useful if your
PHP setup doesn’t offer them natively.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Much like the <cite>common functions &lt;common_functions&gt;</cite>, the
compatibility functions are always available, as long as
their dependencies are met.</p>
</div>
<div class="contents local topic" id="contents">
<ul class="simple">
<li><a class="reference internal" href="#password-hashing" id="id7">Password Hashing</a><ul>
<li><a class="reference internal" href="#dependencies" id="id8">Dependencies</a></li>
<li><a class="reference internal" href="#constants" id="id9">Constants</a></li>
<li><a class="reference internal" href="#function-reference" id="id10">Function reference</a></li>
</ul>
</li>
<li><a class="reference internal" href="#hash-message-digest" id="id11">Hash (Message Digest)</a><ul>
<li><a class="reference internal" href="#id1" id="id12">Dependencies</a></li>
<li><a class="reference internal" href="#id2" id="id13">Function reference</a></li>
</ul>
</li>
<li><a class="reference internal" href="#multibyte-string" id="id14">Multibyte String</a><ul>
<li><a class="reference internal" href="#id3" id="id15">Dependencies</a></li>
<li><a class="reference internal" href="#id4" id="id16">Function reference</a></li>
</ul>
</li>
<li><a class="reference internal" href="#standard-functions" id="id17">Standard Functions</a><ul>
<li><a class="reference internal" href="#id5" id="id18">Dependencies</a></li>
<li><a class="reference internal" href="#id6" id="id19">Function reference</a></li>
</ul>
</li>
</ul>
</div>
<div class="custom-index container"></div><div class="section" id="password-hashing">
<h2><a class="toc-backref" href="#id7">Password Hashing</a><a class="headerlink" href="#password-hashing" title="Permalink to this headline">¶</a></h2>
<p>This set of compatibility functions offers a “backport” of PHP’s
standard <a class="reference external" href="http://php.net/password">Password Hashing extension</a>
that is otherwise available only since PHP 5.5.</p>
<div class="section" id="dependencies">
<h3><a class="toc-backref" href="#id8">Dependencies</a><a class="headerlink" href="#dependencies" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li>PHP 5.3.7</li>
<li><tt class="docutils literal"><span class="pre">CRYPT_BLOWFISH</span></tt> support for <tt class="docutils literal"><span class="pre">crypt()</span></tt></li>
</ul>
</div>
<div class="section" id="constants">
<h3><a class="toc-backref" href="#id9">Constants</a><a class="headerlink" href="#constants" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><tt class="docutils literal"><span class="pre">PASSWORD_BCRYPT</span></tt></li>
<li><tt class="docutils literal"><span class="pre">PASSWORD_DEFAULT</span></tt></li>
</ul>
</div>
<div class="section" id="function-reference">
<h3><a class="toc-backref" href="#id10">Function reference</a><a class="headerlink" href="#function-reference" title="Permalink to this headline">¶</a></h3>
<dl class="function">
<dt id="password_get_info">
<tt class="descname">password_get_info</tt><big>(</big><em>$hash</em><big>)</big><a class="headerlink" href="#password_get_info" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$hash</strong> (<em>string</em>) – Password hash</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Information about the hashed password</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">array</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/password_get_info">PHP manual for
password_get_info()</a>.</p>
</dd></dl>

<dl class="function">
<dt id="password_hash">
<tt class="descname">password_hash</tt><big>(</big><em>$password</em>, <em>$algo</em><span class="optional">[</span>, <em>$options = array()</em><span class="optional">]</span><big>)</big><a class="headerlink" href="#password_hash" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$password</strong> (<em>string</em>) – Plain-text password</li>
<li><strong>$algo</strong> (<em>int</em>) – Hashing algorithm</li>
<li><strong>$options</strong> (<em>array</em>) – Hashing options</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Hashed password or FALSE on failure</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">string</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/password_hash">PHP manual for
password_hash()</a>.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Unless you provide your own (and valid) salt, this function
has a further dependency on an available CSPRNG source. Each
of the following would satisfy that:
- <tt class="docutils literal"><span class="pre">mcrypt_create_iv()</span></tt> with <tt class="docutils literal"><span class="pre">MCRYPT_DEV_URANDOM</span></tt>
- <tt class="docutils literal"><span class="pre">openssl_random_pseudo_bytes()</span></tt>
- /dev/arandom
- /dev/urandom</p>
</div>
</dd></dl>

<dl class="function">
<dt id="password_needs_rehash">
<tt class="descname">password_needs_rehash</tt><big>(</big><big>)</big><a class="headerlink" href="#password_needs_rehash" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$hash</strong> (<em>string</em>) – Password hash</li>
<li><strong>$algo</strong> (<em>int</em>) – Hashing algorithm</li>
<li><strong>$options</strong> (<em>array</em>) – Hashing options</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">TRUE if the hash should be rehashed to match the given algorithm and options, FALSE otherwise</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">bool</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/password_needs_rehash">PHP manual for
password_needs_rehash()</a>.</p>
</dd></dl>

<dl class="function">
<dt id="password_verify">
<tt class="descname">password_verify</tt><big>(</big><em>$password</em>, <em>$hash</em><big>)</big><a class="headerlink" href="#password_verify" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$password</strong> (<em>string</em>) – Plain-text password</li>
<li><strong>$hash</strong> (<em>string</em>) – Password hash</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">TRUE if the password matches the hash, FALSE if not</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">bool</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/password_verify">PHP manual for
password_verify()</a>.</p>
</dd></dl>

</div>
</div>
<div class="section" id="hash-message-digest">
<h2><a class="toc-backref" href="#id11">Hash (Message Digest)</a><a class="headerlink" href="#hash-message-digest" title="Permalink to this headline">¶</a></h2>
<p>This compatibility layer contains backports for the <tt class="docutils literal"><span class="pre">hash_equals()</span></tt>
and <tt class="docutils literal"><span class="pre">hash_pbkdf2()</span></tt> functions, which otherwise require PHP 5.6 and/or
PHP 5.5 respectively.</p>
<div class="section" id="id1">
<h3><a class="toc-backref" href="#id12">Dependencies</a><a class="headerlink" href="#id1" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li>None</li>
</ul>
</div>
<div class="section" id="id2">
<h3><a class="toc-backref" href="#id13">Function reference</a><a class="headerlink" href="#id2" title="Permalink to this headline">¶</a></h3>
<dl class="function">
<dt id="hash_equals">
<tt class="descname">hash_equals</tt><big>(</big><em>$known_string</em>, <em>$user_string</em><big>)</big><a class="headerlink" href="#hash_equals" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$known_string</strong> (<em>string</em>) – Known string</li>
<li><strong>$user_string</strong> (<em>string</em>) – User-supplied string</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">TRUE if the strings match, FALSE otherwise</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">string</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/hash_equals">PHP manual for
hash_equals()</a>.</p>
</dd></dl>

<dl class="function">
<dt id="hash_pbkdf2">
<tt class="descname">hash_pbkdf2</tt><big>(</big><em>$algo</em>, <em>$password</em>, <em>$salt</em>, <em>$iterations</em><span class="optional">[</span>, <em>$length = 0</em><span class="optional">[</span>, <em>$raw_output = FALSE</em><span class="optional">]</span><span class="optional">]</span><big>)</big><a class="headerlink" href="#hash_pbkdf2" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$algo</strong> (<em>string</em>) – Hashing algorithm</li>
<li><strong>$password</strong> (<em>string</em>) – Password</li>
<li><strong>$salt</strong> (<em>string</em>) – Hash salt</li>
<li><strong>$iterations</strong> (<em>int</em>) – Number of iterations to perform during derivation</li>
<li><strong>$length</strong> (<em>int</em>) – Output string length</li>
<li><strong>$raw_output</strong> (<em>bool</em>) – Whether to return raw binary data</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Password-derived key or FALSE on failure</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">string</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/hash_pbkdf2">PHP manual for
hash_pbkdf2()</a>.</p>
</dd></dl>

</div>
</div>
<div class="section" id="multibyte-string">
<h2><a class="toc-backref" href="#id14">Multibyte String</a><a class="headerlink" href="#multibyte-string" title="Permalink to this headline">¶</a></h2>
<p>This set of compatibility functions offers limited support for PHP’s
<a class="reference external" href="http://php.net/mbstring">Multibyte String extension</a>. Because of
the limited alternative solutions, only a few functions are available.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">When a character set parameter is ommited,
<tt class="docutils literal"><span class="pre">$config['charset']</span></tt> will be used.</p>
</div>
<div class="section" id="id3">
<h3><a class="toc-backref" href="#id15">Dependencies</a><a class="headerlink" href="#id3" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li><a class="reference external" href="http://php.net/iconv">iconv</a> extension</li>
</ul>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">This dependency is optional and these functions will
always be declared. If iconv is not available, they WILL
fall-back to their non-mbstring versions.</p>
</div>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">Where a character set is supplied, it must be
supported by iconv and in a format that it recognizes.</p>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">For you own dependency check on the actual mbstring
extension, use the <tt class="docutils literal"><span class="pre">MB_ENABLED</span></tt> constant.</p>
</div>
</div>
<div class="section" id="id4">
<h3><a class="toc-backref" href="#id16">Function reference</a><a class="headerlink" href="#id4" title="Permalink to this headline">¶</a></h3>
<dl class="function">
<dt id="mb_strlen">
<tt class="descname">mb_strlen</tt><big>(</big><em>$str</em><span class="optional">[</span>, <em>$encoding = NULL</em><span class="optional">]</span><big>)</big><a class="headerlink" href="#mb_strlen" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$str</strong> (<em>string</em>) – Input string</li>
<li><strong>$encoding</strong> (<em>string</em>) – Character set</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Number of characters in the input string or FALSE on failure</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">string</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/mb_strlen">PHP manual for
mb_strlen()</a>.</p>
</dd></dl>

<dl class="function">
<dt id="mb_strpos">
<tt class="descname">mb_strpos</tt><big>(</big><em>$haystack</em>, <em>$needle</em><span class="optional">[</span>, <em>$offset = 0</em><span class="optional">[</span>, <em>$encoding = NULL</em><span class="optional">]</span><span class="optional">]</span><big>)</big><a class="headerlink" href="#mb_strpos" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$haystack</strong> (<em>string</em>) – String to search in</li>
<li><strong>$needle</strong> (<em>string</em>) – Part of string to search for</li>
<li><strong>$offset</strong> (<em>int</em>) – Search offset</li>
<li><strong>$encoding</strong> (<em>string</em>) – Character set</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Numeric character position of where $needle was found or FALSE if not found</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">mixed</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/mb_strpos">PHP manual for
mb_strpos()</a>.</p>
</dd></dl>

<dl class="function">
<dt id="mb_substr">
<tt class="descname">mb_substr</tt><big>(</big><em>$str</em>, <em>$start</em><span class="optional">[</span>, <em>$length = NULL</em><span class="optional">[</span>, <em>$encoding = NULL</em><span class="optional">]</span><span class="optional">]</span><big>)</big><a class="headerlink" href="#mb_substr" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$str</strong> (<em>string</em>) – Input string</li>
<li><strong>$start</strong> (<em>int</em>) – Position of first character</li>
<li><strong>$length</strong> (<em>int</em>) – Maximum number of characters</li>
<li><strong>$encoding</strong> (<em>string</em>) – Character set</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Portion of $str specified by $start and $length or FALSE on failure</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">string</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/mb_substr">PHP manual for
mb_substr()</a>.</p>
</dd></dl>

</div>
</div>
<div class="section" id="standard-functions">
<h2><a class="toc-backref" href="#id17">Standard Functions</a><a class="headerlink" href="#standard-functions" title="Permalink to this headline">¶</a></h2>
<p>This set of compatibility functions offers support for a few
standard functions in PHP that otherwise require a newer PHP version.</p>
<div class="section" id="id5">
<h3><a class="toc-backref" href="#id18">Dependencies</a><a class="headerlink" href="#id5" title="Permalink to this headline">¶</a></h3>
<ul class="simple">
<li>None</li>
</ul>
</div>
<div class="section" id="id6">
<h3><a class="toc-backref" href="#id19">Function reference</a><a class="headerlink" href="#id6" title="Permalink to this headline">¶</a></h3>
<dl class="function">
<dt id="array_column">
<tt class="descname">array_column</tt><big>(</big><em>array $array</em>, <em>$column_key</em><span class="optional">[</span>, <em>$index_key = NULL</em><span class="optional">]</span><big>)</big><a class="headerlink" href="#array_column" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$array</strong> (<em>array</em>) – Array to fetch results from</li>
<li><strong>$column_key</strong> (<em>mixed</em>) – Key of the column to return values from</li>
<li><strong>$index_key</strong> (<em>mixed</em>) – Key to use for the returned values</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">An array of values representing a single column from the input array</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">array</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/array_column">PHP manual for
array_column()</a>.</p>
</dd></dl>

<dl class="function">
<dt id="array_replace">
<tt class="descname">array_replace</tt><big>(</big><em>array $array1</em><span class="optional">[</span>, <em>...</em><span class="optional">]</span><big>)</big><a class="headerlink" href="#array_replace" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$array1</strong> (<em>array</em>) – Array in which to replace elements</li>
<li><strong>...</strong> (<em>array</em>) – Array (or multiple ones) from which to extract elements</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Modified array</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">array</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/array_replace">PHP manual for
array_replace()</a>.</p>
</dd></dl>

<dl class="function">
<dt id="array_replace_recursive">
<tt class="descname">array_replace_recursive</tt><big>(</big><em>array $array1</em><span class="optional">[</span>, <em>...</em><span class="optional">]</span><big>)</big><a class="headerlink" href="#array_replace_recursive" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$array1</strong> (<em>array</em>) – Array in which to replace elements</li>
<li><strong>...</strong> (<em>array</em>) – Array (or multiple ones) from which to extract elements</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Modified array</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">array</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/array_replace_recursive">PHP manual for
array_replace_recursive()</a>.</p>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">Only PHP’s native function can detect endless recursion.
Unless you are running PHP 5.3+, be careful with references!</p>
</div>
</dd></dl>

<dl class="function">
<dt id="hex2bin">
<tt class="descname">hex2bin</tt><big>(</big><em>$data</em><big>)</big><a class="headerlink" href="#hex2bin" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$data</strong> (<em>array</em>) – Hexadecimal representation of data</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">Binary representation of the given data</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">string</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/hex2bin">PHP manual for hex2bin()</a>.</p>
</dd></dl>

<dl class="function">
<dt id="quoted_printable_encode">
<tt class="descname">quoted_printable_encode</tt><big>(</big><em>$str</em><big>)</big><a class="headerlink" href="#quoted_printable_encode" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$str</strong> (<em>string</em>) – Input string</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Returns:</th><td class="field-body"><p class="first">8bit-encoded string</p>
</td>
</tr>
<tr class="field-odd field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">string</p>
</td>
</tr>
</tbody>
</table>
<p>For more information, please refer to the <a class="reference external" href="http://php.net/quoted_printable_encode">PHP manual for
quoted_printable_encode()</a>.</p>
</dd></dl>

</div>
</div>
</div>
<div class="section" id="uri-routing">
<h1>URI Routing<a class="headerlink" href="#uri-routing" title="Permalink to this headline">¶</a></h1>
<p>Typically there is a one-to-one relationship between a URL string and
its corresponding controller class/method. The segments in a URI
normally follow this pattern:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">class</span><span class="o">/</span><span class="nx">function</span><span class="o">/</span><span class="nx">id</span><span class="o">/</span>
</pre></div>
</div>
<p>In some instances, however, you may want to remap this relationship so
that a different class/method can be called instead of the one
corresponding to the URL.</p>
<p>For example, let’s say you want your URLs to have this prototype:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">product</span><span class="o">/</span><span class="mi">1</span><span class="o">/</span>
<span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">product</span><span class="o">/</span><span class="mi">2</span><span class="o">/</span>
<span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">product</span><span class="o">/</span><span class="mi">3</span><span class="o">/</span>
<span class="nx">example</span><span class="o">.</span><span class="nx">com</span><span class="o">/</span><span class="nx">product</span><span class="o">/</span><span class="mi">4</span><span class="o">/</span>
</pre></div>
</div>
<p>Normally the second segment of the URL is reserved for the method
name, but in the example above it instead has a product ID. To
overcome this, CodeIgniter allows you to remap the URI handler.</p>
<div class="section" id="setting-your-own-routing-rules">
<h2>Setting your own routing rules<a class="headerlink" href="#setting-your-own-routing-rules" title="Permalink to this headline">¶</a></h2>
<p>Routing rules are defined in your <em>application/config/routes.php</em> file.
In it you’ll see an array called <tt class="docutils literal"><span class="pre">$route</span></tt> that permits you to specify
your own routing criteria. Routes can either be specified using wildcards
or Regular Expressions.</p>
</div>
<div class="section" id="wildcards">
<h2>Wildcards<a class="headerlink" href="#wildcards" title="Permalink to this headline">¶</a></h2>
<p>A typical wildcard route might look something like this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'product/:num'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'catalog/product_lookup'</span><span class="p">;</span>
</pre></div>
</div>
<p>In a route, the array key contains the URI to be matched, while the
array value contains the destination it should be re-routed to. In the
above example, if the literal word “product” is found in the first
segment of the URL, and a number is found in the second segment, the
“catalog” class and the “product_lookup” method are instead used.</p>
<p>You can match literal values or you can use two wildcard types:</p>
<p><strong>(:num)</strong> will match a segment containing only numbers.
<strong>(:any)</strong> will match a segment containing any character (except for ‘/’, which is the segment delimiter).</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Wildcards are actually aliases for regular expressions, with
<strong>:any</strong> being translated to <strong>[^/]+</strong> and <strong>:num</strong> to <strong>[0-9]+</strong>,
respectively.</p>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Routes will run in the order they are defined. Higher routes
will always take precedence over lower ones.</p>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Route rules are not filters! Setting a rule of e.g.
‘foo/bar/(:num)’ will not prevent controller <em>Foo</em> and method
<em>bar</em> to be called with a non-numeric value if that is a valid
route.</p>
</div>
</div>
<div class="section" id="examples">
<h2>Examples<a class="headerlink" href="#examples" title="Permalink to this headline">¶</a></h2>
<p>Here are a few routing examples:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'journals'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'blogs'</span><span class="p">;</span>
</pre></div>
</div>
<p>A URL containing the word “journals” in the first segment will be
remapped to the “blogs” class.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'blog/joe'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'blogs/users/34'</span><span class="p">;</span>
</pre></div>
</div>
<p>A URL containing the segments blog/joe will be remapped to the “blogs”
class and the “users” method. The ID will be set to “34”.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'product/(:any)'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'catalog/product_lookup'</span><span class="p">;</span>
</pre></div>
</div>
<p>A URL with “product” as the first segment, and anything in the second
will be remapped to the “catalog” class and the “product_lookup”
method.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'product/(:num)'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'catalog/product_lookup_by_id/$1'</span><span class="p">;</span>
</pre></div>
</div>
<p>A URL with “product” as the first segment, and a number in the second
will be remapped to the “catalog” class and the
“product_lookup_by_id” method passing in the match as a variable to
the method.</p>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">Do not use leading/trailing slashes.</p>
</div>
</div>
<div class="section" id="regular-expressions">
<h2>Regular Expressions<a class="headerlink" href="#regular-expressions" title="Permalink to this headline">¶</a></h2>
<p>If you prefer you can use regular expressions to define your routing
rules. Any valid regular expression is allowed, as are back-references.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">If you use back-references you must use the dollar syntax
rather than the double backslash syntax.</p>
</div>
<p>A typical RegEx route might look something like this:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'products/([a-z]+)/(\d+)'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'$1/id_$2'</span><span class="p">;</span>
</pre></div>
</div>
<p>In the above example, a URI similar to products/shirts/123 would instead
call the “shirts” controller class and the “id_123” method.</p>
<p>With regular expressions, you can also catch multiple segments at once.
For example, if a user accesses a password protected area of your web
application and you wish to be able to redirect them back to the same
page after they log in, you may find this example useful:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'login/(.+)'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'auth/login/$1'</span><span class="p">;</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">In the above example, if the <tt class="docutils literal"><span class="pre">$1</span></tt> placeholder contains a
slash, it will still be split into multiple parameters when
passed to <tt class="docutils literal"><span class="pre">Auth::login()</span></tt>.</p>
</div>
<p>For those of you who don’t know regular expressions and want to learn
more about them, <a class="reference external" href="http://www.regular-expressions.info/">regular-expressions.info</a>
might be a good starting point.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">You can also mix and match wildcards with regular expressions.</p>
</div>
</div>
<div class="section" id="callbacks">
<h2>Callbacks<a class="headerlink" href="#callbacks" title="Permalink to this headline">¶</a></h2>
<p>If you are using PHP &gt;= 5.3 you can use callbacks in place of the normal
routing rules to process the back-references. Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'products/([a-zA-Z]+)/edit/(\d+)'</span><span class="p">]</span> <span class="o">=</span> <span class="k">function</span> <span class="p">(</span><span class="nv">$product_type</span><span class="p">,</span> <span class="nv">$id</span><span class="p">)</span>
<span class="p">{</span>
        <span class="k">return</span> <span class="s1">'catalog/product_edit/'</span> <span class="o">.</span> <span class="nb">strtolower</span><span class="p">(</span><span class="nv">$product_type</span><span class="p">)</span> <span class="o">.</span> <span class="s1">'/'</span> <span class="o">.</span> <span class="nv">$id</span><span class="p">;</span>
<span class="p">};</span>
</pre></div>
</div>
</div>
<div class="section" id="using-http-verbs-in-routes">
<h2>Using HTTP verbs in routes<a class="headerlink" href="#using-http-verbs-in-routes" title="Permalink to this headline">¶</a></h2>
<p>It is possible to use HTTP verbs (request method) to define your routing rules.
This is particularly useful when building RESTful applications. You can use standard HTTP
verbs (GET, PUT, POST, DELETE, PATCH) or a custom one such (e.g. PURGE). HTTP verb rules
are case-insensitive. All you need to do is to add the verb as an array key to your route.
Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'products'</span><span class="p">][</span><span class="s1">'put'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'product/insert'</span><span class="p">;</span>
</pre></div>
</div>
<p>In the above example, a PUT request to URI “products” would call the <tt class="docutils literal"><span class="pre">Product::insert()</span></tt>
controller method.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'products/(:num)'</span><span class="p">][</span><span class="s1">'DELETE'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'product/delete/$1'</span><span class="p">;</span>
</pre></div>
</div>
<p>A DELETE request to URL with “products” as first the segment and a number in the second will be
mapped to the <tt class="docutils literal"><span class="pre">Product::delete()</span></tt> method, passing the numeric value as the first parameter.</p>
<p>Using HTTP verbs is of course, optional.</p>
</div>
<div class="section" id="reserved-routes">
<h2>Reserved Routes<a class="headerlink" href="#reserved-routes" title="Permalink to this headline">¶</a></h2>
<p>There are three reserved routes:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'default_controller'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'welcome'</span><span class="p">;</span>
</pre></div>
</div>
<p>This route points to the action that should be executed if the URI contains
no data, which will be the case when people load your root URL.
The setting accepts a <strong>controller/method</strong> value and <tt class="docutils literal"><span class="pre">index()</span></tt> would be
the default method if you don’t specify one. In the above example, it is
<tt class="docutils literal"><span class="pre">Welcome::index()</span></tt> that would be called.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">You can NOT use a directory as a part of this setting!</p>
</div>
<p>You are encouraged to always have a default route as otherwise a 404 page
will appear by default.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'404_override'</span><span class="p">]</span> <span class="o">=</span> <span class="s1">''</span><span class="p">;</span>
</pre></div>
</div>
<p>This route indicates which controller class should be loaded if the
requested controller is not found. It will override the default 404
error page. Same per-directory rules as with ‘default_controller’
apply here as well.</p>
<p>It won’t affect to the <tt class="docutils literal"><span class="pre">show_404()</span></tt> function, which will
continue loading the default <em>error_404.php</em> file at
<em>application/views/errors/error_404.php</em>.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$route</span><span class="p">[</span><span class="s1">'translate_uri_dashes'</span><span class="p">]</span> <span class="o">=</span> <span class="k">FALSE</span><span class="p">;</span>
</pre></div>
</div>
<p>As evident by the boolean value, this is not exactly a route. This
option enables you to automatically replace dashes (‘-‘) with
underscores in the controller and method URI segments, thus saving you
additional route entries if you need to do that.
This is required, because the dash isn’t a valid class or method name
character and would cause a fatal error if you try to use it.</p>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">The reserved routes must come before any wildcard or
regular expression routes.</p>
</div>
</div>
</div>
<div class="section" id="error-handling">
<h1>Error Handling<a class="headerlink" href="#error-handling" title="Permalink to this headline">¶</a></h1>
<p>CodeIgniter lets you build error reporting into your applications using
the functions described below. In addition, it has an error logging
class that permits error and debugging messages to be saved as text
files.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">By default, CodeIgniter displays all PHP errors. You might
wish to change this behavior once your development is complete. You’ll
find the error_reporting() function located at the top of your main
index.php file. Disabling error reporting will NOT prevent log files
from being written if there are errors.</p>
</div>
<p>Unlike most systems in CodeIgniter, the error functions are simple
procedural interfaces that are available globally throughout the
application. This approach permits error messages to get triggered
without having to worry about class/function scoping.</p>
<p>CodeIgniter also returns a status code whenever a portion of the core
calls <tt class="docutils literal"><span class="pre">exit()</span></tt>. This exit status code is separate from the HTTP status
code, and serves as a notice to other processes that may be watching of
whether the script completed successfully, or if not, what kind of
problem it encountered that caused it to abort. These values are
defined in <em>application/config/constants.php</em>. While exit status codes
are most useful in CLI settings, returning the proper code helps server
software keep track of your scripts and the health of your application.</p>
<p>The following functions let you generate errors:</p>
<dl class="function">
<dt id="show_error">
<tt class="descname">show_error</tt><big>(</big><em>$message</em>, <em>$status_code</em>, <em>$heading = 'An Error Was Encountered'</em><big>)</big><a class="headerlink" href="#show_error" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$message</strong> (<em>mixed</em>) – Error message</li>
<li><strong>$status_code</strong> (<em>int</em>) – HTTP Response status code</li>
<li><strong>$heading</strong> (<em>string</em>) – Error page heading</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">void</p>
</td>
</tr>
</tbody>
</table>
<p>This function will display the error message supplied to it using
the error template appropriate to your execution:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">application</span><span class="o">/</span><span class="nx">views</span><span class="o">/</span><span class="nx">errors</span><span class="o">/</span><span class="nx">html</span><span class="o">/</span><span class="nx">error_general</span><span class="o">.</span><span class="nx">php</span>
</pre></div>
</div>
<p>or:</p>
<blockquote>
<div>application/views/errors/cli/error_general.php</div></blockquote>
<p>The optional parameter <tt class="docutils literal"><span class="pre">$status_code</span></tt> determines what HTTP status
code should be sent with the error. If <tt class="docutils literal"><span class="pre">$status_code</span></tt> is less
than 100, the HTTP status code will be set to 500, and the exit
status code will be set to <tt class="docutils literal"><span class="pre">$status_code</span> <span class="pre">+</span> <span class="pre">EXIT__AUTO_MIN</span></tt>.
If that value is larger than <tt class="docutils literal"><span class="pre">EXIT__AUTO_MAX</span></tt>, or if
<tt class="docutils literal"><span class="pre">$status_code</span></tt> is 100 or higher, the exit status code will be set
to <tt class="docutils literal"><span class="pre">EXIT_ERROR</span></tt>.
You can check in <em>application/config/constants.php</em> for more detail.</p>
</dd></dl>

<dl class="function">
<dt id="show_404">
<tt class="descname">show_404</tt><big>(</big><em>$page = ''</em>, <em>$log_error = TRUE</em><big>)</big><a class="headerlink" href="#show_404" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$page</strong> (<em>string</em>) – URI string</li>
<li><strong>$log_error</strong> (<em>bool</em>) – Whether to log the error</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">void</p>
</td>
</tr>
</tbody>
</table>
<p>This function will display the 404 error message supplied to it
using the error template appropriate to your execution:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">application</span><span class="o">/</span><span class="nx">views</span><span class="o">/</span><span class="nx">errors</span><span class="o">/</span><span class="nx">html</span><span class="o">/</span><span class="nx">error_404</span><span class="o">.</span><span class="nx">php</span>
</pre></div>
</div>
<p>or:</p>
<blockquote>
<div>application/views/errors/cli/error_404.php</div></blockquote>
<p>The function expects the string passed to it to be the file path to
the page that isn’t found. The exit status code will be set to
<tt class="docutils literal"><span class="pre">EXIT_UNKNOWN_FILE</span></tt>.
Note that CodeIgniter automatically shows 404 messages if
controllers are not found.</p>
<p>CodeIgniter automatically logs any <tt class="docutils literal"><span class="pre">show_404()</span></tt> calls. Setting the
optional second parameter to FALSE will skip logging.</p>
</dd></dl>

<dl class="function">
<dt id="log_message">
<tt class="descname">log_message</tt><big>(</big><em>$level</em>, <em>$message</em><big>)</big><a class="headerlink" href="#log_message" title="Permalink to this definition">¶</a></dt>
<dd><table class="docutils field-list" frame="void" rules="none">
<colgroup><col class="field-name">
<col class="field-body">
</colgroup><tbody valign="top">
<tr class="field-odd field"><th class="field-name">Parameters:</th><td class="field-body"><ul class="first simple">
<li><strong>$level</strong> (<em>string</em>) – Log level: ‘error’, ‘debug’ or ‘info’</li>
<li><strong>$message</strong> (<em>string</em>) – Message to log</li>
</ul>
</td>
</tr>
<tr class="field-even field"><th class="field-name">Return type:</th><td class="field-body"><p class="first last">void</p>
</td>
</tr>
</tbody>
</table>
<p>This function lets you write messages to your log files. You must
supply one of three “levels” in the first parameter, indicating what
type of message it is (debug, error, info), with the message itself
in the second parameter.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nv">$some_var</span> <span class="o">==</span> <span class="s1">''</span><span class="p">)</span>
<span class="p">{</span>
        <span class="nx">log_message</span><span class="p">(</span><span class="s1">'error'</span><span class="p">,</span> <span class="s1">'Some variable did not contain a value.'</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">else</span>
<span class="p">{</span>
        <span class="nx">log_message</span><span class="p">(</span><span class="s1">'debug'</span><span class="p">,</span> <span class="s1">'Some variable was correctly set'</span><span class="p">);</span>
<span class="p">}</span>

<span class="nx">log_message</span><span class="p">(</span><span class="s1">'info'</span><span class="p">,</span> <span class="s1">'The purpose of some variable is to provide some value.'</span><span class="p">);</span>
</pre></div>
</div>
<p>There are three message types:</p>
<ol class="arabic simple">
<li>Error Messages. These are actual errors, such as PHP errors or
user errors.</li>
<li>Debug Messages. These are messages that assist in debugging. For
example, if a class has been initialized, you could log this as
debugging info.</li>
<li>Informational Messages. These are the lowest priority messages,
simply giving information regarding some process.</li>
</ol>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">In order for the log file to actually be written, the
<em>logs/</em> directory must be writable. In addition, you must
set the “threshold” for logging in
<em>application/config/config.php</em>. You might, for example,
only want error messages to be logged, and not the other
two types. If you set it to zero logging will be disabled.</p>
</div>
</dd></dl>

</div>
<div class="section" id="web-page-caching">
<h1>Web Page Caching<a class="headerlink" href="#web-page-caching" title="Permalink to this headline">¶</a></h1>
<p>CodeIgniter lets you cache your pages in order to achieve maximum
performance.</p>
<p>Although CodeIgniter is quite fast, the amount of dynamic information
you display in your pages will correlate directly to the server
resources, memory, and processing cycles utilized, which affect your
page load speeds. By caching your pages, since they are saved in their
fully rendered state, you can achieve performance that nears that of
static web pages.</p>
<div class="section" id="how-does-caching-work">
<h2>How Does Caching Work?<a class="headerlink" href="#how-does-caching-work" title="Permalink to this headline">¶</a></h2>
<p>Caching can be enabled on a per-page basis, and you can set the length
of time that a page should remain cached before being refreshed. When a
page is loaded for the first time, the cache file will be written to
your application/cache folder. On subsequent page loads the cache file
will be retrieved and sent to the requesting user’s browser. If it has
expired, it will be deleted and refreshed before being sent to the
browser.</p>
</div>
<div class="section" id="enabling-caching">
<h2>Enabling Caching<a class="headerlink" href="#enabling-caching" title="Permalink to this headline">¶</a></h2>
<p>To enable caching, put the following tag in any of your controller
methods:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">output</span><span class="o">-&gt;</span><span class="na">cache</span><span class="p">(</span><span class="nv">$n</span><span class="p">);</span>
</pre></div>
</div>
<p>Where <tt class="docutils literal"><span class="pre">$n</span></tt> is the number of <strong>minutes</strong> you wish the page to remain
cached between refreshes.</p>
<p>The above tag can go anywhere within a method. It is not affected by
the order that it appears, so place it wherever it seems most logical to
you. Once the tag is in place, your pages will begin being cached.</p>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">Because of the way CodeIgniter stores content for output,
caching will only work if you are generating display for your
controller with a <a class="reference internal" href="views.html"><em>view</em></a>.</p>
</div>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">If you change configuration options that might affect
your output, you have to manually delete your cache files.</p>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Before the cache files can be written you must set the file
permissions on your <em>application/cache/</em> directory such that
it is writable.</p>
</div>
</div>
<div class="section" id="deleting-caches">
<h2>Deleting Caches<a class="headerlink" href="#deleting-caches" title="Permalink to this headline">¶</a></h2>
<p>If you no longer wish to cache a file you can remove the caching tag and
it will no longer be refreshed when it expires.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Removing the tag will not delete the cache immediately. It will
have to expire normally.</p>
</div>
<p>If you need to manually delete the cache, you can use the <tt class="docutils literal"><span class="pre">delete_cache()</span></tt>
method:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="c1">// Deletes cache for the currently requested URI</span>
<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">output</span><span class="o">-&gt;</span><span class="na">delete_cache</span><span class="p">();</span>

<span class="c1">// Deletes cache for /foo/bar</span>
<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">output</span><span class="o">-&gt;</span><span class="na">delete_cache</span><span class="p">(</span><span class="s1">'/foo/bar'</span><span class="p">);</span>
</pre></div>
</div>
</div>
</div>
<div class="section" id="profiling-your-application">
<h1>Profiling Your Application<a class="headerlink" href="#profiling-your-application" title="Permalink to this headline">¶</a></h1>
<p>The Profiler Class will display benchmark results, queries you have run,
and <tt class="docutils literal"><span class="pre">$_POST</span></tt> data at the bottom of your pages. This information can be
useful during development in order to help with debugging and
optimization.</p>
<div class="section" id="initializing-the-class">
<h2>Initializing the Class<a class="headerlink" href="#initializing-the-class" title="Permalink to this headline">¶</a></h2>
<div class="admonition important">
<p class="first admonition-title">Important</p>
<p class="last">This class does NOT need to be initialized. It is loaded
automatically by the <a class="reference internal" href="../user_guide/libraries/output.html"><em>Output Library</em></a>
if profiling is enabled as shown below.</p>
</div>
</div>
<div class="section" id="enabling-the-profiler">
<h2>Enabling the Profiler<a class="headerlink" href="#enabling-the-profiler" title="Permalink to this headline">¶</a></h2>
<p>To enable the profiler place the following line anywhere within your
<a class="reference internal" href="controllers.html"><em>Controller</em></a> methods:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">output</span><span class="o">-&gt;</span><span class="na">enable_profiler</span><span class="p">(</span><span class="k">TRUE</span><span class="p">);</span>
</pre></div>
</div>
<p>When enabled a report will be generated and inserted at the bottom of
your pages.</p>
<p>To disable the profiler you will use:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$this</span><span class="o">-&gt;</span><span class="na">output</span><span class="o">-&gt;</span><span class="na">enable_profiler</span><span class="p">(</span><span class="k">FALSE</span><span class="p">);</span>
</pre></div>
</div>
</div>
<div class="section" id="setting-benchmark-points">
<h2>Setting Benchmark Points<a class="headerlink" href="#setting-benchmark-points" title="Permalink to this headline">¶</a></h2>
<p>In order for the Profiler to compile and display your benchmark data you
must name your mark points using specific syntax.</p>
<p>Please read the information on setting Benchmark points in the
<a class="reference internal" href="../user_guide/libraries/benchmark.html"><em>Benchmark Library</em></a> page.</p>
</div>
<div class="section" id="enabling-and-disabling-profiler-sections">
<h2>Enabling and Disabling Profiler Sections<a class="headerlink" href="#enabling-and-disabling-profiler-sections" title="Permalink to this headline">¶</a></h2>
<p>Each section of Profiler data can be enabled or disabled by setting a
corresponding config variable to TRUE or FALSE. This can be done one of
two ways. First, you can set application wide defaults with the
<em>application/config/profiler.php</em> config file.</p>
<p>Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$config</span><span class="p">[</span><span class="s1">'config'</span><span class="p">]</span>          <span class="o">=</span> <span class="k">FALSE</span><span class="p">;</span>
<span class="nv">$config</span><span class="p">[</span><span class="s1">'queries'</span><span class="p">]</span>         <span class="o">=</span> <span class="k">FALSE</span><span class="p">;</span>
</pre></div>
</div>
<p>In your controllers, you can override the defaults and config file
values by calling the <tt class="docutils literal"><span class="pre">set_profiler_sections()</span></tt> method of the
<a class="reference internal" href="../user_guide/libraries/output.html"><em>Output Library</em></a>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$sections</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span>
        <span class="s1">'config'</span>  <span class="o">=&gt;</span> <span class="k">TRUE</span><span class="p">,</span>
        <span class="s1">'queries'</span> <span class="o">=&gt;</span> <span class="k">TRUE</span>
<span class="p">);</span>

<span class="nv">$this</span><span class="o">-&gt;</span><span class="na">output</span><span class="o">-&gt;</span><span class="na">set_profiler_sections</span><span class="p">(</span><span class="nv">$sections</span><span class="p">);</span>
</pre></div>
</div>
<p>Available sections and the array key used to access them are described
in the table below.</p>
<div class="wy-table-responsive"><table border="1" class="docutils">
<colgroup>
<col width="23%">
<col width="68%">
<col width="8%">
</colgroup>
<thead valign="bottom">
<tr class="row-odd"><th class="head">Key</th>
<th class="head">Description</th>
<th class="head">Default</th>
</tr>
</thead>
<tbody valign="top">
<tr class="row-even"><td><strong>benchmarks</strong></td>
<td>Elapsed time of Benchmark points and total execution time</td>
<td>TRUE</td>
</tr>
<tr class="row-odd"><td><strong>config</strong></td>
<td>CodeIgniter Config variables</td>
<td>TRUE</td>
</tr>
<tr class="row-even"><td><strong>controller_info</strong></td>
<td>The Controller class and method requested</td>
<td>TRUE</td>
</tr>
<tr class="row-odd"><td><strong>get</strong></td>
<td>Any GET data passed in the request</td>
<td>TRUE</td>
</tr>
<tr class="row-even"><td><strong>http_headers</strong></td>
<td>The HTTP headers for the current request</td>
<td>TRUE</td>
</tr>
<tr class="row-odd"><td><strong>memory_usage</strong></td>
<td>Amount of memory consumed by the current request, in bytes</td>
<td>TRUE</td>
</tr>
<tr class="row-even"><td><strong>post</strong></td>
<td>Any POST data passed in the request</td>
<td>TRUE</td>
</tr>
<tr class="row-odd"><td><strong>queries</strong></td>
<td>Listing of all database queries executed, including execution time</td>
<td>TRUE</td>
</tr>
<tr class="row-even"><td><strong>uri_string</strong></td>
<td>The URI of the current request</td>
<td>TRUE</td>
</tr>
<tr class="row-odd"><td><strong>session_data</strong></td>
<td>Data stored in the current session</td>
<td>TRUE</td>
</tr>
<tr class="row-even"><td><strong>query_toggle_count</strong></td>
<td>The number of queries after which the query block will default to
hidden.</td>
<td>25</td>
</tr>
</tbody>
</table></div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Disabling the <a class="reference internal" href="../user_guide/database/configuration.html"><em>save_queries</em></a> setting in
your database configuration will also effectively disable profiling for
database queries and render the ‘queries’ setting above useless. You can
optionally override this setting with <tt class="docutils literal"><span class="pre">$this-&gt;db-&gt;save_queries</span> <span class="pre">=</span> <span class="pre">TRUE;</span></tt>.
Without this setting you won’t be able to view the queries or the
<cite>last_query &lt;database/helpers&gt;</cite>.</p>
</div>
</div>
</div>
<div class="section" id="security">
<h1>Security<a class="headerlink" href="#security" title="Permalink to this headline">¶</a></h1>
<p>This page describes some “best practices” regarding web security, and
details CodeIgniter’s internal security features.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">If you came here looking for a security contact, please refer to
our <cite>Contribution Guide &lt;../contributing/index&gt;</cite>.</p>
</div>
<div class="section" id="uri-security">
<h2>URI Security<a class="headerlink" href="#uri-security" title="Permalink to this headline">¶</a></h2>
<p>CodeIgniter is fairly restrictive regarding which characters it allows
in your URI strings in order to help minimize the possibility that
malicious data can be passed to your application. URIs may only contain
the following:</p>
<ul class="simple">
<li>Alpha-numeric text (latin characters only)</li>
<li>Tilde: ~</li>
<li>Percent sign: %</li>
<li>Period: .</li>
<li>Colon: :</li>
<li>Underscore: _</li>
<li>Dash: -</li>
<li>Space</li>
</ul>
</div>
<div class="section" id="register-globals">
<h2>Register_globals<a class="headerlink" href="#register-globals" title="Permalink to this headline">¶</a></h2>
<p>During system initialization all global variables that are found to exist
in the <tt class="docutils literal"><span class="pre">$_GET</span></tt>, <tt class="docutils literal"><span class="pre">$_POST</span></tt>, <tt class="docutils literal"><span class="pre">$_REQUEST</span></tt> and <tt class="docutils literal"><span class="pre">$_COOKIE</span></tt> are unset.</p>
<p>The unsetting routine is effectively the same as <em>register_globals = off</em>.</p>
</div>
<div class="section" id="display-errors">
<h2>display_errors<a class="headerlink" href="#display-errors" title="Permalink to this headline">¶</a></h2>
<p>In production environments, it is typically desirable to “disable” PHP’s
error reporting by setting the internal <em>display_errors</em> flag to a value
of 0. This disables native PHP errors from being rendered as output,
which may potentially contain sensitive information.</p>
<p>Setting CodeIgniter’s <strong>ENVIRONMENT</strong> constant in index.php to a value of
<strong>‘production’</strong> will turn off these errors. In development mode, it is
recommended that a value of ‘development’ is used. More information
about differentiating between environments can be found on the
<a class="reference internal" href="environments.html"><em>Handling Environments</em></a> page.</p>
</div>
<div class="section" id="magic-quotes-runtime">
<h2>magic_quotes_runtime<a class="headerlink" href="#magic-quotes-runtime" title="Permalink to this headline">¶</a></h2>
<p>The <em>magic_quotes_runtime</em> directive is turned off during system
initialization so that you don’t have to remove slashes when retrieving
data from your database.</p>
<div class="section" id="best-practices">
<h3>Best Practices<a class="headerlink" href="#best-practices" title="Permalink to this headline">¶</a></h3>
<p>Before accepting any data into your application, whether it be POST data
from a form submission, COOKIE data, URI data, XML-RPC data, or even
data from the SERVER array, you are encouraged to practice this three
step approach:</p>
<ol class="arabic simple">
<li>Validate the data to ensure it conforms to the correct type, length,
size, etc.</li>
<li>Filter the data as if it were tainted.</li>
<li>Escape the data before submitting it into your database or outputting
it to a browser.</li>
</ol>
<p>CodeIgniter provides the following functions and tips to assist you
in this process:</p>
</div>
</div>
<div class="section" id="xss-filtering">
<h2>XSS Filtering<a class="headerlink" href="#xss-filtering" title="Permalink to this headline">¶</a></h2>
<p>CodeIgniter comes with a Cross Site Scripting filter. This filter
looks for commonly used techniques to embed malicious JavaScript into
your data, or other types of code that attempt to hijack cookies or
do other malicious things. The XSS Filter is described
<a class="reference internal" href="../user_guide/libraries/security.html"><em>here</em></a>.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">XSS filtering should <em>only be performed on output</em>. Filtering
input data may modify the data in undesirable ways, including
stripping special characters from passwords, which reduces
security instead of improving it.</p>
</div>
</div>
<div class="section" id="csrf-protection">
<h2>CSRF protection<a class="headerlink" href="#csrf-protection" title="Permalink to this headline">¶</a></h2>
<p>CSRF stands for Cross-Site Request Forgery, which is the process of an
attacker tricking their victim into unknowingly submitting a request.</p>
<p>CodeIgniter provides CSRF protection out of the box, which will get
automatically triggered for every non-GET HTTP request, but also needs
you to create your submit forms in a certain way. This is explained in
the <a class="reference internal" href="../user_guide/libraries/security.html"><em>Security Library</em></a> documentation.</p>
</div>
<div class="section" id="password-handling">
<h2>Password handling<a class="headerlink" href="#password-handling" title="Permalink to this headline">¶</a></h2>
<p>It is <em>critical</em> that you handle passwords in your application properly.</p>
<p>Unfortunately, many developers don’t know how to do that, and the web is
full of outdated or otherwise wrongful advices, which doesn’t help.</p>
<p>We would like to give you a list of combined do’s and don’ts to help you
with that. Please read below.</p>
<ul>
<li><p class="first">DO NOT store passwords in plain-text format.</p>
<p>Always <strong>hash</strong> your passwords.</p>
</li>
<li><p class="first">DO NOT use Base64 or similar encoding for storing passwords.</p>
<p>This is as good as storing them in plain-text. Really. Do <strong>hashing</strong>,
not <em>encoding</em>.</p>
<p>Encoding, and encryption too, are two-way processes. Passwords are
secrets that must only be known to their owner, and thus must work
only in one direction. Hashing does that - there’s <em>no</em> un-hashing or
de-hashing, but there is decoding and decryption.</p>
</li>
<li><p class="first">DO NOT use weak or broken hashing algorithms like MD5 or SHA1.</p>
<p>These algorithms are old, proven to be flawed, and not designed for
password hashing in the first place.</p>
<p>Also, DON’T invent your own algorithms.</p>
<p>Only use strong password hashing algorithms like BCrypt, which is used
in PHP’s own <a class="reference external" href="http://php.net/password">Password Hashing</a> functions.</p>
<p>Please use them, even if you’re not running PHP 5.5+, CodeIgniter
provides them for you as long as you’re running at least PHP version
5.3.7 (and if you don’t meet that requirement - please, upgrade).</p>
<p>If you’re one of the really unlucky people who can’t even upgrade to a
more recent PHP version, use <cite>hash_pbkdf() &lt;http://php.net/hash_pbkdf2&gt;</cite>,
which we also provide in our compatibility layer.</p>
</li>
<li><p class="first">DO NOT ever display or send a password in plain-text format!</p>
<p>Even to the password’s owner, if you need a “Forgotten password”
feature, just randomly generate a new, one-time (this is also important)
password and send that instead.</p>
</li>
<li><p class="first">DO NOT put unnecessary limits on your users’ passwords.</p>
<p>If you’re using a hashing algorithm other than BCrypt (which has a limit
of 72 characters), you should set a relatively high limit on password
lengths in order to mitigate DoS attacks - say, 1024 characters.</p>
<p>Other than that however, there’s no point in forcing a rule that a
password can only be up to a number of characters, or that it can’t
contain a certain set of special characters.</p>
<p>Not only does this <strong>reduce</strong> security instead of improving it, but
there’s literally no reason to do it. No technical limitations and
no (practical) storage constraints apply once you’ve hashed them, none!</p>
</li>
</ul>
</div>
<div class="section" id="validate-input-data">
<h2>Validate input data<a class="headerlink" href="#validate-input-data" title="Permalink to this headline">¶</a></h2>
<p>CodeIgniter has a <a class="reference internal" href="../user_guide/libraries/form_validation.html"><em>Form Validation Library</em></a> that assists you in
validating, filtering, and prepping your data.</p>
<p>Even if that doesn’t work for your use case however, be sure to always
validate and sanitize all input data. For example, if you expect a numeric
string for an input variable, you can check for that with <tt class="docutils literal"><span class="pre">is_numeric()</span></tt>
or <tt class="docutils literal"><span class="pre">ctype_digit()</span></tt>. Always try to narrow down your checks to a certain
pattern.</p>
<p>Have it in mind that this includes not only <tt class="docutils literal"><span class="pre">$_POST</span></tt> and <tt class="docutils literal"><span class="pre">$_GET</span></tt>
variables, but also cookies, the user-agent string and basically
<em>all data that is not created directly by your own code</em>.</p>
</div>
<div class="section" id="escape-all-data-before-database-insertion">
<h2>Escape all data before database insertion<a class="headerlink" href="#escape-all-data-before-database-insertion" title="Permalink to this headline">¶</a></h2>
<p>Never insert information into your database without escaping it.
Please see the section that discusses <a class="reference internal" href="../user_guide/database/queries.html"><em>database queries</em></a> for more information.</p>
</div>
<div class="section" id="hide-your-files">
<h2>Hide your files<a class="headerlink" href="#hide-your-files" title="Permalink to this headline">¶</a></h2>
<p>Another good security practice is to only leave your <em>index.php</em>
and “assets” (e.g. .js, css and image files) under your server’s
<em>webroot</em> directory (most commonly named “htdocs/”). These are
the only files that you would need to be accessible from the web.</p>
<p>Allowing your visitors to see anything else would potentially
allow them to access sensitive data, execute scripts, etc.</p>
<p>If you’re not allowed to do that, you can try using a .htaccess
file to restrict access to those resources.</p>
<p>CodeIgniter will have an index.html file in all of its
directories in an attempt to hide some of this data, but have
it in mind that this is not enough to prevent a serious
attacker.</p>
</div>
</div>
<div class="section" id="php-style-guide">
<h1><a class="toc-backref" href="#id1">PHP Style Guide</a><a class="headerlink" href="#php-style-guide" title="Permalink to this headline">¶</a></h1>
<p>The following page describes the coding styles adhered to when
contributing to the development of CodeIgniter. There is no requirement
to use these styles in your own CodeIgniter application, though they
are recommended.</p>
<div class="contents topic" id="table-of-contents">
<p class="topic-title first">Table of Contents</p>
<ul class="simple">
<li><a class="reference internal" href="#php-style-guide" id="id1">PHP Style Guide</a><ul>
<li><a class="reference internal" href="#file-format" id="id2">File Format</a><ul>
<li><a class="reference internal" href="#textmate" id="id3">TextMate</a></li>
<li><a class="reference internal" href="#bbedit" id="id4">BBEdit</a></li>
</ul>
</li>
<li><a class="reference internal" href="#php-closing-tag" id="id5">PHP Closing Tag</a></li>
<li><a class="reference internal" href="#file-naming" id="id6">File Naming</a></li>
<li><a class="reference internal" href="#class-and-method-naming" id="id7">Class and Method Naming</a></li>
<li><a class="reference internal" href="#variable-names" id="id8">Variable Names</a></li>
<li><a class="reference internal" href="#commenting" id="id9">Commenting</a></li>
<li><a class="reference internal" href="#constants" id="id10">Constants</a></li>
<li><a class="reference internal" href="#true-false-and-null" id="id11">TRUE, FALSE, and NULL</a></li>
<li><a class="reference internal" href="#logical-operators" id="id12">Logical Operators</a></li>
<li><a class="reference internal" href="#comparing-return-values-and-typecasting" id="id13">Comparing Return Values and Typecasting</a></li>
<li><a class="reference internal" href="#debugging-code" id="id14">Debugging Code</a></li>
<li><a class="reference internal" href="#whitespace-in-files" id="id15">Whitespace in Files</a></li>
<li><a class="reference internal" href="#compatibility" id="id16">Compatibility</a></li>
<li><a class="reference internal" href="#one-file-per-class" id="id17">One File per Class</a></li>
<li><a class="reference internal" href="#whitespace" id="id18">Whitespace</a></li>
<li><a class="reference internal" href="#line-breaks" id="id19">Line Breaks</a></li>
<li><a class="reference internal" href="#code-indenting" id="id20">Code Indenting</a></li>
<li><a class="reference internal" href="#bracket-and-parenthetic-spacing" id="id21">Bracket and Parenthetic Spacing</a></li>
<li><a class="reference internal" href="#localized-text" id="id22">Localized Text</a></li>
<li><a class="reference internal" href="#private-methods-and-variables" id="id23">Private Methods and Variables</a></li>
<li><a class="reference internal" href="#php-errors" id="id24">PHP Errors</a></li>
<li><a class="reference internal" href="#short-open-tags" id="id25">Short Open Tags</a></li>
<li><a class="reference internal" href="#one-statement-per-line" id="id26">One Statement Per Line</a></li>
<li><a class="reference internal" href="#strings" id="id27">Strings</a></li>
<li><a class="reference internal" href="#sql-queries" id="id28">SQL Queries</a></li>
<li><a class="reference internal" href="#default-function-arguments" id="id29">Default Function Arguments</a></li>
</ul>
</li>
</ul>
</div>
<div class="section" id="file-format">
<h2><a class="toc-backref" href="#id2">File Format</a><a class="headerlink" href="#file-format" title="Permalink to this headline">¶</a></h2>
<p>Files should be saved with Unicode (UTF-8) encoding. The BOM should
<em>not</em> be used. Unlike UTF-16 and UTF-32, there’s no byte order to
indicate in a UTF-8 encoded file, and the BOM can have a negative side
effect in PHP of sending output, preventing the application from being
able to set its own headers. Unix line endings should be used (LF).</p>
<p>Here is how to apply these settings in some of the more common text
editors. Instructions for your text editor may vary; check your text
editor’s documentation.</p>
<div class="section" id="textmate">
<h3><a class="toc-backref" href="#id3">TextMate</a><a class="headerlink" href="#textmate" title="Permalink to this headline">¶</a></h3>
<ol class="arabic simple">
<li>Open the Application Preferences</li>
<li>Click Advanced, and then the “Saving” tab</li>
<li>In “File Encoding”, select “UTF-8 (recommended)”</li>
<li>In “Line Endings”, select “LF (recommended)”</li>
<li><em>Optional:</em> Check “Use for existing files as well” if you wish to
modify the line endings of files you open to your new preference.</li>
</ol>
</div>
<div class="section" id="bbedit">
<h3><a class="toc-backref" href="#id4">BBEdit</a><a class="headerlink" href="#bbedit" title="Permalink to this headline">¶</a></h3>
<ol class="arabic simple">
<li>Open the Application Preferences</li>
<li>Select “Text Encodings” on the left.</li>
<li>In “Default text encoding for new documents”, select “Unicode (UTF-8,
no BOM)”</li>
<li><em>Optional:</em> In “If file’s encoding can’t be guessed, use”, select
“Unicode (UTF-8, no BOM)”</li>
<li>Select “Text Files” on the left.</li>
<li>In “Default line breaks”, select “Mac OS X and Unix (LF)”</li>
</ol>
</div>
</div>
<div class="section" id="php-closing-tag">
<h2><a class="toc-backref" href="#id5">PHP Closing Tag</a><a class="headerlink" href="#php-closing-tag" title="Permalink to this headline">¶</a></h2>
<p>The PHP closing tag on a PHP document <strong>?&gt;</strong> is optional to the PHP
parser. However, if used, any whitespace following the closing tag,
whether introduced by the developer, user, or an FTP application, can
cause unwanted output, PHP errors, or if the latter are suppressed,
blank pages. For this reason, all PHP files MUST OMIT the PHP closing
tag and end with a single empty line instead.</p>
</div>
<div class="section" id="file-naming">
<h2><a class="toc-backref" href="#id6">File Naming</a><a class="headerlink" href="#file-naming" title="Permalink to this headline">¶</a></h2>
<p>Class files must be named in a Ucfirst-like manner, while any other file name
(configurations, views, generic scripts, etc.) should be in all lowercase.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">somelibrary</span><span class="o">.</span><span class="nx">php</span>
<span class="nx">someLibrary</span><span class="o">.</span><span class="nx">php</span>
<span class="nx">SOMELIBRARY</span><span class="o">.</span><span class="nx">php</span>
<span class="nx">Some_Library</span><span class="o">.</span><span class="nx">php</span>

<span class="nx">Application_config</span><span class="o">.</span><span class="nx">php</span>
<span class="nx">Application_Config</span><span class="o">.</span><span class="nx">php</span>
<span class="nx">applicationConfig</span><span class="o">.</span><span class="nx">php</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">Somelibrary</span><span class="o">.</span><span class="nx">php</span>
<span class="nx">Some_library</span><span class="o">.</span><span class="nx">php</span>

<span class="nx">applicationconfig</span><span class="o">.</span><span class="nx">php</span>
<span class="nx">application_config</span><span class="o">.</span><span class="nx">php</span>
</pre></div>
</div>
<p>Furthermore, class file names should match the name of the class itself.
For example, if you have a class named <cite>Myclass</cite>, then its filename must
be <strong>Myclass.php</strong>.</p>
</div>
<div class="section" id="class-and-method-naming">
<h2><a class="toc-backref" href="#id7">Class and Method Naming</a><a class="headerlink" href="#class-and-method-naming" title="Permalink to this headline">¶</a></h2>
<p>Class names should always start with an uppercase letter. Multiple words
should be separated with an underscore, and not CamelCased.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">superclass</span>
<span class="k">class</span> <span class="nc">SuperClass</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">Super_class</span>
</pre></div>
</div>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">class</span> <span class="nc">Super_class</span> <span class="p">{</span>

        <span class="k">public</span> <span class="k">function</span> <span class="nf">__construct</span><span class="p">()</span>
        <span class="p">{</span>

        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>Class methods should be entirely lowercased and named to clearly
indicate their function, preferably including a verb. Try to avoid
overly long and verbose names. Multiple words should be separated
with an underscore.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">fileproperties</span><span class="p">()</span>               <span class="c1">// not descriptive and needs underscore separator</span>
<span class="k">function</span> <span class="nf">fileProperties</span><span class="p">()</span>               <span class="c1">// not descriptive and uses CamelCase</span>
<span class="k">function</span> <span class="nf">getfileproperties</span><span class="p">()</span>            <span class="c1">// Better!  But still missing underscore separator</span>
<span class="k">function</span> <span class="nf">getFileProperties</span><span class="p">()</span>            <span class="c1">// uses CamelCase</span>
<span class="k">function</span> <span class="nf">get_the_file_properties_from_the_file</span><span class="p">()</span>        <span class="c1">// wordy</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">get_file_properties</span><span class="p">()</span>  <span class="c1">// descriptive, underscore separator, and all lowercase letters</span>
</pre></div>
</div>
</div>
<div class="section" id="variable-names">
<h2><a class="toc-backref" href="#id8">Variable Names</a><a class="headerlink" href="#variable-names" title="Permalink to this headline">¶</a></h2>
<p>The guidelines for variable naming are very similar to those used for
class methods. Variables should contain only lowercase letters,
use underscore separators, and be reasonably named to indicate their
purpose and contents. Very short, non-word variables should only be used
as iterators in for() loops.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$j</span> <span class="o">=</span> <span class="s1">'foo'</span><span class="p">;</span>             <span class="c1">// single letter variables should only be used in for() loops</span>
<span class="nv">$Str</span>                    <span class="c1">// contains uppercase letters</span>
<span class="nv">$bufferedText</span>           <span class="c1">// uses CamelCasing, and could be shortened without losing semantic meaning</span>
<span class="nv">$groupid</span>                <span class="c1">// multiple words, needs underscore separator</span>
<span class="nv">$name_of_last_city_used</span> <span class="c1">// too long</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">for</span> <span class="p">(</span><span class="nv">$j</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="nv">$j</span> <span class="o">&lt;</span> <span class="mi">10</span><span class="p">;</span> <span class="nv">$j</span><span class="o">++</span><span class="p">)</span>
<span class="nv">$str</span>
<span class="nv">$buffer</span>
<span class="nv">$group_id</span>
<span class="nv">$last_city</span>
</pre></div>
</div>
</div>
<div class="section" id="commenting">
<h2><a class="toc-backref" href="#id9">Commenting</a><a class="headerlink" href="#commenting" title="Permalink to this headline">¶</a></h2>
<p>In general, code should be commented prolifically. It not only helps
describe the flow and intent of the code for less experienced
programmers, but can prove invaluable when returning to your own code
months down the line. There is not a required format for comments, but
the following are recommended.</p>
<p><a class="reference external" href="http://manual.phpdoc.org/HTMLSmartyConverter/HandS/phpDocumentor/tutorial_phpDocumentor.howto.pkg.html#basics.docblock">DocBlock</a>
style comments preceding class, method, and property declarations so they can be
picked up by IDEs:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="sd">/**</span>
<span class="sd"> * Super Class</span>
<span class="sd"> *</span>
<span class="sd"> * @package     Package Name</span>
<span class="sd"> * @subpackage  Subpackage</span>
<span class="sd"> * @category    Category</span>
<span class="sd"> * @author      Author Name</span>
<span class="sd"> * @link        http://example.com</span>
<span class="sd"> */</span>
<span class="k">class</span> <span class="nc">Super_class</span> <span class="p">{</span>
</pre></div>
</div>
<div class="highlight-ci"><div class="highlight"><pre><span class="sd">/**</span>
<span class="sd"> * Encodes string for use in XML</span>
<span class="sd"> *</span>
<span class="sd"> * @param       string  $str    Input string</span>
<span class="sd"> * @return      string</span>
<span class="sd"> */</span>
<span class="k">function</span> <span class="nf">xml_encode</span><span class="p">(</span><span class="nv">$str</span><span class="p">)</span>
</pre></div>
</div>
<div class="highlight-ci"><div class="highlight"><pre><span class="sd">/**</span>
<span class="sd"> * Data for class manipulation</span>
<span class="sd"> *</span>
<span class="sd"> * @var array</span>
<span class="sd"> */</span>
<span class="k">public</span> <span class="nv">$data</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span>
</pre></div>
</div>
<p>Use single line comments within code, leaving a blank line between large
comment blocks and code.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="c1">// break up the string by newlines</span>
<span class="nv">$parts</span> <span class="o">=</span> <span class="nb">explode</span><span class="p">(</span><span class="s2">"</span><span class="se">\n</span><span class="s2">"</span><span class="p">,</span> <span class="nv">$str</span><span class="p">);</span>

<span class="c1">// A longer comment that needs to give greater detail on what is</span>
<span class="c1">// occurring and why can use multiple single-line comments.  Try to</span>
<span class="c1">// keep the width reasonable, around 70 characters is the easiest to</span>
<span class="c1">// read.  Don't hesitate to link to permanent external resources</span>
<span class="c1">// that may provide greater detail:</span>
<span class="c1">//</span>
<span class="c1">// http://example.com/information_about_something/in_particular/</span>

<span class="nv">$parts</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">foo</span><span class="p">(</span><span class="nv">$parts</span><span class="p">);</span>
</pre></div>
</div>
</div>
<div class="section" id="constants">
<h2><a class="toc-backref" href="#id10">Constants</a><a class="headerlink" href="#constants" title="Permalink to this headline">¶</a></h2>
<p>Constants follow the same guidelines as do variables, except constants
should always be fully uppercase. <em>Always use CodeIgniter constants when
appropriate, i.e. SLASH, LD, RD, PATH_CACHE, etc.</em></p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">myConstant</span>      <span class="c1">// missing underscore separator and not fully uppercase</span>
<span class="nx">N</span>               <span class="c1">// no single-letter constants</span>
<span class="nx">S_C_VER</span>         <span class="c1">// not descriptive</span>
<span class="nv">$str</span> <span class="o">=</span> <span class="nb">str_replace</span><span class="p">(</span><span class="s1">'{foo}'</span><span class="p">,</span> <span class="s1">'bar'</span><span class="p">,</span> <span class="nv">$str</span><span class="p">);</span>       <span class="c1">// should use LD and RD constants</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nx">MY_CONSTANT</span>
<span class="nx">NEWLINE</span>
<span class="nx">SUPER_CLASS_VERSION</span>
<span class="nv">$str</span> <span class="o">=</span> <span class="nb">str_replace</span><span class="p">(</span><span class="nx">LD</span><span class="o">.</span><span class="s1">'foo'</span><span class="o">.</span><span class="nx">RD</span><span class="p">,</span> <span class="s1">'bar'</span><span class="p">,</span> <span class="nv">$str</span><span class="p">);</span>
</pre></div>
</div>
</div>
<div class="section" id="true-false-and-null">
<h2><a class="toc-backref" href="#id11">TRUE, FALSE, and NULL</a><a class="headerlink" href="#true-false-and-null" title="Permalink to this headline">¶</a></h2>
<p><strong>TRUE</strong>, <strong>FALSE</strong>, and <strong>NULL</strong> keywords should always be fully
uppercase.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nv">$foo</span> <span class="o">==</span> <span class="k">true</span><span class="p">)</span>
<span class="nv">$bar</span> <span class="o">=</span> <span class="k">false</span><span class="p">;</span>
<span class="k">function</span> <span class="nf">foo</span><span class="p">(</span><span class="nv">$bar</span> <span class="o">=</span> <span class="k">null</span><span class="p">)</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nv">$foo</span> <span class="o">==</span> <span class="k">TRUE</span><span class="p">)</span>
<span class="nv">$bar</span> <span class="o">=</span> <span class="k">FALSE</span><span class="p">;</span>
<span class="k">function</span> <span class="nf">foo</span><span class="p">(</span><span class="nv">$bar</span> <span class="o">=</span> <span class="k">NULL</span><span class="p">)</span>
</pre></div>
</div>
</div>
<div class="section" id="logical-operators">
<h2><a class="toc-backref" href="#id12">Logical Operators</a><a class="headerlink" href="#logical-operators" title="Permalink to this headline">¶</a></h2>
<p>Use of the <tt class="docutils literal"><span class="pre">||</span></tt> “or” comparison operator is discouraged, as its clarity
on some output devices is low (looking like the number 11, for instance).
<tt class="docutils literal"><span class="pre">&amp;&amp;</span></tt> is preferred over <tt class="docutils literal"><span class="pre">AND</span></tt> but either are acceptable, and a space should
always precede and follow <tt class="docutils literal"><span class="pre">!</span></tt>.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nv">$foo</span> <span class="o">||</span> <span class="nv">$bar</span><span class="p">)</span>
<span class="k">if</span> <span class="p">(</span><span class="nv">$foo</span> <span class="k">AND</span> <span class="nv">$bar</span><span class="p">)</span>  <span class="c1">// okay but not recommended for common syntax highlighting applications</span>
<span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="nv">$foo</span><span class="p">)</span>
<span class="k">if</span> <span class="p">(</span><span class="o">!</span> <span class="nb">is_array</span><span class="p">(</span><span class="nv">$foo</span><span class="p">))</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nv">$foo</span> <span class="k">OR</span> <span class="nv">$bar</span><span class="p">)</span>
<span class="k">if</span> <span class="p">(</span><span class="nv">$foo</span> <span class="o">&amp;&amp;</span> <span class="nv">$bar</span><span class="p">)</span> <span class="c1">// recommended</span>
<span class="k">if</span> <span class="p">(</span> <span class="o">!</span> <span class="nv">$foo</span><span class="p">)</span>
<span class="k">if</span> <span class="p">(</span> <span class="o">!</span> <span class="nb">is_array</span><span class="p">(</span><span class="nv">$foo</span><span class="p">))</span>
</pre></div>
</div>
</div>
<div class="section" id="comparing-return-values-and-typecasting">
<h2><a class="toc-backref" href="#id13">Comparing Return Values and Typecasting</a><a class="headerlink" href="#comparing-return-values-and-typecasting" title="Permalink to this headline">¶</a></h2>
<p>Some PHP functions return FALSE on failure, but may also have a valid
return value of “” or 0, which would evaluate to FALSE in loose
comparisons. Be explicit by comparing the variable type when using these
return values in conditionals to ensure the return value is indeed what
you expect, and not a value that has an equivalent loose-type
evaluation.</p>
<p>Use the same stringency in returning and checking your own variables.
Use <strong>===</strong> and <strong>!==</strong> as necessary.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="c1">// If 'foo' is at the beginning of the string, strpos will return a 0,</span>
<span class="c1">// resulting in this conditional evaluating as TRUE</span>
<span class="k">if</span> <span class="p">(</span><span class="nb">strpos</span><span class="p">(</span><span class="nv">$str</span><span class="p">,</span> <span class="s1">'foo'</span><span class="p">)</span> <span class="o">==</span> <span class="k">FALSE</span><span class="p">)</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nb">strpos</span><span class="p">(</span><span class="nv">$str</span><span class="p">,</span> <span class="s1">'foo'</span><span class="p">)</span> <span class="o">===</span> <span class="k">FALSE</span><span class="p">)</span>
</pre></div>
</div>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">build_string</span><span class="p">(</span><span class="nv">$str</span> <span class="o">=</span> <span class="s2">""</span><span class="p">)</span>
<span class="p">{</span>
        <span class="k">if</span> <span class="p">(</span><span class="nv">$str</span> <span class="o">==</span> <span class="s2">""</span><span class="p">)</span> <span class="c1">// uh-oh!  What if FALSE or the integer 0 is passed as an argument?</span>
        <span class="p">{</span>

        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">build_string</span><span class="p">(</span><span class="nv">$str</span> <span class="o">=</span> <span class="s2">""</span><span class="p">)</span>
<span class="p">{</span>
        <span class="k">if</span> <span class="p">(</span><span class="nv">$str</span> <span class="o">===</span> <span class="s2">""</span><span class="p">)</span>
        <span class="p">{</span>

        <span class="p">}</span>
<span class="p">}</span>
</pre></div>
</div>
<p>See also information regarding <a class="reference external" href="http://php.net/manual/en/language.types.type-juggling.php#language.types.typecasting">typecasting</a>,
which can be quite useful. Typecasting has a slightly different effect
which may be desirable. When casting a variable as a string, for
instance, NULL and boolean FALSE variables become empty strings, 0 (and
other numbers) become strings of digits, and boolean TRUE becomes “1”:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$str</span> <span class="o">=</span> <span class="p">(</span><span class="nx">string</span><span class="p">)</span> <span class="nv">$str</span><span class="p">;</span> <span class="c1">// cast $str as a string</span>
</pre></div>
</div>
</div>
<div class="section" id="debugging-code">
<h2><a class="toc-backref" href="#id14">Debugging Code</a><a class="headerlink" href="#debugging-code" title="Permalink to this headline">¶</a></h2>
<p>Do not leave debugging code in your submissions, even when commented out.
Things such as <tt class="docutils literal"><span class="pre">var_dump()</span></tt>, <tt class="docutils literal"><span class="pre">print_r()</span></tt>, <tt class="docutils literal"><span class="pre">die()</span></tt>/<tt class="docutils literal"><span class="pre">exit()</span></tt> should not be included
in your code unless it serves a specific purpose other than debugging.</p>
</div>
<div class="section" id="whitespace-in-files">
<h2><a class="toc-backref" href="#id15">Whitespace in Files</a><a class="headerlink" href="#whitespace-in-files" title="Permalink to this headline">¶</a></h2>
<p>No whitespace can precede the opening PHP tag or follow the closing PHP
tag. Output is buffered, so whitespace in your files can cause output to
begin before CodeIgniter outputs its content, leading to errors and an
inability for CodeIgniter to send proper headers.</p>
</div>
<div class="section" id="compatibility">
<h2><a class="toc-backref" href="#id16">Compatibility</a><a class="headerlink" href="#compatibility" title="Permalink to this headline">¶</a></h2>
<p>CodeIgniter recommends PHP 5.6 or newer to be used, but it should be
compatible with PHP 5.3.7. Your code must either be compatible with this
requirement, provide a suitable fallback, or be an optional feature that
dies quietly without affecting a user’s application.</p>
<p>Additionally, do not use PHP functions that require non-default libraries
to be installed unless your code contains an alternative method when the
function is not available.</p>
</div>
<div class="section" id="one-file-per-class">
<h2><a class="toc-backref" href="#id17">One File per Class</a><a class="headerlink" href="#one-file-per-class" title="Permalink to this headline">¶</a></h2>
<p>Use separate files for each class, unless the classes are <em>closely related</em>.
An example of a CodeIgniter file that contains multiple classes is the
Xmlrpc library file.</p>
</div>
<div class="section" id="whitespace">
<h2><a class="toc-backref" href="#id18">Whitespace</a><a class="headerlink" href="#whitespace" title="Permalink to this headline">¶</a></h2>
<p>Use tabs for whitespace in your code, not spaces. This may seem like a
small thing, but using tabs instead of whitespace allows the developer
looking at your code to have indentation at levels that they prefer and
customize in whatever application they use. And as a side benefit, it
results in (slightly) more compact files, storing one tab character
versus, say, four space characters.</p>
</div>
<div class="section" id="line-breaks">
<h2><a class="toc-backref" href="#id19">Line Breaks</a><a class="headerlink" href="#line-breaks" title="Permalink to this headline">¶</a></h2>
<p>Files must be saved with Unix line breaks. This is more of an issue for
developers who work in Windows, but in any case ensure that your text
editor is setup to save files with Unix line breaks.</p>
</div>
<div class="section" id="code-indenting">
<h2><a class="toc-backref" href="#id20">Code Indenting</a><a class="headerlink" href="#code-indenting" title="Permalink to this headline">¶</a></h2>
<p>Use Allman style indenting. With the exception of Class declarations,
braces are always placed on a line by themselves, and indented at the
same level as the control statement that “owns” them.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">foo</span><span class="p">(</span><span class="nv">$bar</span><span class="p">)</span> <span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>

<span class="k">foreach</span> <span class="p">(</span><span class="nv">$arr</span> <span class="k">as</span> <span class="nv">$key</span> <span class="o">=&gt;</span> <span class="nv">$val</span><span class="p">)</span> <span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>

<span class="k">if</span> <span class="p">(</span><span class="nv">$foo</span> <span class="o">==</span> <span class="nv">$bar</span><span class="p">)</span> <span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span> <span class="k">else</span> <span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>

<span class="k">for</span> <span class="p">(</span><span class="nv">$i</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="nv">$i</span> <span class="o">&lt;</span> <span class="mi">10</span><span class="p">;</span> <span class="nv">$i</span><span class="o">++</span><span class="p">)</span>
        <span class="p">{</span>
        <span class="k">for</span> <span class="p">(</span><span class="nv">$j</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="nv">$j</span> <span class="o">&lt;</span> <span class="mi">10</span><span class="p">;</span> <span class="nv">$j</span><span class="o">++</span><span class="p">)</span>
                <span class="p">{</span>
                <span class="c1">// ...</span>
                <span class="p">}</span>
        <span class="p">}</span>

<span class="k">try</span> <span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>
<span class="k">catch</span><span class="p">()</span> <span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">foo</span><span class="p">(</span><span class="nv">$bar</span><span class="p">)</span>
<span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>

<span class="k">foreach</span> <span class="p">(</span><span class="nv">$arr</span> <span class="k">as</span> <span class="nv">$key</span> <span class="o">=&gt;</span> <span class="nv">$val</span><span class="p">)</span>
<span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>

<span class="k">if</span> <span class="p">(</span><span class="nv">$foo</span> <span class="o">==</span> <span class="nv">$bar</span><span class="p">)</span>
<span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>
<span class="k">else</span>
<span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>

<span class="k">for</span> <span class="p">(</span><span class="nv">$i</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="nv">$i</span> <span class="o">&lt;</span> <span class="mi">10</span><span class="p">;</span> <span class="nv">$i</span><span class="o">++</span><span class="p">)</span>
<span class="p">{</span>
        <span class="k">for</span> <span class="p">(</span><span class="nv">$j</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span> <span class="nv">$j</span> <span class="o">&lt;</span> <span class="mi">10</span><span class="p">;</span> <span class="nv">$j</span><span class="o">++</span><span class="p">)</span>
        <span class="p">{</span>
                <span class="c1">// ...</span>
        <span class="p">}</span>
<span class="p">}</span>

<span class="k">try</span>
<span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>
<span class="k">catch</span><span class="p">()</span>
<span class="p">{</span>
        <span class="c1">// ...</span>
<span class="p">}</span>
</pre></div>
</div>
</div>
<div class="section" id="bracket-and-parenthetic-spacing">
<h2><a class="toc-backref" href="#id21">Bracket and Parenthetic Spacing</a><a class="headerlink" href="#bracket-and-parenthetic-spacing" title="Permalink to this headline">¶</a></h2>
<p>In general, parenthesis and brackets should not use any additional
spaces. The exception is that a space should always follow PHP control
structures that accept arguments with parenthesis (declare, do-while,
elseif, for, foreach, if, switch, while), to help distinguish them from
functions and increase readability.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$arr</span><span class="p">[</span> <span class="nv">$foo</span> <span class="p">]</span> <span class="o">=</span> <span class="s1">'foo'</span><span class="p">;</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$arr</span><span class="p">[</span><span class="nv">$foo</span><span class="p">]</span> <span class="o">=</span> <span class="s1">'foo'</span><span class="p">;</span> <span class="c1">// no spaces around array keys</span>
</pre></div>
</div>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">foo</span> <span class="p">(</span> <span class="nv">$bar</span> <span class="p">)</span>
<span class="p">{</span>

<span class="p">}</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">foo</span><span class="p">(</span><span class="nv">$bar</span><span class="p">)</span> <span class="c1">// no spaces around parenthesis in function declarations</span>
<span class="p">{</span>

<span class="p">}</span>
</pre></div>
</div>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">foreach</span><span class="p">(</span> <span class="nv">$query</span><span class="o">-&gt;</span><span class="na">result</span><span class="p">()</span> <span class="k">as</span> <span class="nv">$row</span> <span class="p">)</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">foreach</span> <span class="p">(</span><span class="nv">$query</span><span class="o">-&gt;</span><span class="na">result</span><span class="p">()</span> <span class="k">as</span> <span class="nv">$row</span><span class="p">)</span> <span class="c1">// single space following PHP control structures, but not in interior parenthesis</span>
</pre></div>
</div>
</div>
<div class="section" id="localized-text">
<h2><a class="toc-backref" href="#id22">Localized Text</a><a class="headerlink" href="#localized-text" title="Permalink to this headline">¶</a></h2>
<p>CodeIgniter libraries should take advantage of corresponding language files
whenever possible.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">return</span> <span class="s2">"Invalid Selection"</span><span class="p">;</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">return</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">lang</span><span class="o">-&gt;</span><span class="na">line</span><span class="p">(</span><span class="s1">'invalid_selection'</span><span class="p">);</span>
</pre></div>
</div>
</div>
<div class="section" id="private-methods-and-variables">
<h2><a class="toc-backref" href="#id23">Private Methods and Variables</a><a class="headerlink" href="#private-methods-and-variables" title="Permalink to this headline">¶</a></h2>
<p>Methods and variables that are only accessed internally,
such as utility and helper functions that your public methods use for
code abstraction, should be prefixed with an underscore.</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">public</span> <span class="k">function</span> <span class="nf">convert_text</span><span class="p">()</span>
<span class="k">private</span> <span class="k">function</span> <span class="nf">_convert_text</span><span class="p">()</span>
</pre></div>
</div>
</div>
<div class="section" id="php-errors">
<h2><a class="toc-backref" href="#id24">PHP Errors</a><a class="headerlink" href="#php-errors" title="Permalink to this headline">¶</a></h2>
<p>Code must run error free and not rely on warnings and notices to be
hidden to meet this requirement. For instance, never access a variable
that you did not set yourself (such as <tt class="docutils literal"><span class="pre">$_POST</span></tt> array keys) without first
checking to see that it <tt class="docutils literal"><span class="pre">isset()</span></tt>.</p>
<p>Make sure that your dev environment has error reporting enabled
for ALL users, and that display_errors is enabled in the PHP
environment. You can check this setting with:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">if</span> <span class="p">(</span><span class="nb">ini_get</span><span class="p">(</span><span class="s1">'display_errors'</span><span class="p">)</span> <span class="o">==</span> <span class="mi">1</span><span class="p">)</span>
<span class="p">{</span>
        <span class="k">exit</span> <span class="s2">"Enabled"</span><span class="p">;</span>
<span class="p">}</span>
</pre></div>
</div>
<p>On some servers where <em>display_errors</em> is disabled, and you do not have
the ability to change this in the php.ini, you can often enable it with:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nb">ini_set</span><span class="p">(</span><span class="s1">'display_errors'</span><span class="p">,</span> <span class="mi">1</span><span class="p">);</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Setting the <a class="reference external" href="http://php.net/manual/en/errorfunc.configuration.php#ini.display-errors">display_errors</a>
setting with <tt class="docutils literal"><span class="pre">ini_set()</span></tt> at runtime is not identical to having
it enabled in the PHP environment. Namely, it will not have any
effect if the script has fatal errors.</p>
</div>
</div>
<div class="section" id="short-open-tags">
<h2><a class="toc-backref" href="#id25">Short Open Tags</a><a class="headerlink" href="#short-open-tags" title="Permalink to this headline">¶</a></h2>
<p>Always use full PHP opening tags, in case a server does not have
<em>short_open_tag</em> enabled.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span> <span class="k">echo</span> <span class="nv">$foo</span><span class="p">;</span> <span class="cp">?&gt;</span>

<span class="cp">&lt;?</span><span class="o">=</span><span class="nv">$foo</span><span class="cp">?&gt;</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="o">&lt;?</span><span class="nx">php</span> <span class="k">echo</span> <span class="nv">$foo</span><span class="p">;</span> <span class="cp">?&gt;</span>
</pre></div>
</div>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">PHP 5.4 will always have the <strong>&lt;?=</strong> tag available.</p>
</div>
</div>
<div class="section" id="one-statement-per-line">
<h2><a class="toc-backref" href="#id26">One Statement Per Line</a><a class="headerlink" href="#one-statement-per-line" title="Permalink to this headline">¶</a></h2>
<p>Never combine statements on one line.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$foo</span> <span class="o">=</span> <span class="s1">'this'</span><span class="p">;</span> <span class="nv">$bar</span> <span class="o">=</span> <span class="s1">'that'</span><span class="p">;</span> <span class="nv">$bat</span> <span class="o">=</span> <span class="nb">str_replace</span><span class="p">(</span><span class="nv">$foo</span><span class="p">,</span> <span class="nv">$bar</span><span class="p">,</span> <span class="nv">$bag</span><span class="p">);</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$foo</span> <span class="o">=</span> <span class="s1">'this'</span><span class="p">;</span>
<span class="nv">$bar</span> <span class="o">=</span> <span class="s1">'that'</span><span class="p">;</span>
<span class="nv">$bat</span> <span class="o">=</span> <span class="nb">str_replace</span><span class="p">(</span><span class="nv">$foo</span><span class="p">,</span> <span class="nv">$bar</span><span class="p">,</span> <span class="nv">$bag</span><span class="p">);</span>
</pre></div>
</div>
</div>
<div class="section" id="strings">
<h2><a class="toc-backref" href="#id27">Strings</a><a class="headerlink" href="#strings" title="Permalink to this headline">¶</a></h2>
<p>Always use single quoted strings unless you need variables parsed, and
in cases where you do need variables parsed, use braces to prevent
greedy token parsing. You may also use double-quoted strings if the
string contains single quotes, so you do not have to use escape
characters.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="s2">"My String"</span>                                     <span class="c1">// no variable parsing, so no use for double quotes</span>
<span class="s2">"My string </span><span class="si">$foo</span><span class="s2">"</span>                                <span class="c1">// needs braces</span>
<span class="s1">'SELECT foo FROM bar WHERE baz = \'bag\''</span>       <span class="c1">// ugly</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="s1">'My String'</span>
<span class="s2">"My string </span><span class="si">{</span><span class="nv">$foo</span><span class="si">}</span><span class="s2">"</span>
<span class="s2">"SELECT foo FROM bar WHERE baz = 'bag'"</span>
</pre></div>
</div>
</div>
<div class="section" id="sql-queries">
<h2><a class="toc-backref" href="#id28">SQL Queries</a><a class="headerlink" href="#sql-queries" title="Permalink to this headline">¶</a></h2>
<p>SQL keywords are always capitalized: SELECT, INSERT, UPDATE, WHERE,
AS, JOIN, ON, IN, etc.</p>
<p>Break up long queries into multiple lines for legibility, preferably
breaking for each clause.</p>
<p><strong>INCORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="c1">// keywords are lowercase and query is too long for</span>
<span class="c1">// a single line (... indicates continuation of line)</span>
<span class="nv">$query</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">db</span><span class="o">-&gt;</span><span class="na">query</span><span class="p">(</span><span class="s2">"select foo, bar, baz, foofoo, foobar as raboof, foobaz from exp_pre_email_addresses</span>
<span class="s2">...where foo != 'oof' and baz != 'zab' order by foobaz limit 5, 100"</span><span class="p">);</span>
</pre></div>
</div>
<p><strong>CORRECT</strong>:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="nv">$query</span> <span class="o">=</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">db</span><span class="o">-&gt;</span><span class="na">query</span><span class="p">(</span><span class="s2">"SELECT foo, bar, baz, foofoo, foobar AS raboof, foobaz</span>
<span class="s2">                                FROM exp_pre_email_addresses</span>
<span class="s2">                                WHERE foo != 'oof'</span>
<span class="s2">                                AND baz != 'zab'</span>
<span class="s2">                                ORDER BY foobaz</span>
<span class="s2">                                LIMIT 5, 100"</span><span class="p">);</span>
</pre></div>
</div>
</div>
<div class="section" id="default-function-arguments">
<h2><a class="toc-backref" href="#id29">Default Function Arguments</a><a class="headerlink" href="#default-function-arguments" title="Permalink to this headline">¶</a></h2>
<p>Whenever appropriate, provide function argument defaults, which helps
prevent PHP errors with mistaken calls and provides common fallback
values which can save a few lines of code. Example:</p>
<div class="highlight-ci"><div class="highlight"><pre><span class="k">function</span> <span class="nf">foo</span><span class="p">(</span><span class="nv">$bar</span> <span class="o">=</span> <span class="s1">''</span><span class="p">,</span> <span class="nv">$baz</span> <span class="o">=</span> <span class="k">FALSE</span><span class="p">)</span>
</pre></div>
</div>
</div>
</div>



</div>
</div>
</body>
</html>