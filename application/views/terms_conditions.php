<div class="row content">
    <div class="small-12 columns page-title text-center">
        <?= lang('terms_and_conditions_title') ?>
    </div>
    <div class="small-12 columns entry textpad">
        <?= lang('terms_and_conditions_text') ?>
    </div>
</div>