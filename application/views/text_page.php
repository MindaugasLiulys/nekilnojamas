    <div class="row content">
      <div class="small-12 columns page-title text-center">
        <?php echo $title; ?>
      </div>
      <div class="small-12 columns entry textpad">
        <h2><?php echo $title; ?></h2>
        <p>
          Lorem ipsum dolor sit amet, at amet lorem metus pellentesque sed id, donec suscipit lacinia. Id libero libero tristique commodo. Mattis elit amet risus nam ac, vel lectus vehicula proin massa eget vitae, egestas egestas praesent. Penatibus mollis massa vestibulum, consectetuer eu aliquam, ultrices etiam et non duis, sagittis purus vivamus augue. Lobortis eros, augue vehicula. Ultrices nonummy erat interdum lacus est non, ornare in platea, mollis dolor sapien rutrum lectus suspendisse, tempor donec odio maecenas. Tincidunt est semper odio elementum, odio aenean, at ut in nunc nec, laoreet dui maecenas lacus. Proin ultrices sed wisi, velit in consectetuer, libero libero eu quis, pellentesque in a vestibulum sagittis, pede suscipit. Magni risus, sed eget vestibulum elementum, wisi molestie, quis sit diam sapien, morbi lacus suscipit ultricies in. Pede vel magna, sapien id ut id ut diam consectetuer, porta nec vitae faucibus aenean sollicitudin, parturient velit metus pretium, tristique rutrum nulla et. Lorem quos dui placerat platea id ut. Tristique suspendisse facilisis sollicitudin, arcu cursus, sit  mauris et integer at duis amet. Vel est massa congue odit, sed justo orci adipiscing consequatur urna feugiat, nec lobortis suspendisse turpis, interdum aenean. Amet vel potenti luctus a vivamus, id condimentum sem cras pede, condimentum arcu, commodo a. Et arcu lacus pulvinar. Justo amet erat laoreet luctus proin sit, iaculis amet aenean, diam in in lectus nec ligula. Ut pharetra, pretium odio porttitor habitasse, tortor vitae libero, malesuada eu eget, elit vulputate amet vulputate.
        </p>
        <ul>
          <li>Mauris quis erat et molestie et, malesuada leo id sem non ante, sed sollicitudin lacinia lorem tempor fusce, diam neque;</li>
          <li>Placerat in conubia, id sit donec orci mauris duis congue, praesent suscipit morbi feugiat;</li>
          <li>lectus eget enim eu orci vitae, aliquet et est. Blandit cum a integer maecenas, sed augue</li>
        </ul>
        <p>
          Lorem ipsum dolor sit amet, at amet lorem metus pellentesque sed id, donec suscipit lacinia. Id libero libero tristique commodo. Mattis elit amet risus nam ac, vel lectus vehicula proin massa eget vitae, egestas egestas praesent. Penatibus mollis massa vestibulum, consectetuer eu aliquam, ultrices etiam et non duis, sagittis purus vivamus augue. Lobortis eros, augue vehicul.
        </p>
        <div class="row">
          <div class="small-12 columns gallery nopad">
            <div class="small-4 columns">
              <img src="<?php echo base_url(); ?>assets/img/img.png">
            </div>
            <div class="small-4 columns">
              <img src="<?php echo base_url(); ?>assets/img/img.png">
            </div>
            <div class="small-4 columns">
              <img src="<?php echo base_url(); ?>assets/img/img.png">
            </div>
          </div>text       </div>
        <h2>Lorem ipsum (h2)</h2>
        <p>
          Lorem ipsum dolor sit amet, at amet lorem metus pellentesque sed id, donec suscipit lacinia. Id libero libero tristique commodo. Mattis elit amet risus nam ac, vel lectus vehicula proin massa eget vitae, egestas egestas praesent. Penatibus mollis massa vestibulum, consectetuer eu aliquam, ultrices etiam et non duis, sagittis purus vivamus augue. Lobortis eros, augue vehicula. Ultrices nonummy erat interdum lacus est non, ornare in platea, mollis dolor sapien rutrum lectus suspendisse, tempor donec odio maecenas. Tincidunt est semper odio elementum, odio aenean, at ut in nunc nec, laoreet dui maecenas lacus. Proin ultrices sed wisi, velit in consectetuer, libero libero eu quis, pellentesque in a vestibulum sagittis, pede suscipit. Magni risus, sed eget vestibulum elementum, wisi molestie, quis sit diam sapien, morbi lacus suscipit ultricies in. Pede vel magna, sapien id ut id ut diam consectetuer, porta nec vitae faucibus aenean sollicitudin, parturient velit metus pretium, tristique rutrum nulla et. Lorem quos dui placerat platea id ut. Tristique suspendisse facilisis sollicitudin, arcu cursus, sit  mauris et integer at duis amet. Vel est massa congue odit, sed justo orci adipiscing consequatur urna feugiat, nec lobortis suspendisse turpis, interdum aenean. Amet vel potenti luctus a vivamus, id condimentum sem cras pede, condimentum arcu, commodo a. Et arcu lacus pulvinar. Justo amet erat laoreet luctus proin sit, iaculis amet aenean, diam in in lectus nec ligula. Ut pharetra, pretium odio porttitor habitasse, tortor vitae libero, malesuada eu eget, elit vulputate amet vulputate.
        </p>
        <h2>Lorem ipsum (h2)</h2>
        <p>
          Lorem ipsum dolor sit amet, at amet lorem metus pellentesque sed id, donec suscipit lacinia. Id libero libero tristique commodo. Mattis elit amet risus nam ac, vel lectus vehicula proin massa eget vitae, egestas egestas praesent. Penatibus mollis massa vestibulum, consectetuer eu aliquam, ultrices etiam et non duis, sagittis purus vivamus augue. Lobortis eros, augue vehicula. Ultrices nonummy erat interdum lacus est non, ornare in platea, mollis dolor sapien rutrum lectus suspendisse, tempor donec odio maecenas. Tincidunt est semper odio elementum, odio aenean, at ut in nunc nec, laoreet dui maecenas lacus.
        </p>
        <h2>Lorem ipsum (h2)</h2>
        <p>
          Lorem ipsum dolor sit amet, at amet lorem metus pellentesque sed id, donec suscipit lacinia. Id libero libero tristique commodo. Mattis elit amet risus nam ac, vel lectus vehicula proin massa eget vitae, egestas egestas praesent. Penatibus mollis massa vestibulum, consectetuer eu aliquam, ultrices etiam et non duis, sagittis purus vivamus augue. Lobortis eros, augue vehicula. Ultrices nonummy erat interdum lacus est non, ornare in platea, mollis dolor sapien rutrum lectus suspendisse, tempor donec odio maecenas. Tincidunt est semper odio elementum, odio aenean, at ut in nunc nec, laoreet dui maecenas lacus. Proin ultrices sed wisi, velit in consectetuer, libero libero eu quis, pellentesque in a vestibulum sagittis, pede suscipit. Magni risus, sed eget vestibulum elementum, wisi molestie, quis sit diam sapien, morbi lacus suscipit ultricies in. Pede vel magna, sapien id ut id ut diam consectetuer, porta nec vitae faucibus aenean sollicitudin, parturient velit metus pretium, tristique rutrum nulla et. Lorem quos dui placerat platea id ut. Tristique suspendisse facilisis sollicitudin, arcu cursus, sit  mauris et integer at duis amet. Vel est massa congue odit, sed justo orci adipiscing consequatur urna feugiat, nec lobortis suspendisse turpis, interdum aenean. Amet vel potenti luctus a vivamus, id condimentum sem cras pede, condimentum arcu, commodo a. Et arcu lacus pulvinar. Justo amet erat laoreet luctus proin sit, iaculis amet aenean, diam in in lectus nec ligula. Ut pharetra, pretium odio porttitor habitasse, tortor vitae libero, malesuada eu eget, elit vulputate amet vulputate.
        </p>
        

      </div>
    </div>