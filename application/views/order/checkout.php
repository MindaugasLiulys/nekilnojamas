<div class="row content">

    <?php
    if ($response->meta->result) { //Create Checkout session OK
        ?>

        <script type="text/javascript">
            (function (n, t, i, r, u, f, e) {
                n[u] = n[u] || function () {
                        (n[u].q = n[u].q || []).push(arguments)
                    };
                f = t.createElement(i);
                e = t.getElementsByTagName(i)[0];
                f.async = 1;
                f.src = r;
                e.parentNode.insertBefore(f, e)
            })(window, document, "script",
                "https://v1.checkout.bambora.com/assets/paymentwindow-v1.min.js", "bam");
        </script>

        <script type="text/javascript">
            var options = {
                windowstate: 2
            }
            bam("open", "<?php echo $response->url ?>", options);
        </script>

    <?php
    } else { //Create Checkout session fail
    ?>

        <p>Error: <?php echo $response->meta->message->enduser; ?></p>

        <?php
    }
    ?>
</div>
    