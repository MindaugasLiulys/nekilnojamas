<div class="row content">
    <div class="small-12 columns list-edit-your-space payment-page nopad">
        <div class="small-12 medium-push-6 medium-6 large-6 about-room columns">

            <div class="small-12 columns about-room-content nopad">
                <div class="small-12 medium-6 large-6 columns about-room-attributes nopad">
                    <div class="small-12 columns nopad">
                        <div class="small-6 columns attribute-title text-right nopadleft">
                            <?= lang('price') ?>:
                        </div>
                        <div class="small-6 columns attribute-text nopadright">
                            <?= format($info['price']) ?><?php if($info['category'] != 0){echo('/'); echo(lang('month'));}?>
                        </div>
                    </div>
                    <div class="small-12 columns nopad">
                        <div class="small-6 columns attribute-title text-right nopadleft">
                            <?= lang('start_date') ?>
                        </div>
                        <div class="small-6 columns attribute-text nopadright">
                            <?= $order['start_date'] ?>
                        </div>
                    </div>
                    <div class="small-12 columns nopad">
                        <div class="small-6 columns attribute-title text-right nopadleft">
                            <?= lang('end_date') ?>
                        </div>
                        <div class="small-6 columns attribute-text nopadright">
                            <?= $order['end_date'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="small-12 medium-pull-6 medium-6 large-6 columns">
            <div class="small-12 columns about-room-title nopad">
                <?= lang('about_spare_room') ?>
            </div>
            <div class="small-12 medium-6 large-12 columns about-room-text nopadleft">
                <?= utf8_decode($info['description']) ?> 
            </div>
        </div>
    </div>
    
    <div class="small-12 columns payment-total-price">
        <div style="font-size:13px" id="success"><i style="padding-right:10px" class="fa fa-info-circle" aria-hidden="true"></i><?= lang('payment_explanation') ?></div>
        <div style="display:none" id="errors"></div>
        <form action="https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx" method="post">
            <input type="hidden" name="merchantnumber" value="<?= EPAY_MERCHANT_NUMBER ?>">
            <input type="hidden" name="language" value="<?= $language_code ?>">
            <input type="hidden" name="amount" value="<?= $order['price']*100 ?>">
            <input type="hidden" name="currency" value="<?= $this->session->userdata('currency');  ?>">
            <input type="hidden" name="windowstate" value="3">
            <input type="hidden" name="callbackurl" value="<?= $callback ?>">
            <input type="hidden" name="accepturl" value="<?= $accepturl ?>">
            <input type="hidden" name="cancelurl" value="<?= $cancelurl ?>">
            <input type="hidden" name="orderid" value="<?= ($order['conversation_id']+100) ?>">
            <input type="hidden" name="hash" value="<?= $hash ?>">
            <div class="small-12 columns end-right align-center">
              <?= lang('total_price') ?>: <?= format($order['price']) ?>
            </div>

            <div class="small-12 columns end-right align-center font-size-12">
                <?= lang('agree_to') ?> <a target="_blank" href="<?php echo base_url('terms/index/terms-and-conditions') ?>"><?= lang('terms_and_conditions') ?></a>&nbsp;&nbsp;
                <input type="checkbox" id="terms" value="0" />
            </div>

            <div class="small-12 columns end-right align-center">
                <button class="btn-green" type="submit"><?= lang('pay') ?></button>
            </div>
        </form>    
    </div>
</div>
<script type="text/javascript">
    $(document)
    .on('click', 'form button[type=submit]', function(e) {
        var terms = $('#terms').is(":checked");

        $('#errors').hide();
        if(terms == false) {
            e.preventDefault(); //prevent the default action
            $('#errors').html('<?= lang('have_to_agree_terms') ?>');
            $('#errors').show();
        }
    });
</script>
