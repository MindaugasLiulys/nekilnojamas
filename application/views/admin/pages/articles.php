<div class="row">
	<div class="col-lg-12">
		<h2>Straipsniai (apie mus spaudoje)</h2>
		<button onclick="location.href='<?= site_url('admin/articles/add')?>';" type="button" class="btn btn-success">Pridėti naują</button>
		<div style="margin-top: 10px" class="table-responsive">
			<table class="table table-bordered table-hover table-striped tablesorter">
				<thead>
					<tr>
						<th class="header">ID <i class="fa fa-sort"></i></th>
						<th class="header">Sritis <i class="fa fa-sort"></i></th>
						<th class="header">Pavadinimas <i class="fa fa-sort"></i></th>
						<th class="header">Rikiavimas <i class="fa fa-sort"></i></th>
						<th class="header">Veiksmai</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($articles)): ?>
						<?php foreach($articles as $k => $v): ?>
							<tr>
								<td><?= $v['article_id'] ?></td>
								<td><?= $v['type'] ?></td>
								<td><?= $v['title'] ?></td>
								<td><?= $v['sort'] ?></td>
								<td>
									<a href="<?= site_url('admin/articles/edit/'.$v['article_id']) ?>">Redaguoti</a><br/>
									<a style="color:red" href="<?= site_url('admin/articles/delete/'.$v['article_id']) ?>">Trinti</a>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>