<style type="text/css">
	.huge {
	    font-size: 40px;
	}
	.panel-green {
	    border-color: #5cb85c;
	}
	.panel-green .panel-heading {
	    border-color: #5cb85c;
	    color: #fff;
	    background-color: #5cb85c;
	}
	.panel-yellow {
	    border-color: #f0ad4e;
	}
	.panel-yellow .panel-heading {
	    border-color: #f0ad4e;
	    color: #fff;
	    background-color: #f0ad4e;
	}
</style>
<div class="row">
	<div class="col-lg-12">
		<h2>General information</h2>
	</div>
	<div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-map-marker fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?= $spaces ?></div>
                        <div>Sparerooms</div>
                    </div>
                </div>
            </div>
            <a href="<?= site_url('admin/sparerooms')?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
	<div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?= $users ?></div>
                        <div>Users</div>
                    </div>
                </div>
            </div>
            <a href="<?= site_url('admin/users')?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
	<div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-credit-card fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?= $transactions ?></div>
                        <div>Transactions</div>
                    </div>
                </div>
            </div>
            <a href="<?= site_url('admin/transactions')?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>