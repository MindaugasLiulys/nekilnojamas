<div class="row">
	<div class="col-lg-12">
		<h2>Visi paveiksliukai</h2>
		<form enctype='multipart/form-data' role="form" method="post" action="<?= site_url('admin/images/add') ?>">
			<div class="panel panel-success">
				<div class="panel-body">
					<div class="form-group">
						<div class="col-md-2">
							<label>Kategorija</label>
						</div>
						<div class="col-md-10">
	                		<select name="cat_id">
	                			<?php foreach($categories as $k => $c): ?>
	                				<option value="<?= $k ?>"><?= $c; ?></option>
	                			<?php endforeach; ?>
	                		</select>
	                	</div>	
	                	<div class="clearfix"></div>
              		</div>
					<div class="form-group">
						<div class="col-md-2">
							<label>Prierašas/rikiavimas</label>
						</div>
						<div class="col-md-9">
                			<input type="text" class="form-control" value="" name="description" />
              			</div>
              			<div class="col-md-1">
                			<input type="text" class="form-control" name="sort" value="1" />
              			</div>
              			<div class="clearfix"></div>
              		</div>
					<div class="form-group">
						<div class="col-md-2">
							<label>Paveiksliukas</label>
						</div>
						<div class="col-md-10">
                			<input type="file" name="image" />
              			</div>
              			<div class="clearfix"></div>
              		</div>
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-5">
							<button type="submit" class="btn btn-primary">Išsaugoti</button>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</form>
		<div style="margin-top: 10px;padding-bottom:50px" class="table-responsive">
			<table class="table table-bordered table-hover table-striped tablesorter">
				<thead>
					<tr>
						<th class="header">Paveiksliukas <i class="fa fa-sort"></i></th>
						<th class="header">Kategorija <i class="fa fa-sort"></i></th>
						<th class="header">Prierašas <i class="fa fa-sort"></i></th>
						<th class="header">Rikiavimas <i class="fa fa-sort"></i></th>
						<th class="header">Veiksmas</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($images)): ?>
						<?php foreach($images as $k => $v): ?>
							<tr id="img_<?= $v['image_id'] ?>">
								<td>
									<img src="<?= base_url();?>images/thumbs/<?= $v['file_name']; ?>" />
								</td>
								<td><?= $categories[$v['cat_id']] ?></td>
								<td><?= $v['description_lt'] ?></td>
								<td><?= $v['sort'] ?></td>
								<td>
									<a style="color:green" href="<?= site_url('admin/images/edit/'.$v['image_id']) ?>">Redaguoti</a><br/><br/>
									<a style="color:red" href="<?= site_url('admin/images/delete/'.$v['image_id']) ?>">Trinti</a>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>