<div class="row">
	<div class="col-lg-12">
		<h2>Transactions</h2>
		<div style="margin-top: 10px" class="table-responsive">
			<?php if(!empty($transactions)): ?>
				<table class="table table-bordered table-hover table-striped tablesorter">
					<thead>
						<tr>
							<th class="header">ID</th>
							<th class="header">From user</th>
							<th class="header">To user</th>
							<th class="header">Amount (full)</th>
							<th class="header">Card number</th>
							<th class="header">Txnid</th>
							<th class="header">Date paid</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($transactions as $k => $v): ?>
							<tr>
								<td><?= $v['transaction_id'] ?></td>
								<td><?= $v['user_from'] ?></td>
								<td><?= $v['user_to'] ?></td>
								<td><?= $v['amount'] ?> DKK</td>
								<td><?= $v['card_number'] ?></td>
								<td><?= $v['txnid'] ?></td>
								<td><?= $v['date_added'] ?></td>
<!-- 								<td>
									<a href="<?= site_url('admin/articles/edit/'.$v['article_id']) ?>">Redaguoti</a><br/>
									<a style="color:red" href="<?= site_url('admin/articles/delete/'.$v['article_id']) ?>">Trinti</a>
								</td> -->
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else: ?>
				<b style="font-size:15px;">There are no transactions.</b>
			<?php endif; ?>
		</div>
	</div>
</div>