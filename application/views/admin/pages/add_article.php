<div class="row">
	<div class="col-lg-12">
		<h2>Edit</h2>
		<div class="bs-example">
			<ul class="nav nav-tabs" style="margin-bottom: 15px;">
				<?php foreach($this->config->item('langs') AS $lang): ?>
					<li class="<?= (($lang == 'lt') ? 'active' : ''); ?>">
						<a href="#<?= $lang ?>" data-toggle="tab">
							<img src="<?= site_url('images/flags/'.$lang)?>.jpg" />
						</a>
					</li>
				<?php endforeach; ?>
					<li class="">
						<a href="#bendra" data-toggle="tab">Bendrai</a>
					</li>
			</ul>
			<form enctype='multipart/form-data' role="form" method="post" action="<?= site_url('admin/articles/save'); ?>">
				<div id="myTabContent" class="tab-content">
					<?php foreach($this->config->item('langs') AS $lang): ?>
						<div class="tab-pane fade <?= (($lang == 'lt') ? 'active in' : ''); ?>" id="<?= $lang ?>">
							<h4>Antraštė</h4>
							<div class="form-group">
								<div class="col-md-12">
									<input name="info[<?= $lang ?>][title]" value="<?= ((isset($articles[$lang]['title'])) ? $articles[$lang]['title'] : '') ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Antraštė">
								</div>
							</div>
							<h4>Straipsnio pagrindinis tekstas</h4>
							<div class="form-group">
				                <div class="col-md-12">
				                	<textarea class="form-control" rows="12" name="info[<?= $lang ?>][info]"><?= ((isset($articles[$lang]['info'])) ? $articles[$lang]['info'] : '') ?></textarea>
			              		</div>
			              	</div>

						</div>
					<?php endforeach; ?>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-5 col-sm-5">
						<input type="hidden" name="update" value="<?= $update ?>" />
						<button type="submit" class="btn btn-primary">Išsaugoti</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<br/>
<br/>
<br/>	