<div class="row">
	<div class="col-lg-12">
		<h2>Pradinis puslapis</h2>
		<div class="bs-example">
			<ul class="nav nav-tabs" style="margin-bottom: 15px;">
				<?php foreach($this->config->item('langs') AS $lang): ?>
					<li class="<?= (($lang == 'lt') ? 'active' : ''); ?>">
						<a href="#<?= $lang ?>" data-toggle="tab">
							<img src="<?= site_url('images/flags/'.$lang)?>.jpg" />
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
			<div id="myTabContent" class="tab-content">
				<?php foreach($this->config->item('langs') AS $lang): ?>
					<div class="tab-pane fade <?= (($lang == 'lt') ? 'active in' : ''); ?>" id="<?= $lang ?>">
						<form role="form" method="post" action="<?= site_url('admin/'.$save_name.'/save'); ?>">
							<?php foreach($productions AS $key => $v): ?>
								<?php if($lang == $v['lang']): ?>
									<h4>Antraštė</h4>
									<div class="form-group">
										<div class="col-md-12">
											<input name="productions[<?= $key ?>][title]" value="<?= $v['title'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Antraštė">
										</div>
									</div>
									<div class="form-group">
						                <label>Tekstas po antrašte</label>
						                <div class="col-md-12">
						                	<textarea class="form-control" rows="4" name="productions[<?= $key ?>][top_text]"><?= $v['top_text'] ?></textarea>
					              		</div>
					              	</div>
					              	<div class="form-group">
						                <label>Kairiojo sąrašo antraštė</label>
						                <div class="col-md-12">
											<input name="productions[<?= $key ?>][left_list_name]" value="<?= $v['left_list_name'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Kairiojo sąrašo antraštė">
					              		</div>
					              	</div>
					              	<div class="panel panel-info">
              							<div class="panel-heading">
                							<h3 class="panel-title">Kairysis sąrašas (svetainėje rikiuojama didėjančiai)</h3>
              							</div>
              							<div class="panel-body">
              								<?php foreach($lists AS $k => $list): ?>
              									<?php if($lang == $list['lang']): ?>
		              								<div class="row_<?= $k; ?>">
										              	<div class="form-group">
															<div class="col-md-10">
																<input name="lists[<?= $k; ?>][title]" value="<?= $list['title'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Sąrašo antraštė">
															</div>
															<div class="col-md-1">
																<input name="lists[<?= $k; ?>][sort]" value="<?= $list['sort'] ?>" type="text" class="form-control" id="exampleInputText2" placeholder="Rikiavimas">
															</div>
															<div class="col-md-1">
																<a href="javascript:void(0)" onclick="remove_list_item(<?= $list['list_texts_id'] ?>, 'row_<?= $k; ?>')" title="Ištrinti">
																	<img src="<?= site_url('assets/admin') ?>/remove.png" alt="Ištrinti">
																</a>
															</div>
															<div class="clearfix"></div>
														</div>	
														<div class="form-group">
															<div class="col-md-12">
																<input name="lists[<?= $k; ?>][info]" value="<?= $list['info'] ?>" type="text" class="form-control" id="exampleInputText2" placeholder="Sąrašo antraštės papildymas">
															</div>
														</div>
														<input type="hidden" name="lists[<?= $k; ?>][id]" value="<?= $list['list_texts_id'] ?>" />
														<div class="bs-example">
										              		<div class="progress" style="height:3px">
										                		<div class="progress-bar" style="width: 100%;"></div>
										              		</div>
										            	</div>
										            </div>
										        <?php endif; ?>
										    <?php endforeach; ?>
										    <button onclick="addRow('<?= $lang ?>', '')" type="button" id="add_new_row_<?= $lang ?>" class="btn btn-primary">Pridėti naują sąrašo elementą</button>     
							            </div>
            						</div>
            						<div class="form-group">
						                <label>Dešiniojo sąrašo antraštė</label>
						                <div class="col-md-12">
											<input name="productions[<?= $key ?>][right_list_name]" value="<?= $v['right_list_name'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Dešiniojo sąrašo antraštė">
					              		</div>
					              	</div> 
            						<div class="panel panel-warning">
              							<div class="panel-heading">
                							<h3 class="panel-title">Dešinysis sąrašas (svetainėje rikiuojama didėjančiai)</h3>
              							</div>
              							<div class="panel-body">
              								<?php foreach($lists_right AS $k => $list): ?>
              									<?php if($lang == $list['lang']): ?>
		              								<div class="row_<?= $k; ?>">
										              	<div class="form-group">
															<div class="col-md-10">
																<input name="lists_r[<?= $k; ?>][title]" value="<?= $list['title'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Sąrašo antraštė">
															</div>
															<div class="col-md-1">
																<input name="lists_r[<?= $k; ?>][sort]" value="<?= $list['sort'] ?>" type="text" class="form-control" id="exampleInputText2" placeholder="Rikiavimas">
															</div>
															<div class="col-md-1">
																<a href="javascript:void(0)" onclick="remove_list_item(<?= $list['list_texts_id'] ?>, 'row_<?= $k; ?>')" title="Ištrinti">
																	<img src="<?= site_url('assets/admin') ?>/remove.png" alt="Ištrinti">
																</a>
															</div>
															<div class="clearfix"></div>
														</div>	
														<div class="form-group">
															<div class="col-md-12">
																<input name="lists_r[<?= $k; ?>][info]" value="<?= $list['info'] ?>" type="text" class="form-control" id="exampleInputText2" placeholder="Sąrašo antraštės papildymas">
															</div>
														</div>
														<input type="hidden" name="lists_r[<?= $k; ?>][id]" value="<?= $list['list_texts_id'] ?>" />
														<div class="bs-example">
										              		<div class="progress" style="height:3px">
										                		<div class="progress-bar" style="width: 100%;"></div>
										              		</div>
										            	</div>
										            </div>
										        <?php endif; ?>
										    <?php endforeach; ?>
										    <button onclick="addRow('<?= $lang ?>', '_r')" type="button" id="add_new_row_<?= $lang ?>_r" class="btn btn-warning">Pridėti naują sąrašo elementą</button>
              							</div>
              						</div>
    	
    							    <div class="form-group">
						                <label>Tekstas apačioje</label>
						                <div class="col-md-12">
						                	<textarea class="form-control" rows="4" name="productions[<?= $key ?>][bottom_text]"><?= $v['bottom_text'] ?></textarea>
					              		</div>
					              	</div>
					           		<div class="panel panel-success">
              							<div class="panel-heading">
                							<h3 class="panel-title"> 
                								<a onclick="$('#pnlb_<?= $lang ?>').show()" href="javascript:void(0)">SEO informacija</a>
                							</h3>
              							</div>
              							<div style="display:none;" class="panel-body" id="pnlb_<?= $lang ?>">
											<div class="form-group">
												<label>Title</label>
												<div class="col-md-12">
													<input name="productions[<?= $key ?>][meta_title]" value="<?= $v['meta_title'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Title">
												</div>
											</div>
											<div class="form-group">
								                <label>Meta keywords</label>
								                <div class="col-md-12">
								                	<textarea class="form-control" rows="3" name="productions[<?= $key ?>][meta_keywords]"><?= $v['meta_keywords'] ?></textarea>
							              		</div>
							              	</div>
											<div class="form-group">
								                <label>Meta description</label>
								                <div class="col-md-12">
								                	<textarea class="form-control" rows="3" name="productions[<?= $key ?>][meta_description]"><?= $v['meta_description'] ?></textarea>
							              		</div>
							              	</div>							              	
              							</div>
              						</div>		
					            <?php endif; ?>
			              	<?php endforeach; ?>
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-5">
									<input type="hidden" name="lang" value="<?= $lang; ?>" />
									<button type="submit" class="btn btn-primary">Išsaugoti</button>
								</div>
							</div>
						</form>  
					</div>
				<?php endforeach; ?>
			</div>
        </div>
	</div>
</div>
<br/><br/><br/>
<script type="text/javascript">
	var row = <?php echo count($lists); ?>;
	function addRow(lang, type) {
		html  = '<div class="row_'+row+'">';
		html += '	<div class="form-group">';
		html += '		<div class="col-md-10">';
		html += '			<input name="lists'+type+'['+row+'][title]" value="" type="text" class="form-control" id="exampleInputText1" placeholder="Sąrašo antraštė">';
		html += '		</div>';
		html += '		<div class="col-md-2">';
		html += '			<input name="lists'+type+'['+row+'][sort]" value="" type="text" class="form-control" id="exampleInputText2" placeholder="Rikiavimas">';
		html += '		</div>';
		html += '		<div class="clearfix"></div>';
		html += '	</div>';
		html += '	<div class="form-group">';
		html += '		<div class="col-md-12">';
		html += '			<input name="lists'+type+'['+row+'][info]" value="" type="text" class="form-control" id="exampleInputText2" placeholder="Sąrašo antraštės papildymas">';
		html += '		</div>';
		html += '	</div>';
		html += '	<input type="hidden" name="lists'+type+'['+row+'][id]" value="0" />';
		html += '	<div class="bs-example">';
		html += '		<div class="progress" style="height:3px">';
		html += '			<div class="progress-bar" style="width: 100%;"></div>';
		html += '		</div>';
		html += '	</div>';
		html += '</div>';
		if(type == '')
		{
			$('#add_new_row_'+lang).before(html);
		} else
		{
			$('#add_new_row_'+lang+type).before(html);
		}
		row++;
	}
</script>