<div class="row">
    <div class="col-lg-12">
        <h2>Information pages</h2>
        <form role="form" method="get" action="<?= site_url('admin/terms/' . $page_name) ?>">
            <div class="panel panel-success">
                <div class="panel-body">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Language</label>
                            <select class="form-control" name="language">
<!--                                --><?php //foreach($this->config->item('lang_uri_abbr') as $lang => $name): ?>
<!--                                    <option --><?//= ($language == $name) ? 'selected="selected"' : '' ?><!-- value="--><?//= $name ?><!--">--><?//= ucfirst($name); ?><!--</option>-->
<!--                                --><?php //endforeach; ?>

                                    <option selected="selected" value="lithuanian">Lithuanian</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <button style="margin-top:25px" type="submit" class="btn btn-primary">Open</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </form>
        <form role="form" method="post" action="<?= site_url('admin/terms/save') ?>">
            <div class="form-group">
                <div class="col-sm-offset-11 col-sm-1">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
            <div style="margin-top: 10px;padding-bottom:50px" class="table-responsive">
                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th class="header">Key</th>
                        <th class="header">Show in Website</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($strings)):
                        $input = 0;
                        ?>
                        <?php foreach($strings as $k => $row):?>
                            <tr>
                                <td><b><?= $row['id'] ?></b></td>
                                <td><?php if ($input++ % 2 == 1 ){ ?>
                                    <textarea name="data[<?= $row['id'] ?>]" class="form-control myTextarea" id="myTextarea"><?= $row['to'] ?></textarea>
                                    <?php }else{ ?>
                                    <input type="text" name="data[<?= $row['id'] ?>]" value="<?= $row['to'] ?>" class="form-control" />
                                </td>
                            </tr>
                        <?php }endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-5 col-sm-5">
                    <input type="hidden" name="language" value="lithuanian" />
                    <input type="hidden" name="file" value="<?= $file ?>" />
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    tinymce.init({
        selector: '.myTextarea',
        entity_encoding: 'numeric',
        height : 200,
        menubar: false,
        image_caption: true,
        toolbar: 'undo redo | styleselect | bold italic | fontsizeselect | link image | forecolor backcolor | numlist bullist | table | charmap | code',
        fontsize_formats: '8pt 10pt 12pt 14pt 17pt 18pt 24pt 36pt',
        plugins: "textcolor colorpicker image link lists table charmap code"

    });
</script>