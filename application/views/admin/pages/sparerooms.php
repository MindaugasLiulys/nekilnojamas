<div class="row">
	<div class="col-lg-12">
		<h2>Sparerooms</h2>
		<div style="margin-top: 10px" class="table-responsive">
			<table class="table table-bordered table-hover table-striped tablesorter">
				<thead>
					<tr>
						<th class="header">ID</th>
						<th class="header">Price</th>
						<th class="header">Size</th>
						<th class="header">Start date</th>
						<th class="header">End date</th>
						<th class="header">Date added</th>
						<th class="header">Active?</th>
						<th class="header">Deleted?</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($sparerooms)): ?>
						<?php foreach($sparerooms as $k => $v): ?>
							<tr>
								<td><?= $v['space_id'] ?></td>
								<td><?= $v['price'] ?> DKK</td>
								<td><?= $v['size'] ?> m<sup>2</sup></td>
								<td><?= $v['start_date'] ?></td>
								<td><?= $v['end_date'] ?></td>
								<td><?= $v['date_added'] ?></td>
								<td><?= $v['active'] ?></td>
								<td><?= $v['deleted'] ?></td>
<!-- 								<td>
									<a href="<?= site_url('admin/articles/edit/'.$v['article_id']) ?>">Redaguoti</a><br/>
									<a style="color:red" href="<?= site_url('admin/articles/delete/'.$v['article_id']) ?>">Trinti</a>
								</td> -->
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>