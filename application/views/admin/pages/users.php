<div class="row">
	<div class="col-lg-12">
		<h2>Users</h2>
		<div style="margin-top: 10px" class="table-responsive">
			<table class="table table-bordered table-hover table-striped tablesorter">
				<thead>
					<tr>
						<th class="header">ID</th>
						<th class="header">First name</th>
						<th class="header">Last name</th>
						<th class="header">Email</th>
						<th class="header">Phone</th>
						<th class="header">Created</th>
						<th class="header">Last login</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($users)): ?>
						<?php foreach($users as $k => $v): ?>
							<tr>
								<td><?= $v['id'] ?></td>
								<td><?= $v['first_name'] ?></td>
								<td><?= $v['last_name'] ?></td>
								<td><?= $v['email'] ?></td>
								<td><?= $v['phone'] ?></td>
								<td><?= $v['created_on'] ?></td>
								<td><?= $v['last_login'] ?></td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>