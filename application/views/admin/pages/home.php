<div class="row">
	<div class="col-lg-12">
		<h2>Pradinis puslapis</h2>
		<div class="bs-example">
			<ul class="nav nav-tabs" style="margin-bottom: 15px;">
				<?php foreach($this->config->item('langs') AS $lang): ?>
					<li class="<?= (($lang == 'lt') ? 'active' : ''); ?>">
						<a href="#<?= $lang ?>" data-toggle="tab">
							<img src="<?= site_url('images/flags/'.$lang)?>.jpg" />
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
			<div id="myTabContent" class="tab-content">
				<?php foreach($this->config->item('langs') AS $lang): ?>
					<div class="tab-pane fade <?= (($lang == 'lt') ? 'active in' : ''); ?>" id="<?= $lang ?>">
						<form role="form" method="post" action="<?= site_url('admin/home/save'); ?>">
							<h4>Gamyba</h4>
							<?php foreach($productions AS $key => $v): ?>
								<?php if($lang == $v['lang']): ?>
									<?php foreach($services AS $k => $s): ?>
										<?php if($lang == $s['lang']): ?>
											<div class="form-group">
												<div class="col-md-4">
													<input name="services[<?= $k ?>][title]" value="<?= $s['title'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Antraštė">
												</div>
												<div class="col-md-8">
													<input name="services[<?= $k ?>][info]" value="<?= $s['info'] ?>" type="text" class="form-control" id="exampleInputText2" placeholder="Paaiškinimas">
											</div>
												<div class="clearfix"></div>
											</div>
											<input name="services[<?= $k ?>][id]" type="hidden" value="<?= $s['list_texts_id'] ?>">
										<?php endif; ?>
									<?php endforeach; ?>
									<h4>Paslaugos (pavadinimas)</h4>
					         		<div class="form-group">
						                <div class="col-md-12">
											<input name="productions[<?= $key ?>][left_list_name]" value="<?= $v['left_list_name'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Pavadinimas">
					              		</div>
					              	</div>										
									<h4>Antraštė</h4>
									<div class="form-group">
										<div class="col-md-12">
											<input name="productions[<?= $key ?>][title]" value="<?= $v['title'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Antraštė">
										</div>
									</div>
									<div class="form-group">
						                <label>Tekstas virš foto apačioje</label>
						                <div class="col-md-12">
						                	<textarea class="form-control" rows="3" name="productions[<?= $key ?>][top_text]"><?= $v['top_text'] ?></textarea>
					              		</div>
					              	</div>
					         		<div class="form-group">
						                <label>Tekstas kairėje foto</label>
						                <div class="col-md-12">
						                	<textarea class="form-control" rows="3" name="productions[<?= $key ?>][middle_text]"><?= $v['middle_text'] ?></textarea>
					              		</div>
					              	</div>
					         		<div class="form-group">
						                <label>Tekstas kairėje foto</label>
						                <div class="col-md-12">
						                	<textarea class="form-control" rows="3" name="productions[<?= $key ?>][bottom_text]"><?= $v['bottom_text'] ?></textarea>
					              		</div>
					              	</div>					              	
					              	<div class="panel panel-success">
              							<div class="panel-heading">
                							<h3 class="panel-title"> 
                								<a onclick="$('#pnlb_<?= $lang ?>').show()" href="javascript:void(0)">SEO informacija</a>
                							</h3>
              							</div>
              							<div style="display:none;" class="panel-body" id="pnlb_<?= $lang ?>">
											<div class="form-group">
												<label>Title</label>
												<div class="col-md-12">
													<input name="productions[<?= $key ?>][meta_title]" value="<?= $v['meta_title'] ?>" type="text" class="form-control" id="exampleInputText1" placeholder="Title">
												</div>
											</div>
											<div class="form-group">
								                <label>Meta keywords</label>
								                <div class="col-md-12">
								                	<textarea class="form-control" rows="3" name="productions[<?= $key ?>][meta_keywords]"><?= $v['meta_keywords'] ?></textarea>
						              		</div>
						              	</div>
											<div class="form-group">
								                <label>Meta description</label>
								                <div class="col-md-12">
							                	<textarea class="form-control" rows="3" name="productions[<?= $key ?>][meta_description]"><?= $v['meta_description'] ?></textarea>
							              		</div>
							              	</div>							              	
              							</div>
              						</div>	
								<?php endif; ?>
							<?php endforeach; ?>
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-5">
									<input type="hidden" name="lang" value="<?= $lang; ?>" />
									<button type="submit" class="btn btn-primary">Išsaugoti</button>
								</div>
							</div>	
						</form>  
					</div>
			<?php endforeach; ?>
			</div>
        </div>
	</div>
</div>