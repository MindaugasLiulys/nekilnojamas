<div class="row">
	<div class="col-lg-12">
		<h2>Translation</h2>
		<form role="form" method="get" action="<?= site_url('admin/translation/') ?>">
			<div class="panel panel-success">
				<div class="panel-body">
					<div class="col-md-5">
						<div class="form-group">
							<label>Language</label>
	                		<select class="form-control" name="language">
<!--	                			--><?php //foreach($this->config->item('lang_uri_abbr') as $lang => $name): ?>
<!--	                				<option --><?//= ($language == $name) ? 'selected="selected"' : '' ?><!-- value="--><?//= $name ?><!--">--><?//= ucfirst($name); ?><!--</option>-->
<!--	                			--><?php //endforeach; ?>

	                				<option selected="selected" value="lithuanian">Lithuanian</option>
	                		</select>
	                	</div>
	                </div>
					<div class="col-md-5">
						<div class="form-group">
							<label>File</label>
	                		<select class="form-control" name="file">
	                			<option <?= ($file == 'main') ? 'selected="selected"' : '' ?> value="main">Main</option>
	                			<option <?= ($file == 'emails') ? 'selected="selected"' : '' ?> value="emails">Emails</option>
	                			<option <?= ($file == 'form_validation') ? 'selected="selected"' : '' ?> value="form_validation">Form validation</option>
	                			<option <?= ($file == 'auth') ? 'selected="selected"' : '' ?> value="auth">Authentication</option>
	                			<option <?= ($file == 'ion_auth') ? 'selected="selected"' : '' ?> value="ion_auth">Authentication lib</option>
	                		</select>
	                	</div>
                	</div>
					<div class="col-md-2">
						<div class="form-group">
							<button style="margin-top:25px" type="submit" class="btn btn-primary">Open</button>
						</div>
                	</div>	
	                <div class="clearfix"></div>
				</div>
			</div>
		</form>
		<form role="form" method="post" action="<?= site_url('admin/translation/save') ?>">
			<div class="form-group">
				<div class="col-sm-offset-11 col-sm-1">
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</div>
			<div style="margin-top: 10px;padding-bottom:50px" class="table-responsive">
				<table class="table table-hover table-striped">
					<thead>
						<tr>
							<th style="width:50%" class="header">Original text</th>
							<th class="header">Show in Website</th>
						</tr>
					</thead>
					<tbody>
						<?php if(!empty($strings)): ?>
							<?php foreach($strings as $k => $row): ?>
								<tr>
									<td><b><?= $row['from'] ?></b></td>
									<td>
										<input type="text" name="data[<?= $row['id'] ?>]" value="<?= $row['to'] ?>" class="form-control" />
									</td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-5">
					<input type="hidden" name="language" value="<?= $language ?>" />
					<input type="hidden" name="file" value="<?= $file ?>" />
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>