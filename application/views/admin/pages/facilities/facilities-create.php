<div class="row">
	<div class="col-lg-12">
		<h2>Create new facility</h2>
		<div style="margin-top: 10px" class="table-responsive">
			<form action="<?= base_url('admin/facilities/save') ?>" method="post" style="width: 30%">
				<div class="form-group">
					<label for="facility_id">Facility ID</label>
					<input id="facility_id" class="form-control" type="text" name="facility_id" placeholder="facility id">
				</div>
				<div class="form-group">
					<label for="name">Name</label>
					<input id="name" class="form-control" type="text" name="name" placeholder="name">
				</div>
				<input type="submit" class="btn btn-success" value="Save">
			</form>
		</div>
	</div>
</div>