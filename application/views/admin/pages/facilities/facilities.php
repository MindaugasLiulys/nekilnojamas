<div class="row">
	<div class="col-lg-12">
		<h2>Facilities</h2>
		<div style="margin-top: 10px" class="table-responsive">
			<table class="table table-bordered table-hover table-striped tablesorter">
				<thead>
					<tr>
						<th class="header">ID</th>
						<th class="header">Facility ID</th>
						<th class="header">Name</th>
						<th class="header">Action</th>
					</tr>
				</thead>
				<tbody>
				<div style="margin: 15px 0">
					<a href="<?= base_url('admin/facilities/create')?>" class="btn btn-primary"><i class="fa fa-plus-circle" aria-hidden="true"></i>
						 Add new</a>
				</div>
					<?php if(!empty($facilities)): ?>
						<?php foreach($facilities as $k => $v): ?>
							<tr>
								<td><?= $v['id'] ?></td>
								<td><?= $v['facility_id'] ?></td>
								<td><?= $v['name'] ?></td>
								<td>
									<a href="<?= base_url('admin/facilities/edit') . '/' . $v['id'] ?>" class="btn btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
									<a href="<?= base_url('admin/facilities/delete') . '/' . $v['id'] ?>" onclick="return confirm('Are you sure want to delete this?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>