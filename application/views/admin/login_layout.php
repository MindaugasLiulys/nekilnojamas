<!DOCTYPE html>
<html lang="lt">
<head>
	<title>Share-cms</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="<?php echo $this->config->item('dir_css_admin'); ?>bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('dir_css_admin'); ?>sb-admin.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="wrapper" style="padding-left:0px;margin-top:130px">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<?php if ($this->session->flashdata('message') || validation_errors()): ?>
					<div class="alert alert-dismissable alert-danger">
	              		<strong class="admin-login-error"><?= $this->session->flashdata('message'); echo validation_errors(); ?></strong>
	            	</div>
            	<?php endif ?>
		    	<div class="panel panel-primary">
		        	<div class="panel-heading">
		                <h3 class="panel-title">Share-cms login</h3>
		            </div>
		            <div class="panel-body">
		                <form class="form-horizontal" role="form" action="<?= site_url('admin/main/login') ?>" method="post">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">E-mail</label>
								<div class="col-sm-8">
									<input name="email" type="text" class="form-control" id="inputEmail3" placeholder="E-mail" value="<?php echo set_value('email'); ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Password</label>
								<div class="col-sm-8">
									<input name="password" type="password" class="form-control" id="inputPassword3" placeholder="Password">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-8">
									<button type="submit" class="btn btn-primary">Log in</button>
								</div>
							</div>
						</form>
		            </div>
		        </div>
		    </div>
	    </div>
	</div>
</body>    
</html>