<?php $controller = $this->uri->segment(2); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Share-cms</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="<?php echo $this->config->item('dir_css_admin'); ?>bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->config->item('dir_css_admin'); ?>sb-admin.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->config->item('dir_font_awesome'); ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo $this->config->item('dir_js_admin'); ?>jquery-1.10.2.js"></script>
		<script src="<?php echo $this->config->item('dir_js_admin'); ?>bootstrap.js"></script>
		<script src="<?php echo $this->config->item('dir_js_admin'); ?>functions.js"></script>
		<script src="<?php echo $this->config->item('dir_js_admin'); ?>tinymce/tinymce.min.js"></script>
	</head>
<body>
	<div id="wrapper">
		<!-- Sidebar -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-brand">Share-cms Admin</div>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li <?= (($controller == 'general') ? 'class="active"' : '') ?>>
						<a href="<?= site_url('admin/general')?>"><i class="fa fa-desktop"></i> General</a>
					</li>
					<li <?= (($controller == 'translation') ? 'class="active"' : '') ?>>
						<a href="<?= site_url('admin/translation?language=lithuanian&file=main')?>"><i class="fa fa-wrench"></i> Translation</a>
					</li>
					<li <?= (($controller == 'sparerooms') ? 'class="active"' : '') ?>>
						<a href="<?= site_url('admin/sparerooms')?>"><i class="fa fa-map-marker"></i> Sparerooms</a>
					</li>
					<li <?= (($controller == 'users') ? 'class="active"' : '') ?>>
						<a href="<?= site_url('admin/users')?>"><i class="fa fa-user"></i> Users</a>
					</li>
                    <li>
						<a href="javascript:;" data-toggle="collapse" data-target="#demo">
                            <i class="fa fa-file"></i> Pages<i class="fa fa-fw fa-caret-down"></i>
                        </a>
						<ul id="demo" <?= (($controller == 'terms') ? 'class="in"' : 'class="collapse"') ?>>
                            <li <?= (($controller == 'terms') ? 'class="active"' : '') ?>>
                                <a href="<?= base_url('admin/terms/terms')?>"> Terms and Conditions</a>
							</li>
							<li <?= (($controller == 'terms') ? 'class="active"' : '') ?>>
                                <a href="<?= base_url('admin/terms/about_us')?>"> About us</a>
							</li>
							<li <?= (($controller == 'terms') ? 'class="active"' : '') ?>>
                                <a href="<?= base_url('admin/terms/information')?>"> Information</a>
							</li>
						</ul>
					</li>
					<li <?= (($controller == 'facilities') ? 'class="active"' : '') ?>>
						<a href="<?= site_url('admin/facilities')?>"><i class="fa fa-user"></i> Facilities</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right navbar-user">
					<li>
						<a href="<?= site_url() ?>">Go To Page</a>
					</li>
					<li>
						<a href="<?= site_url('admin/main/logout') ?>"><i class="fa fa-power-off"></i> Log out</a>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>
		<div id="page-wrapper">
			<?= $html; ?>
		</div><!-- /#page-wrapper -->
	</div><!-- /#wrapper -->
</body>
</html>		