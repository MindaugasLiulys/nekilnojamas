<?php if(isset($error) && !empty($error)): ?>
	<div id="errors"><?php echo $error ?></div>
<?php else: ?>
	<div class="row small-text-center">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<img src="<?php echo base_url(); ?>assets/img/logo_small.png" alt="sharextraspace"><br/>
		<?php if(isset($success) && !empty($success)): ?>
			<h3><?php echo $success; ?></h3>
		<?php endif; ?>
	</div>
<?php endif; ?>