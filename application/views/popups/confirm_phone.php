<div class="pop-up-box">
    <div class="social-login text-center font14">
        <?= lang('confirm_phone_number') ?>
    </div><br/>
    <div style="display:none" id="errors"></div><div style="display:none" id="success"></div>
    <div class="form-element">
        <input value="<?= $phone ?>" type="text" maxlength="11" name="phone_number" id="phone_number" placeholder="<?= lang('phone_number') ?>">
    </div>
    <div class="pop-up-button">
        <button class="btn-green wide" id="confirm_number" href="#"><?= lang('confirm_phone_number') ?></button>
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#confirm_number').click(function(e) {
            e.preventDefault();
            var info = "&phone_number=" + $('#phone_number').val();
            $.ajax({
                type: "POST",
                url: "/users/confirm_phone_number/",
                data: info,
                dataType: "json",
                success: function(data) {
                    if(data.error == 1)
                    {
                        $('#pop_up_content #errors').html(data.response);
                        $('#pop_up_content #errors').show();
                    } else {
                        $('#pop_up_content #errors').hide();
                        $('#pop_up_content').html(data.response);
                        // $('#pop_up_content #success').show();
                        // window.setTimeout(function(){location.reload()},500)
                    }
                }
            });

        });
    });
</script>