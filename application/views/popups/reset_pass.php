<div class="pop-up-box">
    <div class="social-login text-center">
        <?= lang('reset_pass_title') ?>
    </div>
    <div style="display:none" id="errors"></div><div style="display:none" id="success"></div>
    <div class="form-element">
        <input type="text" name="email" id="email" placeholder="<?= lang('email') ?>">
    </div>
    <div class="pop-up-button">
        <button class="btn-green wide" id="reset_passw" href="#"><?= lang('send_reset_link') ?></button>
    </div>
    <div class="pop-up-bottom">
        <?= lang('have_account') ?> <a href="<?php echo base_url('ajax/get/sign_up') ?>" data-reveal-id="sign-up" data-reveal-ajax="true"><?= lang('sign_up') ?></a>
    </div>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#reset_passw').click(function(e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: "/auth/forgot_password/",
                data: {
                    'email': $('#email').val(),
                },
                dataType: "json",
                success: function(data) {
                    if(data.error == 1)
                    {
                        $('#reset-pass #errors').html(data.response);
                        $('#reset-pass #errors').show();
                    } else {
                        $('#reset-pass #errors').hide();
                        $('#reset-pass #success').html(data.response);
                        $('#reset-pass #success').show();
                        // window.setTimeout(function(){location.reload()},500)
                    }
                }
            });
        });
    });
</script>