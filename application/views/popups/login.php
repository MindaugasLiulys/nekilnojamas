<div class="pop-up-box">
    <div class="social-login text-center">
        <h2>PRISIJUNGIMAS</h2>
    </div>
    <div style="display:none" id="errors"></div><div style="display:none" id="success"></div>
    <div class="form-element">
        <input type="text" name="identity" id="identity" placeholder="<?= lang('email') ?>">
    </div>
    <div class="form-element">
        <input type="password" name="password" id="password" placeholder="<?= lang('password') ?>">
    </div>
    <div class="remember">
        <label>
            <input type="checkbox" name="remember" value="1" id="remember">
            <?= lang('remember_me') ?>
        </label>
    </div>
    <div class="pop-up-button">
        <button class="btn-green wide" id="login_btn" href="#"><?= lang('log_in') ?></button>
    </div>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#login_btn').click(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "/auth/login/",
                data: {
                    "identity": $('#identity').val(),
                    "password": $('#password').val(),
                    "remember": $('#remember').val(),
                },
                dataType: "json",
                success: function(data) {
                    if(data.error == 1)
                    {
                        $('#errors').html(data.response);
                        $('#errors').show();
                    } else {
                        $('#errors').hide();
                        $('#success').html(data.response);
                        $('#success').show();
                        window.setTimeout(function(){location.reload()},500)
                    }
                }

            });

        });
    });
</script>