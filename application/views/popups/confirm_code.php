<div class="pop-up-box">
    <div class="social-login text-center font14">
        <?= lang('enter_digit_code') ?>
    </div>
    <p class="text-center margin-bottom-0"><?= lang('sms_to') ?> <?php echo $this->session->userdata('validation_phone_number') ?>.</p>
    <p class="text-center"><?= lang('enter_code') ?></p>
    <div style="display:none" id="errors"></div><div style="display:none" id="success"></div>
    <div class="form-element">
        <input type="text" maxlength="4" name="code" id="code" placeholder="<?= lang('digital_code') ?>">
    </div>
    <div class="pop-up-button">
        <button class="btn-green wide" id="confirm_code" href="#"><?= lang('confirm_code') ?></button>
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#confirm_code').click(function(e) {
            e.preventDefault();
            var info = "&code=" + $('#code').val();
            $.ajax({
                type: "POST",
                url: "/users/confirm_code/",
                data: info,
                dataType: "json",
                success: function(data) {
                    if(data.error == 1)
                    {
                        $('#pop_up_content #errors').html(data.response);
                        $('#pop_up_content #errors').show();
                    } else {
                        $('#pop_up_content #errors').hide();
                        $('#pop_up_content').html(data.response);
                        // $('#pop_up_content #success').show();
                        window.setTimeout(function(){$('#rent-btn').click();},600)
                    }
                }
            });

        });
    });
</script>