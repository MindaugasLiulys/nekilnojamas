<div class="pop-up-box">
    <div class="social-login-text text-center">
        <?= lang('sign_up_with') ?> <a href="<?php echo $fb_link ?>">Facebook</a> <?= lang('or') ?> <a href="<?php echo $google_link ?>">Google</a>
    </div>
    <div class="or text-center">
        <?= lang('or') ?>
    </div>
    <div style="display:none" id="errors"></div><div style="display:none" id="success"></div>
    <div class="form-element">
        <input type="text" id="first_name" name="first_name" placeholder="<?= lang('first_name') ?>">
    </div>
    <div class="form-element">
        <input type="text" id="last_name" name="last_name" placeholder="<?= lang('last_name') ?>">
    </div>
    <div class="form-element">
        <input type="text" id="email" name="email" placeholder="<?= lang('email') ?>">
    </div>
    <div class="form-element">
        <input type="password" id="reg_password" name="password" placeholder="<?= lang('password') ?>">
    </div>
    <div class="form-element">
        <input type="password" id="password_confirm" name="password_confirm" placeholder="<?= lang('confirm_pass') ?>">
    </div>
    <div class="remember">
        <label>
            <input id="newsletter" type="checkbox" name="newsletter" value="1" checked="checked">
            <?= lang('send_me_newsletter') ?>
        </label>
    </div>
    <div class="pop-up-text">
        <?= lang('sign_up_agree') ?> <a href="<?php echo base_url('terms/index/terms-of-service') ?>"><?= lang('terms_of_service') ?></a>.
    </div>
    <div class="pop-up-button">
        <button class="btn-green wide" id="sign_up_btn" href="#"><?= lang('sign_up') ?></button>
    </div>
    <div class="pop-up-bottom">
        <?= lang('already_member') ?> <a href="<?php echo base_url('ajax/get/login') ?>" data-reveal-id="login" data-reveal-ajax="true"><?= lang('log_in') ?></a>
    </div>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#sign_up_btn').click(function() {
            $.ajax({
                type: "POST",
                url: "/auth/create_user/",
                data: {
                    "first_name": $('#first_name').val(),
                    "last_name": $('#last_name').val(),
                    "email": $('#email').val(),
                    "password": $('#reg_password').val(),
                    "password_confirm": $('#password_confirm').val(),
                    "newsletter": $('#newsletter').val(),
                },
                dataType: "json",
                success: function(data) {
                    if(data.error == 1)
                    {
                        $('#sign-up-email #errors').html(data.response);
                        $('#sign-up-email #errors').show();
                    } else {
                        $('#sign-up-email #errors').hide();
                        $('#sign-up-email #success').html(data.response);
                        $('#sign-up-email #success').show();
                        window.setTimeout(function(){location.reload()},500);
                    }
                }
            });

        });
    });
</script>