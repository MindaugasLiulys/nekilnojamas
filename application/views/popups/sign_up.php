<div class="pop-up-box">
    <div class="social-login text-center">
        <a class="login-fb" href="<?php echo $fb_link ?>"><i class="left fa fa-2x fa-facebook" aria-hidden="true"></i> <?= lang('sign_up_fb') ?></a>
        <a class="login-gplus" href="<?php echo $google_link ?>"><i class="left fa fa-2x fa-google-plus" aria-hidden="true"></i> <?= lang('sign_up_g') ?></a>
    </div>
    <div class="or text-center">
      <?= lang('or') ?>
    </div>
    <div class="with-email">
        <a href="<?php echo base_url('ajax/get/sign_up_email') ?>" class="login-email" data-reveal-id="sign-up-email" data-reveal-ajax="true"><?= lang('sign_up_email') ?></a>
    </div>
    <div class="pop-up-text">
        <?= lang('sign_up_agree') ?> <a target="_blank" href="<?php echo base_url('terms/index/terms-of-service') ?>"><?= lang('terms_of_service') ?></a>.
    </div>
    <div class="pop-up-bottom">
        <?= lang('already_member') ?> <a href="<?php echo base_url('ajax/get/login') ?>" data-reveal-id="login" data-reveal-ajax="true"><?= lang('log_in') ?></a>
    </div>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>