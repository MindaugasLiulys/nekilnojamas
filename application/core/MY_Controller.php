<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if (!logged_in()) {
			redirect('/');
		}
	}
}

class Backend_Controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->library('ion_auth');
		// Redirect to login if not logged as administrator

		if (!$this->ion_auth->logged_in()) {
			redirect('/admin/main/login');
		} elseif (!$this->ion_auth->in_group('admin')) {
			redirect('/');
		}
	}
}