<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		if($this->router->method != "show")
		{
			if(!logged_in()) {
				redirect('/');
			}
		}

		$this->load->model('users_model');
	}

	public function show($id)
	{
		$tmp['info'] = $this->users_model->get_user($id);

		if(!empty($tmp['info']['id']) && logged_in())
		{
			$data['html'] = $this->load->view('account/profile_view', $tmp, true);
			$this->load->view('layout', $data);
		} else {
        	$this->session->set_flashdata('message', lang('error_show_user_log_in'));
			redirect('/');
		}
	}

	public function confirm_phone_number()
	{
		//validate form input
		$this->form_validation->set_rules('phone_number', lang('phone_number'), 'required|is_natural');

		if (!$this->form_validation->run())
		{
			$response['error'] = 1;
			$response['response'] = validation_errors();
			$this->output->set_output(json_encode($response));
			return;
		}

		$phone_number = $this->input->post('phone_number');

		$code = $this->users_model->send_validation_sms($phone_number);

		$this->session->set_userdata(array(
			'validation_code' => $code, 'validation_phone_number' => $phone_number));
		$response['error'] = 0;
		$response['response'] = $this->load->view('popups/confirm_code', NULL, TRUE);
		$this->output->set_output(json_encode($response));
	}

	public function confirm_code()
	{
		$this->form_validation->set_rules('code', lang('digital_code'), 'required|exact_length[4]|is_natural|callback_code_check');

		if ($this->form_validation->run() == false)
		{
			$response['error'] = 1;
			$response['response'] = validation_errors();
			$this->output->set_output(json_encode($response));
		}
		else
		{
			$this->users_model->update_fields(
				array('phone' => $this->session->userdata('validation_phone_number'), 'phone_verif' => 1));
			$this->session->set_userdata(array('phone_verif' => 1));

			$response['error'] = 0;
			$response['response'] = $this->load->view('popups/confirm_code_success', NULL, TRUE);
			$this->output->set_output(json_encode($response));
		}
	}

	public function code_check($str)
    {
        if ($str != $this->session->userdata('validation_code'))
        {
            $this->form_validation->set_message('code_check', lang('wrong_code'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

	public function edit()
	{
		$tmp['active'] = 'account';
		$tmp['info'] = $this->users_model->get_user();

		$allow_list_space = $this->session->userdata('allow_list_space');
		if($allow_list_space == 0)
		{
			$tmp['validation_errors'] = lang('error_complete_profile');
		}

		$tmp['login_type'] = $this->session->userdata('login_type');

		$data['html'] = $this->load->view('account/profile', $tmp, true);
		$this->load->view('layout', $data);
	}

	public function save()
	{
		$tmp['login_type'] = $this->session->userdata('login_type');

		$this->form_validation->set_rules('first_name', lang('first_name'), 'required');
		$this->form_validation->set_rules('last_name', lang('last_name'), 'required');

		if($tmp['login_type'] == 0)
		{
			$this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
		}

		$this->form_validation->set_rules('phone', lang('phone_number'), 'required');
		$this->form_validation->set_rules('location', lang('location'), 'required|max_length[128]');
		$this->form_validation->set_rules('i_am', lang('i_am'), 'trim');
		$this->form_validation->set_rules('month', lang('month'), 'trim');
		$this->form_validation->set_rules('day', lang('day'), 'trim');
		$this->form_validation->set_rules('year', lang('birth_date'), 'callback_birthdate_check');

		// $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'required');

		if ($this->form_validation->run() == true)
		{
			$data = $this->input->post(NULL, TRUE);
			$allow_list_space = $this->session->userdata('allow_list_space');

			$this->users_model->save_user($data);

			$tmp['success_message'] = lang('success_profile_updated');
			if($allow_list_space == 0 && $this->session->userdata('allow_list_space') == 1)
			{
				$redirect_url = $this->session->userdata('space_create_link');
				if(!empty($redirect_url))
				{
					unset($_SESSION['space_create_link']);
					$this->session->set_userdata( array('allow_list_space' => 1) );
					redirect($redirect_url);
				}
			}
		} else
		{
			$tmp['validation_errors'] = validation_errors();
		}

		$tmp['active'] = 'account';
		$tmp['info'] = $this->users_model->get_user();

		$data['html'] = $this->load->view('account/profile', $tmp, true);
		$this->load->view('layout', $data);		
	}

	public function transaction_history()
	{
		$tmp['active'] = 'transaction';

		$this->load->model('transaction_model');
		$tmp['future_transactions'] = $this->transaction_model->get_future_transactions();

		$data['html'] = $this->load->view('account/transaction_history', $tmp, true);
		$this->load->view('layout', $data);		
	}

	public function notifications()
	{
		$tmp['active'] = 'notifications';
		$tmp['settings'] = $this->users_model->get_notification_settings();

		$data['html'] = $this->load->view('account/notifications', $tmp, true);
		$this->load->view('layout', $data);		
	}

	public function update_notifications()
	{
		$this->form_validation->set_rules('msg_rezervation_status', lang('form_validation_numeric'), 'in_list[0,1]');
		$this->form_validation->set_rules('msg_rezervation_request', lang('form_validation_numeric'), 'in_list[0,1]');
		$this->form_validation->set_rules('msg_account_changes', lang('form_validation_numeric'), 'in_list[0,1]');
		$this->form_validation->set_rules('msg_promotions', lang('form_validation_numeric'), 'in_list[0,1]');
		$this->form_validation->set_rules('msg_review_reminder', lang('form_validation_numeric'), 'in_list[0,1]');

		if ($this->form_validation->run() == true)
		{
			$data = $this->input->post(NULL, TRUE);
			$this->users_model->update_notification_settings($data);
			$tmp['success_message'] = lang('success_notifications_updated');
		} else
		{
			$tmp['validation_errors'] = validation_errors();
		}

		$tmp['active'] = 'notifications';
		$tmp['settings'] = $this->users_model->get_notification_settings();

		$data['html'] = $this->load->view('account/notifications', $tmp, true);
		$this->load->view('layout', $data);	
	}

	public function birthdate_check($year)
    {
    	$date = $year . '-' . str_pad($_POST['month'], 2, "0", STR_PAD_LEFT) . '-' . str_pad($_POST['day'], 2, "0", STR_PAD_LEFT);

        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
        {
            $this->form_validation->set_message('birthdate_check', lang('form_validation_required'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

}
