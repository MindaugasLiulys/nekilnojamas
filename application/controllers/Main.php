<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Main class
 *
 * @author equesas@gmail.com
 **/
class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->model('space_model');
		$tmp['featured'] = array_chunk($this->space_model->get_featured_spaces(), 4);
		$tmp['available'] = array_chunk($this->space_model->get_available_now_spaces(), 3);

        $tmp['frontpage'] = TRUE;
		$data['html'] = $this->load->view('frontpage_layout', $tmp, true);
		$this->load->view('layout', $data);

		// $this->load->view('under_construction', null);
	}

	public function setView($type)
    {
        $tmp['frontpage'] = TRUE;
        $data['html'] = $this->load->view('frontpage_layout', $tmp, true);
        $this->load->view('layout', $data);
    }
}
