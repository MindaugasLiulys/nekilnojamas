<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inbox extends CI_Controller
{

	public $currency;

	function __construct()
	{
		parent::__construct();
		$this->load->model('messages_model');

		if (!logged_in()) {
			redirect('/');
		}
	}

	public function index()
	{
		$status_filter = $this->input->get('filter', true);

		$status_filter = is_null($status_filter) ? 'all' : $status_filter;

		if (in_array($status_filter, array('all', 'unread', 'waiting', 'reservations'))) {
			$tmp['active'] = 'messages';

			$tmp['active_filter'] = $status_filter;
			$tmp['list'] = $this->messages_model->get_conversations($status_filter);

            $currUser = $this->session->userdata('user_id');

            $query = $this->db->query("SELECT * from tr56_messages WHERE `to` = $currUser");
            $tmp['messages'] = $query->result();

			$data['html'] = $this->load->view('inbox/list', $tmp, true);
			$this->load->view('layout', $data);
		} else {
			redirect('/inbox');
		}
	}

	public function delete($conversation_id)
	{
		$info = $this->messages_model->get_conversation($conversation_id);

		if (!empty($info)) {
			$this->db->where('conversation_id', $conversation_id);
			
			if ($info['from'] == $this->session->userdata('user_id')) {
				$this->db->update('conversations', array('deleted_from' => 1));
			} elseif ($info['to'] == $this->session->userdata('user_id')) {
				$this->db->update('conversations', array('deleted_to' => 1));
			}
		}

		redirect('/inbox');
	}

	public function conversation($conversation_id)
	{
		$tmp['active'] = 'messages';
//		$tmp['info'] = $this->messages_model->get_conversation($conversation_id);

		$tmp['info'] =  $query = $this->db->query("SELECT * from tr56_messages WHERE `conversation_id` = $conversation_id");
        $tmp['message'] = $query->result();

        $spaceId = $this->db->query("SELECT * from tr56_conversations WHERE `conversation_id` = $conversation_id");
        $conversation = $spaceId->result();

        foreach ($conversation as $kx => $conv){
            $tmp['spaceId'] = $conv->space_id;
            $space_id = $conv->space_id;
        }

        $space = $this->db->query("SELECT * from tr56_spaces WHERE `space_id` = $space_id");
        $spaces = $space->result();

        foreach ($spaces as $kxw => $space_slug){
            $tmp['space_slug'] = $space_slug->slug;
        }

//        $query2 = $this->db->query("SELECT slug from tr56_spaces WHERE `space_id` = $spaceId");

//        $tmp['space'] = $query2->result();

			$data['html'] = $this->load->view('inbox/conversation', $tmp, true);
			$this->load->view('layout', $data);
	}

	public function decline($conversation_id)
	{
		$info = $this->messages_model->get_conversation($conversation_id, 1);

		if (!empty($info)) {
			$this->load->model('emails_model');
			$this->emails_model->send_status_declined($info);

			$this->messages_model->update_fields($conversation_id, array('status' => 1));

			$this->session->set_flashdata('success', lang('success_reservation_declined'));
		}
		redirect('/inbox');
	}

	public function approve($conversation_id)
	{
		$info = $this->messages_model->get_conversation($conversation_id, 1);

		if (!empty($info)) {
			$hash = $this->messages_model->make_hash_for_rezervation($info);
			$login_hash = $this->messages_model->generate_login_hash($info);

			$this->load->model('emails_model');
			$this->emails_model->send_status_approved($info, $hash, $login_hash);

			$this->session->set_flashdata('success', lang('success_reservation_approved'));
		}
		redirect('/inbox');
	}

	public function send_msg()
	{
		$this->form_validation->set_rules('msg', lang('message'), 'required|min_length[5]|max_length[21000]');

		$conversation = $this->messages_model->get_conversation_row($this->input->post('conversation_id', TRUE));

		if ($this->form_validation->run() == false || empty($conversation['conversation_id'])) {
			$response['error'] = 1;
			$response['response'] = validation_errors();

			if (empty($response['response'])) {
				$response['response'] = lang('no_reservation');
			}
			$this->output->set_output(json_encode($response));
		} else {
			$info = $this->messages_model->add_message($conversation, $this->input->post('msg', TRUE));

			$response['msg'] = $info['message'];
			$response['date_added'] = $info['date_added'];
			$response['error'] = 0;

			$this->output->set_output(json_encode($response));
		}
	}
}