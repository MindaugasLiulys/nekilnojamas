<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function doc()
	{
		$this->load->view('documentation', NULL);
	}

	public function index()
	{
		$this->load->view('frontpage_layout');
	}

	public function clear()
	{
		/**
	 	 * redirect('/');
		 * return;
		 */

		$this->db->truncate('conversations');
		$this->db->truncate('messages');
		$this->db->truncate('spaces');
		$this->db->truncate('users');
		$this->db->truncate('users_groups');
		$this->db->truncate('users_notification_settings');
		$this->db->truncate('wishlists');
		$this->db->truncate('transactions');

		$directories = scandir('images');

		foreach ($directories as $k => $name) {
			if($name !== "." && $name !== ".." && $name !== "avatars" && $name !== "empty.jpg" && $name !== "under_construction.png")
			{
				if(is_dir('images/'.$name)) {
					$sub_directories = scandir('images/'.$name);

					foreach ($sub_directories as $r => $file_name) {
						unlink('images/'.$name.'/'.$file_name);
					}

					$gallery_dir = scandir('images/'.$name.'/gallery');

					foreach ($gallery_dir as $p => $g_file_name) {
						unlink('images/'.$name.'/gallery/'.$g_file_name);
					}

					rmdir('images/'.$name.'/gallery');
					rmdir('images/'.$name);
				}
			}
		}

		$avatars = scandir('images/avatars');

		foreach ($avatars as $a => $a_file_name) {
			if($a_file_name !== "default.jpg")
			{
				unlink('images/avatars/'.$a_file_name);
			}
		}

		unset($_SESSION);
		session_destroy();
		redirect('/');
	}

	public function get($template = "")
	{
		$templates = array('login', 'sign_up', 'sign_up_email', 'reset_pass');

		if(in_array($template, $templates))
		{
			$tmp['fb_link'] = "";

			if(in_array($template, array('login', 'sign_up', 'sign_up_email')))
			{
				$this->load->library('facebook');
				$tmp['fb_link'] = $this->facebook->login_url();

				include_once APPPATH . "libraries/ci_google_oauth/src/Google/autoload.php";
				include_once APPPATH . "libraries/ci_google_oauth/src/Google/Client.php";

				// Create Client Request to access Google API
				$client = new Google_Client();
				$client->setApplicationName("PHP Google OAuth Login Example");
				$client->setClientId(GOOGLE_CLIENT_ID);
				$client->setClientSecret(GOOGLE_CLIENT_SECRET);
				$client->setRedirectUri(site_url('auth/oauth2_callback'));
				$client->setDeveloperKey(GOOGLE_API_KEY);
				$client->addScope("https://www.googleapis.com/auth/userinfo.email");

				$tmp['google_link'] = $client->createAuthUrl();
			}

			$html = $this->load->view('popups/'.$template, $tmp, TRUE);
			$this->output->set_output($html);
		}
	}

}