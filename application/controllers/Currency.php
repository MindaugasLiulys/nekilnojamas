<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Currency extends CI_Controller
{
    public function __construct() {
        parent::__construct();     
    }
 
    public function set($currency = "") {
        
        $currency = ($currency != "") ? $currency : "DKK";
        $this->session->set_userdata('currency', strtoupper($currency));
        
        redirect($_SERVER['HTTP_REFERER']);
    }
}