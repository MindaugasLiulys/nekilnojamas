<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Space extends CI_Controller
{

    public $currency;

    function __construct()
    {
        parent::__construct();
        $this->load->model('space_model');
        $this->load->library('Ajax_pagination');
        $this->perPage = 9;
    }

    public function create()
    {
        $query = $this->db->get('facilities', 20);
        foreach($query->result_array() AS $k => $row)
        {
            $tmp['facilities'][] = $row;
        }

        $allow_list_space = $this->session->userdata('allow_list_space');
        if ($allow_list_space == 0) {
            $this->session->set_userdata(array('space_create_link' => site_url('naujas-skelbimas')));
            redirect('users/edit');
        } else {
            $data['html'] = $this->load->view('space/new', $tmp, true);
            $this->load->view('layout', $data);
        }
    }

    public function save()
    {
        // validate form input
        $this->form_validation->set_rules('description', lang('description'), 'required|min_length[50]');
        $this->form_validation->set_rules('price', lang('price'), 'required|is_natural_no_zero');
        $this->form_validation->set_rules('start_date', lang('start_date'), 'required|callback_date_check');
        $this->form_validation->set_rules('size', lang('size'), 'required|is_natural_no_zero');
        $this->form_validation->set_rules('lat', lang('city_and_street'), 'required');

        $main_file = $this->session->userdata('main_file_name');
        if ($this->form_validation->run() == true && !is_null($main_file) && logged_in()) {
            $this->load->model('images_model');

            $data = $this->input->post(NULL, TRUE);
            $data['start_date'] = date("Y-m-d", strtotime($data['start_date']));

            $location = str_replace(', ', '-', get_address($data['lat'], $data['lng']));
            $location = str_replace(' ', '-', $location);

            $data['slug'] = ($data['room_number'] . '-' . room_plural($data['room_number']) . '-' . room_type_helper($data['room_type']) . '-' . $location);


            $space_id = $this->space_model->insert_space($data);
            $this->images_model->process_uploaded_images($space_id);

            $created_spaces = $this->session->userdata('created_spaces');
            $created_spaces++;
            $this->session->set_userdata(array('created_spaces' => $created_spaces));

            // make a redirect somewhere
            $response['error'] = 0;
            $response['response'] = site_url('sparerooms/complete/' . $space_id);
            $this->output->set_output(json_encode($response));
            // redirect("auth", 'refresh');
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $response['error'] = 1;
            $response['response'] = validation_errors();

            if (!logged_in()) {
                $response['response'] .= '<p>' . lang('error_log_in_first') . '</p>';
            }

            if (is_null($main_file)) {
                $response['response'] .= '<p>' . lang('error_forgot_main_file') . '</p>';
            }
            // $html = $this->load->view('popups/sign_up_email', $this->data, TRUE);
            $this->output->set_output(json_encode($response));
        }
    }

    public function view($slug, $space_id)
    {
        if (is_numeric($space_id)) {
            $tmp['info'] = $this->space_model->get_space_information($space_id);
            if (empty($tmp['info']['user_id'])) {
                redirect('/');
            } else {
                $this->load->model('spacereview_model');
                $this->load->model('wishlist_model');
                $this->load->model('transaction_model');

                $this->space_model->increase_views_count($space_id);
                $tmp['rating'] = $this->spacereview_model->get_ratings($space_id);
                $tmp['reviews'] = $this->spacereview_model->get_reviews($space_id);
                $tmp['is_in_wishlist'] = $this->wishlist_model->space_saved_in_wishlist($space_id);

                //google data for search
                $gdata['size_from'] = SLIDER_SIZE_FROM;
                $gdata['size_to'] = SLIDER_SIZE_TO;
                $gdata['lat'] = number_format($tmp['info']['lat'], 6, '.', '');
                $gdata['lng'] = number_format($tmp['info']['lng'], 6, '.', '');
                $gdata['price_from'] = 1;
                $gdata['price_to'] = 2000;

                $gdata['input_start_date'] = "";
                $gdata['input_end_date'] = "";
                $gdata['space_id'] = $space_id;

                $tmp['total_spaces'] = $this->space_model->get_related_spaces(false, $gdata);
                $tmp['json_prods'] = $this->space_model->results_for_map($tmp['total_spaces'], $space_id);
                $tmp['results'] = $this->space_model->get_related_spaces(true, $gdata, array('start' => 0, 'limit' => 4));
                if (logged_in()) {
                    $tmp['user_rating'] = $this->transaction_model->has_user_rented($space_id);
                }

                $start_date = $this->input->get('sd', true);
                $end_date = $this->input->get('ed', true);
                $comment = $_SESSION['last_page_comment'];

                if (!is_null($start_date) && !is_null($end_date)) {
                    $start_date = date("Y-m-d", strtotime($start_date));
                    $end_date = date("Y-m-d", strtotime($end_date));
                }

                if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $start_date)
                    && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $end_date)
                ) {

                    $tmp['start_date'] = $start_date;
                    $tmp['end_date'] = $end_date;

                    $tmp['final_price'] = $this->space_model->get_final_price($start_date, $end_date, $tmp['info']['price']);
                    $_SESSION['last_page'] = site_url('space/view/' . $space_id . '?sd=' . $start_date . '&ed=' . $end_date);

                } else {
                    $_SESSION['last_page'] = site_url('space/view/' . $space_id);
                }

                $directory = "images/" . $tmp['info']['folder_name'] . '/gallery/';
                $images = glob($directory . "*thumb.*");

                $tmp['comment'] = $comment;
                $tmp['images'] = $images;

                $data['html'] = $this->load->view('space/view', $tmp, true);
                $this->load->view('layout', $data);
            }
        } else {
            redirect('/');
        }

    }

    public function ajax_load_more_spaces()
    {
        $tmp = $this->input->post(NULL, TRUE);
        $tmp['info'] = $this->space_model->get_space_information($tmp['space_id']);
        if (!$tmp['page']) {
            $offset = 0;
        } else {
            $offset = $tmp['page'];
        }
        //google data for search
        $data['size_from'] = SLIDER_SIZE_FROM;
        $data['size_to'] = SLIDER_SIZE_TO;
        $data['lat'] = number_format($tmp['info']['lat'], 6, '.', '');
        $data['lng'] = number_format($tmp['info']['lng'], 6, '.', '');
        $data['price_from'] = 1;
        $data['price_to'] = 2000;

        $data['input_start_date'] = "";
        $data['input_end_date'] = "";
        $data['space_id'] = $tmp['space_id'];

        $tmp['results'] = $this->space_model->get_related_spaces(true, $data, array('start' => $offset, 'limit' => 4));
        $tmp['size'] = 3;
        $response = $this->load->view('space/space_wishlist', $tmp, true);
        $this->output->set_output($response);
    }

    public function view_all()
    {

        $this->load->library('lib_pagination');
        $pg_config['sql'] = $this->space_model->get_all_featured_spaces_sql();
        $pg_config['per_page'] = 9;
        $dataPagination = $this->lib_pagination->create_pagination($pg_config);


        $data['html'] = $this->load->view('space/all_spaces', $dataPagination, true);
        $this->load->view('layout', $data);

    }

    public function s()
    {
        $price_range = $this->input->get('price_range', TRUE);
        $tmp = $this->input->get(NULL, TRUE);

        $tmp['size_from'] = $this->space_model->get_min_space_size();
        $tmp['size_to'] = $this->space_model->get_max_space_size();

        $tmp['searchpage'] = TRUE;

        if (!empty($tmp['lat']) && !empty($tmp['lng']) && $this->date_check($tmp['start_date']) &&
            $this->date_check($tmp['end_date'])
        ) {
            $prices = $this->space_model->split_price_range($price_range);
            $tmp['price_from'] = $prices['from'];
            $tmp['price_to'] = $prices['to'];

            $tmp['lat'] = number_format($tmp['lat'], 6, '.', '');
            $tmp['lng'] = number_format($tmp['lng'], 6, '.', '');

            $totalRec = count($this->space_model->get_search_results_sql($tmp));

            $config['target'] = '#search_results';
            $config['base_url'] = base_url() . 'space/ajaxPaginationData';
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;

            $this->ajax_pagination->initialize($config);

            $tmp['results'] = $this->space_model->get_search_results($tmp, array('limit' => $this->perPage));
            $tmp['in_wishlist'] = $this->space_model->get_spaces_in_wishlist();

            if ($this->space_model->date_check($tmp['start_date']) && $this->space_model->date_check($tmp['end_date'])) {
                $tmp['start_date'] = date("Y-m-d", strtotime($tmp['start_date']));
                $tmp['end_date'] = date("Y-m-d", strtotime($tmp['end_date']));
                $tmp['input_start_date'] = date(DATE_FORMAT, strtotime($tmp['start_date']));
                $tmp['input_end_date'] = date(DATE_FORMAT, strtotime($tmp['end_date']));
            }

            $tmp['json_prods'] = $this->space_model->results_for_map($tmp['results']);

            if (!$totalRec) {
                $tmp['error'] = lang('error_fill_place_dates');
            }
        } else {
            $tmp['price_from'] = $this->space_model->get_min_space_price();
            $tmp['price_to'] = $this->space_model->get_max_space_price();
            
            $tmp['lat'] = DEFAULT_LAT;
            $tmp['lng'] = DEFAULT_LNG;

            $tmp['start_date'] = "";
            $tmp['end_date'] = "";

            //pagination
            $totalRec = count($this->space_model->get_all_results_sql($tmp));

            $config['target'] = '#search_results';
            $config['base_url'] = base_url() . 'space/ajaxPaginationData';
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $this->ajax_pagination->initialize($config);

            $tmp['results'] = $this->space_model->get_all_results($tmp, array('limit' => $this->perPage));
            $tmp['json_prods'] = $this->space_model->results_for_map($tmp['results']);
            if (!$totalRec) {
                $tmp['error'] = lang('error_fill_place_dates');
            }
        }

        $query = $this->db->get('facilities', 20);
        foreach($query->result_array() AS $k => $row)
        {
            $tmp['facilities'][] = $row;
        }

        if($_GET['paieska'] == 'nuoma' || $_GET['paieska'] == 'pardavimas' ){
            $tmp['checkGet'] = 1;
        }else{
            $tmp['checkGet'] = 0;
        }

        if($_GET['paieska'] == 'nuoma'){
            $tmp['checkbox'] = 'rent';
        }elseif($_GET['paieska'] == 'pardavimas'){
            $tmp['checkbox'] = 'sell';
        }

        $data['html'] = $this->load->view('space/search', $tmp, true);
        $this->load->view('layout', $data);
    }

    function ajaxPaginationData()
    {
        $tmp = $this->input->post(NULL, TRUE);
        if (!$tmp['page']) {
            $offset = 0;
        } else {
            $offset = $tmp['page'];
        }

        if ($tmp['lat'] == DEFAULT_LAT && $tmp['lng'] == DEFAULT_LNG) {

            //pagination
            $totalRec = count($this->space_model->get_all_results_sql($tmp));
            $config['target'] = '#search_results';
            $config['base_url'] = base_url() . 'space/ajaxPaginationData';
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $this->ajax_pagination->initialize($config);

            $data['results'] = $this->space_model->get_all_results($tmp, array('start' => $offset, 'limit' => $this->perPage));
            $response['json_prods'] = $this->space_model->results_for_map($data['results']);
            if (!$totalRec) {
                $tmp['error'] = lang('error_fill_place_dates');
            }
            $response['lat'] = number_format($data['lat'], 7, '.', '');
            $response['lng'] = number_format($data['lng'], 7, '.', '');
            $data['size'] = 4;
        }else {
            $tmp['size_from'] = SLIDER_SIZE_FROM;
            $tmp['size_to'] = SLIDER_SIZE_TO;

            $tmp['input_start_date'] = "";
            $tmp['input_end_date'] = "";

            $tmp['lat'] = number_format($tmp['lat'], 6, '.', '');
            $tmp['lng'] = number_format($tmp['lng'], 6, '.', '');

            //total rows count
            $totalRec = count($this->space_model->get_search_results_sql($tmp));

            //pagination configuration
            $config['target'] = '#search_results';
            $config['base_url'] = base_url() . 'space/ajaxPaginationData';
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;

            $this->ajax_pagination->initialize($config);

            //get the posts data
            $data['results'] = $this->space_model->get_search_results($tmp, array('start' => $offset, 'limit' => $this->perPage));

            //load map
            $response['json_prods'] = $this->space_model->results_for_map($data['results']);
            $response['lat'] = number_format($data['lat'], 7, '.', '');
            $response['lng'] = number_format($data['lng'], 7, '.', '');
            $data['size'] = 4;
        }

        //load the view
        $response['spaces'] = $this->load->view('space/space_wishlist', $data, true);
        $this->output->set_output(json_encode($response));
    }

    public function ajax_final_price()
    {
        $space_id = $this->input->post('space_id', TRUE);
        $start_date = $this->input->post('start_date', TRUE);
        $end_date = $this->input->post('end_date', TRUE);

        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));

        $response['error'] = 1;

        if (is_numeric($space_id)) {
            $space_info = $this->space_model->get_space_information($space_id);

            if (!empty($space_info)) {
                if ($this->space_model->validate_date($start_date)
                    && $this->space_model->validate_date($end_date)
                    && (strtotime($start_date) < strtotime($end_date))
                    && (strtotime($start_date) >= strtotime(date('Y-m-d')))
                ) {
                    $response['error'] = 0;
                    $response['response'] = $this->space_model->get_final_price($start_date, $end_date, $space_info['price']);
                    $response['response'] = format($response['response']);
                } else {
                    $response['response'] = lang('error_check_dates');
                }
                $this->output->set_output(json_encode($response));
            }
        }
    }

    public function rent()
    {
        $space_id = (int)$this->input->post('space_id', TRUE);
        $start_date = $this->input->post('start_date', TRUE);
        $end_date = $this->input->post('end_date', TRUE);
        $comments = $this->input->post('comments', TRUE);
        $name = $this->input->post('name', TRUE);
        $surname = $this->input->post('surname', TRUE);
        $email = $this->input->post('email', TRUE);
        $phone = $this->input->post('phone', TRUE);

        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));

        $response['error'] = 1;

//        if (!logged_in()) {
//            $_SESSION['last_page'] = site_url('space/view/' . $space_id . '?sd=' . $start_date . '&ed=' . $end_date);
//            $_SESSION['last_page_comment'] = $comments;
//            $tmp['error'] = lang('error_rent_login') . '<br/><a href="' . site_url() . 'ajax/get/login" data-reveal-id="login" data-reveal-ajax="true">' . lang('click_here_to_log_in') . '</a>';
//            $response['response'] = $this->load->view('popups/information', $tmp, true);
//            $this->output->set_output(json_encode($response));
//            return;
//        }

        unset($_SESSION['last_page_comment']);

        $space_info = $this->space_model->get_space_information($space_id);

        if (empty($comments)) {
            $this->output->set_output(json_encode([
                'response' => $this->load->view('popups/information', [
                    'error' => lang('error_comment_empty'),
                ], true)
            ]));
            return;
        }

        if (empty($name)) {
            $this->output->set_output(json_encode([
                'response' => $this->load->view('popups/information', [
                    'error' => 'name tuscias',
                ], true)
            ]));
            return;
        }

        if (empty($surname)) {
            $this->output->set_output(json_encode([
                'response' => $this->load->view('popups/information', [
                    'error' => 'surname tuscias',
                ], true)
            ]));
            return;
        }

        if (empty($email)) {
            $this->output->set_output(json_encode([
                'response' => $this->load->view('popups/information', [
                    'error' => 'email tuscias',
                ], true)
            ]));
            return;
        }

        if (empty($phone)) {
            $this->output->set_output(json_encode([
                'response' => $this->load->view('popups/information', [
                    'error' => 'phone tuscias',
                ], true)
            ]));
            return;
        }

        if (empty($space_info)) {
            $this->output->set_output(json_encode([
                'response' => $this->load->view('popups/information', [
                    'error' => lang('error_space_not_found'),
                ], true)
            ]));
            return;
        }

        if ($this->session->userdata('user_id') == $space_info['user_id']) {
            $this->output->set_output(json_encode([
                'response' => $this->load->view('popups/information', [
                    'error' => lang('error_same_owner_renter'),
                ], true)
            ]));
            return;
        }

        $this->load->model('emails_model');
        $this->emails_model->send_for_owner_about_rent($space_info, $start_date, $end_date, $comments, $name, $surname, $email, $phone, 0);

        $this->output->set_output(json_encode([
            'response' => $this->load->view('popups/information', [
                'success' => lang('success_reservation_sended'),
            ], true)
        ]));
    }

    public function get_commission_price()
    {
        $price = $this->input->post('price', TRUE);
        $category = $this->input->post('category', TRUE);
        $response['error'] = 1;

        if($category != 0){
            if (is_numeric($price)) {
                $response['error'] = 0;
                $response['response'] = number_format((real_price($price)), 0, '.', '');
                $response['response'] = format($response['response']);
            }
        }

        $response['response'] = $price;
        $response['response'] = format($response['response']);

        $this->output->set_output(json_encode($response));
    }

    public function ajax_search()
    {
        $this->form_validation->set_rules('size_from', lang('size_range'), 'is_natural_no_zero');
        $this->form_validation->set_rules('size_to', lang('size_range'), 'is_natural_no_zero');

        if ($this->form_validation->run() == FALSE) {
            $response['error'] = 1;
            $response['response'] = validation_errors();
            $this->output->set_output(json_encode($response));
            return;
        }

        $data = $this->input->post(NULL, TRUE);
        if ($data['lat'] == DEFAULT_LAT && $data['lng'] == DEFAULT_LNG)  {
            //pagination
            $totalRec = count($this->space_model->get_all_results_sql($data));

            $config['target'] = '#search_results';
            $config['base_url'] = base_url() . 'space/ajaxPaginationData';
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;
            $this->ajax_pagination->initialize($config);

            $tmp['results'] = $this->space_model->get_all_results($data, array('limit' => $this->perPage));
            $tmp['size'] = 4;
            $response['products'] = $this->load->view('space/space_wishlist', $tmp, TRUE);
            $response['lat'] = number_format($data['lat'], 7, '.', '');
            $response['lng'] = number_format($data['lng'], 7, '.', '');

            $response['json_prods'] = $this->space_model->results_for_map($tmp['results']);
            if (!$totalRec) {
                $tmp['error'] = lang('error_fill_place_dates');
            }
        }else {
            $totalRec = count($this->space_model->get_search_results_sql($data));

            $config['target'] = '#search_results';
            $config['base_url'] = base_url() . 'space/ajaxPaginationData';
            $config['total_rows'] = $totalRec;
            $config['per_page'] = $this->perPage;

            $this->ajax_pagination->initialize($config);

            $tmp['results'] = $this->space_model->get_search_results($data, array('limit' => $this->perPage));
            $tmp['size'] = 4;
            $response['products'] = $this->load->view('space/space_wishlist', $tmp, TRUE);

            $response['json_prods'] = $this->space_model->results_for_map($tmp['results']);
            $response['lat'] = number_format($data['lat'], 7, '.', '');
            $response['lng'] = number_format($data['lng'], 7, '.', '');

            // make a redirect somewhere
            if (!empty($response['products'])) {
                $response['error'] = 0;
            } else {
                $response['error'] = 2;
            }
        }

        $this->output->set_output(json_encode($response));
    }

    public function date_check($str)
    {
        $str = date("Y-m-d", strtotime($str));
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $str)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('date_check', lang('not_valid_date'));
            return FALSE;
        }
    }

    public function room_type_check($array)
    {
        if (!empty($array)) {
            foreach ($array as $k => $room_type) {
                if (!in_array($room_type, array(1, 2, 3, 4))) {
                    $this->form_validation->set_message('room_type', lang('not_valida_data'));
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    public function update_street_addresses() {
        $spaces = $this->db->get('spaces')->result_array();
        $spacesAddresses = [];

        foreach ($spaces as $space) {
            $address = get_address($space['lat'], $space['lng']);
            if (empty($address)) {
                continue;
            }

            $spacesAddresses[] = [
                'space_id' => $space['space_id'],
                'street_address' => $address,
            ];
        }

        $this->db->update_batch('spaces', $spacesAddresses, 'space_id');
    }
}