<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->load->library('form_validation');
    }     

    public function delete($space_id = 0)
    {
        if(!logged_in()) {
            $this->output->set_output(json_encode(array("error" => lang('error_log_in')) ));
            return;
        }

        $image_type = $this->input->post('image');
        if($space_id == 0) {
            $folder_name = $this->session->userdata('user_id');
        } else {
            // get space info
            $this->load->model('sparerooms_model');
            $info = $this->sparerooms_model->get_space_plain_info($space_id);

            if(!empty($info['folder_name'])) {
                $folder_name = $info['folder_name'];
            } else {
                $response['error'] = 0;
                $response['response'] = '';
                $this->output->set_output(json_encode($response));
                return;
            }
        }

        $path_to_file = FCPATH.'images/'.$folder_name.'/';

        $response['error'] = 1;

        switch ($image_type) {
            case 'main':

                if(!empty($info['image']))
                {
                    $file_name = $info['image'];
                } else {
                    $file_name = $this->session->userdata('main_file_name');
                }

                $temp      = pathinfo($path_to_file.$file_name);
                $dimensions = explode('|', '580x326|491x376');
                foreach($dimensions as $dimension)
                {
                    unlink($path_to_file.$temp['filename'].'_'.$dimension.'.'.$temp['extension']);
                }

                if (!empty($file_name) && file_exists($path_to_file.$file_name)) {
                    unlink($path_to_file.$file_name);
                }

                $this->session->unset_userdata('main_file_name');
                if($space_id != 0) {
                    $this->db->where('space_id', $space_id);
                    $this->db->update('spaces', array('image' => 'empty.jpg'));
                }

                $response['error'] = 0;
                $response['response'] = base_url().'assets/img/img-place.png';

                break;
            default:
                $gallery = $this->session->userdata('gallery');

                if($space_id == 0)
                {
                    $path_to_file .= 'gallery/';
                } else {
                    $path_to_file = FCPATH;
                }

                if(is_array($gallery) && is_numeric($image_type))
                {
                    if (!empty($gallery[$image_type]) && file_exists($path_to_file.$gallery[$image_type])) {
                        unlink($path_to_file.$gallery[$image_type]);
                    }

                    if($space_id == 0)
                    {
                        $file_to_delete = str_replace('.', '_thumb.', $gallery[$image_type]);
                    } else {
                        $file_to_delete = str_replace('_thumb', '', $gallery[$image_type]);
                    }

                    if (!empty($gallery[$image_type]) && file_exists($path_to_file.$file_to_delete)) {
                        unlink($path_to_file.$file_to_delete);
                    }

                    unset($gallery[$image_type]);
                    $this->session->set_userdata(array('gallery' => $gallery));

                    $response['error'] = 0;
                    $response['response'] = '';
                }
                break;


        }
        $this->output->set_output(json_encode($response));
    }

    public function upload_main_img($space_id = 0)
    {
        if(!logged_in())
        {
            $this->output->set_output(json_encode(array("error" => lang('error_log_in')) ));
        } else
        {
            $info = new stdClass();

            if($space_id == 0)
            {
                $folder_name = $this->create_directories();  
                $upload_path_url = base_url().'images/'.$folder_name.'/';
            } else {
                $this->load->model('sparerooms_model');
                $space_info = $this->sparerooms_model->get_space_plain_info($space_id);

                if(!empty($space_info['folder_name'])) {
                    $upload_path_url = base_url().'images/'.$space_info['folder_name'];
                    $folder_name = $space_info['folder_name'];
                } else {
                    $response['error'] = 0;
                    $response['response'] = '';
                    $this->output->set_output(json_encode($response));
                }
            }

            $config['upload_path']   = FCPATH.'images/'.$folder_name;
            $config['allowed_types'] = 'jpg|jpeg|png|jpe';
            $config['max_size']      = '5000';
            $config['max_filename']  = 128;
            $config['overwrite']     = true;
            
            $this->load->library('upload', $config);

            $this->load->library('image_nation');

            if (!$this->upload->do_upload('main_image')) {
                $this->output->set_output(json_encode(array("error" => $this->upload->display_errors('', ''))));
            } else {
                $data = $this->upload->data();

                if($data['image_width'] >= 300 AND $data['image_height'] >= 300)
                {
                    $pathinfo = pathinfo($data['file_name']);
                    $new_filename = $pathinfo['filename'];

                    $this->session->set_userdata(array('main_file_name' => $data['file_name']));

                    // print_R($data);die();
                    $dimensions = array(
                        '580x326' => array(
                            'master_dim'        => 'auto',
                            'keep_aspect_ratio' => FALSE,
                            'style'             => array('vertical'=>'center','horizontal'=>'center'),
                            'overwrite'         => TRUE,        
                            'quality'           => '90%',
                            'directory'         => $config['upload_path'].'/',
                            'file_name'         => $new_filename.'_580x326'
                        ),
                        '491x376' => array(
                            'master_dim'        => 'auto',
                            'keep_aspect_ratio' => FALSE,
                            'style'             => array('vertical'=>'center','horizontal'=>'center'),
                            'overwrite'         => TRUE,        
                            'quality'           => '90%',
                            'directory'         => $config['upload_path'].'/',
                            'file_name'         => $new_filename.'_491x376'
                        )
                    );

                    $this->image_nation->add_size($dimensions);
                    // 580x326
                    $this->image_nation->source($config['upload_path'] . '/' . $data['file_name'], TRUE);
                    $this->image_nation->process('580x326|491x376');

                    if(!$this->image_nation->get_errors()) {
                        $processed_images = $this->image_nation->get_processed();

                        if($space_id != 0)
                        {
                            $this->db->where('space_id', $space_id);
                            $this->db->update('spaces', array('image' => $data['file_name']));
                        }

                        //set the img for the json array   
                        $info->name = $processed_images[0]['580x326']['file_name'];
                        $info->size = $data['file_size'];
                        $info->type = $data['file_type'];
                        $info->url = $upload_path_url .'/'. $info->name . '?'.date('Ymdhms');;
                        // I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
                        $info->error = null;

                        $files[] = $info;
                        //this is why we put this in the constants to pass only json data
                        $this->output->set_output(json_encode(array("files" => $files, "error" => 0)));
                    } else
                    {
                        $this->output->set_output(json_encode(array("error" => $this->image_nation->get_errors() ) ));
                    }
                } else
                {
                    $this->output->set_output(json_encode(array("error" => lang('error_minimum_dimensions'))));
                }
            }
        }
    }

    public function upload_images($space_id = 0)
    {
        if(!logged_in())
        {
            $this->output->set_output(json_encode(array("error" => lang('error_log_in')) ));
        } else
        {
            $info = new stdClass();
            $folder_name = $this->create_directories(); 

            if($space_id == 0)
            {
                $folder_name = $this->create_directories();  
                $upload_path_url = base_url().'images/'.$folder_name.'/';
            } else {
                $this->load->model('sparerooms_model');
                $space_info = $this->sparerooms_model->get_space_plain_info($space_id);

                if(!empty($space_info['folder_name'])) {
                    $upload_path_url = base_url().'images/'.$space_info['folder_name'].'/gallery/';
                    $folder_name = $space_info['folder_name'].'/gallery';
                } else {
                    $response['error'] = 0;
                    $response['response'] = '';
                    $this->output->set_output(json_encode($response));
                }
            }

            $config['upload_path']   = FCPATH.'images/'.$folder_name;
            $config['allowed_types'] = 'jpg|jpeg|png|jpe';
            $config['max_size']      = '5000';
            $config['max_filename']  = 128;
            $config['overwrite']     = true;
            
            $this->load->library('upload', $config);

            if (!$this->upload->do_multi_upload('files')) {
                $this->output->set_output(json_encode(array("error" => $this->upload->display_errors('', ''))));
            } else {
                $this->load->library('image_nation');
                $images = $this->upload->get_multi_upload_data();
                $number = 1;
                $gallery = $this->session->userdata('gallery');
                $gallery = is_null($gallery) ? array() : $gallery;

                foreach ($images as $k => $img) {
                    if($img['image_width'] >= 300 AND $img['image_height'] >= 300)
                    {
                        $new_filename = $img['file_name'];

                        array_push($gallery, $new_filename);
                        $inserted_index = array_search($new_filename, $gallery);
                        // print_R($img);die();
                        $dimensions = array(
                            '580x326' => array(
                                'master_dim'        => 'auto',
                                'keep_aspect_ratio' => FALSE,
                                'style'             => array('vertical'=>'center','horizontal'=>'center'),
                                'overwrite'         => TRUE,        
                                'quality'           => '90%',
                                'directory'         => $config['upload_path'].'/',
                                'file_name'         => $img['raw_name'].'_thumb'
                            )
                        );

                        $this->image_nation->add_size($dimensions);
                        // 580x326
                        $this->image_nation->source($config['upload_path'] . '/' . $img['file_name'], TRUE);
                        $this->image_nation->process('580x326');

                        if(!$this->image_nation->get_errors()) {
                            $this->session->set_userdata(array('gallery' => $gallery));
                            $processed_images = $this->image_nation->get_processed();

                            //set the img for the json array   
                            $info->name = $processed_images[0]['580x326']['file_name'];
                            $info->size = $img['file_size'];
                            $info->type = $img['file_type'];
                            $info->ident = $inserted_index;
                            $info->url = $upload_path_url . $info->name . '?'.date('Ymdhms');;
                            // I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
                            $info->error = null;

                            $files[] = $info;
                            //this is why we put this in the constants to pass only json data
                            $this->output->set_output(json_encode(array("files" => $files, "error" => 0)));
                        } else
                        {
                            $this->output->set_output(json_encode(array("error" => $this->image_nation->get_errors() ) ));
                        }
                    } else
                    {
                        $this->output->set_output(json_encode(array("error" => lang('error_minimum_dimensions'))));
                    } 
                }
            }
        }
    }

    public function upload_profile_img()
    {
        if(!logged_in())
        {
            $this->output->set_output(json_encode(array("error" => lang('error_log_in')) ));
        } else
        {
            $info = new stdClass();
            $folder_name = $this->create_directories(); 
        
            $upload_path_url = base_url().'images/avatars/';

            $config['upload_path']   = FCPATH.'images/avatars';
            $config['allowed_types'] = 'jpg|jpeg|png|jpe';
            $config['max_size']      = '5000';
            $config['max_filename']  = 128;
            $config['overwrite']     = true;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('profile_photo')) {
                $this->output->set_output(json_encode(array("error" => $this->upload->display_errors('', ''))));
            } else {
                $data = $this->upload->data();

                $this->load->library('image_nation');

                if($data['image_width'] >= 200 AND $data['image_height'] >= 200)
                {
                    // $new_filename = SHA1(AVATAR_SALT.$this->session->userdata('user_id')).$data['file_ext'];

                    $dimensions = array(
                        '200x200' => array(
                            'master_dim'        => 'auto',
                            'keep_aspect_ratio' => FALSE,
                            'style'             => array('vertical'=>'center','horizontal'=>'center'),
                            'overwrite'         => TRUE,        
                            'quality'           => '90%',
                            'directory'         => $config['upload_path'].'/',
                            'file_name'         => SHA1(AVATAR_SALT.$this->session->userdata('user_id'))
                        )
                    );

                    $this->image_nation->add_size($dimensions);
                    // 580x326
                    $this->image_nation->source($config['upload_path'] . '/' . $data['file_name'], TRUE);
                    $this->image_nation->process('200x200');

                    if(!$this->image_nation->get_errors()) {
                        $processed_images = $this->image_nation->get_processed();

                        //set the img for the json array   
                        $info->name = $processed_images[0]['200x200']['file_name'];
                        $info->size = $data['file_size'];
                        $info->type = $data['file_type'];
                        $info->url = $upload_path_url . $info->name . '?'.date('Ymdhms');;
                        // I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
                        $info->error = null;

                        $this->db->where('id', $this->session->userdata('user_id'));
                        $this->db->update('users', array('avatar' => $info->name));

                        $this->session->set_userdata(array('avatar' => $info->name));

                        unset($data['full_path']);

                        $files[] = $info;
                        //this is why we put this in the constants to pass only json data
                        $this->output->set_output(json_encode(array("files" => $files, "error" => 0)));
                    } else
                    {
                        $this->output->set_output(json_encode(array("error" => $this->image_nation->get_errors() ) ));
                    }
                } else
                {
                    $this->output->set_output(json_encode(array("error" => lang('error_minimum_dimensions'))));
                }
            }
        }
    }

    private function create_directories()
    {
        $folder_name = $this->session->userdata('user_id');

        // if(empty($folder_name))
        // {
        //     $folder_name = uniqid();
        //     $this->session->set_userdata(array('img_folder' => $folder_name));
        // }

        // create main directory
        if(!file_exists(realpath(FCPATH . 'images/'.$folder_name)))
        {
            mkdir(FCPATH . 'images/'.$folder_name, 0777);
        }

        if(!file_exists(realpath(FCPATH . 'images/'.$folder_name.'/gallery')))
        {
            mkdir(FCPATH . 'images/'.$folder_name.'/gallery', 0777);
        }
        // // create thumbs directory
        // if(!file_exists(realpath(FCPATH . 'images/temp/'.$folder_name.'/thumbs')))
        // {
        //     mkdir(FCPATH . 'images/temp/'.$folder_name.'/thumbs', 0777);
        // }
        // // create profile directory
        // if(!file_exists(realpath(FCPATH . 'images/temp/'.$folder_name.'/profile')))
        // {
        //     mkdir(FCPATH . 'images/temp/'.$folder_name.'/profile', 0777);
        // }

        // // create main flat picture
        // if(!file_exists(realpath(FCPATH . 'images/temp/'.$folder_name.'/main')))
        // {
        //     mkdir(FCPATH . 'images/temp/'.$folder_name.'/main', 0777);
        // }

        return $folder_name;  
    }
}