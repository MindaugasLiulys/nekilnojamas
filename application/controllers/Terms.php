<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Terms extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('space_model');
    }

    public function index($title = "")
    {
        $tmp['title'] = ucfirst(str_replace('-', ' ', $title));

        if ($tmp['title'] == "Help") {
            $data['html'] = $this->load->view('help_page', $tmp, true);
        } elseif ($tmp['title'] == "Terms and conditions" || $tmp['title'] == "Privacy policy") {
            $data['html'] = $this->load->view('terms_conditions', $tmp, true);
        } elseif ($tmp['title'] == "Information" || $tmp['title'] == "Privacy policy") {
            $data['html'] = $this->load->view('information', $tmp, true);
        } elseif ($tmp['title'] == "About us" || $tmp['title'] == "Privacy policy") {
            $data['html'] = $this->load->view('about_us', $tmp, true);
        } else {
            $data['html'] = $this->load->view('text_page', $tmp, true);
        }

        $this->load->view('layout', $data);
    }
}
