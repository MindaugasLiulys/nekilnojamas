<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Translation Controller 
*
* @copyright Copyright (c) 2014, Zygimantas Kiriliauskas
* @author  Zygimantas Kiriliauskas
*/
class Translation extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$result = array();

//		$language = $this->input->get('language', true);
		$language = 'lithuanian';
		$file = $this->input->get('file', true);

		if(!isset($file) || empty($file)) { 
			$file = 'main';
		}

		if(!isset($language) || empty($language)) { 
			$language = 'lithuanian';
		}

		$file_name = $file.'_lang.php';
		$en_list = $this->lang->load($file_name, 'english', true);
		$active_list = $this->lang->load($file_name, $language, true);

		foreach ($active_list as $name => $translation) {
			$result[] = array(
				'from' => $en_list[$name],
				'to'   => $translation,
				'id'   => $name
			);
		}

		$tmp['file'] = $file;
		$tmp['language'] = $language;

		$tmp['strings'] = $result;
		$data['html'] = $this->load->view('admin/pages/translation', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}

	public function save()
	{
		$file_name = $this->input->post('file', TRUE);
		$language = $this->input->post('language', TRUE);
		$data = $this->input->post('data');

		if(in_array($file_name, array('main', 'form_validation', 'emails', 'auth', 'ion_auth', 'information', 'terms')) && in_array($language, $this->config->item('lang_uri_abbr')))
		{
	        $file_content = "<?php defined('BASEPATH') OR exit('No direct script access allowed');\n";
	        foreach ($data as $key => $val) {
	            $file_content .= '$lang["'.$key.'"] = "'.$val.'";'."\n";
	        }

	        $file = getcwd().'/application/language/'.$language.'/'.$file_name.'_lang.php';
	        if(file_exists($file)){
	            file_put_contents($file, $file_content);
	        }
		}

		redirect('admin/translation', 'refresh');
	}
}