<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Translation Controller 
*
* @copyright Copyright (c) 2014, Zygimantas Kiriliauskas
* @author  Zygimantas Kiriliauskas
*/
class Facilities extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$query = $this->db->get('facilities', 20);
        foreach($query->result_array() AS $k => $row)
        {
            $tmp['facilities'][] = $row;
        }

		$data['html'] = $this->load->view('admin/pages/facilities/facilities', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}

	public function edit($facility_id)
    {
        $query = $this->db->query("SELECT * FROM tr56_facilities WHERE id=$facility_id");
        $editId = $query->result_array();

        $tmp['edit'] = $editId[0];

        $data['html'] = $this->load->view('admin/pages/facilities/facilities-edit', $tmp, true);
        $this->load->view('admin/main_layout', $data);
    }

    public function update()
    {
        $clean = array(
            'facility_id' => $this->input->post('facility_id'),
            'name' => $this->input->post('name'),
        );

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('facilities', $clean);

        redirect('admin/facilities/', 'refresh');
    }

    public function delete($facility_id)
    {
        $this->db->where('id', $facility_id);
        $this->db->delete('facilities');

        redirect('admin/facilities/', 'refresh');
    }

    public function create()
    {

        $data['html'] = $this->load->view('admin/pages/facilities/facilities-create', '', true);
        $this->load->view('admin/main_layout', $data);
    }

    public function save()
    {
        $clean = array(
            'facility_id' => $this->input->post('facility_id'),
            'name' => $this->input->post('name'),
        );

        $this->db->insert('facilities', $clean);
        redirect('admin/facilities/', 'refresh');
    }

}