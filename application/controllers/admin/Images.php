<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Dashboard Controller 
*
* @copyright Copyright (c) 2014, Zygimanas Kiriliauskas
* @author  Zygimanas Kiriliauskas
*/
class Images extends Backend_Controller {
	
	var $categories = array(); 

	function __construct()
	{
		parent::__construct();

		$this->categories = array(
			1 => 'Mūsų projektai (dizaino puslapis)',
			2 => 'Naujausi prototipai',
			3 => 'Įrangos projektavimas',
			4 => 'Inžinerinis projektavimas',
			5 => 'Inžinierių nuoma (paveiksliukai)'
		);
	}

	function index()
	{
		$tmp['categories'] = $this->categories;

		$tmp['images'] = $this->db->order_by('cat_id', 'DESC')->order_by('sort', 'ASC')->get('images')->result_array();

		$data['html'] = $this->load->view('admin/pages/images', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}

	function edit($id)
	{
		$tmp['categories'] = $this->categories;
		$tmp['image'] = $this->db->where('image_id', $id)->get('images')->row_array();

		$data['html'] = $this->load->view('admin/pages/edit_image', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}

	function delete($id)
	{
		$this->db->where('image_id', $id);
		$this->db->delete('images');

		redirect('admin/images');
	}

	function add()
	{
		//Load the needed libraries
		$this->load->library('upload');
		// $this->CI->load->library('image_lib');
		$this->load->library('image_moo');
		$config['upload_path'] = FCPATH . 'images/';
		$config['allowed_types'] = 'png|jpg|jpeg';
    	$this->upload->initialize($config);
    	if(!empty($_FILES['image']['name'])) {
	    	if ( ! $this->upload->do_upload('image'))
	    	{
	    		echo $this->upload->display_errors('','');
	    		return false;
	    	} else
	    	{
	    		$file_data = $this->upload->data();
	    		$save_path = FCPATH . 'images/small/'.$file_data['file_name'];
	    		$thumb_path = FCPATH . 'images/thumbs/'.$file_data['file_name'];
	    		$this->image_moo->load($file_data['full_path'])->resize(290,270, true)->save($save_path, TRUE);
	    		$this->image_moo->load($file_data['full_path'])->resize_crop(80, 60)->save($thumb_path, TRUE);
	    		
	    		$sort = $this->input->post('sort', true);

	    		if(!is_numeric($sort))
	    		{
	    			$sort = 1;
	    		}

	    		$array = array(
					'cat_id'         => $this->input->post('cat_id', true),
					'description_lt' => $this->input->post('description_lt', true),
					'description_en' => $this->input->post('description_en', true),
					'description_de' => $this->input->post('description_de', true),
					'description_fr' => $this->input->post('description_fr', true),
					'sort'           => $sort,
					'file_name'      => $file_data['file_name']	
	    		);
	    		$update = $this->input->post('update', true);

	    		if(is_numeric($update))
	    		{
	    			$this->db->where('image_id', $update);
	    			$this->db->update('images', $array);
	    		} else
	    		{
	    			$this->db->insert('images', $array);
	    		}

	    		redirect('admin/images');
	    	}
	    } else 
	    {
    		$sort = $this->input->post('sort', true);

    		if(!is_numeric($sort))
    		{
    			$sort = 1;
    		}

    		$array = array(
				'cat_id'         => $this->input->post('cat_id', true),
				'description_lt' => $this->input->post('description_lt', true),
				'description_en' => $this->input->post('description_en', true),
				'description_de' => $this->input->post('description_de', true),
				'description_fr' => $this->input->post('description_fr', true),
				'sort'           => $sort
    		);
    		$update = $this->input->post('update', true);

    		if(is_numeric($update))
    		{
    			$this->db->where('image_id', $update);
    			$this->db->update('images', $array);
    		} else
    		{
    			$this->db->insert('images', $array);
    		}
	    	redirect('admin/images');
	    }

	}
}