<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Transaction Controller 
*
* @copyright Copyright (c) 2014, Zygimantas Kiriliauskas
* @author  Zygimantas Kiriliauskas
*/
class Transactions extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
        $this->db->select('transactions.*, CONCAT(u_from.first_name, " ", u_from.last_name) as user_from, CONCAT(u_to.first_name, " ", u_to.last_name) as user_to,', FALSE);
        $this->db->join('users as u_from', 'u_from.id = transactions.user_from', 'left');
        $this->db->join('users as u_to', 'u_to.id = transactions.user_to', 'left');
		$query = $this->db->get('transactions', 100);
        foreach($query->result_array() AS $k => $row)
        {
            $tmp['transactions'][] = $row;
        }

		$data['html'] = $this->load->view('admin/pages/transactions', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}
}