<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Sparerooms Controller 
*
* @copyright Copyright (c) 2014, Zygimantas Kiriliauskas
* @author  Zygimantas Kiriliauskas
*/
class Sparerooms extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$query = $this->db->get('spaces', 100);
        foreach($query->result_array() AS $k => $row)
        {
        	$row['price'] = $this->currency->convert(real_price($row['price']));
        	$row['active'] = ($row['active'] == 1) ? "Yes" : "No";
        	$row['deleted'] = ($row['deleted'] == 1) ? "Yes" : "No";
            $tmp['sparerooms'][] = $row;
        }

		$data['html'] = $this->load->view('admin/pages/sparerooms', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}
}