<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Main Admin Controller
 *
 * @uses CI_Controller
 * @copyright Copyright (c) 2014, Zygimanas Kiriliauskas
 * @author  Zygimanas Kiriliauskas
 */

class Main extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
    }

    public function index()
    {
        if($this->ion_auth->logged_in()) {
            redirect('admin/general');
        } else {
            redirect('admin/main/login');
        }
    }

    public function login()
    {
        if($this->ion_auth->logged_in()) {
            redirect('admin/general');
            return;
        }

        $this->form_validation->set_rules('email', 'Vartotojas', 'trim|required');
        $this->form_validation->set_rules('password', 'Slaptažodis', 'trim|required|min_length[6]');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/login_layout');
            return;
        }

        $this->_validate_credentials();

    }

    private function _validate_credentials()
    {
        if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'))) {
            redirect('admin/general');
            return;
        } else {
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect('admin/main/login');
        }
    }

    function logout()
    {
        $this->ion_auth->logout();
        redirect('admin/main/login');
    }
}