<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terms extends Backend_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function terms()
    {
        $result = array();
        $this->lang->load("terms_lang");
//        $language = $this->input->get('language', true);
        $language = 'lithuanian';
        $file = $this->input->get('file', true);

        if(!isset($file) || empty($file)) {
            $file = 'terms';
        }

        if(!isset($language) || empty($language)) {
            $language = 'lithuanian';
        }

        $file_name = $file.'_lang.php';
        $en_list = $this->lang->load($file_name, 'english', true);
        $active_list = $this->lang->load($file_name, $language, true);
        foreach ($active_list as $name => $translation) {
            $result[] = array(
                'from' => $en_list[$name],
                'to'   => $translation,
                'id'   => $name
            );
        }
        $tmp['page_name'] = 'terms';
        $tmp['file'] = $file;
        $tmp['language'] = $language;
        $tmp['strings'] = $result;
        $data['html'] = $this->load->view('admin/pages/terms', $tmp, true);
        $this->load->view('admin/main_layout', $data);
    } 
    
    public function about_us()
    {
        $result = array();
        $this->lang->load("about_us_lang");
//        $language = $this->input->get('language', true);
        $language = 'lithuanian';
        $file = $this->input->get('file', true);

        if(!isset($file) || empty($file)) {
            $file = 'about_us';
        }

        if(!isset($language) || empty($language)) {
            $language = 'lithuanian';
        }

        $file_name = $file.'_lang.php';
        $en_list = $this->lang->load($file_name, 'english', true);
        $active_list = $this->lang->load($file_name, $language, true);
        foreach ($active_list as $name => $translation) {
            $result[] = array(
                'from' => $en_list[$name],
                'to'   => $translation,
                'id'   => $name
            );
        }

        $tmp['page_name'] = 'about_us';
        $tmp['file'] = $file;
        $tmp['language'] = $language;
        $tmp['strings'] = $result;
        $data['html'] = $this->load->view('admin/pages/terms', $tmp, true);
        $this->load->view('admin/main_layout', $data);
    } 
    
    public function information()
    {
        $result = array();
        $this->lang->load("information_lang");
//        $language = $this->input->get('language', true);
        $language = 'lithuanian';
        $file = $this->input->get('file', true);

        if(!isset($file) || empty($file)) {
            $file = 'information';
        }

        if(!isset($language) || empty($language)) {
            $language = 'lithuanian';
        }

        $file_name = $file.'_lang.php';
        $en_list = $this->lang->load($file_name, 'english', true);
        $active_list = $this->lang->load($file_name, $language, true);
        foreach ($active_list as $name => $translation) {
            $result[] = array(
                'from' => $en_list[$name],
                'to'   => $translation,
                'id'   => $name
            );
        }
        
        $tmp['page_name'] = 'information';
        $tmp['file'] = $file;
        $tmp['language'] = $language;
        $tmp['strings'] = $result;
        $data['html'] = $this->load->view('admin/pages/terms', $tmp, true);
        $this->load->view('admin/main_layout', $data);
    }

    public function save()
    {
        $file_name = $this->input->post('file', TRUE);
        $language = $this->input->post('language', TRUE);
        $data = $this->input->post('data');


        $file_content = "<?php defined('BASEPATH') OR exit('No direct script access allowed');\n";

        foreach ($data as $key => $val) {
            $file_text = $val;
            $file_text = str_replace('"', "'", $file_text);
            $file_content .= '$lang["'.$key.'"] = "'.$file_text .'";'."\n";
        }

        $file = getcwd().'/application/language/'.$language.'/'.$file_name.'_lang.php';

        file_put_contents($file, $file_content);
        redirect('admin/terms/'. $file_name, 'refresh');
    }
}