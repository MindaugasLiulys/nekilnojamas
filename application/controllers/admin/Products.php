<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Transaction Controller 
*
* @copyright Copyright (c) 2014, Zygimantas Kiriliauskas
* @author  Zygimantas Kiriliauskas
*/
class Transactions extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$query = $this->db->get('transactions', 100);
        foreach($query->result_array() AS $k => $row)
        {
            $tmp['transactions'][] = $row;
        }

		$data['html'] = $this->load->view('admin/pages/transactions', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}
}