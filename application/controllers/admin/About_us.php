<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Dashboard Controller 
*
* @copyright Copyright (c) 2014, Zygimanas Kiriliauskas
* @author  Zygimanas Kiriliauskas
*/
class About_us extends Backend_Controller {
	var $save_name = 'about_us';
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$tmp['productions'] = $this->db->get('about_us')->result_array();
		$tmp['save_name']   = $this->save_name;
		$data['html'] = $this->load->view('admin/pages/about_us', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}
	function save()
	{
		$productions = $this->input->post('productions', TRUE);
		$lang        = $this->input->post('lang', TRUE);;
		$this->_update_productions($productions, $lang);
		redirect('admin/'.$this->save_name);
	}
	private function _update_productions($productions, $lang)
	{
		if(!empty($productions))
		{
			$data = array();
			foreach($productions AS $k => $v)
			{
				$data[] = array(
					'lang'            => $lang,
					'title'           => $v['title'],
					'top_text'        => $v['top_text'],
					'moto'     		  => $v['moto'],
					'right_list_name' => $v['right_list_name'],
					'meta_title'	  => $v['meta_title'],
					'meta_keywords'	  => $v['meta_keywords'],
					'meta_description'=> $v['meta_description']	
				);
			}
			$this->db->update_batch('about_us', $data, 'lang');
		}
	}
}