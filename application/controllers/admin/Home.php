<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Dashboard Controller 
*
* @copyright Copyright (c) 2014, Zygimanas Kiriliauskas
* @author  Zygimanas Kiriliauskas
*/

class Home extends Backend_Controller {

	var $save_name = 'home';

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$tmp['productions'] = $this->db->get('home')->result_array();
		$tmp['services'] = $this->db->where('type', 1)->order_by('sort', 'asc')->get('list_texts')->result_array();
		$tmp['save_name']   = $this->save_name;

		$data['html'] = $this->load->view('admin/pages/home', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}



	function save()

	{

		$services = $this->input->post('services', TRUE);

		$lang     = $this->input->post('lang', TRUE);

		$productions = $this->input->post('productions', TRUE);



		$this->_update_productions($productions, $lang);

		$this->_update_services($services);



		redirect('admin/'.$this->save_name);

	}



	private function _update_services($services)

	{

		if(!empty($services))

		{

			$data = array();

			foreach($services AS $k => $v)

			{

				$data[] = array(

					'list_texts_id' => $v['id'],

					'title'			=> $v['title'],

					'info'			=> $v['info']

				);

			}

			$this->db->update_batch('list_texts', $data, 'list_texts_id');

		}

	}



	private function _update_productions($productions, $lang)

	{

		if(!empty($productions))

		{

			$data = array();

			foreach($productions AS $k => $v)

			{

				$data[] = array(

					'lang'            => $lang,

					'title'           => $v['title'],

					'top_text'        => $v['top_text'],

					'middle_text'	  => $v['middle_text'],	

					'bottom_text'     => $v['bottom_text'],

					'left_list_name'  => $v['left_list_name'],

					'meta_title'	  => $v['meta_title'],

					'meta_keywords'	  => $v['meta_keywords'],

					'meta_description'=> $v['meta_description']	

				);

			}

			$this->db->update_batch('home', $data, 'lang');

		}

	}

}