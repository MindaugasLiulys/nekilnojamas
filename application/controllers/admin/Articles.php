<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Dashboard Controller 
*
* @copyright Copyright (c) 2014, Zygimanas Kiriliauskas
* @author  Zygimanas Kiriliauskas
*/
class Articles extends Backend_Controller {
	var $save_name = 'articles';
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		$tmp['save_name']   = $this->save_name;
		$tmp['articles'] = $this->db->where('lang', 'lt')->order_by('sort', 'ASC')->get('articles_langs')->result_array();
		$data['html'] = $this->load->view('admin/pages/articles', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}
	function add()
	{
		$tmp['update'] = 0;
		$data['html'] = $this->load->view('admin/pages/add_article', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}
	function edit($article_id)
	{
		if(!is_numeric($article_id))
		{
			redirect('admin');
		}
		$tmp['update'] = $article_id;
		$tmp['articles'] = $this->db->where('article_id', $article_id)->get('articles_langs')->result_array('lang');
		$temp = $this->db->where('article_id', $article_id)->get('articles')->row_array();
		$tmp['image']        = $temp['image'];
		$tmp['url']          = $temp['more_link'];
		$tmp['show_in_news'] = $temp['show_in_news'];
		$data['html'] = $this->load->view('admin/pages/add_article', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}
	function save() {
		$info         = $this->input->post('info', TRUE);
		$update       = $this->input->post('update');
		$image		  = $this->input->post('image');
		$link 		  = $this->input->post('url');
		$show_in_news = $this->input->post('show_in_news');
		$ins_array    = array();
		$update_array = array();
		$article_id   = 0;
		$image_name   = '';	

		$show_in_news = (!empty($show_in_news)) ? 1 : 0;

		$image_name = $this->_upload_image();
		if($update == 0)
		{
			$this->db->insert('articles', array('image' => $image_name, 'more_link' => $link, 'show_in_news' => $show_in_news));
			$article_id = $this->db->insert_id();
		}
		foreach ($info as $lang => $v) {
			if($article_id != 0) {
				// make insert
				$ins_array = array(
					'article_id'	   => $article_id,	
					'lang'             => $lang,
					'title'            => $v['title'],
					'type'             => $v['type'],
					'short'            => $v['short'],
					'info'             => $v['info'],
					'sort'             => $v['sort'],
					'slang'			   => $this->sanitize_text_for_urls($v['title']),	
					'meta_title'       => $v['meta_title'],
					'meta_keywords'    => $v['meta_keywords'],
					'meta_description' => $v['meta_description']	
				);
				$this->db->insert('articles_langs', $ins_array);
			} else
			{
				if($image_name != '')
				{
					$this->db->where('article_id', $update);
					$this->db->update('articles', array('image' => $image_name, 'more_link' => $link, 'show_in_news' => $show_in_news));
				} else {
					$this->db->where('article_id', $update);
					$this->db->update('articles', array('more_link' => $link, 'show_in_news' => $show_in_news));					
				}
				// make update
				$update_array = array(
					'title'            => $v['title'],
					'type'             => $v['type'],
					'short'            => $v['short'],
					'info'             => $v['info'],
					'sort'             => $v['sort'],
					'slang'			   => $this->sanitize_text_for_urls($v['title']),	
					'meta_title'       => $v['meta_title'],
					'meta_keywords'    => $v['meta_keywords'],
					'meta_description' => $v['meta_description']	
				);
				$this->db->where('lang', $lang);
				$this->db->where('article_id', $update);
				$this->db->update('articles_langs', $update_array);
			}
		}
		redirect('admin/articles');
	}

	public function delete($article_id)
	{
		$this->db->where('article_id', $article_id);
		$this->db->delete('articles');

		$this->db->where('article_id', $article_id);
		$this->db->delete('articles_langs');

		redirect('admin/articles');
	}

	function sanitize_text_for_urls( $str ) 
	{
		$this->load->helper('text');
		$str = convert_accented_characters($str);
  		$str = strtolower( strtr( preg_replace('/[^a-zA-Z0-9-\s]/u', '', iconv( 'UTF-8', 'ASCII//TRANSLIT', $str )), ' ', '-') );
		$str = str_replace('---', '-', $str);
		return $str;
	}
	private function _upload_image()
	{
		//Load the needed libraries
		$this->load->library('upload');
		// $this->CI->load->library('image_lib');
		$this->load->library('image_moo');
		$config['upload_path'] = FCPATH . 'images/articles/';
		$config['allowed_types'] = 'png|jpg|jpeg';
    	$this->upload->initialize($config);
    	if(!empty($_FILES['image']['name'])) {
	    	if ( ! $this->upload->do_upload('image'))
	    	{
	    		echo $this->upload->display_errors('','');
	    		return false;
	    	} else
	    	{
	    		$file_data = $this->upload->data();
	    		$save_path = FCPATH . 'images/articles/small/'.$file_data['file_name'];
	    		$save_path_new = FCPATH . 'images/articles/news/'.$file_data['file_name'];
	    		$save_path_m = FCPATH . 'images/articles/medium/'.$file_data['file_name'];
	    		$this->image_moo->load($file_data['full_path'])->resize_crop(130,90, true)->save($save_path, TRUE);
	    		$this->image_moo->load($file_data['full_path'])->resize_crop(160,120, true)->save($save_path_new, TRUE);
	    		$this->image_moo->load($file_data['full_path'])->resize_crop(260,180, true)->save($save_path_m, TRUE);
	    		return $file_data['file_name'];
	    	}
	    } else 
	    {
	    	return '';
	    }
	}
}