<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Translation Controller 
*
* @copyright Copyright (c) 2014, Zygimantas Kiriliauskas
* @author  Zygimantas Kiriliauskas
*/
class Users extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$query = $this->db->get('users', 100);
        foreach($query->result_array() AS $k => $row)
        {
        	$row['created_on'] = date('Y-m-d', $row['created_on']);
        	$row['last_login'] = date('Y-m-d H:i:s', $row['last_login']);
            $tmp['users'][] = $row;
        }

		$data['html'] = $this->load->view('admin/pages/users', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}
}