<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Dashboard Controller 
*
* @copyright Copyright (c) 2014, Zygimanas Kiriliauskas
* @author  Zygimanas Kiriliauskas
*/
class Production extends Backend_Controller {

	var $save_name = 'production';

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$tmp['productions'] = $this->db->get('production')->result_array();
		$tmp['lists']		= $this->db->where('type', 2)->order_by('sort', 'asc')->get('list_texts')->result_array();
		$tmp['lists_right'] = $this->db->where('type', 3)->order_by('sort', 'asc')->get('list_texts')->result_array();
		$tmp['save_name']   = $this->save_name;

		$data['html'] = $this->load->view('admin/pages/production', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}

	function save()
	{
		$productions = $this->input->post('productions', TRUE);
		$lang        = $this->input->post('lang', TRUE);
		$lists		 = $this->input->post('lists', TRUE);
		$lists_right = $this->input->post('lists_r', TRUE);

		$this->_update_productions($productions, $lang);
		$this->_update_insert_lists($lists, $lang, 2);
		$this->_update_insert_lists($lists_right, $lang, 3);

		redirect('admin/'.$this->save_name);
	}

	private function _update_insert_lists($lists, $lang, $type)
	{
		if(!empty($lists))
		{
			foreach ($lists as $k => $list) {
				if(!empty($list['title']))
				{
					// update
					if($list['id'] != 0)
					{
						$this->db->where('list_texts_id', $list['id']);
						$this->db->update('list_texts', array('title' => $list['title'], 'info' => $list['info'], 'sort' => $list['sort'])); 
					} else // insert
					{
						$data = array(
							'lang'  => $lang,
							'type'	=> $type,
							'title' => $list['title'],
							'info'  => $list['info'],
							'sort'  => (!empty($list['sort']) ? $list['sort'] : '1')
						);
						$this->db->insert('list_texts', $data); 
					}
				}
			}
		}
	}

	private function _update_productions($productions, $lang)
	{
		if(!empty($productions))
		{
			$data = array();
			foreach($productions AS $k => $v)
			{
				$data[] = array(
					'lang'            => $lang,
					'title'           => $v['title'],
					'top_text'        => $v['top_text'],
					'bottom_text'     => $v['bottom_text'],
					'left_list_name'  => $v['left_list_name'],
					'right_list_name' => $v['right_list_name'],
					'meta_title'	  => $v['meta_title'],
					'meta_keywords'	  => $v['meta_keywords'],
					'meta_description'=> $v['meta_description']	
				);
			}
			$this->db->update_batch('production', $data, 'lang');
		}
	}
}