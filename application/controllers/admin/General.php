<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Dashboard Controller 
*
* @copyright Copyright (c) 2014, Zygimanas Kiriliauskas
* @author  Zygimanas Kiriliauskas
*/
class General extends Backend_Controller {
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$tmp['users']        = $this->db->count_all('users');
		$tmp['transactions'] = $this->db->count_all('transactions');
		$tmp['spaces']       = $this->db->count_all('spaces');

		$data['html'] = $this->load->view('admin/pages/general', $tmp, true);
		$this->load->view('admin/main_layout', $data);
	}

}