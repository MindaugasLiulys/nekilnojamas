<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Dashboard Controller 
*
* @copyright Copyright (c) 2014, Zygimanas Kiriliauskas
* @author  Zygimanas Kiriliauskas
*/
class Dashboard extends Backend_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->load->view('admin/main_layout');
	}
}