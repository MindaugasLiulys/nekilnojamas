<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Sparerooms extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('sparerooms_model');
	}

	public function index()
	{
		$tmp['active'] = 'sparerooms';
		$tmp['results'] = $this->sparerooms_model->get_my_list();

		$data['html'] = $this->load->view('account/sparerooms', $tmp, true);
		$this->load->view('layout', $data);
	}

	public function save()
	{
		// validate form input
		$this->form_validation->set_rules('description', lang('description'), 'required|min_length[50]');
		$this->form_validation->set_rules('price', lang('price'), 'required|is_natural_no_zero');
		$this->form_validation->set_rules('start_date', lang('start_date'), 'required|callback_date_check');
		$this->form_validation->set_rules('size', lang('size'), 'required|is_natural_no_zero');

		$main_file = $this->session->userdata('main_file_name');
		if ($this->form_validation->run() == true && !is_null($main_file) && logged_in())
		{
			$this->load->model('images_model');

			$data = $this->input->post(NULL, TRUE);
			$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
			$space_id = $this->sparerooms_model->update_space($data);

			// $this->images_model->process_uploaded_images($space_id);

			// make a redirect somewhere
			$response['error'] = 0;
			$response['response'] = lang('success_space_saved');
			$this->output->set_output(json_encode($response));
			// redirect("auth", 'refresh');
		} else
		{
			// display the create user form
			// set the flash data error message if there is one
			$response['error'] = 1;
			$response['response'] = validation_errors();

			if(!logged_in())
			{
				$response['response'] .= '<p>'.lang('error_log_in_first').'</p>';
			}

			if(is_null($main_file))
			{
				$response['response'] .= '<p>'.lang('error_forgot_main_file').'</p>';
			}
			// $html = $this->load->view('popups/sign_up_email', $this->data, TRUE);
			$this->output->set_output(json_encode($response));
		}
	}

	public function delete($space_id)
	{
		if(is_numeric($space_id))
		{
			$this->sparerooms_model->delete($space_id);
		}
		redirect('sparerooms');
	}

	public function complete($space_id)
	{
		if(is_numeric($space_id))
		{
			if($this->sparerooms_model->exist_not_active_space($space_id))
			{
				$tmp['active']   = 'sparerooms';
				$tmp['space_id'] = $space_id;
				$data['html']    = $this->load->view('account/spareroom_complete', $tmp, true);
				$this->load->view('layout', $data);
			} else
			{
				redirect('/');
			}
		} else
		{
			redirect('/');
		}
	}

	public function list_space($space_id)
	{
		if(is_numeric($space_id))
		{
			if($this->sparerooms_model->exist_not_active_space($space_id))
			{
				$this->sparerooms_model->activate_space($space_id);
				redirect('/sparerooms');
			} else
			{
				redirect('/');
			}
		} else
		{
			redirect('/');
		}
	}

	public function unlist_space($space_id)
	{
		if(is_numeric($space_id))
		{
			if($this->sparerooms_model->exist_active_space($space_id))
			{
				$this->sparerooms_model->deactivate_space($space_id);
				redirect('sparerooms');
			} else
			{
				redirect('/');
			}
		} else
		{
			redirect('/');
		}
	}

	public function edit($space_id)
	{
		if(is_numeric($space_id))
		{
			$info = $this->sparerooms_model->get_spareroom($space_id);
			if(!empty($info))
			{
                $query = $this->db->get('facilities', 20);
                foreach($query->result_array() AS $k => $row)
                {
                    $tmp['facilities'][] = $row;
                }

				$tmp['info'] = $info;
				$this->session->set_userdata(array('main_file_name' => $info['image'], 'gallery' => $info['images']));

				$data['html'] = $this->load->view('account/edit', $tmp, true);
				$this->load->view('layout', $data);
			} else {
				redirect('/');
			}
		} else
		{
			redirect('/');
		}
	}

    public function date_check($str)
    {

		$str = date("Y-m-d", strtotime($str));
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$str))
        {
        	return TRUE;
        }
        else
        {
            $this->form_validation->set_message('date_check', lang('not_valid_date'));
            return FALSE;
        }
    }
}
