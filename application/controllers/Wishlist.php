<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Wishlist extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('wishlist_model');
		$this->load->library('Ajax_wishlist_pagination');
		$this->perPage = 6;
	}

	public function add()
	{
		$this->load->model('space_model');
		$space_id = $this->input->post('space_id', NULL, TRUE);

		$response['error'] = 1;
		if(logged_in())
		{
			if($this->space_model->is_space_exist($space_id))
			{
				if(!$this->wishlist_model->space_saved_in_wishlist($space_id))
				{
					$this->wishlist_model->save_space($space_id);

					$response['error'] = 0;
					$tmp['success'] = lang('success_added_to_wishlist');
					$tmp['id'] = $space_id;
					$response['wishlist_btn'] = $this->load->view('space/includes/wishlist_added', $tmp, true);
				} else
				{
					// error about saved space
					$tmp['error'] = lang('already_saved_spareroom');
				}
			} else
			{
				// error
				$tmp['error'] = lang('error_spareroom_not_exist');
			}
		} else
		{
			$tmp['error'] = lang('log_in_to_add_to_wishlist');	
		}		
		
		$response['response'] = $this->load->view('popups/information', $tmp, true);
		$this->output->set_output(json_encode($response));
	}

	public function my()
	{
		$tmp['active'] = 'wishlist';
		$tmp['results'] = $this->wishlist_model->get_user_list();

		//pagination
		$totalRec = count($tmp['results']);

		$config['target'] = '#wishlist';
		$config['base_url'] = base_url() . 'wishlist/ajax_wishlist_pagination';
		$config['total_rows'] = $totalRec;
		$config['per_page'] = $this->perPage;
		
		$this->ajax_wishlist_pagination->initialize($config);
		
		$tmp['results'] = array_slice($tmp['results'], 0, $this->perPage);

		$data['html'] = $this->load->view('account/wishlist', $tmp, true);
		$this->load->view('layout', $data);
	}
	
	public function ajax_wishlist_pagination($offset)
	{
		$tmp = $this->input->post(NULL, TRUE);
		$tmp['active'] = 'wishlist';

		//pagination
		$totalRec = count($this->wishlist_model->get_user_list());

		$config['target'] = '#wishlist';
		$config['base_url'] = base_url() . 'wishlist/ajax_wishlist_pagination';
		$config['total_rows'] = $totalRec;
		$config['per_page'] = $this->perPage;

		$this->ajax_wishlist_pagination->initialize($config);

		$tmp['results'] = $this->wishlist_model->get_user_list(array('start' => $offset, 'limit' => $this->perPage));

		$response['wishlist'] = $this->load->view('account/includes/whislist_pagination', $tmp, true);
		$this->output->set_output(json_encode($response));
	}
	
	public function remove($space_id)
	{
		if(is_numeric($space_id))
		{
			$this->wishlist_model->remove($space_id);
		}
		redirect('wishlist/my');
	}
	
	public function ajax_remove_wishlist()
	{
		$tmp['id'] = $this->input->post('id', TRUE);
		$response['error'] = 0;

		if(is_numeric($tmp['id']))
		{
			$this->wishlist_model->remove($tmp['id']);
			$tmp['success'] = lang('success_removed_from_wishlist');
		}else {
			$tmp['error'] = lang('error_spareroom_not_exist');
		}
		$response['wishlist_btn'] = $this->load->view('space/includes/add_to_wishlist_btn', $tmp, true);
		$response['response'] = $this->load->view('popups/information', $tmp, true);
		$this->output->set_output(json_encode($response));
	}
}