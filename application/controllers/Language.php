<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Language extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function switchLang($language = "")
    {

        $language = ($language != "") ? $language : "lithuanian";
        $this->session->set_userdata('lang', $language);

        redirect($_SERVER['HTTP_REFERER']);
    }
}