<?php defined('BASEPATH') OR exit('No direct script access allowed');
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
require_once 'twilio/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

Class Twilio
{

    public function send_message($code, $message_to)
    {
        $sid = 'AC97bbf944868888be818e95589ea620bc';
        $token = '3dfd91e4fe40ed3461a62fa444916cf9';
        $client = new Client($sid, $token);

        $client->messages->create(
            // the number you'd like to send the message to
            $message_to,
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '37066802028',
                // the body of the text message you'd like to send
                'body' => $code
            )
        );
    }

}


