<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Currency {
    public function __construct()
    {

    }   

    public function convert($amount, $fromCurrency = "", $toCurrency = "", $saveIntoDb = 1, $hourDifference = 1) {
        $amountConverted = $this->convertToFloat($amount, $fromCurrency, $toCurrency, $saveIntoDb, $hourDifference);
        return number_format((double)$amountConverted, 0, '.', '');
    }

    public function convertToFloat($amount, $fromCurrency = "", $toCurrency = "", $saveIntoDb = 1, $hourDifference = 1) {

        if($fromCurrency == "") { $fromCurrency = "DKK"; }
        if($toCurrency == "") { $toCurrency = strtoupper($_SESSION['currency']); }

        if($fromCurrency == $toCurrency){
            return number_format((double)$amount, 0, '.', '');
        }
        
        $CI =& get_instance();
        $rate = 0;

        if ($fromCurrency=="PDS")
            $fromCurrency = "GBP";

        if(!$saveIntoDb) {
            $rate = $this->_getRates($fromCurrency, $toCurrency);
            $value = (double)$rate * (double)$amount;

            return $value;
        }
        
        $this->_ensureTableExists();

        $CI->db->select('*');
        $CI->db->from('currency_converter');
        $CI->db->where('from', $fromCurrency);
        $CI->db->where('to', $toCurrency);
        $query = $CI->db->get();
        $find = 0;

        foreach ($query->result() as $row){
            $find = 1;
            $lastUpdated = $row->modified;
            $dStart = new DateTime();
            $dEnd = new DateTime($lastUpdated);
            $diff = $dStart->diff($dEnd);

            if(((int)$diff->y >= 1) || ((int)$diff->m >= 1) || ((int)$diff->d >= 1) || ((int)$diff->h >= $hourDifference) || ((double)$row->rates == 0)){
                $rate = $this->_getRates($fromCurrency, $toCurrency);

                $data = array(
                    'from'  => $fromCurrency,
                    'to' => $toCurrency,
                    'rates' => $rate,
                    'modified' => date('Y-m-d H:i:s'),
                 );

                 $CI->db->where('id', $row->id);
                 $CI->db->update('currency_converter',$data);
            }
            else{
                $rate = $row->rates;
            }
        }

        if($find == 0){
            $rate = $this->_getRates($fromCurrency, $toCurrency);

            $data = array(
                'from'  => $fromCurrency,
                'to' => $toCurrency,
                'rates' => $rate,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            );

            $CI->db->insert('currency_converter',$data);
        }

        $value = (double)$rate * (double)$amount;

        return $value;
    }

    private function _getRates($fromCurrency, $toCurrency){
        $url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='. $fromCurrency . $toCurrency .'=X';
        $handle = @fopen($url, 'r');
         
        if ($handle) {
            $result = fgets($handle, 4096);
            fclose($handle);
        }

        if (empty($result)) {
            return 0;
        }

        $allData =  str_getcsv($result);
        return $allData[1];
    }

    private function _ensureTableExists() {
        $CI =& get_instance();

        if ($CI->db->table_exists('currency_converter') ){
            return;
        }

        $CI->load->dbforge();
        $CI->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'from' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'null' => FALSE
            ),
            'to' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
                'null' => FALSE
            ),
            'rates' => array(
                'type' => 'VARCHAR',
                'constraint' => '10',
                'null' => FALSE
            ),
            'created' => array(
                'type' => 'DATETIME'
            ),
            'modified' => array(
                'type' => 'DATETIME'
            )
        ));

        $CI->dbforge->add_key('id', TRUE);
        $CI->dbforge->create_table('currency_converter',TRUE);
    }
}
